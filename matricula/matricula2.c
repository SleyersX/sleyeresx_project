#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 *   Autor : Walter Moura
 *   Data  : 2019-02-10
 *   Modificado : 2019-02-11
 *   Versão : 2.0
 *   Algoritimo para gerar digito verificador matricula
 *
 */

//Declaração de funções variaveis
char *gets(char *entrada);

//Declaração de funções
//int gerar_digito(int matricula[6], int digito[6]);
int converter_string(char entrada[6], int num_caracteres);
int gerar_digito(int matricula[6], int digito[6], int num_caract);

int main(){

    char entrada[6];
    int num_caract, verificador;

    //Entrada o usuário
    printf("Digite sua matricula: ");
    gets(entrada);

    //Contamos a quantidade de caracteres para a condicional
    num_caract = strlen(entrada);

    //Chamar função de conversão
    converter_string(entrada, num_caract);

    return 0;
}

int converter_string(char entrada[6], int num_caracteres){

    //Variáveis
    int x, y, matricula[6], digito[6];

    //Separação de caracteres

    if(num_caracteres == 1){
        x=5;
        y=(num_caracteres - 1);
        matricula[0] = 0;
        matricula[1] = 0;
        matricula[2] = 0;
        matricula[3] = 0;
        matricula[4] = 0;
        digito[0] = 0;
        digito[1] = 0;
        digito[2] = 0;
        digito[3] = 0;
        digito[4] = 0;
    }else if(num_caracteres == 2){
        x=4;
        y=(num_caracteres - 1);
        matricula[0] = 0;
        matricula[1] = 0;
        matricula[2] = 0;
        matricula[3] = 0;
        digito[0] = 0;
        digito[1] = 0;
        digito[2] = 0;
        digito[3] = 0;
    }else if(num_caracteres == 3){
        x=3;
        y=(num_caracteres - 1);
        matricula[0] = 0;
        matricula[1] = 0;
        matricula[2] = 0;
        digito[0] = 0;
        digito[1] = 0;
        digito[2] = 0;
    }else if(num_caracteres == 4){
        x=2;
        y=(num_caracteres - 1);
        matricula[0] = 0;
        matricula[1] = 0;
        digito[0] = 0;
        digito[1] = 0;
    }else if(num_caracteres == 5){
        x=1;
        y=(num_caracteres - 1);
        matricula[0] = 0;
        digito[0] = 0;
    }else{
        x=0;
        y=5;
    }

    //Convertamos o retorno ASCII para seu inteiro respectivo

    for(int i = 0 ; i <= y ; i++){
        if(entrada[i] == 48){
            matricula[x] = 0;
            digito[x] = 0;
        }else if(entrada[i] == 49){
            matricula[x] = 1;
            digito[x] = 1;
        }else if(entrada[i] == 50){
            matricula[x] = 2;
            digito[x] = 2;
        }else if(entrada[i] == 51){
            matricula[x] = 3;
            digito[x] = 3;
        }else if(entrada[i] == 52){
            matricula[x] = 4;
            digito[x] = 4;
        }else if(entrada[i] == 53){
            matricula[x] = 5;
            digito[x] = 5;
        }else if(entrada[i] == 54){
            matricula[x] = 6;
            digito[x] = 6;
        }else if(entrada[i] == 55){
            matricula[x] = 7;
            digito[x] = 7;
        }else if(entrada[i] == 56){
            matricula[x] = 8;
            digito[x] = 8;
        }else if(entrada[i] == 57){
            matricula[x] = 9;
            digito[x] = 9;
        }else{
            printf("Um dos digitos não é númerico.\n");
            printf("Programa será encerrado!\n");
            return 0;
        }   
        x=(x+1);
    }

    //Chamamos a função para gerar digito
    gerar_digito(matricula, digito, num_caracteres);

    return 0;
}

int gerar_digito(int matricula[6], int digito[6], int num_caract){

    //Declaração de variáveis
    int fator_1[6], fator_2[6], fator_3[6], fator_4[7], fator_5[6], fator_6[7], fator_7[6], fator_8[5],  verificador,
    digito_1, soma_fator_2, truncar_div_fator_2, condicao_1, mult_codicao_1, digito_2, soma_fator_6, truncar_div_fator_6, condicao_2, mult_codicao_2, x, y;
    float div_soma_fator_2, div_soma_fator_6;

    //Processamento
    x=0;
    
    //1º Fator
    if(matricula[5] >= 0){
        fator_1[5] = 3;
    }
    x=5;
    y=0;
    for(int i = 0 ; i <= 4 ; i++){
        y=(x-1);
        if(fator_1[x] == 3){
            fator_1[y] = 1;
        }else{
            fator_1[y] = 3;
        }
        x=(x-1);
    }
   
    //2º Fator
    for(int i = 0 ; i <= 5 ; i++){
        fator_2[i] = (matricula[i] * fator_1[i]);
    }

    //3º Passo
    if(digito[5] >= 0){
        fator_3[5] = 3;
    }
    x=5;
    y=0;
    for(int i = 0 ; i <= 4 ; i++){
        y=(x-1);
        if(fator_3[x] == 3){
            fator_3[y] = 1;
        }else{
            fator_3[y] = 3;
        }
        x=(x-1);
    }
    
    //4º Passo
    soma_fator_2 = 0;
    for(int i = 0 ; i <= 5 ; i++){
        soma_fator_2 = (soma_fator_2 + fator_2[i]);
    }
    div_soma_fator_2 = ((float)soma_fator_2 / (float)10);
    truncar_div_fator_2 = ((int)div_soma_fator_2);
    if(div_soma_fator_2 == truncar_div_fator_2){
        condicao_1 = truncar_div_fator_2;
    }else{
        condicao_1 = (truncar_div_fator_2 + 1);
    }
    mult_codicao_1 = (condicao_1 * 10);
    digito_1 = (mult_codicao_1 - soma_fator_2);

    //5º Passo
    if(digito_1 >= 0){
        fator_4[6] = 3;
    }
    x=6;
    y=0;
    for(int i = 0 ; i <= 5 ; i++){
        y=(x-1);
        if(fator_4[x] == 3){
            fator_4[y] = 1;
        }else{
            fator_4[y] = 3;
        }
        x=(x-1);
    }

    //6º Passo
    if(fator_2[5] >= 0){
        fator_5[5] = 3;
    }
    if(fator_2[4] >= 0){
        fator_5[4] = 3;
    }
    x=4;
    y=0;
    for(int i = 0 ; i <= 3 ; i++){
        y=(x-1);
        if(fator_5[x] == 3){
            fator_5[y] = 1;
        }else{
            fator_5[y] = 3;
        }
        x=(x-1);
    }

    //7º Passo
    fator_6[6] = (digito_1 * fator_4[6]);
    for(int i = 0 ; i <= 5 ; i++){
        fator_6[i] = (digito[i] * fator_4[i]);
    }

    //8º Passo
    for(int i = 0 ; i <= 5 ; i++){
        fator_7[i] = (digito[i] * fator_3[i]);
    }

    //9º Passo
    for(int i = 0 ; i <= 4 ; i++){
        fator_8[i] = (digito[i] * fator_5[i]);
    }

    //10º Passo
    soma_fator_6 = 0;
    for(int i = 0 ; i <= 6 ; i++){
        soma_fator_6 = (soma_fator_6 + fator_6[i]);
    }
    div_soma_fator_6 = ((float)soma_fator_6 / (float)10);
    truncar_div_fator_6 = ((int)div_soma_fator_6);
    if(div_soma_fator_6 == truncar_div_fator_6){
        condicao_2 = truncar_div_fator_6;
    }else{
        condicao_2 = (truncar_div_fator_6 + 1);
    }
    mult_codicao_2 = (condicao_2 * 10);
    digito_2 = (mult_codicao_2 - soma_fator_6);
    verificador = 0;

    //11º Retorno da função, melhorar depois
    //por enquanto sem uso
    if(digito_1 >= 0){
        verificador=((digito_1 * 10) + digito_2);
    }else{
        verificador=0;
    }

    //12º Exibição do resultado para o usuário conforme número de caracteres
    printf("\nMatricula\tDigito\t\tCódigo\n");
    for(int i = 0 ; i <= 5 ; i++){
        printf("%d", matricula[i]);
    }
    printf("\t\t%d%d\t\t", digito_1, digito_2);
    if(num_caract == 1){
        for(int i = 5 ; i <= 5 ; i++){
            printf("%d", matricula[i]);
        }
        printf("%d%d\n", digito_1, digito_2);
    }else if(num_caract == 2){
        for(int i = 4 ; i <= 5 ; i++){
            printf("%d", matricula[i]);
        }
        printf("%d%d\n", digito_1, digito_2);
    }else if(num_caract == 3){
        for(int i = 3 ; i <= 5 ; i++){
            printf("%d", matricula[i]);
        }
        printf("%d%d\n", digito_1, digito_2);
    }else if(num_caract == 4){
        for(int i = 2 ; i <= 5 ; i++){
            printf("%d", matricula[i]);
        }
        printf("%d%d\n", digito_1, digito_2);
    }else if(num_caract == 5){
        for(int i = 1 ; i <= 5 ; i++){
            printf("%d", matricula[i]);
        }
        printf("%d%d\n", digito_1, digito_2);
    }else if(num_caract == 6){
        for(int i = 0 ; i <= 5 ; i++){
            printf("%d", matricula[i]);
        }
        printf("%d%d\n", digito_1, digito_2);
    }
    
    return 0;
}
