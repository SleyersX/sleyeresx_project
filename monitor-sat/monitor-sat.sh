#!/bin/bash
#
#
#
#

IFS="|"
LOG="/usr/local/monitor-sat/log/monitor-sat.log"

echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:Executando o programa." >> $LOG
echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:Carregando arquivo de configuracao." >> $LOG
echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:[ $(cat /usr/local/monitor-sat/monitor-sat.ini) ]." >> $LOG
. /usr/local/monitor-sat/monitor-sat.ini

HOSTAPI=${host}
codigoAtivacao=${codigo}

. /confdia/bin/setvari

VISOR=$(cat /var/log/cajera.log | tail -n1 | cut -d ":" -f4 | sed "s/ //g" | tr -d "\n" | cut -d "/" -f1 | tr -d '[\200-\377]' | sed 's/[^A-Za-z0-9:|. ]//g')

if [ "$VISOR" == "TOTALACOBRAR" ] || [ "$VISOR" == "VENDACARTAO" ] || [ "$VISOR" == "PAGODELIVERY" ] || [ "$VISOR" == "PAGODEVOL.CLIE" ] || [ "$VISOR" == "PAGODEVOL.VASI" ] || [ "$VISOR" == "PAGODEVOL.IFOOD" ] || [ "$VISOR" == "PAGODEVOL.VASILH" ] || [ "$VISOR" == "0CREDITO" ] || [ "$VISOR" == "PAGOTEFENCURSO" ] || [ "$VISOR" == "CartaoDebito" ] || [ "$VISOR" == "TRANSAAOACEITA" ] || [ "$VISOR" == "TROCO" ] || [ "$VISOR" == "CartaoCredito" ] || [ "$VISOR" == "PAGAM.NAOEFETUADO" ] || [ "$VISOR" == "PROCESANDOCUPONS" ] || [ "$VISOR" == "CLIENTENOEXISTE" ] || [ "$VISOR" == "CLIENTEFIDELIZACION" ] || [ "$VISOR" == "ENT.DECAIX." ] || [ "$VISOR" == "CODIGOOPERARIO" ] || [ "$VISOR" == "SAIDAOPERADORA" ]; then
	echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:SAT em operação ['$VISOR']." >> $LOG
	exit 1
else
	echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:Operação liberada ['$VISOR']." >> $LOG
fi
statusOperacional=$(chroot /srv/Debian6.0/ /opt/monitor-sat/getStatusOperSat $codigoAtivacao | tr -d '[\200-\377]' | sed 's/[^A-Za-z0-9:|. ]//g' | tr -d '\n' | rev | cut -c 1-1 | rev | tr -d '\n')
retornoStatusOperacional=$(chroot /srv/Debian6.0/ /opt/monitor-sat/getStatusOperSat $codigoAtivacao | tr -d '[\200-\377]' | sed 's/[^A-Za-z0-9:|. ]//g'| tr -d '\n'| cut -d "|" -f2)

if [ "$retornoStatusOperacional" == "Porta serial no identificada." ]; then
	echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:Porta serial nao identificada." >> $LOG
	exit 1
fi

CHAVE1=`chroot /srv/Debian6.0/ /opt/monitor-sat/getStatusOperSat $codigoAtivacao |  tr -d '[\200-\377]' | tr -d '\n' | sed 's/[^A-Za-z0-9:|. ]//g' | cut -d "|" -f21`
NCRT_CHAVE_1=${#CHAVE1}
TEST_CHAVE_1=$(expr $(echo -n ${CHAVE1//[^0-9]/} | wc -c))

if [ "$statusOperacional" == "0" ] && [ "$retornoStatusOperacional" == "10000" ]; then
	if [ $NCRT_CHAVE_1 -eq $TEST_CHAVE_1 ]; then
		
		chroot /srv/Debian6.0/ /opt/monitor-sat/getStatusOperSat $codigoAtivacao | tr -d '[\200-\377]' | sed 's/[^A-Za-z0-9:|. ]//g'| tr -d '\n' > /usr/local/monitor-sat/StatusOperSat.txt
		
		echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:Obetendo Status Operacional SAT." >> $LOG
		echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:[$(cat /usr/local/monitor-sat/StatusOperSat.txt)]." >> $LOG

		numeroSessao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f1`
		retornoStatusOperacional=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f2`
		msgStatusOperacional=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f3 | sed "s/ /%20/g"`
		avisosSefaz=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f4`
		msgAvisoSefaz=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f5 | sed "s/ /%20/g"`
		numeroSerie=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f6`
		tipoLan=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f7`
		endIP=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f8`
		endMac=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f9`
		endMascara=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f10`
		endGateway=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f11`
		endDnsPrimario=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f12`
		endDnsSecundario=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f13`
		statusRede=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f14 | sed "s/ /%20/g"`
		nivelBateria=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f15`
		memoriaTotal=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f16 | sed "s/ /%20/g"`
		memoriaUsada=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f17 | sed "s/ /%20/g"`
		dataHoraAtual=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f18`
		versaoSWBasico=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f19`
		versaoLayout=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f20`
		ultimoCFeSATEmitido=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f21`
		primeiroCFeSATMemoria=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f22`
		ultimoCFeSATMemoria=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f23`
		dataHoraTransmissao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f24`
		dataHoraComunicacao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f25`
		certEmisao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f26`
		certVencimento=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f27`
		estadoOperacao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f28`
		totalCFeEmis=`echo $ultimoCFeSATEmitido | cut -c 32-37 | tr -d '\n'`
		lastCFe=`echo $ultimoCFeSATMemoria | cut -c 32-37 | tr -d '\n'`
		primaryCFe=`echo $primeiroCFeSATMemoria | cut -c 32-37 | tr -d '\n'`
		lastCFe=`seq $lastCFe $lastCFe`
		primaryCFe=`seq $primaryCFe $primaryCFe`
		nCFeMemoria=0
		let 'nCFeMemoria = lastCFe - primaryCFe'

		if [ -z $tipoLan ]; then
			tipoLan="DHCP"
		fi
		if [ -z $avisoSefaz ]; then
			avisoSefaz="null"
		fi
		if [ -z $msgAvisoSefaz ]; then
			msgAvisoSefaz="null"
		fi

		echo -e "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:Eviando dados para API [$HOSTAPI]:data \n[ {\n
		\ttoken:7c6ce378b1ef0a29180dd36a0d436a91,
		\treturnStateOper:$retornoStatusOperacional,
		\tmsgStateOper:$msgStatusOperacional,
		\tavisoSefaz:$avisosSefaz,
		\tmsgAvisoSefaz:$msgAvisoSefaz,
		\tsat:$numeroSerie,
		\tstore:${NUMETIEN},
		\tpos:${NUMECAJA},
		\ttypeLan:$tipoLan,
		\tip:$endIP,
		\tmac:$endMac,
		\tmask:$endMascara,
		\tgw:$endGateway,
		\tdnsPrimary:$endDnsPrimario,
		\tdnsSecundary:$endDnsSecundario,
		\tstatusWAN:$statusRede,
		\tnivelBatery:$nivelBateria,
		\tdisk:$memoriaTotal,
		\tusedDisk:$memoriaUsada,
		\tdateHAtual:$dataHoraAtual,
		\tfirmware:$versaoSWBasico,
		\tlayout:$versaoLayout,
		\tlastCFeEmis:$ultimoCFeSATEmitido,
		\tprimaryCFeMemory:$primeiroCFeSATMemoria,
		\tlastCFeMemory:$ultimoCFeSATMemoria,
		\tcfesEmitidos:$totalCFeEmis,
		\tcfesMemory:$nCFeMemoria,
		\tdateHRTransm:$dataHoraTransmissao,
		\tdateHRComuni:$dataHoraComunicacao,
		\tcertEmisao:$certEmisao,
		\tcertVencimento:$certVencimento,
		\testadoOperacao:$estadoOperacao,
		\tsatEmFalha:0
		} ]." >> $LOG

		URL="http://$HOSTAPI/restapi/v1/api/source/Controllers/Sats.php?token=7c6ce378b1ef0a29180dd36a0d436a91&returnStateOper=$retornoStatusOperacional&msgStateOper=$msgStatusOperacional&avisoSefaz=$avisosSefaz&msgAvisoSefaz=$msgAvisoSefaz&sat=$numeroSerie&store=${NUMETIEN}&pos=${NUMECAJA}&typeLan=$tipoLan&ip=$endIP&mac=$endMac&mask=$endMascara&gw=$endGateway&dnsPrimary=$endDnsPrimario&dnsSecundary=$endDnsSecundario&statusWAN=$statusRede&nivelBatery=$nivelBateria&disk=$memoriaTotal&usedDisk=$memoriaUsada&dateHAtual=$dataHoraAtual&firmware=$versaoSWBasico&layout=$versaoLayout&lastCFeEmis=$ultimoCFeSATEmitido&primaryCFeMemory=$primeiroCFeSATMemoria&lastCFeMemory=$ultimoCFeSATMemoria&cfesEmitidos=$totalCFeEmis&cfesMemory=$nCFeMemoria&dateHRTransm=$dataHoraTransmissao&dateHRComuni=$dataHoraComunicacao&certEmisao=$certEmisao&certVencimento=$certVencimento&estadoOperacao=$estadoOperacao&satEmFalha=0"

	else

		PARTE_1=`chroot /srv/Debian6.0/ /opt/monitor-sat/getStatusOperSat $codigoAtivacao | tr -d '[\200-\377]' | sed 's/[^A-Za-z0-9:|. ]//g'| tr -d '\n' | cut -d "|" -f1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20`
		PARTE_2=`chroot /srv/Debian6.0/  /opt/monitor-sat/getStatusOperSat $codigoAtivacao | tr -d '[\200-\377]' | sed 's/[^A-Za-z0-9:|. ]//g' | tr -d '\n' | rev | cut -d "|" -f1,2,3,4,5 | rev`

		echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:Obetendo Status Operacional SAT." >> $LOG
		echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:[$(cat /usr/local/monitor-sat/StatusOperSat.txt)]." >> $LOG
		echo -n "$PARTE_1""|""$PARTE_2"> /usr/local/monitor-sat/StatusOperSat.txt

		numeroSessao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f1`
		retornoStatusOperacional=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f2`
		msgStatusOperacional=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f3 | sed "s/ /%20/g"`
		avisosSefaz=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f4`
		msgAvisoSefaz=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f5 | sed "s/ /%20/g"`
		numeroSerie=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f6`
		tipoLan=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f7`
		endIP=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f8`
		endMac=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f9`
		endMascara=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f10`
		endGateway=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f11`
		endDnsPrimario=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f12`
		endDnsSecundario=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f13`
		statusRede=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f14 | sed "s/ /%20/g"`
		nivelBateria=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f15`
		memoriaTotal=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f16 | sed "s/ /%20/g"`
		memoriaUsada=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f17 | sed "s/ /%20/g"`
		dataHoraAtual=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f18`
		versaoSWBasico=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f19`
		versaoLayout=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f20`
		ultimoCFeSATEmitido="00000000000000000000000000000000000000000000"
		primeiroCFeSATMemoria="00000000000000000000000000000000000000000000"
		ultimoCFeSATMemoria="00000000000000000000000000000000000000000000"
		dataHoraTransmissao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f21`
		dataHoraComunicacao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f22`
		certEmisao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f23`
		certVencimento=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f24`
		estadoOperacao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f25`
		totalCFeEmis=`echo $ultimoCFeSATEmitido | cut -c 32-37 | tr -d '\n'`
		lastCFe=`echo $ultimoCFeSATMemoria | cut -c 32-37 | tr -d '\n'`
		primaryCFe=`echo $primeiroCFeSATMemoria | cut -c 32-37 | tr -d '\n'`
		lastCFe=`seq $lastCFe $lastCFe`
		primaryCFe=`seq $primaryCFe $primaryCFe`
		nCFeMemoria=0
		let 'nCFeMemoria = lastCFe - primaryCFe'

		if [ -z $tipoLan ]; then
			tipoLan="DHCP"
		fi
		if [ -z $avisoSefaz ]; then
			avisoSefaz="null"
		fi
		if [ -z $msgAvisoSefaz ]; then
			msgAvisoSefaz="null"
		fi

		echo -e "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:Eviando dados para API [$HOSTAPI]:data \n[ {\n
		\ttoken:7c6ce378b1ef0a29180dd36a0d436a91,
		\treturnStateOper:$retornoStatusOperacional,
		\tmsgStateOper:$msgStatusOperacional,
		\tavisoSefaz:$avisosSefaz,
		\tmsgAvisoSefaz:$msgAvisoSefaz,
		\tsat:$numeroSerie,
		\tstore:${NUMETIEN},
		\tpos:${NUMECAJA},
		\ttypeLan:$tipoLan,
		\tip:$endIP,
		\tmac:$endMac,
		\tmask:$endMascara,
		\tgw:$endGateway,
		\tdnsPrimary:$endDnsPrimario,
		\tdnsSecundary:$endDnsSecundario,
		\tstatusWAN:$statusRede,
		\tnivelBatery:$nivelBateria,
		\tdisk:$memoriaTotal,
		\tusedDisk:$memoriaUsada,
		\tdateHAtual:$dataHoraAtual,
		\tfirmware:$versaoSWBasico,
		\tlayout:$versaoLayout,
		\tlastCFeEmis:$ultimoCFeSATEmitido,
		\tprimaryCFeMemory:$primeiroCFeSATMemoria,
		\tlastCFeMemory:$ultimoCFeSATMemoria,
		\tcfesEmitidos:$totalCFeEmis,
		\tcfesMemory:$nCFeMemoria,
		\tdateHRTransm:$dataHoraTransmissao,
		\tdateHRComuni:$dataHoraComunicacao,
		\tcertEmisao:$certEmisao,
		\tcertVencimento:$certVencimento,
		\testadoOperacao:$estadoOperacao,
		\tsatEmFalha:1
		} ]." >> $LOG

		URL="http://$HOSTAPI/restapi/v1/api/source/Controllers/Sats.php?token=7c6ce378b1ef0a29180dd36a0d436a91&returnStateOper=$retornoStatusOperacional&msgStateOper=$msgStatusOperacional&avisoSefaz=$avisosSefaz&msgAvisoSefaz=$msgAvisoSefaz&sat=$numeroSerie&store=${NUMETIEN}&pos=${NUMECAJA}&typeLan=$tipoLan&ip=$endIP&mac=$endMac&mask=$endMascara&gw=$endGateway&dnsPrimary=$endDnsPrimario&dnsSecundary=$endDnsSecundario&statusWAN=$statusRede&nivelBatery=$nivelBateria&disk=$memoriaTotal&usedDisk=$memoriaUsada&dateHAtual=$dataHoraAtual&firmware=$versaoSWBasico&layout=$versaoLayout&lastCFeEmis=$ultimoCFeSATEmitido&primaryCFeMemory=$primeiroCFeSATMemoria&lastCFeMemory=$ultimoCFeSATMemoria&cfesEmitidos=$totalCFeEmis&cfesMemory=$nCFeMemoria&dateHRTransm=$dataHoraTransmissao&dateHRComuni=$dataHoraComunicacao&certEmisao=$certEmisao&certVencimento=$certVencimento&estadoOperacao=$estadoOperacao&satEmFalha=1"
		
	fi
elif [ "$retornoStatusOperacional" == "10001" ] && [ "$statusOperacional" == "2" ] || [ "$statusOperacional" == "3" ] || [ "$statusOperacional" == "04" ]; then
	if [ $NCRT_CHAVE_1 -eq $TEST_CHAVE_1 ]; then
		
		chroot /srv/Debian6.0/ /opt/monitor-sat/getStatusOperSat $codigoAtivacao | tr -d '[\200-\377]' | sed 's/[^A-Za-z0-9:|. ]//g'| tr -d '\n' > /usr/local/monitor-sat/StatusOperSat.txt
		
		echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:Obetendo Status Operacional SAT." >> $LOG
		echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:[$(cat /usr/local/monitor-sat/StatusOperSat.txt)]." >> $LOG

		numeroSessao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f1`
		retornoStatusOperacional=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f2`
		msgStatusOperacional=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f3 | sed "s/ /%20/g"`
		avisosSefaz=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f4`
		msgAvisoSefaz=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f5 | sed "s/ /%20/g"`
		numeroSerie=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f6`
		tipoLan=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f7`
		endIP=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f8`
		endMac=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f9`
		endMascara=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f10`
		endGateway=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f11`
		endDnsPrimario=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f12`
		endDnsSecundario=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f13`
		statusRede=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f14 | sed "s/ /%20/g"`
		nivelBateria=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f15`
		memoriaTotal=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f16 | sed "s/ /%20/g"`
		memoriaUsada=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f17 | sed "s/ /%20/g"`
		dataHoraAtual=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f18`
		versaoSWBasico=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f19`
		versaoLayout=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f20`
		ultimoCFeSATEmitido=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f21`
		primeiroCFeSATMemoria=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f22`
		ultimoCFeSATMemoria=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f23`
		dataHoraTransmissao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f24`
		dataHoraComunicacao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f25`
		certEmisao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f26`
		certVencimento=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f27`
		estadoOperacao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f28`
		totalCFeEmis=`echo $ultimoCFeSATEmitido | cut -c 32-37 | tr -d '\n'`
		lastCFe=`echo $ultimoCFeSATMemoria | cut -c 32-37 | tr -d '\n'`
		primaryCFe=`echo $primeiroCFeSATMemoria | cut -c 32-37 | tr -d '\n'`
		lastCFe=`seq $lastCFe $lastCFe`
		primaryCFe=`seq $primaryCFe $primaryCFe`
		nCFeMemoria=0
		let 'nCFeMemoria = lastCFe - primaryCFe'

		if [ -z $tipoLan ]; then
			tipoLan="DHCP"
		fi
		if [ -z $avisoSefaz ]; then
			avisoSefaz="null"
		fi
		if [ -z $msgAvisoSefaz ]; then
			msgAvisoSefaz="null"
		fi

		echo -e "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:Eviando dados para API [$HOSTAPI]:data \n[ {\n
		\ttoken:7c6ce378b1ef0a29180dd36a0d436a91,
		\treturnStateOper:$retornoStatusOperacional,
		\tmsgStateOper:$msgStatusOperacional,
		\tavisoSefaz:$avisosSefaz,
		\tmsgAvisoSefaz:$msgAvisoSefaz,
		\tsat:$numeroSerie,
		\tstore:${NUMETIEN},
		\tpos:${NUMECAJA},
		\ttypeLan:$tipoLan,
		\tip:$endIP,
		\tmac:$endMac,
		\tmask:$endMascara,
		\tgw:$endGateway,
		\tdnsPrimary:$endDnsPrimario,
		\tdnsSecundary:$endDnsSecundario,
		\tstatusWAN:$statusRede,
		\tnivelBatery:$nivelBateria,
		\tdisk:$memoriaTotal,
		\tusedDisk:$memoriaUsada,
		\tdateHAtual:$dataHoraAtual,
		\tfirmware:$versaoSWBasico,
		\tlayout:$versaoLayout,
		\tlastCFeEmis:$ultimoCFeSATEmitido,
		\tprimaryCFeMemory:$primeiroCFeSATMemoria,
		\tlastCFeMemory:$ultimoCFeSATMemoria,
		\tcfesEmitidos:$totalCFeEmis,
		\tcfesMemory:$nCFeMemoria,
		\tdateHRTransm:$dataHoraTransmissao,
		\tdateHRComuni:$dataHoraComunicacao,
		\tcertEmisao:$certEmisao,
		\tcertVencimento:$certVencimento,
		\testadoOperacao:$estadoOperacao,
		\tsatEmFalha:0
		} ]." >> $LOG

		URL="http://$HOSTAPI/restapi/v1/api/source/Controllers/Sats.php?token=7c6ce378b1ef0a29180dd36a0d436a91&returnStateOper=$retornoStatusOperacional&msgStateOper=$msgStatusOperacional&avisoSefaz=$avisosSefaz&msgAvisoSefaz=$msgAvisoSefaz&sat=$numeroSerie&store=${NUMETIEN}&pos=${NUMECAJA}&typeLan=$tipoLan&ip=$endIP&mac=$endMac&mask=$endMascara&gw=$endGateway&dnsPrimary=$endDnsPrimario&dnsSecundary=$endDnsSecundario&statusWAN=$statusRede&nivelBatery=$nivelBateria&disk=$memoriaTotal&usedDisk=$memoriaUsada&dateHAtual=$dataHoraAtual&firmware=$versaoSWBasico&layout=$versaoLayout&lastCFeEmis=$ultimoCFeSATEmitido&primaryCFeMemory=$primeiroCFeSATMemoria&lastCFeMemory=$ultimoCFeSATMemoria&cfesEmitidos=$totalCFeEmis&cfesMemory=$nCFeMemoria&dateHRTransm=$dataHoraTransmissao&dateHRComuni=$dataHoraComunicacao&certEmisao=$certEmisao&certVencimento=$certVencimento&estadoOperacao=$estadoOperacao&satEmFalha=0"

	else

		PARTE_1=`chroot /srv/Debian6.0/ /opt/monitor-sat/getStatusOperSat $codigoAtivacao | tr -d '[\200-\377]' | sed 's/[^A-Za-z0-9:|. ]//g'| tr -d '\n' | cut -d "|" -f1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20`
		PARTE_2=`chroot /srv/Debian6.0/  /opt/monitor-sat/getStatusOperSat $codigoAtivacao | tr -d '[\200-\377]' | sed 's/[^A-Za-z0-9:|. ]//g' | tr -d '\n' | rev | cut -d "|" -f1,2,3,4,5 | rev`

		echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:Obetendo Status Operacional SAT." >> $LOG
		echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:[$(cat /usr/local/monitor-sat/StatusOperSat.txt)]." >> $LOG
		echo -n "$PARTE_1""|""$PARTE_2"> /usr/local/monitor-sat/StatusOperSat.txt

		numeroSessao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f1`
		retornoStatusOperacional=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f2`
		msgStatusOperacional=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f3 | sed "s/ /%20/g"`
		avisosSefaz=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f4`
		msgAvisoSefaz=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f5 | sed "s/ /%20/g"`
		numeroSerie=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f6`
		tipoLan=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f7`
		endIP=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f8`
		endMac=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f9`
		endMascara=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f10`
		endGateway=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f11`
		endDnsPrimario=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f12`
		endDnsSecundario=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f13`
		statusRede=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f14 | sed "s/ /%20/g"`
		nivelBateria=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f15`
		memoriaTotal=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f16 | sed "s/ /%20/g"`
		memoriaUsada=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f17 | sed "s/ /%20/g"`
		dataHoraAtual=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f18`
		versaoSWBasico=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f19`
		versaoLayout=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f20`
		ultimoCFeSATEmitido="00000000000000000000000000000000000000000000"
		primeiroCFeSATMemoria="00000000000000000000000000000000000000000000"
		ultimoCFeSATMemoria="00000000000000000000000000000000000000000000"
		dataHoraTransmissao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f21`
		dataHoraComunicacao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f22`
		certEmisao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f23`
		certVencimento=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f24`
		estadoOperacao=`cat /usr/local/monitor-sat/StatusOperSat.txt | cut -d "|" -f25`
		totalCFeEmis=`echo $ultimoCFeSATEmitido | cut -c 32-37 | tr -d '\n'`
		lastCFe=`echo $ultimoCFeSATMemoria | cut -c 32-37 | tr -d '\n'`
		primaryCFe=`echo $primeiroCFeSATMemoria | cut -c 32-37 | tr -d '\n'`
		lastCFe=`seq $lastCFe $lastCFe`
		primaryCFe=`seq $primaryCFe $primaryCFe`
		nCFeMemoria=0
		let 'nCFeMemoria = lastCFe - primaryCFe'

		if [ -z $tipoLan ]; then
			tipoLan="DHCP"
		fi
		if [ -z $avisoSefaz ]; then
			avisoSefaz="null"
		fi
		if [ -z $msgAvisoSefaz ]; then
			msgAvisoSefaz="null"
		fi

		echo -e "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:Eviando dados para API [$HOSTAPI]:data \n[ {\n
		\ttoken:7c6ce378b1ef0a29180dd36a0d436a91,
		\treturnStateOper:$retornoStatusOperacional,
		\tmsgStateOper:$msgStatusOperacional,
		\tavisoSefaz:$avisosSefaz,
		\tmsgAvisoSefaz:$msgAvisoSefaz,
		\tsat:$numeroSerie,
		\tstore:${NUMETIEN},
		\tpos:${NUMECAJA},
		\ttypeLan:$tipoLan,
		\tip:$endIP,
		\tmac:$endMac,
		\tmask:$endMascara,
		\tgw:$endGateway,
		\tdnsPrimary:$endDnsPrimario,
		\tdnsSecundary:$endDnsSecundario,
		\tstatusWAN:$statusRede,
		\tnivelBatery:$nivelBateria,
		\tdisk:$memoriaTotal,
		\tusedDisk:$memoriaUsada,
		\tdateHAtual:$dataHoraAtual,
		\tfirmware:$versaoSWBasico,
		\tlayout:$versaoLayout,
		\tlastCFeEmis:$ultimoCFeSATEmitido,
		\tprimaryCFeMemory:$primeiroCFeSATMemoria,
		\tlastCFeMemory:$ultimoCFeSATMemoria,
		\tcfesEmitidos:$totalCFeEmis,
		\tcfesMemory:$nCFeMemoria,
		\tdateHRTransm:$dataHoraTransmissao,
		\tdateHRComuni:$dataHoraComunicacao,
		\tcertEmisao:$certEmisao,
		\tcertVencimento:$certVencimento,
		\testadoOperacao:$estadoOperacao,
		\tsatEmFalha:1
		} ]." >> $LOG

		URL="http://$HOSTAPI/restapi/v1/api/source/Controllers/Sats.php?token=7c6ce378b1ef0a29180dd36a0d436a91&returnStateOper=$retornoStatusOperacional&msgStateOper=$msgStatusOperacional&avisoSefaz=$avisosSefaz&msgAvisoSefaz=$msgAvisoSefaz&sat=$numeroSerie&store=${NUMETIEN}&pos=${NUMECAJA}&typeLan=$tipoLan&ip=$endIP&mac=$endMac&mask=$endMascara&gw=$endGateway&dnsPrimary=$endDnsPrimario&dnsSecundary=$endDnsSecundario&statusWAN=$statusRede&nivelBatery=$nivelBateria&disk=$memoriaTotal&usedDisk=$memoriaUsada&dateHAtual=$dataHoraAtual&firmware=$versaoSWBasico&layout=$versaoLayout&lastCFeEmis=$ultimoCFeSATEmitido&primaryCFeMemory=$primeiroCFeSATMemoria&lastCFeMemory=$ultimoCFeSATMemoria&cfesEmitidos=$totalCFeEmis&cfesMemory=$nCFeMemoria&dateHRTransm=$dataHoraTransmissao&dateHRComuni=$dataHoraComunicacao&certEmisao=$certEmisao&certVencimento=$certVencimento&estadoOperacao=$estadoOperacao&satEmFalha=1"
		
	fi
elif [ "$retornoStatusOperacional" == "10001" ]; then
	echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:Codigo de ativaca SAT invalido." >> $LOG
fi
echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:Utilizando CURL para enviar os dados via URL [curl -X PUT -H Content-Type: application/json $URL]." >> $LOG
RETURN=`curl -X PUT -H "Content-Type: application/json" "$URL"`
echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:monitor-sat:Retorno Application/json -> [$RETURN]" >> $LOG