#!/bin/bash

primaryCFeMemory="35200517238497000183590006739940259071675000"
lastCFeMemory="35200517238497000183590006739940259449440540"
lastCFe=`echo $lastCFeMemory | cut -c 32-37 | tr -d '\n'`
primaryCFe=`echo $primaryCFeMemory | cut -c 32-37 | tr -d '\n'`
lastCFe=`seq $lastCFe $lastCFe`
primaryCFe=`seq $primaryCFe $primaryCFe`
nCfeMemory=0

let 'nCfeMemory = lastCFe - primaryCFe'

echo $nCfeMemory