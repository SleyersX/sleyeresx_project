#!/bin/bash
#
#
#
#
#
#
function status(){
    PID_TAXGO_MONITOR=`pidof taxgo-monitor`
    PID_TAXGO_WATCHDOG=`pidof taxgo-watchdog`
    PID_TAXGO_PROBE_SATMFE=`pidof taxgo-probe-satmfe`
    if [ -z $PID_TAXGO_MONITOR ]; then
        TAXGO_MONITOR=1
    else
        TAXGO_MONITOR=0
    fi
    if [ -z $PID_TAXGO_WATCHDOG ]; then
        TAXGO_WATCHDOG=1
    else
        TAXGO_WATCHDOG=0
    fi
    if [ -z $PID_TAXGO_PROBE_SATMFE ]; then
        TAXGO_PROBE_SATMFE=1
    else
        TAXGO_PROBE_SATMFE=0
    fi

}
function start(){
    if [ $TAXGO_MONITOR -eq 1 ] && [ $TAXGO_WATCHDOG -eq 1 ] && [ $TAXGO_PROBE_SATMFE -eq 1 ]; then
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-watchdog start
        sleep 1
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-monitor start
        sleep 1
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-probe-satmfe start
        sleep 1
    elif [ $TAXGO_MONITOR -eq 1 ] && [ $TAXGO_WATCHDOG -eq 1 ] && [ $TAXGO_PROBE_SATMFE -eq 0 ]; then
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-watchdog start
        sleep 1
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-monitor start
        sleep 1
    elif [ $TAXGO_MONITOR -eq 1 ] && [ $TAXGO_WATCHDOG -eq 0 ] && [ $TAXGO_PROBE_SATMFE -eq 0 ]; then    
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-monitor start
        sleep 1
    elif [ $TAXGO_MONITOR -eq 0 ] && [ $TAXGO_WATCHDOG -eq 0 ] && [ $TAXGO_PROBE_SATMFE -eq 0 ]; then
        echo "All services is UP."
    fi
}
function restart(){
    if [ $TAXGO_MONITOR -eq 1 ] && [ $TAXGO_WATCHDOG -eq 1 ] && [ $TAXGO_PROBE_SATMFE -eq 1 ]; then
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-watchdog start
        sleep 1
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-monitor start
        sleep 1
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-probe-satmfe start
        sleep 1
    elif [ $TAXGO_MONITOR -eq 1 ] && [ $TAXGO_WATCHDOG -eq 1 ] && [ $TAXGO_PROBE_SATMFE -eq 0 ]; then
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-watchdog start
        sleep 1
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-monitor start
        sleep 1
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-probe-satmfe restart
        sleep 1
    elif [ $TAXGO_MONITOR -eq 1 ] && [ $TAXGO_WATCHDOG -eq 0 ] && [ $TAXGO_PROBE_SATMFE -eq 0 ]; then    
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-watchdog start
        sleep 1
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-monitor restart
        sleep 1
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-probe-satmfe restart
        sleep 1
    elif [ $TAXGO_MONITOR -eq 0 ] && [ $TAXGO_WATCHDOG -eq 0 ] && [ $TAXGO_PROBE_SATMFE -eq 0 ]; then    
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-watchdog restart
        sleep 1
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-monitor restart
        sleep 1
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-probe-satmfe restart
        sleep 1
    fi
}
function stop(){
    if [ $TAXGO_MONITOR -eq 1 ] && [ $TAXGO_WATCHDOG -eq 1 ] && [ $TAXGO_PROBE_SATMFE -eq 1 ]; then
        echo "All services is DOWN."
    elif [ $TAXGO_MONITOR -eq 1 ] && [ $TAXGO_WATCHDOG -eq 1 ] && [ $TAXGO_PROBE_SATMFE -eq 0 ]; then
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-probe-satmfe stop
        sleep 1
    elif [ $TAXGO_MONITOR -eq 1 ] && [ $TAXGO_WATCHDOG -eq 0 ] && [ $TAXGO_PROBE_SATMFE -eq 0 ]; then    
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-watchdog stop
        sleep 1
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-probe-satmfe stop
        sleep 1
    elif [ $TAXGO_MONITOR -eq 0 ] && [ $TAXGO_WATCHDOG -eq 0 ] && [ $TAXGO_PROBE_SATMFE -eq 0 ]; then
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-watchdog stop
        sleep 1
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-monitor stop
        sleep 1
        chroot /srv/Debian6.0/ /etc/init.d/taxgo-probe-satmfe stop
        sleep 1
    fi
}
function main(){
    status
    sleep 1
    start
}
CMD=$1
if [ -z $CMD ]; then
    main
else
    case $CMD in
        start)
            status
            start
        ;;
        restart)
            status
            restart
        ;;
        stop)
            status
            stop
        ;;
        *)
            echo "USE: {status|start|stop|restart}"
            exit 1
    esac
fi