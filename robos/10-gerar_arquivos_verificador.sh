#!/bin/bash
#
#Autor : Walter Moura
#Data : 06/11/2018
#Modificação : 06/03/2020
#Vs 1.4

function status()
{
    PID_T=$(pidof TCgertec)
    PID_A=$(ps -e | grep -a -i aux.sh | awk -F " " '{print $1;}' | tr -d "\n")
    if [ -z $PID_A ]; then
        PID_AUX=1
    else
        PID_AUX=0
    fi
    if [ -z $PID_T ]; then
        PID_TC=1
    else
        PID_TC=0
    fi
    
}

function start(){
	
	if [ $PID_AUX -eq 1 ] && [ $PID_TC -eq 1 ]; then 
        nohup /etc/init.d/tcgertec start
    elif [ $PID_AUX -eq 1 ] && [ $PID_TC -eq 0 ]; then
		nohup /etc/init.d/tcgertec stop
		sleep 5
        nohup /etc/init.d/tcgertec start
    elif [ $PID_AUX -eq 0 ] && [ $PID_TC -eq 1 ]; then
        nohup /etc/init.d/tcgertec stop
		sleep 5
        nohup /etc/init.d/tcgertec start
    elif [ $PID_AUX -eq 0 ] && [ $PID_TC -eq 0 ]; then
        echo -n "TCGertec: All services is UP."
    fi
}

function main(){
	status
	sleep 1
	start
}

main