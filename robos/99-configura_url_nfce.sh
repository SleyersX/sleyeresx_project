#!/bin/bash
# BA
/confdia/bin/dfw_conf nfce set URLS\\BA\\Homologacao http://hnfe.sefaz.ba.gov.br/servicos/nfce/modulos/geral/NFCEC_consulta_chave_acesso.aspx
/confdia/bin/dfw_conf nfce set URLS\\BA\\Producao    http://nfe.sefaz.ba.gov.br/servicos/nfce/modulos/geral/NFCEC_consulta_chave_acesso.aspx
# QR 1
#/confdia/bin/dfw_conf nfce set URLS\\BA\\ChaveConsultaHomologacao  http://hnfe.sefaz.ba.gov.br/servicos/nfce/default.aspx
#/confdia/bin/dfw_conf nfce set URLS\\BA\\ChaveConsultaProducao     http://nfe.sefaz.ba.gov.br/servicos/nfce/default.aspx
# QR 2
/confdia/bin/dfw_conf nfce set URLS\\BA\\ChaveConsultaHomologacao  http://hinternet.sefaz.ba.gov.br/nfce/consulta
/confdia/bin/dfw_conf nfce set URLS\\BA\\ChaveConsultaProducao     http://www.sefaz.ba.gov.br/nfce/consulta

# RS
/confdia/bin/dfw_conf nfce set URLS\\RS\\Homologacao https://www.sefaz.rs.gov.br/NFCE/NFCE-COM.aspx
/confdia/bin/dfw_conf nfce set URLS\\RS\\Producao    https://www.sefaz.rs.gov.br/NFCE/NFCE-COM.aspx
# QR 1
#/confdia/bin/dfw_conf nfce set URLS\\RS\\ChaveConsultaHomologacao www.sefaz.rs.gov.br/NFCE/NFCE-COM.aspx
#/confdia/bin/dfw_conf nfce set URLS\\RS\\ChaveConsultaProducao    www.sefaz.rs.gov.br/NFCE/NFCE-COM.aspx
# QR 2
/confdia/bin/dfw_conf nfce set URLS\\RS\\ChaveConsultaHomologacao www.sefaz.rs.gov.br/nfce/consulta
/confdia/bin/dfw_conf nfce set URLS\\RS\\ChaveConsultaProducao    www.sefaz.rs.gov.br/nfce/consulta

/confdia/bin/dfw_conf nfce set CONFIGURACAO\\VersaoQRCode 2
/confdia/bin/dfw_conf nfce set IDE\\Mod '65'

# MG
/confdia/bin/dfw_conf nfce set URLS\\MG\\Homologacao https://nfce.fazenda.mg.gov.br/portalnfce/sistema/qrcode.xhtml
/confdia/bin/dfw_conf nfce set URLS\\MG\\Producao https://nfce.fazenda.mg.gov.br/portalnfce/sistema/qrcode.xhtml
/confdia/bin/dfw_conf nfce set URLS\\MG\\ChaveConsultaHomologacao http://hnfce.fazenda.mg.gov.br/portalnfce
/confdia/bin/dfw_conf nfce set URLS\\MG\\ChaveConsultaProducao http://nfce.fazenda.mg.gov.br/portalnfce
