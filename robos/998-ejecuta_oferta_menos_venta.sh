#!/bin/bash
#
#
#
#
#
#
#
#
#
#

. /confdia/bin/setvari

function main()
{

	arch="/confdia/ficcaje/ofer_menos_venta.ctrl"
	data=$(date +%Y%m%d)
	if [ -e $arch ]; then
		existe=1
	else
		existe=0
	fi
	if [ $existe -eq 0 ]; then
		if [ -e "/tmp/data_install.txt" ]; then
			fecha=$(cat /tmp/data_install.txt)
			return=$?
			if [ $return != 0 ];then
				echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:${NUMETIEN}:Erro ao executar comando, retorno {$return} ." >> /root/error.log
			else
				if [ -z $fecha ]; then
					echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_DATE:${NUMETIEN}:Arquivo de data está vazio." >> /root/error.log	
				else
					echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:${NUMETIEN}:Comando executado com sucesso." >> /root/error.log
					if [ $data -ge $fecha ];then
						echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_DATE:${NUMETIEN}:Data Ok." >> /root/error.log
						echo "$(date +%Y%m%d-%H%M%S.%s):ACTIVE:${NUMETIEN}:Executando ativação oferta menos venda." >> /root/error.log
						touch /confdia/ficcaje/ofer_menos_venta.ctrl
						return=$?
						if [ $return != 0 ];then
							echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_ACTIVE:${NUMETIEN}:Erro ao ativar oferta, retorno {$return} ." >> /root/error.log
						else
							echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_ACTIVE:${NUMETIEN}:Oferta ativada com sucesso." >> /root/error.log
						fi
					else
						echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_DATE:${NUMETIEN}:Data inválida ou fora do periodo de instalação." >> /root/error.log
					fi
				fi
			fi
		else
			echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILE:${NUMETIEN}:Arquivo de data não existe." >> /root/error.log
		fi
	else
		echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILE:${NUMETIEN}:Arquivo já existe." >> /root/error.log
	fi
}

main