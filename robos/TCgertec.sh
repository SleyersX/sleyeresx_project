#!/bin/bash
#
# Reiniciar os serviçõs do TCgertec que está rodando em 
# backgound no Debian6.0
#
#
. /etc/rc.d/init.d/functions

PID_TC=$(pidof TCgertec)
PID_AX=$(pidof sh)

if [ -z "$PID_TC" ]; then
    PID_TCG=0
else
    PID_TCG=1
fi
if [ -z "$PID_AX" ]; then
    PID_AUX=0
else
    PID_AUX=1
fi

case "$1" in
    status)
        if [ $PID_TCG -eq 1 ] && [ $PID_AUX -eq 1 ]; then
            sleep 1
            echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mTodos os serviços estão UP['$PID_TCG:$PID_AUX'].\033[00;37m"
        elif [ $PID_TCG -eq 1 ] && [ $PID_AUX -eq 0 ]; then
            echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mServiço SH_AUX DOWN['$PID_TCG:$PID_$AUX'].\033[00;37m"
            echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mServiço TCgertec UP.['$PID_TCG:$PID_$AUX'].\033[00;37m"
            sleep 1
        elif [ $PID_TCG -eq 0 ] && [ $PID_AUX -eq 1 ]; then
            sleep 1
            echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mServiço SH_AUX UP['$PID_TCG:$PID_$AUX'].\033[00;37m"
            echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mServiço TCgertec DOWN.['$PID_TCG:$PID_$AUX'].\033[00;37m"
        elif [ $PID_TCG -eq 0 ] && [ $PID_AUX -eq 0 ]; then
            sleep 1
            echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mServiços Busca Preço DOWN.\033[00;37m"
        fi
    ;;
    start)
        if [ $PID_TCG -eq 1 ] && [ $PID_AUX -eq 1 ]; then
            echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mERROR-Serviço já está em execução.\033[00;37m"
        else
            echo -e "\033[01;34mStart Server Busca Preços.\033[00;37m"
            sleep 1
            sleep 60
            bash /confdia/script_sys/10-gerar_arquivos_verificador.sh > /dev/null
            sleep 30
            echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mOK.\033[00;37m"
        fi
    ;;
    stop)
        if [ $PID_TCG -eq 1 ] && [ $PID_AUX -eq 1 ]; then
            echo -e "\033[01;34mParando Server Busca Preços.\033[00;37m"
            sleep 1
            kill -9 $PID_TC $PID_AX
            RETURN=$?
            if [ $RETURN != 0 ]; then
                echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mERROR-Erro ao parar serviço['$RETURN'].\033[00;37m"
                sleep 10
            else
                sleep 60
                echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mServiço parado com sucesso.['$RETURN'].\033[00;37m"
            fi
        elif [ $PID_TCG -eq 1 ] && [ $PID_AUX -eq 0 ]; then
            echo -e "\033[01;34mParando Server Busca Preços.\033[00;37m"
            sleep 1
            kill -9 $PID_TC
            RETURN=$?
            if [ $RETURN != 0 ]; then
                echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mERROR-Erro ao parar serviço['$RETURN'].\033[00;37m"
                sleep 10
            else
                sleep 60
                echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mServiço parado com sucesso.['$RETURN'].\033[00;37m"
            fi
        elif [ $PID_TCG -eq 0 ] && [ $PID_AUX -eq 1 ]; then
            echo -e "\033[01;34mParando Server Busca Preços.\033[00;37m"
            sleep 1
            kill -9 $PID_AX
            RETURN=$?
            if [ $RETURN != 0 ]; then
                echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mERROR-Erro ao parar serviço['$RETURN'].\033[00;37m"
                sleep 10
            else
                sleep 60
                echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mServiço parado com sucesso.['$RETURN'].\033[00;37m"
            fi
        elif [ $PID_TCG -eq 0 ] && [ $PID_AUX -eq 0 ]; then
            echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mServiço já está parado.\033[00;37m"
        fi
    ;;
    restart|reload)
        if [ $PID_TCG -eq 1 ] && [ $PID_AUX -eq 1 ]; then
            echo -e "\033[01;34m134:Parando Server Busca Preços.\033[00;37m"
            sleep 1
            kill -9 $PID_TC $PID_AX
            RETURN=$?
            if [ $RETURN != 0 ]; then
                echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mERROR-Erro ao parar serviço['$RETURN'].\033[00;37m"
                sleep 10
            else
                sleep 60
                bash /confdia/script_sys/10-gerar_arquivos_verificador.sh > /dev/null
                sleep 30
                echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mOK['$RETURN'].\033[00;37m"
            fi
        elif [ $PID_TCG -eq 1 ] && [ $PID_AUX -eq 0 ]; then
            echo -e "\033[01;34mParando Server Busca Preços.\033[00;37m"
            sleep 1
            kill -9 $PID_TC
            RETURN=$?
            if [ $RETURN != 0 ]; then
                echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mERROR-Erro ao parar serviço['$RETURN'].\033[00;37m"
                sleep 10
            else
                sleep 60
                bash /confdia/script_sys/10-gerar_arquivos_verificador.sh > /dev/null
                sleep 30
                echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mOK['$RETURN'].\033[00;37m"
            fi
        elif [ $PID_TCG -eq 0 ] && [ $PID_AUX -eq 1 ]; then
            echo -e "\033[01;34mParando Server Busca Preços.\033[00;37m"
            sleep 1
            kill -9 $PID_AX
            RETURN=$?
            if [ $RETURN != 0 ]; then
                echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mERROR-Erro ao parar serviço['$RETURN'].\033[00;37m"
                sleep 10
            else
                sleep 60
                bash /confdia/script_sys/10-gerar_arquivos_verificador.sh > /dev/null
                sleep 30
                echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mOK.\033[00;37m"
            fi
        elif [ $PID_TCG -eq 0 ] && [ $PID_AUX -eq 0 ]; then
            echo -e "\033[01;34mStart Server Busca Preços.\033[00;37m"
            sleep 60
            bash /confdia/script_sys/10-gerar_arquivos_verificador.sh > /dev/null
            sleep 30
            echo -e "\033[40;01;5;33mTCgertec:\033[00;37m\033[01;32mOK.\033[00;37m"
        fi
    ;;
    *)
    echo -e "\033[40;01;5;33mUsage:\033[00;37m\033[01;33mTCgertec {start|stop|restart|stop}\033[00;37m"
esac
exit 1
