#!/bin/bash
#
# Script para ativar o stock online nas lojas
#
#
#
#

PATH_PROGRAM="/root/srv_remoto"
LIST_SHOPS="$PATH_PROGRAM/arquivos_read/list_tiendas.txt"
LIST_IPS="$PATH_PROGRAM/arquivos_read/list_ipss.txt"
LOG="$PATH_PROGRAM/log/error.activarstock.log"
FILE_TGZ="$PATH_PROGRAM/tgz/stockonline.tgz"
MD5=`md5sum $FILE_TGZ | cut -d " " -f1 | tr -d "\n"`
CONFIG_CRON="$PATH_PROGRAM/tgz/config-crontab.sh"

function main(){

    fnGroupShops
    fnGroupIPs
    
    i=0
    while [ $i != ${#tiendas[@]} ]
    do
        existe=`mysql --connect-timeout=5 -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(id) AS total_registros FROM tb_install_stock_online WHERE shop = '${tiendas[i]}'"`
        RETURN=$?
        if [ $RETURN -eq 0 ]; then
            if [ $existe -ge 1 ]; then
                date_send=`mysql --connect-timeout=5 -u dba srvremoto -h 10.106.77.224 -N -e "SELECT date_send FROM tb_install_stock_online WHERE shop = '${tiendas[i]}'"`
                date_install=`mysql --connect-timeout=5 -u dba srvremoto -h 10.106.77.224 -N -e "SELECT date_install FROM tb_install_stock_online WHERE shop = '${tiendas[i]}'"`
                today=`date +%Y%m%d`
                if [ -n "$date_send" ]; then
                    if [ $today -eq $date_send ] || [ $today -ge $date_send ]; then
                        conn=$(fnValidaComunicacao)
                        RETURN=$?
                        if [ $RETURN != 0 ]; then
                            echo "$(date +%Y%m%d-%H%M%S.%s):CONN:${tiendas[$i]}:Erro ao conectar com a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                        else
                            echo "$(date +%Y%m%d-%H%M%S.%s):CONN:${tiendas[$i]}:Conectado com sucesso:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                            sshpass -p root ssh -o ConnectTimeout=1 -lroot -p10001 ${ips[$i]} ' ls -ltr /confdia/descargas/stockonline.tgz'
                            RETURN=$?
                            if [ $RETURN != 0 ]; then
                                echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_TGZ:${tiendas[$i]}:Arquivo ['stockonline.tgz'] não existe." >> $LOG
                                sshpass -p root scp -P10001 $FILE_TGZ root@${ips[$i]}:/confdia/descargas
                                RETURN=$?
                                if [ $RETURN != 0 ]; then
                                    echo "$(date +%Y%m%d-%H%M%S.%s):SEND_TGZ:${tiendas[$i]}:Falha ao enviar arquivo ['stockonline.tgz']." >> $LOG
                                    sshpass -p root scp -P10001 $FILE_TGZ root@${ips[$i]}:/confdia/descargas
                                    RETURN=$?
                                    if [ $RETURN != 0 ]; then
                                        echo "$(date +%Y%m%d-%H%M%S.%s):SEND_TGZ:${tiendas[$i]}:Falha ao tentar enviar arquivo ['stockonline.tgz'] novamente.Err['$RETURN']" >> $LOG
                                    else
                                        if [ $today -eq $date_install ] || [ $today -ge $date_install ]; then
                                            sshpass -p root ssh -o ConnectTimeout=1 -lroot -p10001 ${ips[$i]} 'tar -xzvf /confdia/descargas/stockonline.tgz -C /'
                                            RETURN=$?
                                            if [ $RETURN -eq 0 ]; then
                                                echo "$(date +%Y%m%d-%H%M%S.%s):SEND_TGZ:${tiendas[$i]}:Arquivo ['stockonline.tgz'] extraido com sucesso." >> $LOG
                                                sshpass -p root scp -P10001 $CONFIG_CRON root@${ips[$i]}:/tmp/
                                                RETURN=$?
                                                if [ $RETURN -eq 0 ]; then
                                                    echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Arquivo cron ['$CONFIG_CRON'] enviado com sucesso." >> $LOG
                                                    sshpass -p root ssh -o ConnectTimeout=1 -lroot -p10001 ${ips[$i]} 'bash -x /tmp/config-crontab.sh'
                                                    RETURN=$?
                                                    if [ $RETURN -eq 0 ]; then
                                                        echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Crontab configurado com sucesso." >> $LOG
                                                    else
                                                        echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Erro ao configurar crontab." >> $LOG
                                                    fi
                                                else
                                                    echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Erro ao enviar arquivo cron ['$CONFIG_CRON']." >> $LOG
                                                fi
                                            else
                                                echo "$(date +%Y%m%d-%H%M%S.%s):SEND_TGZ:${tiendas[$i]}:Falha ao extrair arquivo ['stockonline.tgz']." >> $LOG
                                            fi   
                                        else
                                            echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_DATE:Data ['$date_send'] de instalação para a loja ['${tiendas[$i]}'].['${ips[$i]}'] maior que a data atual ['$today']." >> $LOG
                                        fi
                                    fi
                                else
                                    if [ $today -eq $date_install ] || [ $today -ge $date_install ]; then
                                        sshpass -p root ssh -o ConnectTimeout=1 -lroot -p10001 ${ips[$i]} 'tar -xzvf /confdia/descargas/stockonline.tgz -C /'
                                        RETURN=$?
                                        if [ $RETURN -eq 0 ]; then
                                            echo "$(date +%Y%m%d-%H%M%S.%s):SEND_TGZ:${tiendas[$i]}:Arquivo ['stockonline.tgz'] extraido com sucesso." >> $LOG
                                            sshpass -p root scp -P10001 $CONFIG_CRON root@${ips[$i]}:/tmp/
                                            RETURN=$?
                                            if [ $RETURN -eq 0 ]; then
                                                echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Arquivo cron ['$CONFIG_CRON'] enviado com sucesso." >> $LOG
                                                sshpass -p root ssh -o ConnectTimeout=1 -lroot -p10001 ${ips[$i]} 'bash -x /tmp/config-crontab.sh'
                                                RETURN=$?
                                                if [ $RETURN -eq 0 ]; then
                                                    echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Crontab configurado com sucesso." >> $LOG
                                                else
                                                    echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Erro ao configurar crontab." >> $LOG
                                                fi
                                            else
                                                echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Erro ao enviar arquivo cron ['$CONFIG_CRON']." >> $LOG
                                            fi
                                        else
                                            echo "$(date +%Y%m%d-%H%M%S.%s):SEND_TGZ:${tiendas[$i]}:Falha ao extrair arquivo ['stockonline.tgz']." >> $LOG
                                        fi 
                                    else
                                        echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_DATE:Data ['$date_send'] de instalação para a loja ['${tiendas[$i]}'].['${ips[$i]}'] maior que a data atual ['$today']." >> $LOG
                                    fi
                                fi
                            else
                                if [ $today -eq $date_install ] || [ $today -ge $date_install ]; then
                                    echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_TGZ:${tiendas[$i]}:Arquivo ['stockonline.tgz'] existe." >> $LOG
                                    CHECK_MD5=`sshpass -p root ssh -o ConnectTimeout=1 -lroot -p10001 ${ips[$i]} ' md5sum /confdia/descargas/stockonline.tgz | cut -d " " -f1 | tr -d "\n"'`
                                    if [ "$MD5" == "$CHECK_MD5" ];then
                                        echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_TGZ:${tiendas[$i]}:Arquivo ['stockonline.tgz']:['$CHECK_MD5'] == ['$MD5']." >> $LOG
                                        CHECK_INSTALL=`sshpass -p root ssh -o ConnectTimeout=1 -lroot -p10001 ${ips[$i]} 'A=$(ls -ltr intstockonline.sh aux-stock.sh outquery/list_stock_*.csv.sent | wc -l);B=$(cat /etc/crontab | grep -a "root bash /root/aux-stock.sh" | wc -l | tr -d "\n"); let "C = A +B"; echo $C'`
                                        if [ $CHECK_INSTALL -ge 7 ]; then
                                            echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Serviço instalado e executando." >> $LOG
                                        else
                                            echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Erro nos arquivos encontrados ou corrompido." >> $LOG
                                            sshpass -p root scp -P10001 $CONFIG_CRON root@${ips[$i]}:/tmp/
                                            RETURN=$?
                                            if [ $RETURN -eq 0 ]; then
                                                echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Arquivo cron ['$CONFIG_CRON'] enviado com sucesso." >> $LOG
                                                sshpass -p root ssh -o ConnectTimeout=1 -lroot -p10001 ${ips[$i]} 'bash -x /tmp/config-crontab.sh'
                                                RETURN=$?
                                                if [ $RETURN -eq 0 ]; then
                                                    echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Crontab configurado com sucesso." >> $LOG
                                                else
                                                    echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Erro ao configurar crontab." >> $LOG
                                                fi
                                            else
                                                echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Erro ao enviar arquivo cron ['$CONFIG_CRON']." >> $LOG
                                            fi
                                        fi
                                    else
                                        echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_TGZ:${tiendas[$i]}:Arquivo ['stockonline.tgz']:['$CHECK_MD5'] != ['$MD5']." >> $LOG
                                        sshpass -p root scp -P10001 $FILE_TGZ root@${ips[$i]}:/confdia/descargas
                                        RETURN=$?
                                        if [ $RETURN != 0 ]; then
                                            echo "$(date +%Y%m%d-%H%M%S.%s):SEND_TGZ:${tiendas[$i]}:Falha ao enviar arquivo ['stockonline.tgz']." >> $LOG
                                        else
                                            sshpass -p root ssh -o ConnectTimeout=1 -lroot -p10001 ${ips[$i]} 'tar -xzvf /confdia/descargas/stockonline.tgz -C /'
                                            RETURN=$?
                                            if [ $RETURN -eq 0 ]; then
                                                echo "$(date +%Y%m%d-%H%M%S.%s):SEND_TGZ:${tiendas[$i]}:Arquivo ['stockonline.tgz'] extraido com sucesso." >> $LOG
                                                CHECK_INSTALL=`sshpass -p root ssh -o ConnectTimeout=1 -lroot -p10001 ${ips[$i]} 'A=$(ls -ltr intstockonline.sh aux-stock.sh outquery/list_stock_*.csv.sent | wc -l);B=$(cat /etc/crontab | grep -a "root bash /root/aux-stock.sh" | wc -l | tr -d "\n"); let "C = A +B"; echo $C'`
						if [ $CHECK_INSTALL -ge 7 ]; then
						   echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Serviço instalado e executando." >> $LOG
						else
						   echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Erro nos arquivos encontrados ou corrompido." >> $LOG
						   sshpass -p root scp -P10001 $CONFIG_CRON root@${ips[$i]}:/tmp/
						   RETURN=$?
						   if [ $RETURN -eq 0 ]; then
						      echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Arquivo cron ['$CONFIG_CRON'] enviado com sucesso." >> $LOG
						      sshpass -p root ssh -o ConnectTimeout=1 -lroot -p10001 ${ips[$i]} 'bash -x /tmp/config-crontab.sh'
						      RETURN=$?
						      if [ $RETURN -eq 0 ]; then
							 echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Crontab configurado com sucesso." >> $LOG
						      else
							 echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Erro ao configurar crontab." >> $LOG
						      fi
						   else
						      echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:${tiendas[$i]}:Erro ao enviar arquivo cron ['$CONFIG_CRON']." >> $LOG
						   fi
						fi
                                            else
                                                echo "$(date +%Y%m%d-%H%M%S.%s):SEND_TGZ:${tiendas[$i]}:Falha ao extrair arquivo ['stockonline.tgz']." >> $LOG
                                            fi
                                        fi
                                    fi
                                else
                                    echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_DATE:Data ['$date_send'] de instalação para a loja ['${tiendas[$i]}'].['${ips[$i]}'] maior que a data atual ['$today']." >> $LOG
                                fi    
                            fi
                        fi
                    else
                        echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_DATE:Data ['$date_send'] de envio para a loja ['${tiendas[$i]}'].['${ips[$i]}'] maior que a data atual ['$today']." >> $LOG
                    fi
                else
                    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:${tiendas[$i]}:Loja não possui data para ativação stock online." >> $LOG
                fi
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:${tiendas[$i]}:Loja não existe no banco de dados." >> $LOG
            fi
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:${tiendas[$i]}:Erro verificar loja no banco de dados." >> $LOG
        fi
        let "i = i +1"
    done
}

function fnGroupShops(){
    while read loja; 
	do
		progs=("$loja")
	     done < $LIST_SHOPS
	tiendas=(${progs[0]})
}

function fnGroupIPs(){
    while read ip; 
	do
		progss=("$ip")
	done < $LIST_IPS
	ips=(${progss[0]})
}

function fnValidaComunicacao(){
    sshpass -p root ssh -o ConnectTimeout=1 -lroot -p10001 ${ips[$i]} exit
}

main
