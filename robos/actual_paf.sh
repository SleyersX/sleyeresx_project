#!/bin/bash
#
# Autor : Walter Moura
# Data  : 2019-01-29
# Modif.: 2019-01-29
#
# Script para conectar nas lojas pré-selecionadas e validar a quantidade de ficheros
# dentro pasta /confdia/actualPAF, se for maior que 1000 apaga todos os arquivos *txt
# de forma automatica.
#

LOG="/root/srv_remoto/log/error.actualpaf.log"

function main(){

	group_shop
	group_ip
	BD="/root/srv_remoto/srv_remoto.db"
	i=0
	while [ $i != ${#tiendas[@]} ] 
	do
		if [ "$x" == 0 ]; then
			echo "LOJA:"${tiendas[$x]}"."
		else
			active=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT active FROM tb_actual_paf WHERE tienda LIKE '${tiendas[$i]}';")
			if [ -n "$active" ]; then
				if [ $active -eq 1 ];then
					echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_ACTIVE:Loja:['${tiendas[$i]}'].['${ips[$i]}'] ativa." >> $LOG
					conn=$(valida_comunicacao)
					return=$?
					if [ $return != 0 ]; then
						echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_CONN:Falha ao conectar com a loja:['${tiendas[$i]}'].['${ips[$i]}']" >> $LOG
						echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_CONN:Return=>'$return'." >> $LOG
					else
						echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_CONN:Conexão com a loja:['${tiendas[$i]}'].['${ips[$i]}'] com sucesso." >> $LOG
						echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_CONN:Return=>'$return'." >> $LOG
						actualpaf=$(ssh ${ips[$i]} 'ls -ltr /confdia/actualPAF/ | wc -l ')
						return=$?
						if [ $return != 0 ]; then
							echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Falha ao executar comando na loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
							echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Return=>'$return'." >> $LOG
						else
							if [ $actualpaf -gt 1000 ]; then
								del=$(ssh ${ips[$i]} 'rm /confdia/actualPAF/*txt')
								return=$?
								if [ $return != 0 ]; then
									echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Falha ao deletar arquivos na loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
									echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Return=>'$return'." >> $LOG
								else
									echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Arquivo deletado com sucesso na loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
									echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Return=>'$return'." >> $LOG
								fi
							else
								echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Ação não necessaria para a loja :['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
								echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Return=>'$return'." >> $LOG
							fi
						fi
					fi
				else
					echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_ACTIVE:Loja:['${tiendas[$i]}'] não esta ativa para essa funcionalidade." >> $LOG
				fi
			else
				echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_ACTIVE:Loja:['${tiendas[$i]}'] não existe no banco de dados." >> $LOG
			fi		
		fi
		let "i = i +1"			
	done
}

function group_shop(){

	while read loja; 
	do
		progs=("$loja")
	     done < /root/srv_remoto/arquivos_read/list_tiendas.txt
	tiendas=(${progs[0]})
}

function group_ip(){

	while read ip; 
	do
		progss=("$ip")
	done < /root/srv_remoto/arquivos_read/list_ipss.txt
	ips=(${progss[0]})
}

function valida_comunicacao(){

	ssh -o ConnectTimeout=1 ${ips[$i]} exit
}


main
