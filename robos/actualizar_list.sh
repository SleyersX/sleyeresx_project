#!/bin/bash

LISTTEMPNFCE="/root/srv_remoto/arquivos_read/temp_list_nfce.txt"
LISTNFCE="/root/srv_remoto/texto/aux_list_lojas_99.txt"
if [ -e "$LISTTEMPNFCE" ]; then
	rm -f $LISTTEMPNFCE
	mysql -u soporte soporteremotoweb -h 10.105.186.135 -N -e "SELECT numerotienda FROM tiendas WHERE centro IN (720,750)" > $LISTTEMPNFCE
	ARRAYNFCE=`awk -F " " '{print $1;}' $LISTTEMPNFCE | while read tnfce ; do printf "%d\t" $tnfce ; done`
	echo $ARRAYNFCE > $LISTNFCE
else
	mysql -u soporte soporteremotoweb -h 10.105.186.135 -N -e "SELECT numerotienda FROM tiendas WHERE centro IN (720,750)" > $LISTTEMPNFCE
	ARRAYNFCE=`awk -F " " '{print $1;}' $LISTTEMPNFCE | while read tnfce ; do printf "%d\t" $tnfce ; done` 
	echo $ARRAYNFCE > $LISTNFCE
fi

LISTADO="/root/srv_remoto/arquivos_read/listado_tiendas.csv"
ARRAY_SHOPS="/root/srv_remoto/arquivos_read/list_tiendas.txt"
ARRAY_IPS="/root/srv_remoto/arquivos_read/list_ipss.txt"
LIST_TEMP="/root/srv_remoto/arquivos_read/temp_tiendas.txt"
if [ -e "$LISTADO" ]; then
	rm -f $LISTADO
	#wget -O /root/srv_remoto/arquivos_read/listado_tiendas.csv 10.105.186.135/Resources/menu/4_listados/List_Total/json_tiendas.php?csv=1
	mysql soporteremotoweb -h 10.105.186.135 -N -B -e "SELECT numerotienda AS tienda, IP AS ip FROM tiendas WHERE pais LIKE 'BRA'" > $LISTADO
else
	#wget -O /root/srv_remoto/arquivos_read/listado_tiendas.csv 10.105.186.135/Resources/menu/4_listados/List_Total/json_tiendas.php?csv=1
	mysql soporteremotoweb -h 10.105.186.135 -N -B -e "SELECT numerotienda AS tienda, IP AS ip FROM tiendas WHERE pais LIKE 'BRA'" > $LISTADO
fi
if [ -e "$LISTADO" ]; then
	if [ -e "$ARRAY_SHOPS" ]; then
		rm -f $ARRAY_SHOPS
	fi
	if [ -e "$ARRAY_IPS" ]; then
		rm -f $ARRAY_IPS
	fi
	shops=`awk -F " " '{print $1 ;}' $LISTADO |  while read line ; do printf "%05d\t" $line ; done `
	ipss=`awk -F " " '{print $2;}' $LISTADO |  while read line ; do printf "%s\t" $line ; done `
	echo $shops > $ARRAY_SHOPS
	echo $ipss > $ARRAY_IPS
fi
sleep 5
cp $ARRAY_SHOPS $LIST_TEMP
bash -x /root/srv_remoto/scripts_bash/gerar_sql.sh
