#!/bin/bash
#=======================================================
# Autor          : Walter Moura (wam001br)
# Data Criação   : 2019-06-21
# Data Moficação : 2019-06-21
#=======================================================

# variaveis
LOG="/root/srv_remoto/log/error.auxetiqkaizala.log"
BD="/root/srv_remoto/catalog.db"
AUX="/root/srv_remoto/texto/aux_etiq_kaizala.txt"
function group_artigos(){

    while read codigo;
    do
        progs=("$codigo")
    done < /root/srv_remoto/arquivos_read/array_artigos.txt
    artigo=(${progs[0]})

}
function main(){

    group_artigos
    i=0
    while [ $i != ${#artigo[@]} ]
    do
        if [ "$x" == "0" ]; then
            echo "LOJA:${artigo[$x]}""."
        else
            echo "SELECT Item.ItemID, Item.LoyaltyDescription, printf('%.2f', Item.PriceAmount) AS PvpGenerico, printf('%.2f', Item.PriceAmountFid) AS PvpFidelizado, printf('%.2f', Item.PriceAmountNoFid) AS PvpNoFidelizado, ItemEAN.EAN FROM Item INNER JOIN ItemEAN ON Item.ItemID LIKE ItemEAN.ItemID WHERE Item.ItemID LIKE '"${artigo[$i]}"';" | sqlite3 $BD >> $AUX
            let "i = i +1"
        fi
    done 

}
main