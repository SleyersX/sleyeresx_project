#!/bin/bash
#
# Script para checar o espaço em Disco
# e intervir caso necessario.

USERACTUAL=$(grep "x:$EUID" /etc/group | awk -F ":" '{print $1}')
PATH_USER="/$USERACTUAL/srv_remoto"
PATH_LOG="$PATH_USER/log"
BD="$PATH_USER/srv_remoto.db"
ARQRET="$PATH_USER/arquivos_read/relatorio_disco.log"
LOG="$PATH_LOG/error.checkdisco.log"
ARQSHOPS="$PATH_USER/arquivos_read/temp_tiendas.txt"
USER="root"
PASS="root"
DTATUAL="$(date +%Y%m%d)"

function main(){
    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Inicio programa." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Inciando função ['fnGroupShop']." >> $LOG
    fnGroupShop
    ERR=$?
    if [ $ERR -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Função ['FnGroupShop'] executada com sucesso." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Inciando função ['fnGetIP']." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Iniciamos um while no array ['ARRAYSHOPS']" >> $LOG
        i=0
        while [ $i != ${#ARRAYSHOPS[@]} ]
        do
            if [[ ${ARRAYSHOPS[i]} = ?(+|-)+([0-9]) ]]; then
                echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Valor ['${ARRAYSHOPS[i]}'] é do tipo número, incluímos 0 a esquerda." >> $LOG
                SHOP=`echo "${ARRAYSHOPS[i]}"`
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Valor ['${ARRAYSHOPS[i]}'] não é do tipo númerico, será ignorado." >> $LOG
            fi
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Iniciando a função ['GetIP']." >> $LOG
            fnGetIP
            ERR=$?
            x=1
            while (( x <= $tpvs ))
            do
                fnValidaConexao
                ERR=$?
                if [ $ERR -eq 0 ]; then
                    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Conexão com sucesso a loja/caixa ['${ARRAYSHOPS[i]}']:['$ip']:['1000$x']." >> $LOG
                    STDISK=`sshpass -p $PASS ssh -o ConnectTimeout=1 $ip -p1000$x -l $USER 'df -h | grep "sda3\|hda3"' | awk '{printf $5}' | tr -d "%"`
                    if [ $STDISK -ge 50 ]; then
                        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Detectado para ['${ARRAYSHOPS[i]}']:['$ip']:['1000$x'] que o disco está ocupado acima dos 50%, será feita a remoção dos arquivos do diretorio ['/srv/Debian6.0/srv/infised/ficcaje'] com intervalo de 1 semana." >> $LOG
                        sshpass -p $PASS ssh -o ConnectTimeout=1 $ip -p1000$x -l $USER 'rm -vf $( find /srv/Debian6.0/srv/infised/ficcaje -name "*_Auditoria_SAT.txt" -mtime +2)'
                        ERR=$?
                        if [ $ERR -eq  0 ]; then
                            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Arquivos DIR1 removidos com sucesso para loja/caixa ['${ARRAYSHOPS[i]}']:['$ip']:['1000$x']." >> $LOG
                            sshpass -p $PASS ssh -o ConnectTimeout=1 $ip -p1000$x -l $USER 'rm -vf $(find /srv/Debian6.0/srv/infised/lib -name "*_Auditoria_GNE.txt" -mtime +5)'
                            ERR=$?
                            if [ $ERR -eq  0 ]; then
                                echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Arquivos DIR2 removidos com sucesso para loja/caixa ['${ARRAYSHOPS[i]}']:['$ip']:['1000$x']." >> $LOG
                                sshpass -p $PASS ssh -o ConnectTimeout=1 $ip -p1000$x -l $USER 'FILE="/srv/Debian6.0/srv/infised/lib/Auditoria_WS.txt" ; 
                                if [ -e $FILE ]; then
                                    rm -vf $FILE
                                fi'
                                sshpass -p $PASS ssh -o ConnectTimeout=1 $ip -p1000$x -l $USER 'rm -vf $( find /srv/Debian6.0/srv/servidorDFW/lib -name "*_Auditoria_WS.txt" -mtime +2)'
                                ERR=$?
                                if [ $ERR -eq 0 ]; then
                                    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Arquivos DIR3 removidos com sucesso para loja/caixa ['${ARRAYSHOPS[i]}']:['$ip']:['1000$x']." >> $LOG
                                    sshpass -p $PASS ssh -o ConnectTimeout=1 $ip -p1000$x -l $USER 'rm -vf $(find /var/log -name "wsd_ssl.log*" -size "+100000k")'
                                    ERR=$?
                                    if [ $ERR -eq 0 ]; then
                                        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Arquivos DIR4 removidos com sucesso para loja/caixa ['${ARRAYSHOPS[i]}']:['$ip']:['1000$x']." >> $LOG
                                        sshpass -p $PASS ssh -o ConnectTimeout=1 $ip -p1000$x -l $USER 'rm -vf $(find /var/log -name "wsd.log*" -size "+100000k")'
                                        ERR=$?
                                        if [ $ERR -eq 0 ]; then
                                            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Arquivos DIR5 removidos com sucesso para loja/caixa ['${ARRAYSHOPS[i]}']:['$ip']:['1000$x']." >> $LOG
                                        else
                                            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Erro ao conectar com a loja/caixa ['${ARRAYSHOPS[i]}']:['$ip']:['1000$x'] ou na execução do comando DIR5.Err -> ['$ERR']." >> $LOG
                                        fi
                                    else
                                        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Erro ao conectar com a loja/caixa ['${ARRAYSHOPS[i]}']:['$ip']:['1000$x'] ou na execução do comando DIR4.Err -> ['$ERR']." >> $LOG
                                    fi
                                else
                                    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Erro ao conectar com a loja/caixa ['${ARRAYSHOPS[i]}']:['$ip']:['1000$x'] ou na execução do comando DIR3.Err -> ['$ERR']." >> $LOG
                                fi
                            else
                                 echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Erro ao conectar com a loja/caixa ['${ARRAYSHOPS[i]}']:['$ip']:['1000$x'] ou na execução do comando DIR2.Err -> ['$ERR']." >> $LOG
                            fi
                        else
                            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Erro ao conectar com a loja/caixa ['${ARRAYSHOPS[i]}']:['$ip']:['1000$x'] ou na execução do comando DIR1.Err -> ['$ERR']." >> $LOG
                        fi
                    else
                        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Ação não necessária para a loja ['${ARRAYSHOPS[i]}']:['$ip']:['1000$x']." >> $LOG
                    fi
                else
                    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Erro ao conectar com a loja/caixa ['${ARRAYSHOPS[i]}']:['$ip']:['1000$x'].Err -> ['$ERR']." >> $LOG
                fi
                let "x = x +1"
            done
            let "i = i +1"
        done 
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Erro ao iniciar função ['fnGroupShop'] Err -> ['$ERR']." >> $LOG
    fi
}

function fnGroupShop(){

	echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Inciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Verificamos se arquivo ['$ARQTEMP'] existe." >> $LOG
    if [ -e $ARQSHOPS ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Arquivo OK." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Iniciamos um while read no arquivo ['$ARQSHOPS'] e gravamos cada valor em um array." >> $LOG
        while read idshop;
            do
                progress=("$idshop")
        done < $ARQSHOPS
        ARRAYSHOPS=(${progress[0]})
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:ARRAY Shops -> ['$ARRAYSHOPS']." >> $LOG
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Arquivo NOK." >> $LOG
    fi
    
}

function fnValidaConexao(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VALIDACONEXAO:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VALIDACONEXAO:Validando conexão ['$SHOP:$ip']." >> $LOG
    sshpass -p $PASS ssh -o ConnectTimeout=1 -p1000$x -l $USER $ip exit
}

function fnGetIP(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Verificamos se a loja ['$SHOP'] existe no banco ['$BD']." >> $LOG
    EXISTE=`echo "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Retorno existe ['$EXISTE']." >> $LOG
    if [ $EXISTE -eq 1 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Loja ['$SHOP'] existe, buscamos o IP." >> $LOG
        ip=`echo "SELECT ip FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
        tpvs=`echo "SELECT tpvs FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Retorno IP -> ['$ip']." >> $LOG
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Loja ['$SHOP'] não econtrada no banco de dados." >> $LOG
    fi
}
main