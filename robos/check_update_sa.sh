#!/bin/bash
#
# Autor: Walter Moura
# Data Criacao: 2019.08.13
# Data Modificacao: 2019.08.13
# Versao: 0.01
#

VERSION_ACT=$2
GER_LOG=$4
FORCE=$5
BD="/root/srv_remoto/srv_remoto.db"
TIENDAS="tiendas_update.txt"

function group_shop(){

    if [ -e $TIENDAS ] ; then
        while read shop; 
        do
            progs=("$shop")
        done < $TIENDAS 
        array_shops=(${progs[0]})
    else
        echo "Arquivo [$TIENDAS] não foi encontrado no diretorio atual."
    fi
}

function main(){
    group_shop
	i=0
	while [ $i != ${#array_shops[@]} ]
	do
		if [ "$x" == "0" ]; then
			echo "LOJA:"${array_shops[$x]}"."
		else
            # Validamos se os campos informados são númericos
            if [[ ${array_shops[i]} = ?(+|-)+([0-9]) ]]; then
                echo ""
            else
                echo "Ops, valor informado não é um número inteiro!"
                sleep 2
            fi
            
            lj=`echo ${#array_shops[i]}`
            if [ $lj -ge 5 ]; then
                echo "Informe até 4 caracteres númericos. Ex.: 2, 1000, 9527, 165."
                sleep 2
            fi
            loja=`printf "%05d" ${array_shops[i]}`
            EXISTE=`echo "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$loja';" | sqlite3 $BD`
            if [ $EXISTE -eq 1 ]; then
                # GetIP
                ip=`echo "SELECT ip FROM tb_ip WHERE loja LIKE '$loja';" | sqlite3 $BD`
                tpvs=`echo "SELECT tpvs FROM tb_ip WHERE loja LIKE '$loja';" | sqlite3 $BD`

                if [ "$VERSION_ACT" == "SA.43.003.01.BRA-0005" ];then
                    if [ -z $FORCE ]; then
                        echo -e "\033[01;32mTIENDA [$loja]\033[00;37m"
                        for y in $(seq 1 $tpvs); do sshpass -p root ssh -o ConnectTimeout=1 -p1000$y -l root $ip '. /confdia/bin/setvari ; VS=$(cat /confdia/version)
                        if [ "$VS" == "SA.43.003.01.BRA-0005" ]; then
                            echo -e "\033[00;37mTIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ACTUALIZACION=\033[01;32mOK\033[00;37m"
                        else
                            echo -e "\033[00;37mTIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ACTUALIZACION=\033[01;31mNOK\033[00;37m" 
                        fi'; done
                        if [ $GER_LOG -eq 1 ]; then
                            echo "TIENDA [$loja]" >> result_check_$VERSION_ACT.txt
                            for y in $(seq 1 $tpvs); do sshpass -p root ssh -o ConnectTimeout=1 -p1000$y -l root $ip '. /confdia/bin/setvari ; VS=$(cat /confdia/version)
                            if [ "$VS" == "SA.43.003.01.BRA-0005" ]; then
                                echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ACTUALIZACION=OK"
                            else
                                echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ACTUALIZACION=NOK"
                            fi'; done >> result_check_$VERSION_ACT.txt
                        fi
                    elif [ "$FORCE" == "-f" ]; then
                       echo -e "\033[01;32mTIENDA [$loja]\033[00;37m"
                        for y in $(seq 1 $tpvs); do sshpass -p root ssh -o ConnectTimeout=1 -p1000$y -l root $ip '. /confdia/bin/setvari ; VS=$(cat /confdia/version)
                        if [ "$VS" == "SA.43.003.01.BRA-0005" ]; then
                            echo -e "\033[00;37mTIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ACTUALIZACION=\033[01;32mOK\033[00;37m"
                        else
                            echo -e "\033[00;37mTIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ACTUALIZACION=\033[01;31mNOK\033[00;37m"
                            if [ "${NUMECAJA}" == "01" ]; then
                                echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ATUALIZAÇÃO PARA A MASTER DEVE SER MANUAL."
                            else
                                EXE=$(cat /var/log/cajera.log  | tail -n1 | cut -d ":" -f 2)
                                SEL=$(cat /var/log/cajera.log  | tail -n1 | cut -d ":" -f 4 | sed "s/.//")
                                if [ "$SEL" == "SELETOR" ] || [ "$SEL" == "SELETOR MASTER " ] || [ "$SEL" == "SELETOR MA " ] || [ "$SEL" == "ERRO DE VERSAO       / DISTINTA A MASTER   " ];then
                                    echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] FORÇANDO A ATUALIZAÇÃO."
                                    touch /confdia/bin/auto01 ;killall /confdia/bin/$(echo "$EXE" | tr "A-Z" "a-z").exe
                                    if [ $? -eq 0 ]; then
                                        echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] COMANDO EXECUTADO COM SUCESSO."
                                        echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] REFAZER O CHECK POSTERIORMENTE."
                                    else
                                        echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ERRO AO EXECUTAR COMANDO."
                                    fi
                                fi
                            fi 
                        fi'; done
                        if [ $GER_LOG -eq 1 ]; then
                            echo "TIENDA [$loja]" >> result_check_$VERSION_ACT.txt
                            for y in $(seq 1 $tpvs); do sshpass -p root ssh -o ConnectTimeout=1 -p1000$y -l root $ip '. /confdia/bin/setvari ; VS=$(cat /confdia/version)
                            if [ "$VS" == "SA.43.003.01.BRA-0005" ]; then
                                echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ACTUALIZACION=OK"
                            else
                                echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ACTUALIZACION=NOK"
                                if [ "${NUMECAJA}" == "01" ]; then
                                    echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ATUALIZAÇÃO PARA A MASTER DEVE SER MANUAL."
                                else
                                    EXE=$(cat /var/log/cajera.log  | tail -n1 | cut -d ":" -f 2)
                                    SEL=$(cat /var/log/cajera.log  | tail -n1 | cut -d ":" -f 4 | sed "s/.//")
                                    if [ "$SEL" == "SELETOR" ] || [ "$SEL" == "SELETOR MASTER " ] || [ "$SEL" == "SELETOR MA " ] || [ "$SEL" == "ERRO DE VERSAO       / DISTINTA A MASTER   " ];then
                                        echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] FORÇANDO A ATUALIZAÇÃO."
                                        touch /confdia/bin/auto01 ; killall /confdia/bin/$(echo "$EXE" | tr "A-Z" "a-z").exe
                                        if [ $? -eq 0 ]; then
                                            echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] COMANDO EXECUTADO COM SUCESSO."
                                            echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] REFAZER O CHECK POSTERIORMENTE."
                                        else
                                            echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ERRO AO EXECUTAR COMANDO."
                                        fi
                                    fi
                                fi
                            fi'; done >> result_check_$VERSION_ACT.txt
                        fi
                    else
                        echo -e "\033[01;31mError:\033[01;33m Erro de parametros.\033[00;37m"
                        echo -e "\033[01;34mExemplo:\033[01;32m ./check_update_sa -v 'Versão a ser atualizada{SA.00.000.000.BRA-0000}' -l '0=Não gerar log de saída/1=Gerar log de saída' -f 'Para forçar a instalação durante a conexão[Opcional].'\033[00;37m "
                    fi
                elif [ "$VERSION_ACT" == "SA.43.004.03.BRA-0001" ];then
                    if [ -z $FORCE ]; then
                        echo -e "\033[01;32mTIENDA [$loja]\033[00;37m"
                        for y in $(seq 1 $tpvs); do sshpass -p root ssh -o ConnectTimeout=1 -p1000$y -l root $ip '. /confdia/bin/setvari ; VS=$(cat /confdia/version)
                        if [ "$VS" == "SA.43.004.03.BRA-0001" ]; then
                            echo -e "\033[00;37mTIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ACTUALIZACION=\033[01;32mOK\033[00;37m"
                        else
                            echo -e "\033[00;37mTIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ACTUALIZACION=\033[01;31mNOK\033[00;37m"
                        fi'; done
                        if [ $GER_LOG -eq 1 ]; then
                            echo "TIENDA [$loja]" >> result_check_$VERSION_ACT.txt
                            for y in $(seq 1 $tpvs); do sshpass -p root ssh -o ConnectTimeout=1 -p1000$y -l root $ip '. /confdia/bin/setvari ; VS=$(cat /confdia/version)
                            if [ "$VS" == "SA.43.004.03.BRA-0001" ]; then
                                echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ACTUALIZACION=OK"
                            else
                                echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ACTUALIZACION=NOK"
                            fi'; done >> result_check_$VERSION_ACT.txt
                        fi
                    elif [ "$FORCE" == "-f" ]; then
                        echo -e "\033[01;32mTIENDA [$loja]\033[00;37m"
                        for y in $(seq 1 $tpvs); do sshpass -p root ssh -o ConnectTimeout=1 -p1000$y -l root $ip '. /confdia/bin/setvari ; VS=$(cat /confdia/version)
                        if [ "$VS" == "SA.43.004.03.BRA-0001" ]; then
                            echo -e "\033[00;37mTIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ACTUALIZACION=\033[01;32mOK\033[00;37m"
                        else
                            echo -e "\033[00;37mTIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ACTUALIZACION=\033[01;31mNOK\033[00;37m"
                            if [ "${NUMECAJA}" == "01" ]; then
                                echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ATUALIZAÇÃO PARA A MASTER DEVE SER MANUAL."
                            else
                                EXE=$(cat /var/log/cajera.log  | tail -n1 | cut -d ":" -f 2)
                                SEL=$(cat /var/log/cajera.log  | tail -n1 | cut -d ":" -f 4 | sed "s/.//")
                                if [ "$SEL" == "SELETOR" ] || [ "$SEL" == "SELETOR MASTER " ] || [ "$SEL" == "SELETOR MA " ] || [ "$SEL" == "ERRO DE VERSAO       / DISTINTA A MASTER   " ];then
                                    echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] FORÇANDO A ATUALIZAÇÃO."
                                    touch /confdia/bin/auto01 ; killall /confdia/bin/$(echo "$EXE" | tr "A-Z" "a-z").exe
                                    if [ $? -eq 0 ]; then
                                        echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] COMANDO EXECUTADO COM SUCESSO."
                                        echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] REFAZER O CHECK POSTERIORMENTE."
                                    else
                                        echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ERRO AO EXECUTAR COMANDO."
                                    fi
                                fi
                            fi  
                        fi'; done
                        if [ $GER_LOG -eq 1 ]; then
                            echo "TIENDA [$loja]" >> result_check_$VERSION_ACT.txt
                            for y in $(seq 1 $tpvs); do sshpass -p root ssh -o ConnectTimeout=1 -p1000$y -l root $ip '. /confdia/bin/setvari ; VS=$(cat /confdia/version)
                            if [ "$VS" == "SA.43.004.03.BRA-0001" ]; then
                                echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ACTUALIZACION=OK"
                            else
                                echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ACTUALIZACION=NOK"
                                if [ "${NUMECAJA}" == "01" ]; then
                                    echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ATUALIZAÇÃO PARA A MASTER DEVE SER MANUAL."
                                else
                                    EXE=$(cat /var/log/cajera.log  | tail -n1 | cut -d ":" -f 2)
                                    SEL=$(cat /var/log/cajera.log  | tail -n1 | cut -d ":" -f 4 | sed "s/.//")
                                    if [ "$SEL" == "SELETOR" ] || [ "$SEL" == "SELETOR MASTER " ] || [ "$SEL" == "SELETOR MA " ] || [ "$SEL" == "ERRO DE VERSAO       / DISTINTA A MASTER   " ];then
                                        echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] FORÇANDO A ATUALIZAÇÃO."
                                        touch /confdia/bin/auto01 ; killall /confdia/bin/$(echo "$EXE" | tr "A-Z" "a-z").exe
                                        if [ $? -eq 0 ]; then
                                            echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] COMANDO EXECUTADO COM SUCESSO."
                                            echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] REFAZER O CHECK POSTERIORMENTE."
                                        else
                                            echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] ERRO AO EXECUTAR COMANDO."
                                        fi
                                    fi
                                fi
                            fi'; done >> result_check_$VERSION_ACT.txt
                        fi
                    else
                        echo -e "\033[01;31mError:\033[01;33m Erro de parametros.\033[00;37m"
                        echo -e "\033[01;34mExemplo:\033[01;32m ./check_update_sa -v 'Versão a ser atualizada{SA.00.000.000.BRA-0000}' -l '0=Não gerar log de saída/1=Gerar log de saída' -f 'Para forçar a instalação durante a conexão[Opcional].'\033[00;37m "
                    fi
                fi
            else
                echo "Loja [$loja] não encontrada no banco de dados."
            fi
        fi
        let "i = i +1"
    done
}

if [ -z $2 ] || [ -z $4 ]; then
    echo -e "\033[01;31mError:\033[01;33m Erro de parametros.\033[00;37m"
    echo -e "\033[01;34mExemplo:\033[01;32m ./check_update_sa -v 'Versão a ser atualizada{SA.00.000.000.BRA-0000}' -l '0=Não gerar log de saída/1=Gerar log de saída' -f 'Para forçar a instalação durante a conexão[Opcional].'\033[00;37m "
else
    main
fi