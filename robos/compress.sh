#!/bin/bash
#
# Autor: Walter Moura
# Data Criação: 09/10/2019
# Data Modificação: 10/10/2019
# Modificado por: Walter Moura
#
# Script para gerar o arquivo de LOTE.ZIP, para envio de xmls em contigencia, para SEFAZ
# 
# Versão 0.01
# - Leitura de todos os xmls
# - Gravação dos nomes de cada xml lido em ARRAY
# - Leitura do ARRAY, apagando o trecho onde consta "-procNFCe" e incluído no inicio AD em cada nome
#   de xml lido do ARRAY
# - Gerar arquivo zip, limitado 50 arquivos xml e com size máximo de 300kbts
# 
# Versão 0.02
# - Separação dos ZIP, pelo número da loja passado
# - Adaptado o PATH para cada usuario
# - Geração de logs

USER_ACTUAL=$(users | awk -F " " '{print $1;}')
PATH_USER="/home/$USER_ACTUAL/programxml"
DIR_XML_RENAME="$PATH_USER/xmlrename"
DIR_ZIP="$PATH_USER/zip"
DIR_XML_SEND="$PATH_USER/xmlsend"
ARRAY_FILE="$PATH_USER/.names_xml.txt"
ARRAY_FILE_SEND="$DIR_XML_SEND/.send_xml.txt"
LOG="$PATH_USER/error.filesxml.log"

function array_xml(){

	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_ARRAY_XML:Contando arquivos xml no diretorio ['$DIR_XML_RENAME']." >> $LOG
	COUNT_XML=`ls -ltr $DIR_XML_RENAME/* | wc -l`
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_ARRAY_XML:Quantidade de xml contados -> $COUNT_XML." >> $LOG
	if [ $COUNT_XML -eq 0 ]; then
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_ARRAY_XML:Não existe arquivos no diretorio ['$DIR_XML_RENAME']." >> $LOG
		exit 1
	else
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_ARRAY_XML:Acessendo o diretorio ['$DIR_XML_RENAME']." >> $LOG
		cd $DIR_XML_RENAME
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_ARRAY_XML:Gerando array com nome de cada xml e gravando na variável ['NAMES_XML']." >> $LOG
		NAMES_XML=`ls *.xml`
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_ARRAY_XML:Gravando o valor da variável ['NAMES_XML'] > ['ARRAY_FILE']." >> $LOG
		echo $NAMES_XML > $ARRAY_FILE
		if [ $? -eq 0 ]; then
			echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_ARRAY_XML:Comando executado com sucesso [$?]." >> $LOG
			while read namexml;
			do
				array_name=("$namexml")
			done < $ARRAY_FILE
			array_names=(${array_name[0]})
		else
			echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_ARRAY_XML:Erro ao executar comando [$?]." >> $LOG
		fi
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_ARRAY_XML:Retornando a ['function main']." >> $LOG
	fi
 }
 
 function array_xml_zip(){

	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_ARRAY_XML_ZIP:Contando arquivos xml no diretorio ['$DIR_XML_SEND']." >> $LOG
	COUNT_XML=`ls -ltr $DIR_XML_SEND/* | wc -l`
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_ARRAY_XML_ZIP:Quantidade de xml contados -> $COUNT_XML." >> $LOG
	if [ $COUNT_XML -eq 0 ]; then
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_ARRAY_XML_ZIP:Não existe arquivos no diretorio ['$DIR_XML_SEND']."
		exit 1
	else
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_ARRAY_XML_ZIP:Acessendo o diretorio ['$DIR_XML_SEND']." >> $LOG
		cd $DIR_XML_SEND
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_ARRAY_XML_ZIP:Gerando array com nome de cada xml e gravando na variável ['NAMES_XML_SEND']." >> $LOG
		NAMES_XML_SEND=`ls *.xml`
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_ARRAY_XML_ZIP:Gravando o valor da variável ['NAMES_XML_SEND'] > ['ARRAY_FILE_SEND']." >> $LOG
		echo $NAMES_XML_SEND > $ARRAY_FILE_SEND
		if [ $? -eq 0 ]; then
			while read namexmlsend;
			do
				array_xml_send=("$namexmlsend")
			done < $ARRAY_FILE_SEND
			array_xml_sends=(${array_xml_send[0]})
		else
			echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_ARRAY_XML_ZIP:Erro ao executar comando [$?]." >> $LOG
		fi
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_ARRAY_XML_ZIP:Retornando a ['function main']." >> $LOG
	fi
 }

function gerarZIP(){
	
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_GERAR_ZIP:Chamnado a ['function gerarZIP']." >> $LOG
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_GERAR_ZIP:Acessendo o diretorio ['$DIR_XML_SEND']." >> $LOG
	cd $DIR_XML_SEND
	COUNT_XML_SEND=`ls -ltr $DIR_XML_SEND/*.xml | wc -l`
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_GERAR_ZIP:Quantidade de xml contados -> $COUNT_XML_SEND." >> $LOG
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_GERAR_ZIP:Iniciando while." >> $LOG
	while (( COUNT_XML_SEND > 0 ))
	do
		z=0
		while (( z < $COUNT_XML_SEND ))
			do
				COUNT_LOTE=`ls *.zip | wc -l`
				if [ $COUNT_LOTE -eq 0 ]; then
					array_xml_zip
					x=0
					while [ $x != ${#array_xml_sends[@]} ]
					do
						if [ $x -eq 50 ];then
							break
						else
							zip -u LOTE$COUNT_LOTE.zip ${array_xml_sends[x]}
							rm -vf ${array_xml_sends[x]}
							unzip -v LOTE$COUNT_LOTE.zip
							SIZE_ZIP=`du -hsk LOTE$COUNT_LOTE.zip | awk -F " " '{print $1;}'`
							if [ $SIZE_ZIP -ge 301 ];then
								break
							fi
						fi
						let "x = x +1"
					done
				else
					array_xml_zip
					x=0
					while [ $x != ${#array_xml_sends[@]} ]
					do
						if [ $x -eq 50 ];then
							break
						else
							zip -u LOTE$COUNT_LOTE.zip ${array_xml_sends[x]}
							rm -vf ${array_xml_sends[x]}
							unzip -v LOTE$COUNT_LOTE.zip
							SIZE_ZIP=`du -hsk LOTE$COUNT_LOTE.zip | awk -F " " '{print $1;}'`
							if [ $SIZE_ZIP -ge 301 ];then
								break
							fi
						fi
						let "x = x +1"
					done
				fi
			let "COUNT_XML_SEND = COUNT_XML_SEND -50"
			let "z = z +1"
		done
		COUNT_XML_SEND=`ls -ltr $DIR_XML_SEND/*.xml | wc -l`
		if [ $COUNT_XML_SEND -eq 0 ]; then
			break
		fi
	done
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_GERAR_ZIP:While finalizado." >> $LOG
}

function moverZIP(){

	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Chamnado a ['function moverZIP']." >> $LOG
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Acessendo o diretorio ['$DIR_ZIP']." >> $LOG
	cd $DIR_ZIP
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Pedimos o número da loja, para mover o zip para a pasta com número da loja." >> $LOG
	read -p "Digite o número da loja:" numloja
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Testamos se a variavel esta vazia [L-156]." >> $LOG
	while (( numloja == false ))
	do
		read -p "Informe o número da loja:" numloja
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Variavel esta vazia [L-160]." >> $LOG
	done
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Numero da loja -> $numloja." >> $LOG
	lj=`echo ${#numloja}`
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Testamos se é número é um inteiro válido.[L-164]" >> $LOG
	if [[ $numloja = ?(+|-)+([0-9]) ]]; then
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Número de loja válido." >> $LOG
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Criando diretorio ['$DIR_ZIP/$numloja']." >> $LOG
		mkdir $numloja
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Acessando o diretorio ['$DIR_ZIP/$numloja']." >> $LOG
		cd $numloja
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Movendo todos os zips para o diretório ['$DIR_ZIP/$numloja']." >> $LOG
		mv $DIR_XML_SEND/*.zip .
	else
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Pedimos o número da loja, pois o valor informado não é um inteiro válido." >> $LOG
		read -p "Digite o número da loja:" numloja
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Testamos se a variavel esta vazia [L-176]." >> $LOG
		while (( numloja == false ))
		do
			read -p "Informe o número da loja:" numloja
			echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Variavel esta vazia [L-180]." >> $LOG
		done
		lj=`echo ${#numloja}`
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Testamos se é número é um inteiro válido [L-183]." >> $LOG
		if [[ $numloja = ?(+|-)+([0-9]) ]]; then
			echo ""
		else
			echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Testamos se a variavel esta vazia [L-187]." >> $LOG
			while (( numloja == false ))
			do
				read -p "Informe o número da loja:" numloja
				echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Variavel esta vazia [L-191]." >> $LOG
			done
		fi
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Número de loja válido." >> $LOG
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Criando diretorio ['$DIR_ZIP/$numloja']." >> $LOG
		mkdir $numloja
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Acessando o diretorio ['$DIR_ZIP/$numloja']." >> $LOG
		cd $numloja
		echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:Movendo todos os zips para o diretório ['$DIR_ZIP/$numloja']." >> $LOG
		mv $DIR_XML_SEND/*.zip .
	fi
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MOVER_ZIP:While finalizado." >> $LOG
}
function main(){
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MAIN:Iniciando programa." >> $LOG
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MAIN:Chamando a ['function array_xml']." >> $LOG
	array_xml
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MAIN:Iniciando renomeação dos xml [${#array_names[@]}]." >> $LOG
	i=0
	while [ $i != ${#array_names[@]} ]
	do
		XML_RENAME=`echo "AD${array_names[i]}" | sed 's/-procNFCe//'`
		mv -vf ${array_names[i]} $XML_RENAME
		if [ $? -eq 0 ]; then
			mv $XML_RENAME $DIR_XML_SEND		
		fi
		let "i = i +1"
	done
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MAIN:Xmls renomeados." >> $LOG
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MAIN:Chamando a ['function gerarZIP']." >> $LOG
	gerarZIP
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MAIN:Chamando a ['function moverZIP']." >> $LOG
	moverZIP
	echo "$(date +%Y%m%d-%H%M%S.%s):FUNC_MAIN:Fim programa." >> $LOG
}

main