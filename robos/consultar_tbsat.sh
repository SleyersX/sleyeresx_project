#!/bin/bash
#
#
#

function main()
{

echo -e "\033[01;33mEscolha uma opção:\033[00;37m"
echo
echo -e "\033[01;36m"
echo -e " 1.Consultar SATs Desatualizados"
echo -e " 2.Consultar por Loja           "
echo -e " 3.Consultar por SAT            "
echo -e " 4.Alterar Status SAT           "
echo -e " 5.Incluir modelos SATs Null    "
echo -e " 9.Sair                         "
echo -e "\033[00;37m"
echo -e -n "\033[01;33mDigite a opção: \033[00;37m"
read opcao
case $opcao in
	1)
		echo -e "\033[01;32mConsultando SATs Desatualizados.....\033[00;37m"
		sleep 2
		sqlite3 db_tpvs.db ".headers on" ".mode column" \
			"select ID, Loja, Sat, Caixa, IP, Mask, GW, DNS_1, DNS_2, Firmware,Layout, Status, Modelo_SAT from tb_sat where Layout = '00.06' and Status = 'Ativo'  order by Loja;"
		sleep 3
		echo
		(main)
		;;
	2)
		echo -e "\033[01;32mConsultando por Loja.....\033[00;37m"
		read -p "Digite o número da loja:" lj
		loja=$(seq -f "%05g" $lj $lj)
		sleep 2
		sqlite3 db_tpvs.db ".headers on" ".mode column" \
			"select ID, Loja, Sat, Caixa, IP, Mask, GW, DNS_1, DNS_2, Firmware,Layout, Status, Modelo_SAT from tb_sat where Loja = '$loja'  order by Caixa;"
		sleep 3
		echo
		(main)
		;;
	3)
		echo -e "\033[01;32mConsultando por SAT.....\033[00;37m"
		read -p "Digite o número da Sat:" st
		sat=$(seq -f "%09g" $st $st)
		sleep 2
		sqlite3 db_tpvs.db ".headers on" ".mode column" \
			"select ID, Loja, Sat, Caixa, IP, Mask, GW, DNS_1, DNS_2, Firmware,Layout, Status, Modelo_SAT from tb_sat where Sat = '$sat';"
		sleep 3
		echo
		(main)
		;;
	4)
		echo -e "\033[01;32mAterando o Status SAT.....\033[00;37m"
		sleep 2
		sqlite3 db_tpvs.db \
			"UPDATE tb_sat SET Status = 'Ativado'  WHERE Status is null;"
		sleep 3
		echo
		(main)
		;;
	5)
		echo -e "\033[01;32mAterando Modelos SATs = Null.....\033[00;37m"
		sleep 2
		sqlite3 db_tpvs.db \
			"UPDATE tb_sat SET Modelo_SAT = 'DSAT-DIMEP-2.0'  WHERE Firmware = '01.00.00';"
		sqlite3 db_tpvs.db \
			"UPDATE tb_sat SET Modelo_SAT = 'DSAT-DIMEP-1.0'  WHERE Firmware = '01.01.00';"
		sqlite3 db_tpvs.db \
			"UPDATE tb_sat SET Modelo_SAT = 'DSAT-DIMEP-1.0'  WHERE Firmware = '01.02.00';"
		sqlite3 db_tpvs.db \
			"UPDATE tb_sat SET Modelo_SAT = 'DSAT-DIMEP-1.0'  WHERE Firmware = '01.03.00';"
		sqlite3 db_tpvs.db \
			"UPDATE tb_sat SET Modelo_SAT = 'DSAT-DIMEP-1.0'  WHERE Firmware = '01.04.00';"
		sleep 3
		echo
		(main)
		;;
	9)
		echo -e "\033[01;32mSaindo....\033[00;37m" ; sleep 2 ;exit ;
		
		;;
	*)
		echo -e "\033[01;31mOpção inválida......\033[00;37m"
		echo
		sleep 1
		(main)
		;;
esac

}

main
