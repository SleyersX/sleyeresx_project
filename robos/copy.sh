#!/bin/bash -x

# returnToWarehouse_req
# returnReceipt_req
# salesReceipt_req
#
#

BD="/root/srv_remoto/srv_remoto.db"

function group_tiendas(){
	while read loja;
	do
		progs=("$loja")
	done < list_lojas.txt
	lojas=(${progs[0]})
}

function group_datas(){
	DAT=`echo "$(date +%Y%m%d -d "-1 days")"`
	echo $DAT > .arq_data.txt
	while read dt;
	do
		progss=("$dt")
	done < .arq_data.txt
	dts=(${progss[0]})
}
function valida_comunicacao(){
	sshpass -p root ssh -o ConnectTimeout=1 -p 10001 -l root $ip exit
}

function main(){
	
	arq_1="returnToWarehouse_req.xml"
	arq_2="returnReceipt_req.xml"
	arq_3="salesReceipt_req.xml"
	
	group_tiendas
	i=0

	while [ $i != ${#lojas[@]} ]
	do
		if [ "$x" == "0" ]; then
			echo "LOJA:"${lojas[$x]}"."
		else		
			shop=`printf "%05d" ${lojas[$i]}`
			exist=`echo "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$shop';" | sqlite3 $BD`
			if [ $exist -eq 1 ]; then
				ip=`echo "SELECT ip FROM tb_ip WHERE loja LIKE '$shop';" | sqlite3 $BD`
				group_datas
				k=0
				while [ $k != ${#dts[@]} ]
				do
					conn=(valida_comunicacao)
					if [ $? != 0 ]; then
						echo "Erro ao conectar com a loja->$shop" >> erro.log
					else
						touch .send_data
						chmod 777 .send_data
						echo "DATA_FILE=${dts[$k]}" > .send_data
						echo "LOJA=$shop" >> .send_data
						sshpass -p root scp -P10001 .send_data root@$ip:/root/
						if [ $? -eq 0 ]; then
							count=`sshpass -p root ssh $ip -p10001 -l root '. .send_data; ls -ltr /confdia/ws/ciss/output/${DATA_FILE}*{returnToWarehouse_req.xml,returnReceipt_req.xml,salesReceipt_req.xml}	| wc -l'`
							if [ $count -ge 1 ]; then
								sshpass -p root ssh $ip -p10001 -l root ' . .send_data; cd /confdia/ws/ciss/; mkdir support ; mv output/${DATA_FILE}*returnToWarehouse_req.xml support/ ; mv output/${DATA_FILE}*returnReceipt_req.xml support/; mv output/${DATA_FILE}*salesReceipt_req.xml support/ '
							fi 
						else
							echo "Erro ao enviar data->$shop" >> erro.log
						fi
					fi
					let "k = k +1"
				done
				count2=`sshpass -p root ssh $ip -p10001 -l root '. .send_data; ls -ltr /confdia/ws/ciss/support/$(date +%Y%m%d -d "-1 days")*{returnToWarehouse_req.xml,returnReceipt_req.xml,salesReceipt_req.xml}	| wc -l'`
				if [ $count2 -ge 1 ]; then
					sshpass -p root ssh $ip -p10001 -l root ' . .send_data; cd /confdia/ws/ciss/support/ ; tar -czvf ${LOJA}_$(date +%Y%m%d).tgz $(date +%Y%m%d -d "-1 days")*{returnToWarehouse_req.xml,returnReceipt_req.xml,salesReceipt_req.xml} ; mv ${LOJA}_$(date +%Y%m%d).tgz /root/ ; mkdir /pesados/bkp_xml_ciss/ ; mv $(date +%Y%m%d -d "-1 days")*returnToWarehouse_req.xml /pesados/bkp_xml_ciss/ ; mv $(date +%Y%m%d -d "-1 days")*returnReceipt_req.xml /pesados/bkp_xml_ciss/ ; mv $(date +%Y%m%d -d "-1 days")*salesReceipt_req.xml /pesados/bkp_xml_ciss/'
					file_exist=`sshpass -p root ssh $ip -p10001 -l root '. .send_data ; ls -ltr ${LOJA}_$(date +%Y%m%d).tgz | wc -l'`
					if [ $file_exist -ge 1 ]; then
						sshpass -p root scp -P10001 root@$ip:/root/${shop}_$(date +%Y%m%d).tgz .
						if [ $? != 0 ]; then
							echo "Erro ao copiar tgz->$shop" >> erro.log
						else
							sshpass -p root ssh $ip -p10001 -l root '. .send_data ; rm -vf ${LOJA}_$(date +%Y%m%d).tgz'
							if [ $? -eq 0 ]; then
								echo "TGZ deletado com sucesso->$shop" >> erro.log
							else
								echo "Erro ao deletar TGZ->$shop" >> erro.log
							fi
						fi
					else
						echo "Arquivo tgz não disponivel->$shop" >> erro.log
					fi
				else
					echo "Não existe arquivos->$shop" >> erro.log
				fi
			else
				echo "Loja nao encontrada no banco->$shop" >> erro.log
			fi

		fi
		let "i = i +1"
	done
}
main
