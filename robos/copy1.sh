#!/bin/bash -x

# returnToWarehouse_req
# returnReceipt_req
# salesReceipt_req
#
#

BD="/root/srv_remoto/srv_remoto.db"

function group_tiendas(){
	while read loja;
	do
		progs=("$loja")
	done < list_lojas.txt
	lojas=(${progs[0]})
}

function group_datas(){
	DAT=`echo "20190915 20190916 20190917"`
	echo $DAT > .arq_data.txt
	while read dt;
	do
		progss=("$dt")
	done < .arq_data.txt
	dts=(${progss[0]})
}
function valida_comunicacao(){
	sshpass -p root ssh -o ConnectTimeout=1 -p 10001 -l root $ip exit
}

function main(){
	
	arq_1="returnToWarehouse_req.xml"
	arq_2="returnReceipt_req.xml"
	arq_3="salesReceipt_req.xml"
	
	group_tiendas
	i=0

	while [ $i != ${#lojas[@]} ]
	do
		if [ "$x" == "0" ]; then
			echo "LOJA:"${lojas[$x]}"."
		else		
			shop=`printf "%05d" ${lojas[$i]}`
			exist=`echo "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$shop';" | sqlite3 $BD`
			if [ $exist -eq 1 ]; then
				ip=`echo "SELECT ip FROM tb_ip WHERE loja LIKE '$shop';" | sqlite3 $BD`
				group_datas
				k=0
				while [ $k != ${#dts[@]} ]
				do
					conn=(valida_comunicacao)
					if [ $? != 0 ]; then
						echo "Erro ao conectar com a loja->$shop" >> erro.log
					else
						touch .send_data
						chmod 777 .send_data
						echo "DATA_FILE=${dts[$k]}" > .send_data
						echo "LOJA=$shop" >> .send_data
						sshpass -p root scp -P10001 .send_data root@$ip:/root/
						if [ $? -eq 0 ]; then
							count=`sshpass -p root ssh $ip -p10001 -l root '. .send_data; ls -ltr /confdia/ws/ciss/output/${DATA_FILE}*{returnToWarehouse_req.xml,returnReceipt_req.xml,salesReceipt_req.xml}	| wc -l'`
							if [ $count -ge 1 ]; then
								sshpass -p root ssh $ip -p10001 -l root ' . .send_data; cd /confdia/ws/ciss/; if [ -e support ]; then echo "" ; else mkdir support ; fi ; cd output/ ; ARR_FILES=`ls ${DATA_FILE}*{returnToWarehouse_req.xml,returnReceipt_req.xml,salesReceipt_req.xml}` ; echo $ARR_FILES > /root/.arr_files.txt ; while read filee; do progs=("$filee"); done < .arr_files.txt ; files=(${progs[0]})  ; i=0; while [ $i != ${#files[@]} ]; do mv -vf ${files[$i]} ../output/ ; let "i = i +1" ; done'
							fi 
						else
							echo "Erro ao enviar data->$shop" >> erro.log
						fi
					fi
					let "k = k +1"
				done
					count2=`sshpass -p root ssh $ip -p10001 -l root '. .send_data; ls -ltr /confdia/ws/ciss/support/*{returnToWarehouse_req.xml,returnReceipt_req.xml,salesReceipt_req.xml}	| wc -l'`
					if [ $count2 -ge 1 ]; then
						sshpass -p root ssh $ip -p10001 -l root '. .send_data; cd /confdia/ws/ciss/support/ ; tar -czvf ${LOJA}_$(date +%Y%m%d).tgz *{returnToWarehouse_req.xml,returnReceipt_req.xml,salesReceipt_req.xml}'
						exist_tgz=`sshpass -p root ssh $ip -p10001 -l root '. .send_data; ls -ltr ${LOJA}_$(date +%Y%m%d).tgz | wc -l'`
						if [ $exist_tgz -ge 1 ]; then
							sshpass -p root scp -P10001 root@$ip:/root/${shop}_$(date +%Y%m%d).tgz .
							if [ $? != 0 ]; then
								echo "Erro ao copiar tgz->$shop" >> erro.log
							else
								sshpass -p root ssh $ip -p10001 -l root '. .send_data; rm -vf ${LOJA}_$(date +%Y%m%d).tgz'
								if [ $? != 0 ]; then
									echo "Erro ao remover tgz->$shop" >> erro.log
								else
									sshpass -p root ssh $ip -p10001 -l root ' . .send_data; cd /confdia/ws/ciss/output/; if [ -e /pesados/bkp_xml_ciss/ ]; then echo "" ; else mkdir /pesados/bkp_xml_ciss/ ; fi ; ARR_FILES=`ls *{returnToWarehouse_req.xml,returnReceipt_req.xml,salesReceipt_req.xml}` ; echo $ARR_FILES > /root/.arr_files.txt ; while read filee; do progs=("$filee"); done < .arr_files.txt ; files=(${progs[0]})  ; i=0; while [ $i != ${#files[@]} ]; do mv -vf ${files[$i]} /pesados/bkp_xml_ciss/; let "i = i +1" ; done'
								fi
							fi
						else
							echo "Arquivo tgz não existe->$shop" >> erro.log
						fi
					else
						echo "Não existe arquivos a serem copiados->$shop" >> erro.log
					fi 
			else
				echo "Loja nao encontrada no banco->$shop" >> erro.log
			fi

		fi
		let "i = i +1"
	done
}
main
