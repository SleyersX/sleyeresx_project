#!/bin/bash
ORI="/confdia/ctrdatos/"
CP="/root/bd/confdia/ctrdatos/"
IDENERRO="/confdia/bin/idenerro.err"

if [ -e $IDENERRO ]; then
        echo -e  "\033[01;32m$(date +%d/%m/%Y-%H:%M:%S).Removendo  -> $IDENERRO\033[00;37m"
        rm -vf /confdia/bin/idenerro.err
else
        echo -e "\033[01;32m$(date +%d/%m/%Y-%H:%M:%S).Arquivo '$IDENERRO' nao existe.\033[00;37m"
fi

while read linha;

do

#Copiar ficheros ruins

echo -e  "\033[01;32m$(date +%d/%m/%Y-%H:%M:%S).Copiando fichero $linha -> $ORI\033[00;37m"
cp -f $CP$linha.* $ORI
[ "$?" = "0" ] && echo -e  "\033[01;33mFichero copiado com sucesso.Retorno => $?\033[00;37m" || echo -e  "\033[01;31mErro ao copiar fichero.Retorno => $?\033[00;37m "
sleep 1

done < $1
