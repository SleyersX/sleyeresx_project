#!/bin/bash
#Script para criar arquivo de chaves formato linux
#Criado por wam001br

PORT=22
#USUARIO="walter"
#CLAVEPRIVADA="/confdia/bin/id_paf"
LOCAL="/home/pdv/Servidor_Chaves/Chaves/"
CHAVE="chave.txt"
CFE="CFeKey.txt"
LOG="/home/pdv/Servidor_Chaves/Log/.registros.log"
BANCO="/home/pdv/Servidor_Chaves/.login.db"

#Controle de acesso
#Função Principal

function login()
{
    echo -e "\033[01;34mEfetuar Login\033[00;37m"
    echo "$(date +%Y%m%d-%H%M%S.%s):Efetuar Login." >> $LOG
	read -p "Digite sua matricula:" matricula
	stty -echo
	read -p "Digite seu senha:    " senha
	stty echo
	echo
	
	[ "$matricula" ] || { echo -e "\033[01;31mErro: Matricula vazia. \033[00;37m" ; exit ; }
	[ "$senha" ] || { echo -e "\033[01;31mErro: Senha vazia.\033[00;37m" ; exit ; }
	
	cont=$( sqlite3 $BANCO \ "SELECT Contador FROM tb_login WHERE Matricula = $matricula ")
			if [ -f $cont ];then
                echo "$(date +%Y%m%d-%H%M%S.%s):Matricula nao encontrada na base de dados ('$matricula')." >> $LOG
                echo -e "\033[01;31mMatricula nao encontrada na base de dados.\033[00;37m"
			
			else
                nome=$( sqlite3 $BANCO \ "SELECT Nome FROM tb_login WHERE Matricula = $matricula ")
                sen=$( sqlite3 $BANCO \ "SELECT Senha FROM tb_login WHERE Matricula=$matricula " )
                    if [ $senha != $sen ];then
                        echo -e "\033[01;31mSenha invalida.\033[00;37m"
                        echo "$(date +%Y%m%d-%H%M%S.%s):Senha invalida('$nome' '$matricula')." >> $LOG
                        exit 1
                    else
                        echo "$(date +%Y%m%d-%H%M%S.%s):Senha OK('$nome' '$matricula')." >> $LOG
                        if [ $cont -eq 0 ];then
                        echo -e "\033[01;33mO sistema identificou que esse é seu primeiro acesso.\033[00;37m"
                        echo -e "\033[01;33mSera necessario cadastrar uma nova senha.\033[00;37m"
                        stty -echo
                        read -p "  Digite nova senha:" new_senha
                        stty echo
                        echo
                        stty -echo
                        read -p "Confirme nova senha:" conf_new_senha
                        stty echo
                        echo
                        [ "$new_senha" ] || { echo -e "\033[01;31mErro: Campos senhas deixados vazios.\033[00;37m" ; exit ; }
                        [ "$conf_new_senha" ] || { echo -e "\033[01;31mErro: Campos senhas deixados vazios.\033[00;37m" ; exit ; }
                        

                            if [ $new_senha == $sen ];then
                                echo -e "\033[01;33mSenha digitada deve ser diferente da senha padrao.\033[00;37m"
                                echo "$(date +%Y%m%d-%H%M%S.%s):Funcao troca senha('$nome' '$matricula')." >> $LOG
                                echo "$(date +%Y%m%d-%H%M%S.%s):Senha digitada deve ser diferente da senha padrao.('$nome' '$matricula')." >> $LOG
                                exit 1
                            fi
                            
                            if [ $new_senha != $conf_new_senha ];then
                                echo -e "\033[01;33mSenhas digitdas nao coincidem.\033[00;37m"
                                echo "$(date +%Y%m%d-%H%M%S.%s):Funcao troca senha('$nome' '$matricula')." >> $LOG
                                echo "$(date +%Y%m%d-%H%M%S.%s):Senhas digitads nao coincidem('$nome' '$matricula')." >> $LOG
                                exit 1
                            else
                                echo "$(date +%Y%m%d-%H%M%S.%s):Funcao troca senha('$nome' '$matricula')." >> $LOG
                                sqlite3 $BANCO \
                                        "UPDATE tb_login SET Contador = 1, Senha = '$new_senha' WHERE Matricula = $matricula"
                                        RET=$?
                                            if [ $RET != 0 ];then
                                                echo -e "\033[01;33mError ao gravar dados no banco de dados.\033[00;37m"
                                                echo "$(date +%Y%m%d-%H%M%S.%s):Erro ao gravar dados no banco de dados('$nome' '$matricula')." >> $LOG
                                                exit
                                            else
                                                echo "$(date +%Y%m%d-%H%M%S.%s):Senha alterda com sucesso('$nome' '$matricula')." >> $LOG
                                                (login)
                                            fi
                                            
                            fi
                        
                        else
                            #echo -e "\033[01;32m<----------- Acesso OK ------------->\033[00;37m"
                            echo "$(date +%Y%m%d-%H%M%S.%s):Acesso conedido('$nome' '$matricula')." >> $LOG
                            (main)                    
                        fi
                        
                    fi     
            
			fi

	
	
}

function main()
{
		echo -e "\033[01;33m-------------------------------------\033[00;37m"
		echo -e "\033[01;34mEscolha uma das opoes :\033[00;37m "
		echo -e "\033[01;33m-------------------------------------\033[00;37m"
		echo -e " \033[01;36m1. NFCE		      "
		echo -e " 2. SAT		      "
		echo -e " 3. CONSULTAR	      "
		echo -e " 9. SAIR		      \033[00;37m"
		echo -e "\033[01;33m-------------------------------------\033[00;37m"
		echo -n " 	     Digite a opcao:  "	
		read opcao
		case $opcao in
			1) CriarArqNFCE ;;
			2) CriarArqSAT ;;
			3) ConsultarChave ;;
			9) exit ;;
			*) echo "Opcao desconhecia" ; main ; exit ;;
		esac
	#fi

}


#Informativo para o usuário
function dados_nfce()
{
	#echo "----------------------------------------------------"
echo -e "\033[01;32mPreencha os campos para gerar o arquivo .zip\033[00;37m "
	#echo "----------------------------------------------------"
#Pedimos para que digite as opções necessarias para gerar a loja
read -p "Digite o numero da loja:     " loja
read -p "Insira CFeKey          :     " cfekey
read -p "Insira TokenSefaz      :     " tokensefaz
read -p "Insira IDTokenSefaz    :     " idtoken

#Verificamos se temos campos nulos ou vazios
[ "$loja" ] || { echo -e "\033[01;31mERRO: Loja inválida ou vazia!!!\033[00;37m"; exit ; }
[ "$cfekey" ] || { echo -e "\033[01;31mERRO: CFeKey inválido ou vazio!!!\033[00;37m"; exit ; }
[ "$tokensefaz" ] || { echo -e "\033[01;31mERRO: TokenSefaz inválido ou vazio!!!\033[00;37m"; exit ; }
[ "$idtoken" ] || { echo -e "\033[01;31mERRO: IDTokenSefaz inválido ou vazio!!!\033[00;37m"; exit ; }
	#echo "------------------------------------------------------"
	echo -e "------------Processo OK---------------"
	#echo "------------------------------------------------------"
	echo "$(date +%Y%m%d-%H%M%S.%s):Gerando chave NFCe." >> $LOG
	echo "$(date +%Y%m%d-%H%M%S.%s):Usuário conectado('$nome' '$matricula')." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):Dados gravados('$loja' '$cfekey' '$tokensefaz' '00000$idtoken')." >> $LOG	
}
function dados_sat()
{
echo -e "\033[01;32mPreencha os campos para gerar o arquivo .zip\033[00;37m "

#Pedimos para que digite as opções necessarias para gerar a loja
read -p "Digite o numero da loja:     " loja_sat
read -p "Insira CFeKey          :     " cfekey_sat
read -p "Insira AC              :     " ac

#Verificamos se temos campos nulos ou vazios
[ "$loja_sat" ] || { echo -e "\033[01;31mERRO: Loja inválida ou vazia!!!\033[00;37m"; exit ; }
[ "$cfekey_sat" ] || { echo -e "\033[01;31mERRO: CFeKey inválido ou vazio!!!\033[00;37m"; exit ; }
[ "$ac" ] || { echo -e "\033[01;31mERRO: AC inválido ou vazio!!!\033[00;37m"; exit ; }
	echo -e "------------Processo OK---------------"
	echo "$(date +%Y%m%d-%H%M%S.%s):Gerando chave NFCe." >> $LOG
	echo "$(date +%Y%m%d-%H%M%S.%s):Usuário conectado('$nome' '$matricula')." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):Dados gravados('$loja_sat' '$cfekey_sat' '$ac')." >> $LOG	

}  

#Verificamos se existe o diretorio
function ver_existe_dir()
	{
		if [ ! -d "$LOCAL/D$loja" ]; then
                        DIR_EXIS=0
		fi
	}
function ver_existe_dir_sat()
	{
		if [ ! -d "$LOCAL/D$loja_sat" ]; then
                        DIR_EXIS=0
		fi
	}

#Verificamos se existe o arquivo os arquivos
function ver_existe_txt()
	{
		if [ -e "$LOCAL/D$loja/$CFE" ]; then
			CFE_EXIS="1"
		else
			CFE_EXIS="0"
		fi
		if [ -e "$LOCAL/D$loja/$CHAVE" ]; then
			CHAV_EXIS="1"
		else
			CHAV_EXIS="0"
		fi
	}

function ver_existe_txt_sat()
	{
		if [ -e "$LOCAL/D$loja_sat/$CFE" ]; then
			CFE_EXIS="1"
		else
			CFE_EXIS="0"
		fi
		if [ -e "$LOCAL/D$loja_sat/$CHAVE" ]; then
			CHAV_EXIS="1"
		else
			CHAV_EXIS="0"
		fi
	}

	
#Criamos os arquivo necessarios
function criar_diretorio()
	{
		ver_existe_dir
		if [ "$DIR_EXIS" == "0" ]; then
			echo -e "\033[01;33mCriando o diretorio D$loja ...\033[00;37m"
			sleep 2
			mkdir $LOCAL/D$loja
			echo "$(date +%Y%m%d-%H%M%S.%s):Função criar diretório('$nome' '$matricula')." >> $LOG
			echo "$(date +%Y%m%d-%H%M%S.%s):Diretório criado.('$nome' '$matricula')." >> $LOG
			sleep 1
		#elif [ "$DIR_EXIS" == "0" ]; then
		else
			echo -e "\033[01;33mDiretorio ja existe...\033[00;37m"
			echo "$(date +%Y%m%d-%H%M%S.%s):Função criar diretório('$nome' '$matricula')." >> $LOG
			echo "$(date +%Y%m%d-%H%M%S.%s):Diretório já existe.('$nome' '$matricula')." >> $LOG
			sleep 2
		fi
	}
function criar_diretorio_sat()
	{
		ver_existe_dir_sat
		if [ "$DIR_EXIS" == "0" ]; then
			echo -e "\033[01;33mCriando o diretorio D$loja_sat ...\033[00;37m"
			sleep 2
			mkdir $LOCAL/D$loja_sat
			echo "$(date +%Y%m%d-%H%M%S.%s):Função criar diretório('$nome' '$matricula')." >> $LOG
			echo "$(date +%Y%m%d-%H%M%S.%s):Diretório criado.('$nome''$matricula')." >> $LOG
			sleep 1
		#elif [ "$DIR_EXIS" == "0" ]; then
		else
			echo -e "\033[01;33mDiretorio ja existe...\033[00;37m"
			echo "$(date +%Y%m%d-%H%M%S.%s):Função criar diretório('$nome' '$matricula')." >> $LOG
			echo "$(date +%Y%m%d-%H%M%S.%s):Diretóio já existe.('$nome' '$matricula')." >> $LOG
			sleep 2
		fi
	}
	
function criar_arquivos_sat()
	{
		ver_existe_txt_sat
		#Criar arquivo Token
		if [ "$CHAV_EXIS" == "0" ] ; then
			echo -e "\033[01;33mCriando AC\033[00;37m"
			echo "$ac"
			echo $ac >> $LOCAL/D$loja_sat/$CHAVE
			echo "$(date +%Y%m%d-%H%M%S.%s):Função criar chave AC-SAT('$nome' '$matricula')." >> $LOG
			echo "$(date +%Y%m%d-%H%M%S.%s):AC criado.('$nome' '$matricula')." >> $LOG

			sleep 2
		else
			arq_st=$(cat $LOCAL/D$loja_sat/$CHAVE)
						if [ $arq_st == $ac ]; then
							echo -n "Arquivo ja existe, deseja sobrescrever ? [s/n]"
							read resp_st
							
						if [ $resp_st == "s" ]; then
							echo -e "\033[01;31mLembre se que deve criar um novo arquivo ZIP.\033[00;37m"
							echo "$ac"
							echo "$(date +%Y%m%d-%H%M%S.%s):Função atualizar AC-SAT('$nome' '$matricula')." >> $LOG
							echo $ac > $LOCAL/D$loja_sat/$CHAVE
							echo "$(date +%Y%m%d-%H%M%S.%s):AC-SAT atualizado('$nome' '$matricula' '$arq_st' '$ac')." >> $LOG
							else
						echo -e "\033[01;36mNao sera feita nenhuma alteracao.\033[00;37m"
						fi
						elif [ $arq_st != $ac ]; then
							echo -e "\033[01;31mLembre se que deve criar um novo arquivo ZIP.\033[00;37m"
							echo -n "AC diferente, deseja subistituir ? [s/n]"
							read respo_st
						if [ $respo_st == "s" ]; then
							echo "$ac"
							echo "$(date +%Y%m%d-%H%M%S.%s):Função atualizar AC-SAT('$nome' '$matricula')." >> $LOG
							echo $ac > $LOCAL/D$loja_sat/$CHAVE
							echo "$(date +%Y%m%d-%H%M%S.%s):AC-SAT atualizado('$nome' '$matricula' '$arq_st' '$ac')." >> $LOG
							else
							echo -e "\033[01;36mNao sera feita nenhuma alteracao.\033[00;37m"
						fi	
						fi
			sleep 2
		fi
		
		#Criar arquivo CfeKey
		if [ "$CFE_EXIS" == "0" ] ; then
                        echo -e "\033[01;33mCriando CFeKey\033[00;37m"
                        echo -e "\033[00;35m$cfekey_sat\033[00;37m"
                        echo $cfekey_sat >> $LOCAL/D$loja_sat/$CFE
			echo "$(date +%Y%m%d-%H%M%S.%s):Função criar chave Invoicy/Migrate('$nome' '$matricula')." >> $LOG
			echo "$(date +%Y%m%d-%H%M%S.%s):Chave Invoicy/Migrate criada.('$nome' '$matricula')." >> $LOG

                        sleep 2
                else
                        arq_sat=$(cat $LOCAL/D$loja_sat/$CFE)
						if [ $arq_sat == $cfekey_sat ]; then
							echo -e "Arquivo ja existe, deseja sobrescrever ? [s/n]"
							read resp_sat
						if [ $resp_sat == "s" ]; then
							echo -e "\033[01;31mLembre se que deve criar um novo arquivo ZIP.\033[00;37m"
							echo "$cfekey_sat"
							echo "$(date +%Y%m%d-%H%M%S.%s):Função atualizar CFeKey('$nome' '$matricula')." >> $LOG
							echo $cfekey_sat > $LOCAL/D$loja_sat/$CFE
							echo "$(date +%Y%m%d-%H%M%S.%s):AC-SAT atualizado('$nome' '$matricula' '$arq_sat' '$cfekey_sat')." >> $LOG
							else
						echo -e "\033[01;36mNao sera feita nenhuma alteracao.\033[00;37m"
						fi
						elif [ $arq_sat != $cfekey_sat ]; then
							echo -e "CFeKey diferente, deseja subistituir ? [s/n]"
							read respo_sat
						if [ $respo_sat == "s" ]; then
							echo -e "\033[01;31mLembre se que deve criar um novo arquivo ZIP.\033[00;37m"
							echo "$cfekey_sat"
							echo "$(date +%Y%m%d-%H%M%S.%s):Função atualizar CFeKey('$nome' '$matricula')." >> $LOG
							echo $cfekey_sat > $LOCAL/D$loja_sat/$CFE
							echo "$(date +%Y%m%d-%H%M%S.%s):AC-SAT atualizado('$nome' '$matricula' '$arq_sat' '$cfekey_sat')." >> $LOG							
							else
							echo "Nao sera feita nenhuma alteracao."
						fi	
						fi
                        sleep 2
                fi


	}	


function criar_arquivos()
	{
		ver_existe_txt
		#Criar arquivo Token
		if [ "$CHAV_EXIS" == "0" ] ; then
			echo -e "\033[01;33mCriando TokenSefaz\033[00;37m"
			echo -e "\033[01;35m$tokensefaz\033[00;37m"
			echo $tokensefaz > $LOCAL/D$loja/$CHAVE
			echo -e "\033[01;33mCriando IDTokenSefaz\033[00;37m"
			echo -e "\033[01;35m00000$idtoken\033[00;37m"
			echo "00000$idtoken" >> $LOCAL/D$loja/$CHAVE
			echo "$(date +%Y%m%d-%H%M%S.%s):Função criar Token e ID Token('$nome' '$matricula')." >> $LOG
			echo "$(date +%Y%m%d-%H%M%S.%s):Token e ID Token criados('$nome' '$matricula')." >> $LOG			
			sleep 2
		else
			echo "Arquivo ja existe"
			sleep 2
		fi
		
		#Criar arquivo CfeKey
		if [ "$CFE_EXIS" == "0" ] ; then
                        echo -e "\033[01;33mCriando CFeKey\033[00;37m"
                        echo -e "\033[01;35m$cfekey\033[00;37m"
                        echo $cfekey >> $LOCAL/D$loja/$CFE
			echo "$(date +%Y%m%d-%H%M%S.%s):Função criar chave Invoicy/Migrate('$nome' '$matricula')." >> $LOG
			echo "$(date +%Y%m%d-%H%M%S.%s):Chave Invoicy/Migrate criada('$nome' '$matricula')." >> $LOG
                     	sleep 2
                else
                        arq=$(cat $LOCAL/D$loja/$CFE)
						if [ $arq == $cfekey ]; then
							echo -e "Arquivo ja existe, deseja sobrescrever ? [s/n]"
							read resp
						if [ $resp == "s" ]; then
							echo -e "\033[01;31mLembre se que deve criar um novo arquivo ZIP.\033[00;37m"
							echo "$cfekey"
							echo "$(date +%Y%m%d-%H%M%S.%s):Função atualizar CFeKey('$nome' '$matricula')." >> $LOG
							echo "$cfekey" > $LOCAL/D$loja/$CFE
							echo "$(date +%Y%m%d-%H%M%S.%s):Chave CFeKey atualizada('$nome' '$matricula' '$arq' '$cfekey')." >> $LOG
							else
						echo "Nao sera feita nenhuma alteracao."
						fi
						elif [ $arq != $cfekey ]; then
							echo -e "CFeKey diferente, deseja subistituir ? [s/n]"
							read respo
						if [ $respo == "s" ]; then
							echo -e "\033[01;31mLembre se que deve criar um novo arquivo ZIP.\033[00;37m"
							echo "$cfekey"
							echo "$(date +%Y%m%d-%H%M%S.%s):Função atualizar CFeKey('$nome' '$matricula')." >> $LOG
							echo "$cfekey" > $LOCAL/D$loja/$CFE
							echo "$(date +%Y%m%d-%H%M%S.%s):Chave CFeKey atualizada('$nome' '$matricula' '$arq' '$cfekey')." >> $LOG
							else
							echo "Nao sera feita nenhuma alteracao."
						fi	
						fi
                        sleep 2
                fi


	}

#Enviamos arquivos para o servidor
function enviar_arq_srv()
	{
		ORIGEM="$LOCAL/D$loja/D$loja.zip"
		usuario="pafstore"
		senha="pafstore"
		ip="pafbackup"
		porta="22"
		unidade="D//CLAVES_NFCE"
		#Verificamos se os campos digitados estão vazios
		echo -e "\033[01;35mEnviando arquivo $ORIGEM ....\033[00;37m"
		echo "$(date +%Y%m%d-%H%M%S.%s):Local arquivos('$ORIGEM')." >> $LOG			
		echo -e "\033[01;35mConectando e enviando a maquina PAFBACKUP\033[00;37m"
		echo "$(date +%Y%m%d-%H%M%S.%s):Enviando arquivos." >> $LOG
		sleep 1
		enviar=$(sshpass -p $senha scp -P $porta $ORIGEM $usuario@$ip://$unidade//)
		ret=$?
		if [ $ret != "0" ]; then

			echo "$(date +%Y%m%d-%H%M%S.%s):Retorno('$ret')." >> $LOG
			sleep 2
		else
		echo -e "\033[01;32mArquivo zip Loja $loja enviado com sucesso !!!\033[00;37m"
		echo "$(date +%Y%m%d-%H%M%S.%s):Retorno('$ret')." >> $LOG
		sleep 2
	fi		
	}

function enviar_arq_srv_sat()
	{
		ORIGEM="$LOCAL/D$loja_sat/D$loja_sat.zip"
		usuario="pafstore"
		senha="pafstore"
		ip="pafbackup"
		porta="22"
		unidade="D//CLAVES_SAT"
		echo -e "\033[01;35mEnviando arquivo $ORIGEM ....\033[00;37m"
		echo "$(date +%Y%m%d-%H%M%S.%s):Local arquivos('$ORIGEM')." >> $LOG
		echo -e "\033[01;35mConectando e enviando a maquina PAFBACKUP\033[00;37m"
		echo "$(date +%Y%m%d-%H%M%S.%s):Enviando arquivos." >> $LOG
		sleep 1
		enviar=$(sshpass -p $senha scp  -P $porta $ORIGEM $usuario@$ip://$unidade//)
		ret=$?
		if [ $ret != "0" ]; then
			echo "$(date +%Y%m%d-%H%M%S.%s):Retorno('$ret')." >> $LOG
			sleep 2
		else
			echo -e "\033[01;32mArquivo zip Loja $loja_sat enviado com sucesso !!!\033[00;37m"
			echo "$(date +%Y%m%d-%H%M%S.%s):Retorno('$ret')." >> $LOG		
		sleep 2
	fi
	}
#Criamos um arquivo zip

function criar_zip()
	{
		ZIP="$LOCAL/D$loja/D$loja.zip"
		if [ -e "$ZIP" ]; then
			echo -e "\033[01;36mArquivo zip para loja D$loja ja existe...\033[00;37m"
			echo -n "Deseja criar um novo arquivo ? [ s/n ]"
			read decisao
			if [ $decisao == "s" ];then
			rm -f $ZIP
			cd $LOCAL/D$loja/
			echo -e "\033[01;36mCriando arquivo zip para loja D$loja ...\033[00;37m"
			echo "$(date +%Y%m%d-%H%M%S.%s):Função criar arquivo ZIP." >> $LOG
			zip D$loja.zip *.txt
			echo "$(date +%Y%m%d-%H%M%S.%s):ZIP criado('D$loja.zip')." >> $LOG
			else
			echo -e "\033[01;35mNao sera feita nehuma alteracao.\033[00;37m"
			fi
			sleep 1
		else
			cd $LOCAL/D$loja/
			echo -e "\033[01;36mCriando arquivo zip para loja D$loja ...\033[00;37m"
			echo "$(date +%Y%m%d-%H%M%S.%s):Função criar arquivo ZIP." >> $LOG			
			zip D$loja.zip *.txt
			echo "$(date +%Y%m%d-%H%M%S.%s):ZIP criado('D$loja.zip')." >> $LOG
			sleep 2
		fi
	}
function criar_zip_sat()
	{
		ZIP="$LOCAL/D$loja_sat/D$loja_sat.zip"
		if [ -e "$ZIP" ]; then
			echo -e "\033[01;36mArquivo zip para loja D$loja_sat ja existe...\033[00;37m"
			echo -n "Deseja criar um novo arquivo ? [ s/n ]"
			read dec_sat
			if [ $dec_sat == "s" ]; then
				rm -f $ZIP
				cd $LOCAL/D$loja_sat/
				echo -e "\033[01;36mCriando arquivo zip para a loja D$loja_sat...\033[00;37m"
				echo "$(date +%Y%m%d-%H%M%S.%s):Função criar arquivo ZIP." >> $LOG
				zip D$loja_sat.zip *.txt
				echo "$(date +%Y%m%d-%H%M%S.%s):ZIP criado('D$loja_sat.zip')." >> $LOG
			else
				echo -e "\033[01;35mNao sera feita nenhuma alteracao\033[00;37m"
			fi
			sleep 1
		else
			cd $LOCAL/D$loja_sat/
			echo "$(date +%Y%m%d-%H%M%S.%s):Função criar arquivo ZIP." >> $LOG
			echo -e "\033[01;36mCriando arquivo zip para loja D$loja_sat ...\033[00;37m"
			zip D$loja_sat.zip *.txt
			echo "$(date +%Y%m%d-%H%M%S.%s):ZIP criado('D$loja_sat.zip')." >> $LOG
			sleep 2
		fi
	}

function CriarArqNFCE()
	{
		echo "$(date +%Y%m%d-%H%M%S.%s):Função criar chaves NFCe.('$nome' '$matricula')." >> $LOG
		dados_nfce	
		criar_diretorio
		criar_arquivos
		criar_zip
		enviar_arq_srv
		
	}
function CriarArqSAT()
	{
		echo "$(date +%Y%m%d-%H%M%S.%s):Função criar chaves SAT.('$nome' '$matricula')." >> $LOG
		dados_sat
		criar_diretorio_sat
		criar_arquivos_sat
		criar_zip_sat
		enviar_arq_srv_sat
		
		
	}
function GerarPermisao()
{
	
#Opçao 2=SAT
#Opção 1=NFCe
if [ $opcao == "2" ];then
	#Loja SAT criar permisao arquivos
	chmod -R 777 $LOCAL/*
        chmod -R 777 $LOCAL/D$loja_sat/*
	chmod -R 777 $LOCAL/D$loja_sat/*.txt
	chmod -R 777 $LOCAL/D$loja_sat/*.zip
elif [ $opcao == "1" ]; then
	#Loja NFCe criar permisao arquivos
	chmod -R 777 $LOCAL/*
        chmod -R 777 $LOCAL/D$loja/*
	chmod -R 777 $LOCAL/D$loja/*.txt
	chmod -R 777 $LOCAL/D$loja/*.zip

fi

}

function ConsultarChave()
	{
		psw="pafstore"
		usr="pafstore"
		srv="pafbackup"
		pt=22
		read -p " Digite o numero da loja : " busca_loja
		echo "$(date +%Y%m%d-%H%M%S.%s):Função consulta de lojas('$busca_loja')." >> $LOG
		busca=$(sshpass -p $psw scp  -P $pt $usr@$srv://D//CLAVES_SAT//D$busca_loja.zip . )
		ret=$?
			if [ $ret != 0 ]; then
				echo -e "\033[01;33mLoja nao econtrada no servidor de chaves SAT.\033[00;37m"
				echo "$(date +%Y%m%d-%H%M%S.%s):Loja não encontrada no servidor de chaves SAT." >> $LOG
				sleep 1
				busca_I=$(sshpass -p $psw scp -P $pt $usr@$srv://D//CLAVES_NFCE//D$busca_loja.zip . )
				ret_II=$?
					if [ $ret_II != 0 ]; then
						echo -e "\033[01;33mLoja nao encontrada no servidor de chave NFCe.\033[00;37m"
						echo "$(date +%Y%m%d-%H%M%S.%s):Loja não encontrada no servidor de chaves NFCe." >> $LOG
						sleep 1
						(main)
					else
						echo -e "\033[01;32mLoja encontrada no servidor de chaves NFCe.\033[00;37m"
						echo "$(date +%Y%m%d-%H%M%S.%s):Loja encontrada no servidor de chaves NFCe." >> $LOG
						unzip D$busca_loja.zip
						exibir_cfe=$(cat $CFE)
						echo -e "\033[01;31mCFeKey Encontrada =\033[00;37m $exibir_cfe"
						sleep 3
						exibir_token=$(cat $CHAVE )
						echo -e "\033[01;31mIDToken e Token Encontrado =\033[00;37m $exibir_token"
						echo "$(date +%Y%m%d-%H%M%S.%s):Chaves encontradas('$busca_loja' '$exibir_cfe' '$exibir_token')." >> $LOG
						sleep 3
						
						read -p "Deseja comparar as chaves obtidas ? [s/n] " escolha
						echo "$(date +%Y%m%d-%H%M%S.%s):Função comparar chaves obtidas." >> $LOG
						if [ $escolha != "n" ]; then
							read -p "Insira a Chave Invoicy/Migrate : " cfekey
							[ "$cfekey" ] || { echo -e "\033[01;31mERRO: CFeKey inválida ou vazia!!!\033[00;37m"; rm -f *.txt ; rm -f *.zip ;exit ; }
							echo "$(date +%Y%m%d-%H%M%S.%s):Comparando chaves('$busca_loja' '$exibir_cfe' '$cfekey')." >> $LOG
							if [ $exibir_cfe != $cfekey ]; then
									echo -e "\033[01;33mChave Invoicy/Migrate sao diferentes.\033[00;37m"
									echo "$(date +%Y%m%d-%H%M%S.%s):Chave Invoicy/Migrate são diferentes." >> $LOG
									echo -e "\033[01;33mVolte ao menu inicial e gere um novo arquivo de chaves para envio ao servidor.\033[00;37m"
									echo
									sleep 3
								else
									echo -e "\033[01;32mChave Invoicy/Migrate sao identicas.\033[00;37m"
									echo "$(date +%Y%m%d-%H%M%S.%s):Chave Invoicy/Migrate são identicas." >> $LOG
								fi
								
						else
							echo -e "\033[01;35mNao sera feita nenhuma alteracao.\033[00;37m"
						fi
										
						rm -f D$busca_loja.zip
						rm -f *.txt
						sleep 1
						echo -n
						(main)
					fi
				else
					echo -e "\033[01;32mLoja econtrada no servidor de chaves SAT.\033[00;37m"
					echo "$(date +%Y%m%d-%H%M%S.%s):Loja encontrada no servidor de chaves SAT." >> $LOG
					unzip D$busca_loja.zip
					exibir_cfe=$(cat $CFE)
					echo -e "\033[01;31mCFeKey Encontrada =\033[00;37m $exibir_cfe"
					sleep 3
					exibir_ac=$(cat $CHAVE)
					echo -e "\033[01;31mAC Encontrado =\033[00;37m $exibir_ac"
					sleep 3
					echo "$(date +%Y%m%d-%H%M%S.%s):Chaves encontradas('$busca_loja' '$exibir_cfe' '$exibir_ac')." >> $LOG
					read -p "Deseja comparar as chaves obtidas ? [s/n]" escolha
					if [ $escolha != "n" ]; then
						read -p "Digite Chave Invoicy/Migrate : " cfekey
						read -p "Digite Chave AC : " chave_ac
						[ "$cfekey" ] || { echo -e "\033[01;31mERRO: CFeKey inválida ou vazia!!!\033[00;37m"; rm -f *.txt ; rm -f *.zip ;exit ; }
						[ "$chave_ac" ] || { echo -e "\033[01;31mERRO: AC inválida ou vazia!!!\033[00;37m"; rm -f *.zip ; rm -f *.txt ;exit ; }
						echo "$(date +%Y%m%d-%H%M%S.%s):Comparando chaves('$busca_loja' '$exibir_cfe' '$cfekey' '$exibir_ac' '$chave_ac')." >> $LOG
							if [ $exibir_cfe != $cfekey ]; then
								echo -e "\033[01;33mChave Invoicy/Migrate sao diferentes.\033[00;37m"
								echo "$(date +%Y%m%d-%H%M%S.%s):Chave Invoicy/Migrate são diferentes." >> $LOG
								echo -e "\033[01;33mVolte ao menu inicial e gere um novo arquivo de chaves para envio ao servidor.\033[00;37m"
								echo
								sleep 3 
							else
								echo -e "\033[01;32mChaves Invoicy/Migrate sao identicas\033[00;37m"
								echo "$(date +%Y%m%d-%H%M%S.%s):Chave Invoicy/Migrate são identicas." >> $LOG
								sleep 3
							fi
							if [ $exibir_ac != $chave_ac ]; then
								echo -e "\033[01;33mChave AC SAT sao diferentes.\033[00;37m"
								echo "$(date +%Y%m%d-%H%M%S.%s):Chave AC-SAT são diferentes." >> $LOG
								echo -e "\033[01;33mVolte ao menu inicial e gere um novo arquivo de chaves para envio ao servidor.\033[00;37m"
								echo
								sleep 3
							else
								echo -e "\033[01;32mChaves AC SAT sao identicas.\033[00;37m"
								echo "$(date +%Y%m%d-%H%M%S.%s):Chave AC-SAT são diferentes." >> $LOG
								sleep 3
							fi
					else
							echo -e "\033[01;35mNao sera feita nenhuma validacao.\033[00;37m"
							sleep 3
					fi
					rm -f D$busca_loja.zip
					rm -f *.txt
					ESPEBAT="ESPEBAT.BAT"
					if [ -e $ESPEBAT ]; then
						rm -f $ESPEBAT
					fi
					sleep 1
					echo -n
					(main)
				fi
}
login
