#!/bin/bash

LISTADO="/root/srv_remoto/arquivos_read/listado_tiendas.csv"
ARQ_SAIDA="/root/srv_remoto/arquivos_read/lojas.txt"
SQL_FID="/root/srv_remoto/arquivos_sql/arquivo_fid.SQL"
SQL_OFER="/root/srv_remoto/arquivos_sql/arquivo_ofer.SQL"
BD="/root/srv_remoto/srv_remoto.db"
DATA=$(date +%Y%m%d)
DATA1=$(date +%Y%m%d --date="+1 day")
rm -vf $ARQ_SAIDA
rm -vf $SQL_FID
while read tienda ip;
	do
		printf "%05d\n" $tienda >> $ARQ_SAIDA
done < $LISTADO
sleep 2
while read LOJA;
	do
		EXIST=$(echo "SELECT COUNT(id) FROM tb_imp_fidelizacao WHERE loja LIKE '$LOJA';" | sqlite3 $BD)
		if [ $EXIST -eq 0 ]; then
			#echo "INSERT INTO tb_imp_fidelizacao (loja, data) VALUES ('$LOJA', '$DATA');" >> $SQL_FID
			mysql --connect-timeout=5 -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_imp_fidelizacao (loja, data) VALUES ('$LOJA', '$DATA')"
		fi
		EXIST1=$(echo "SELECT COUNT(id) FROM tb_install_oferta WHERE tienda LIKE '$LOJA';" | sqlite3 $BD)
		if [ $EXIST1 -eq 0 ]; then
			#echo "INSERT INTO tb_install_oferta (tienda, n_tpvs, data_instalacao, data_envio) VALUES ('$LOJA',0,'$DATA1','$DATA');" >> $SQL_OFER
			mysql --connect-timeout=5 -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_install_oferta (tienda, n_tpvs, data_instalacao, data_envio) VALUES ('$LOJA',0,'$DATA1','$DATA')"
		fi
		
done < $ARQ_SAIDA
#sleep 3
#if [ -e $SQL_FID ]; then
#	sqlite3 $BD < $SQL_FID
#fi
#if [ -e $SQL_OFER ]; then
#	sqlite3 $BD < $SQL_OFER
#fi
