#!/bin/bash
#
#Autor      : Walter Moura(wam001br)
#Data       : 2018-12-28
#Modificado : 2018-12-28
#
#

function valida_comunicacao(){

	ssh -o ConnectTimeout=1 $IP$I exit

}
function main(){

	LOCAL="/root/srv_remoto/log/"
	LOG="error.getip.log"
	BD="/root/srv_remoto/srv_remoto.db"
	BD1="/home/pdv/.baseDeDados.db"
	IP0="10.105.188."
	IP1="10.105.189."
	IP2="10.105.194."
	IP3="10.105.190."
	IP4="10.106.235."
	IP5="10.106.101."
	IP6="10.106.102."
	IP7="10.106.103."
	IP8="10.106.104."
	IP9="10.106.112."
	
	I=0
	while (( I < 254 ))
	do
		IP=$IP0
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOCAL$LOG
		else
			LOJA=$(for A in $I ; do ssh -o ConnectTimeout=1 $IP$A ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
			NLOJA=$(echo "L$(for Z in $I ; do ssh -o ConnectTimeout=1 $IP$Z ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)")
			TPVS=$(for X in $I ; do ssh -o ConnectTimeout=1 $IP$X ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$LOJA, PDVs:$TPVS, IP:$IP$I ." >> $LOCAL$LOG
			EXISTE=$(sqlite3 $BD \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE" >> $LOCAL$LOG
				sqlite3 $BD \
					"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD \
					"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE1=$(sqlite3 $BD1 \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE1 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE1" >> $LOCAL$LOG
				sqlite3 $BD1 \
						"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD1 \
						"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE2=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA'"`
			if [ $EXISTE2 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE2" >> $LOCAL$LOG
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA'"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA')"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP1
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOCAL$LOG
		else
			LOJA=$(for A in $I ; do ssh -o ConnectTimeout=1 $IP$A ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
			NLOJA=$(echo "L$(for Z in $I ; do ssh -o ConnectTimeout=1 $IP$Z ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)")
			TPVS=$(for X in $I ; do ssh -o ConnectTimeout=1 $IP$X ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$LOJA, PDVs:$TPVS, IP:$IP$I ." >> $LOCAL$LOG
			EXISTE=$(sqlite3 $BD \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE" >> $LOCAL$LOG
				sqlite3 $BD \
					"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD \
					"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE1=$(sqlite3 $BD1 \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE1 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE1" >> $LOCAL$LOG
				sqlite3 $BD1 \
						"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD1 \
						"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE2=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA'"`
			if [ $EXISTE2 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE2" >> $LOCAL$LOG
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA'"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA')"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP2
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOCAL$LOG
		else
			LOJA=$(for A in $I ; do ssh -o ConnectTimeout=1 $IP$A ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
			NLOJA=$(echo "L$(for Z in $I ; do ssh -o ConnectTimeout=1 $IP$Z ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)")
			TPVS=$(for X in $I ; do ssh -o ConnectTimeout=1 $IP$X ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$LOJA, PDVs:$TPVS, IP:$IP$I ." >> $LOCAL$LOG
			EXISTE=$(sqlite3 $BD \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE" >> $LOCAL$LOG
				sqlite3 $BD \
					"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD \
					"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE1=$(sqlite3 $BD1 \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE1 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE1" >> $LOCAL$LOG
				sqlite3 $BD1 \
						"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD1 \
						"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE2=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA'"`
			if [ $EXISTE2 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE2" >> $LOCAL$LOG
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA'"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA')"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP3
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOCAL$LOG
		else
			LOJA=$(for A in $I ; do ssh -o ConnectTimeout=1 $IP$A ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
			NLOJA=$(echo "L$(for Z in $I ; do ssh -o ConnectTimeout=1 $IP$Z ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)")
			TPVS=$(for X in $I ; do ssh -o ConnectTimeout=1 $IP$X ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$LOJA, PDVs:$TPVS, IP:$IP$I ." >> $LOCAL$LOG
			EXISTE=$(sqlite3 $BD \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE" >> $LOCAL$LOG
				sqlite3 $BD \
					"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD \
					"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE1=$(sqlite3 $BD1 \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE1 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE1" >> $LOCAL$LOG
				sqlite3 $BD1 \
						"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD1 \
						"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE2=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA'"`
			if [ $EXISTE2 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE2" >> $LOCAL$LOG
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA'"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA')"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP4
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOCAL$LOG
		else
			LOJA=$(for A in $I ; do ssh -o ConnectTimeout=1 $IP$A ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
			NLOJA=$(echo "L$(for Z in $I ; do ssh -o ConnectTimeout=1 $IP$Z ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)")
			TPVS=$(for X in $I ; do ssh -o ConnectTimeout=1 $IP$X ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$LOJA, PDVs:$TPVS, IP:$IP$I ." >> $LOCAL$LOG
			EXISTE=$(sqlite3 $BD \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE" >> $LOCAL$LOG
				sqlite3 $BD \
					"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD \
					"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE1=$(sqlite3 $BD1 \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE1 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE1" >> $LOCAL$LOG
				sqlite3 $BD1 \
						"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD1 \
						"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE2=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA'"`
			if [ $EXISTE2 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE2" >> $LOCAL$LOG
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA'"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA')"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP5
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOCAL$LOG
		else
			LOJA=$(for A in $I ; do ssh -o ConnectTimeout=1 $IP$A ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
			NLOJA=$(echo "L$(for Z in $I ; do ssh -o ConnectTimeout=1 $IP$Z ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)")
			TPVS=$(for X in $I ; do ssh -o ConnectTimeout=1 $IP$X ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$LOJA, PDVs:$TPVS, IP:$IP$I ." >> $LOCAL$LOG
			EXISTE=$(sqlite3 $BD \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE" >> $LOCAL$LOG
				sqlite3 $BD \
					"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD \
					"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE1=$(sqlite3 $BD1 \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE1 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE1" >> $LOCAL$LOG
				sqlite3 $BD1 \
						"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD1 \
						"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE2=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA'"`
			if [ $EXISTE2 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE2" >> $LOCAL$LOG
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA'"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA')"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP6
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOCAL$LOG
		else
			LOJA=$(for A in $I ; do ssh -o ConnectTimeout=1 $IP$A ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
			NLOJA=$(echo "L$(for Z in $I ; do ssh -o ConnectTimeout=1 $IP$Z ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)")
			TPVS=$(for X in $I ; do ssh -o ConnectTimeout=1 $IP$X ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$LOJA, PDVs:$TPVS, IP:$IP$I ." >> $LOCAL$LOG
			EXISTE=$(sqlite3 $BD \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE" >> $LOCAL$LOG
				sqlite3 $BD \
					"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD \
					"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE1=$(sqlite3 $BD1 \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE1 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE1" >> $LOCAL$LOG
				sqlite3 $BD1 \
						"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD1 \
						"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE2=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA'"`
			if [ $EXISTE2 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE2" >> $LOCAL$LOG
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA'"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA')"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP7
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOCAL$LOG
		else
			LOJA=$(for A in $I ; do ssh -o ConnectTimeout=1 $IP$A ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
			NLOJA=$(echo "L$(for Z in $I ; do ssh -o ConnectTimeout=1 $IP$Z ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)")
			TPVS=$(for X in $I ; do ssh -o ConnectTimeout=1 $IP$X ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$LOJA, PDVs:$TPVS, IP:$IP$I ." >> $LOCAL$LOG
			EXISTE=$(sqlite3 $BD \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE" >> $LOCAL$LOG
				sqlite3 $BD \
					"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD \
					"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE1=$(sqlite3 $BD1 \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE1 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE1" >> $LOCAL$LOG
				sqlite3 $BD1 \
						"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD1 \
						"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE2=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA'"`
			if [ $EXISTE2 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE2" >> $LOCAL$LOG
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA'"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA')"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP8
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOCAL$LOG
		else
			LOJA=$(for A in $I ; do ssh -o ConnectTimeout=1 $IP$A ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
			NLOJA=$(echo "L$(for Z in $I ; do ssh -o ConnectTimeout=1 $IP$Z ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)")
			TPVS=$(for X in $I ; do ssh -o ConnectTimeout=1 $IP$X ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$LOJA, PDVs:$TPVS, IP:$IP$I ." >> $LOCAL$LOG
			EXISTE=$(sqlite3 $BD \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE" >> $LOCAL$LOG
				sqlite3 $BD \
					"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD \
					"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE1=$(sqlite3 $BD1 \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE1 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE1" >> $LOCAL$LOG
				sqlite3 $BD1 \
						"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD1 \
						"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE2=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA'"`
			if [ $EXISTE2 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE2" >> $LOCAL$LOG
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA'"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA')"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP9
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOCAL$LOG
		else
			LOJA=$(for A in $I ; do ssh -o ConnectTimeout=1 $IP$A ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
			NLOJA=$(echo "L$(for Z in $I ; do ssh -o ConnectTimeout=1 $IP$Z ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)")
			TPVS=$(for X in $I ; do ssh -o ConnectTimeout=1 $IP$X ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$LOJA, PDVs:$TPVS, IP:$IP$I ." >> $LOCAL$LOG
			EXISTE=$(sqlite3 $BD \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE" >> $LOCAL$LOG
				sqlite3 $BD \
					"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD \
					"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE1=$(sqlite3 $BD1 \ "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA';")
			if [ $EXISTE1 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE1" >> $LOCAL$LOG
				sqlite3 $BD1 \
						"UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA';"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				sqlite3 $BD1 \
						"INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA');"

				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
			EXISTE2=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(ID) AS Ttotal FROM tb_ip WHERE loja = '$LOJA'"`
			if [ $EXISTE2 != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):$NLOJA ja existe no banco de dados, atualizando dados no banco.Return => $EXISTE2" >> $LOCAL$LOG
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_ip SET tpvs = '$TPVS', ip = '$IP$I', nome_loja = '$NLOJA' WHERE loja = '$LOJA'"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao atualizar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados atualizados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			else
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_ip (loja, tpvs, ip, nome_loja) VALUES ('$LOJA', '$TPVS', '$IP$I', '$NLOJA')"
				RETURN=$?
				if [ $RETURN != 0 ];then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				else
					echo "$(date +%Y%m%d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Return => $RETURN ." >> $LOCAL$LOG
				fi
			fi
		fi
		((I++))
	done

}
main
