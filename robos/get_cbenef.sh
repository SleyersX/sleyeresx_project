#!/bin/bash

LOG="error.cbenef.log"
BD="/root/srv_remoto/srv_remoto.db"
TIENDAS="tiendas_update.txt"

function group_shop(){

	if [ -e $TIENDAS ] ; then
        while read shop; 
        do
            progs=("$shop")
        done < $TIENDAS 
        array_shops=(${progs[0]})
    else
        echo "Arquivo [$TIENDAS] não foi encontrado no diretorio atual."
    fi

}

function valida_comunicacao(){
	
	ssh -o ConnectTimeout=1 -p10001 $ip$i exit  

}

function main(){
	
	group_shop
	i=0
	while [ $i != ${#array_shops[@]} ]
	do
		if [ "$x" == "0" ]; then
			echo "LOJA:"${array_shops[$x]}"."
		else
            # Validamos se os campos informados são númericos
            if [[ ${array_shops[i]} = ?(+|-)+([0-9]) ]]; then
                echo ""
            else
                echo "Ops, valor informado não é um número inteiro!"
                sleep 2
            fi
            
            lj=`echo ${#array_shops[i]}`
            if [ $lj -ge 5 ]; then
                echo "Informe até 4 caracteres númericos. Ex.: 2, 1000, 9527, 165."
                sleep 2
            fi
            loja=`printf "%05d" ${array_shops[i]}`
            EXISTE=`echo "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$loja';" | sqlite3 $BD`
            if [ $EXISTE -eq 1 ]; then
                # GetIP
                ip=`echo "SELECT ip FROM tb_ip WHERE loja LIKE '$loja';" | sqlite3 $BD`
                tpvs=`echo "SELECT tpvs FROM tb_ip WHERE loja LIKE '$loja';" | sqlite3 $BD`
				for y in $(seq 1 $tpvs); do sshpass -p root ssh -o ConnectTimeout=1 -p1000$y -l root $ip '. /confdia/bin/setvari ; echo "TIENDA [${NUMETIEN}] TPV [${NUMECAJA}] COUNT [$(echo "SELECT COUNT(Codi) AS TOTAL FROM ACUARTI1 WHERE Cbenef IS NOT NULL ;" | sqlite3 --noheader)]"'; done
			else
				 echo "Loja [$loja] não encontrada no banco de dados."
			fi
		fi
		let "i = i +1"
	done
}
main
