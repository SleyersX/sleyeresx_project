#!/bin/bash



ARQ_READ="/root/srv_remoto/arquivos_read/lista_dados_fiscais.txt"
LOG="/root/srv_remoto/log/error.dadosfiscais.log"
#Get list
echo "$(date +%Y%m%d"-"%H%M%S):------------------------------------ Inicio do Programa -------------------------------------" >> $LOG
echo "$(date +%Y%m%d"-"%H%M%S):Obtendo lista de dados fiscais." >> $LOG
sshpass -p root ssh -p10001 -l root 10.105.188.222 'echo "SELECT CodiFisc, PersFisc, DireFisc, CodiPostFisc, LocaFisc, ProvFisc, InscEstaClie, CodiEstaClie, BarrClie, NumeDireClie, CodiIden, CodiMuniClie, email FROM MAECLIE1 ;" | sqlite3' | grep -a "^L....." > $ARQ_READ
RETURN=$?
if [ $RETURN -eq 0 ]; then
    #Read list
    echo "$(date +%Y%m%d"-"%H%M%S):Lista obtida com sucesso -> ['$RETURN']." >> $LOG
    echo "$(date +%Y%m%d"-"%H%M%S):Iniciando leitura da lista para gravação no banco." >> $LOG
    IFS="|"
    while read CodiFisc PersFisc DireFisc CodiPostFisc LocaFisc ProvFisc InscEstaClie CodiEstaClie BarrClie NumeDireClie CodiIden CodiMuniClie email;
    do
        CodiFisc=`echo $CodiFisc | tr -d " " | tr -d "\n"`
        CodiPostFisc=`echo $CodiPostFisc | tr -d " " | tr -d "\n"`
        InscEstaClie=`echo $InscEstaClie | tr -d " " | tr -d "\n"`
        CodiIden=`echo $CodiIden | tr -d " " | tr -d "\n"`
        CodiMuniClie=`echo $CodiMuniClie | tr -d " " | tr -d "\n"`
        email=`echo $email | tr -d " " | tr -d "\n"`
        echo "$(date +%Y%m%d"-"%H%M%S):$CodiFisc:Verificando se a loja ['$CodiFisc'] existe no banco de dados." >> $LOG
        CHECK_EXIST=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(id) FROM tb_dados_fiscais_lojas WHERE CodiFisc LIKE '$CodiFisc'"`
        if [ $CHECK_EXIST -ge 1 ]; then
            echo "$(date +%Y%m%d"-"%H%M%S):$CodiFisc:Loja ['$CodiFisc'] já existe no banco de dados." >> $LOG
            echo "$(date +%Y%m%d"-"%H%M%S):$CodiFisc:Será feito o UPDATE dos dados no banco." >> $LOG
            mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_dados_fiscais_lojas SET PersFisc = '$PersFisc', DireFisc = '$DireFisc', CodiPostFisc = '$CodiPostFisc', LocaFisc = '$LocaFisc', ProvFisc = '$ProvFisc', InscEstaClie = '$InscEstaClie', CodiEstaClie = '$CodiEstaClie', BarrClie = '$BarrClie', NumeDireClie = '$NumeDireClie', CodiIden = '$CodiIden', CodiMuniClie = '$CodiMuniClie', email = '$email' WHERE CodiFisc LIKE '$CodiFisc'"
            RETURN=$?
            if [ $RETURN -eq 0 ]; then
                echo "$(date +%Y%m%d"-"%H%M%S):$CodiFisc:Dados atualizados com sucesso -> ['$RETURN']." >> $LOG
                echo "$(date +%Y%m%d"-"%H%M%S):$CodiFisc:['$PersFisc']['$DireFisc']['$CodiPostFisc']['$LocaFisc']['$ProvFisc']['$InscEstaClie']['$CodiEstaClie']['$BarrClie']['$NumeDireClie']['$CodiIden']['$CodiMuniClie']['$email]." >> $LOG
            else
                echo "$(date +%Y%m%d"-"%H%M%S):$CodiFisc:Erro ao atualizar dados -> ['$RETURN']." >> $LOG
                echo "$(date +%Y%m%d"-"%H%M%S):$CodiFisc:['$PersFisc']['$DireFisc']['$CodiPostFisc']['$LocaFisc']['$ProvFisc']['$InscEstaClie']['$CodiEstaClie']['$BarrClie']['$NumeDireClie']['$CodiIden']['$CodiMuniClie']['$email]." >> $LOG
            fi
        else
            echo "$(date +%Y%m%d"-"%H%M%S):$CodiFisc:Loja ['$CodiFisc'] não já existe no banco de dados." >> $LOG
            echo "$(date +%Y%m%d"-"%H%M%S):$CodiFisc:Será feito o INSERT dos dados no banco." >> $LOG
            mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_dados_fiscais_lojas (CodiFisc, PersFisc, DireFisc, CodiPostFisc, LocaFisc, ProvFisc, InscEstaClie, CodiEstaClie, BarrClie, NumeDireClie, CodiIden, CodiMuniClie, email) VALUES ('$CodiFisc', '$PersFisc', '$DireFisc', '$CodiPostFisc', '$LocaFisc', '$ProvFisc', '$InscEstaClie', '$CodiEstaClie', '$BarrClie', '$NumeDireClie', '$CodiIden', '$CodiMuniClie', '$email')"
            RETURN=$?
            if [ $RETURN -eq 0 ]; then
                echo "$(date +%Y%m%d"-"%H%M%S):$CodiFisc:Dados inseridos com sucesso -> ['$RETURN']." >> $LOG
                echo "$(date +%Y%m%d"-"%H%M%S):$CodiFisc:['$PersFisc']['$DireFisc']['$CodiPostFisc']['$LocaFisc']['$ProvFisc']['$InscEstaClie']['$CodiEstaClie']['$BarrClie']['$NumeDireClie']['$CodiIden']['$CodiMuniClie']['$email]." >> $LOG
            else
                echo "$(date +%Y%m%d"-"%H%M%S):$CodiFisc:Erro ao inserir dados -> ['$RETURN']." >> $LOG
                echo "$(date +%Y%m%d"-"%H%M%S):$CodiFisc:['$PersFisc']['$DireFisc']['$CodiPostFisc']['$LocaFisc']['$ProvFisc']['$InscEstaClie']['$CodiEstaClie']['$BarrClie']['$NumeDireClie']['$CodiIden']['$CodiMuniClie']['$email]." >> $LOG
            fi
        fi
    done < $ARQ_READ
else
    echo "$(date +%Y%m%d"-"%H%M%S):Erro ao obter lista -> ['$RETURN']." >> $LOG
fi