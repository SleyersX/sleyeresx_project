#!/bin/bash
#
#Script para obeter dados gerais para uma base de dados para controle de lojas
#Criado 31/08/2017
#Autor wam001br
#Modificado 11/10/2018
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------

function val_comunicacao()
{
	ssh -o ConnectTimeout=1 $ip$i exit

}
function main()
{

log="/root/srv_remoto/log/error.getdadosgerais.log"
REL="relatorio_dados_gerais.txt"
IP0="10.105.188."
IP1="10.105.189."
IP2="10.105.194."
IP3="10.105.190."
IP4="10.106.235."
IP5="10.106.101."
IP6="10.106.102."
IP7="10.106.103."
IP8="10.106.104."
	
	i=0
    while (( i < 254 ))
      do
      ip=$IP0
			resultado=$(val_comunicacao)
			ret=$?
			if [ $ret != 0 ]; then
				echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $log
			else
				loja=$(for l in $i ; do ssh $ip$l ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
				tpvs=$(for x in $i ; do ssh $ip$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
				versao=$(for v in $i ; do ssh $ip$v 'cat /confdia/version '; done)
	
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$loja, PDVs:$tpvs, IP:$ip$i, Vesao da loja:$versao" >> $log			
			#existe=$(sqlite3 db_tpvs.db \ "SELECT COUNT(Loja) AS total FROM tb_dados_loja_I WHERE Loja = '$loja';")
			existe=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(Loja) AS total FROM tb_dados_loja WHERE Loja = '$loja';")
			if [ $existe != 0 ]; then
				echo "$(date +%Y%m%d"-"%H%M%S):Loja ja existe no banco de dados, atualizando dados no banco.Retorno($existe)" >> $log
				echo -e "\033[01;32mLoja ja existe no banco de dados, atualizando dados.\033[00;37m"
				data=$(date +%d"-"%m"-"%Y)
				contador=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT contador FROM tb_dados_loja WHERE Loja = '$loja';")
				soma=$(($contador + 1 ))
				#echo "UPDATE tb_dados_loja_I SET Qntd_Tpvs = '$tpvs', Versao = '$versao', IP = '$ip$i', CONTADOR = '$soma', Data = '$data' WHERE Loja = '$loja';" >> arquivo_update_dados_loja.SQL
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_dados_loja SET Qntd_Tpvs = '$tpvs', Versao = '$versao', IP = '$ip$i', CONTADOR = '$soma', Data = '$data' WHERE Loja = '$loja';"
				retorn=$?
				if [ $retorn != 0 ]; then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Retorno($retorno)" >> $log
					echo -e "\033[01;31mFalha ao gravar dados no banco de dados.\033[00;37m"
				else
					echo "$(date +%Y%m$d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Retorno($retorno)" >> $log
					echo -e "\033[01;32mDados gravados com sucesso no banco de dados.\033[00;37m"
				fi
			else
				#Passamos as informacoes obtids para o banco de dados
				#echo "INSERT INTO tb_dados_loja_I ( Loja, Qntd_Tpvs, Versao, IP, CONTADOR, Data ) VALUES ('$loja', '$tpvs', '$versao', '$ip$i', 1, '$data');" >> arquivo_dados_loja.SQL
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_dados_loja_I ( Loja, Qntd_Tpvs, Versao, IP, CONTADOR, Data ) VALUES ('$loja', '$tpvs', '$versao', '$ip$i', 1, '$data');"
				retorno=$?
					if [ $retorno != 0 ]; then
						echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Retorno($retorno)" >> $log
						echo -e "\033[01;31mFalha ao gravar dados no banco de dados.\033[00;37m"
					else
						echo "$(date +%Y%m$d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Retorno($retorno)" >> $log
						echo -e "\033[01;32mDados gravados com sucesso no banco de dados.\033[00;37m"
					fi

				fi
			fi
			((i++))
		done

	i=0
    while (( i < 254 ))
      do
      ip=$IP1
			resultado=$(val_comunicacao)
			ret=$?
			if [ $ret != 0 ]; then
				echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $log
			else
				loja=$(for l in $i ; do ssh $ip$l ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
				tpvs=$(for x in $i ; do ssh $ip$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
				versao=$(for v in $i ; do ssh $ip$v 'cat /confdia/version '; done)
	
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$loja, PDVs:$tpvs, IP:$ip$i, Vesao da loja:$versao" >> $log
			
			#existe=$(sqlite3 db_tpvs.db \ "SELECT COUNT(Loja) AS total FROM tb_dados_loja_I WHERE Loja = '$loja';")
			existe=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(Loja) AS total FROM tb_dados_loja WHERE Loja = '$loja';")
			if [ $existe != 0 ]; then
				echo "$(date +%Y%m%d"-"%H%M%S):Loja ja existe no banco de dados, atualizando dados no banco.Retorno($existe)" >> $log
				echo -e "\033[01;32mLoja ja existe no banco de dados, atualizando dados.\033[00;37m"
				data=$(date +%d"-"%m"-"%Y)
				contador=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT contador FROM tb_dados_loja WHERE Loja = '$loja';")
				soma=$(($contador + 1 ))
				#echo "UPDATE tb_dados_loja_I SET Qntd_Tpvs = '$tpvs', Versao = '$versao', IP = '$ip$i', CONTADOR = '$soma', Data = '$data' WHERE Loja = '$loja';" >> arquivo_update_dados_loja.SQL
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_dados_loja SET Qntd_Tpvs = '$tpvs', Versao = '$versao', IP = '$ip$i', CONTADOR = '$soma', Data = '$data' WHERE Loja = '$loja';"
				retorn=$?
				if [ $retorn != 0 ]; then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Retorno($retorno)" >> $log
					echo -e "\033[01;31mFalha ao gravar dados no banco de dados.\033[00;37m"
				else
					echo "$(date +%Y%m$d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Retorno($retorno)" >> $log
					echo -e "\033[01;32mDados gravados com sucesso no banco de dados.\033[00;37m"
				fi
			else
				#Passamos as informacoes obtids para o banco de dados
				#echo "INSERT INTO tb_dados_loja_I ( Loja, Qntd_Tpvs, Versao, IP, CONTADOR, Data ) VALUES ('$loja', '$tpvs', '$versao', '$ip$i', 1, '$data');" >> arquivo_dados_loja.SQL
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_dados_loja_I ( Loja, Qntd_Tpvs, Versao, IP, CONTADOR, Data ) VALUES ('$loja', '$tpvs', '$versao', '$ip$i', 1, '$data');"
				retorno=$?
					if [ $retorno != 0 ]; then
						echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Retorno($retorno)" >> $log
						echo -e "\033[01;31mFalha ao gravar dados no banco de dados.\033[00;37m"
					else
						echo "$(date +%Y%m$d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Retorno($retorno)" >> $log
						echo -e "\033[01;32mDados gravados com sucesso no banco de dados.\033[00;37m"
					fi

				fi
			fi
			((i++))
		done

	i=0
    while (( i < 254 ))
      do
      ip=$IP2
			resultado=$(val_comunicacao)
			ret=$?
			if [ $ret != 0 ]; then
				echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $log
			else
				loja=$(for l in $i ; do ssh $ip$l ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
				tpvs=$(for x in $i ; do ssh $ip$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
				versao=$(for v in $i ; do ssh $ip$v 'cat /confdia/version '; done)
	
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$loja, PDVs:$tpvs, IP:$ip$i, Vesao da loja:$versao" >> $log
			
			#existe=$(sqlite3 db_tpvs.db \ "SELECT COUNT(Loja) AS total FROM tb_dados_loja_I WHERE Loja = '$loja';")
			existe=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(Loja) AS total FROM tb_dados_loja WHERE Loja = '$loja';")
			if [ $existe != 0 ]; then
				echo "$(date +%Y%m%d"-"%H%M%S):Loja ja existe no banco de dados, atualizando dados no banco.Retorno($existe)" >> $log
				echo -e "\033[01;32mLoja ja existe no banco de dados, atualizando dados.\033[00;37m"
				data=$(date +%d"-"%m"-"%Y)
				contador=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT contador FROM tb_dados_loja WHERE Loja = '$loja';")
				soma=$(($contador + 1 ))
				#echo "UPDATE tb_dados_loja_I SET Qntd_Tpvs = '$tpvs', Versao = '$versao', IP = '$ip$i', CONTADOR = '$soma', Data = '$data' WHERE Loja = '$loja';" >> arquivo_update_dados_loja.SQL
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_dados_loja SET Qntd_Tpvs = '$tpvs', Versao = '$versao', IP = '$ip$i', CONTADOR = '$soma', Data = '$data' WHERE Loja = '$loja';"
				retorn=$?
				if [ $retorn != 0 ]; then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Retorno($retorno)" >> $log
					echo -e "\033[01;31mFalha ao gravar dados no banco de dados.\033[00;37m"
				else
					echo "$(date +%Y%m$d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Retorno($retorno)" >> $log
					echo -e "\033[01;32mDados gravados com sucesso no banco de dados.\033[00;37m"
				fi
			else
				#Passamos as informacoes obtids para o banco de dados
				#echo "INSERT INTO tb_dados_loja_I ( Loja, Qntd_Tpvs, Versao, IP, CONTADOR, Data ) VALUES ('$loja', '$tpvs', '$versao', '$ip$i', 1, '$data');" >> arquivo_dados_loja.SQL
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_dados_loja_I ( Loja, Qntd_Tpvs, Versao, IP, CONTADOR, Data ) VALUES ('$loja', '$tpvs', '$versao', '$ip$i', 1, '$data');"
				retorno=$?
					if [ $retorno != 0 ]; then
						echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Retorno($retorno)" >> $log
						echo -e "\033[01;31mFalha ao gravar dados no banco de dados.\033[00;37m"
					else
						echo "$(date +%Y%m$d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Retorno($retorno)" >> $log
						echo -e "\033[01;32mDados gravados com sucesso no banco de dados.\033[00;37m"
					fi

				fi
			fi
			((i++))
		done

	i=0
    while (( i < 254 ))
      do
      ip=$IP3
			resultado=$(val_comunicacao)
			ret=$?
			if [ $ret != 0 ]; then
				echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $log
			else
				loja=$(for l in $i ; do ssh $ip$l ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
				tpvs=$(for x in $i ; do ssh $ip$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
				versao=$(for v in $i ; do ssh $ip$v 'cat /confdia/version '; done)
	
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$loja, PDVs:$tpvs, IP:$ip$i, Vesao da loja:$versao" >> $log
			
			#existe=$(sqlite3 db_tpvs.db \ "SELECT COUNT(Loja) AS total FROM tb_dados_loja_I WHERE Loja = '$loja';")
			existe=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(Loja) AS total FROM tb_dados_loja WHERE Loja = '$loja';")
			if [ $existe != 0 ]; then
				echo "$(date +%Y%m%d"-"%H%M%S):Loja ja existe no banco de dados, atualizando dados no banco.Retorno($existe)" >> $log
				echo -e "\033[01;32mLoja ja existe no banco de dados, atualizando dados.\033[00;37m"
				data=$(date +%d"-"%m"-"%Y)
				contador=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT contador FROM tb_dados_loja WHERE Loja = '$loja';")
				soma=$(($contador + 1 ))
				#echo "UPDATE tb_dados_loja_I SET Qntd_Tpvs = '$tpvs', Versao = '$versao', IP = '$ip$i', CONTADOR = '$soma', Data = '$data' WHERE Loja = '$loja';" >> arquivo_update_dados_loja.SQL
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_dados_loja SET Qntd_Tpvs = '$tpvs', Versao = '$versao', IP = '$ip$i', CONTADOR = '$soma', Data = '$data' WHERE Loja = '$loja';"
				retorn=$?
				if [ $retorn != 0 ]; then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Retorno($retorno)" >> $log
					echo -e "\033[01;31mFalha ao gravar dados no banco de dados.\033[00;37m"
				else
					echo "$(date +%Y%m$d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Retorno($retorno)" >> $log
					echo -e "\033[01;32mDados gravados com sucesso no banco de dados.\033[00;37m"
				fi
			else
				#Passamos as informacoes obtids para o banco de dados
				#echo "INSERT INTO tb_dados_loja_I ( Loja, Qntd_Tpvs, Versao, IP, CONTADOR, Data ) VALUES ('$loja', '$tpvs', '$versao', '$ip$i', 1, '$data');" >> arquivo_dados_loja.SQL
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_dados_loja_I ( Loja, Qntd_Tpvs, Versao, IP, CONTADOR, Data ) VALUES ('$loja', '$tpvs', '$versao', '$ip$i', 1, '$data');"
				retorno=$?
					if [ $retorno != 0 ]; then
						echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Retorno($retorno)" >> $log
						echo -e "\033[01;31mFalha ao gravar dados no banco de dados.\033[00;37m"
					else
						echo "$(date +%Y%m$d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Retorno($retorno)" >> $log
						echo -e "\033[01;32mDados gravados com sucesso no banco de dados.\033[00;37m"
					fi

				fi
			fi
			((i++))
		done

	i=0
    while (( i < 254 ))
      do
      ip=$IP4
			resultado=$(val_comunicacao)
			ret=$?
			if [ $ret != 0 ]; then
				echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $log
			else
				loja=$(for l in $i ; do ssh $ip$l ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
				tpvs=$(for x in $i ; do ssh $ip$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
				versao=$(for v in $i ; do ssh $ip$v 'cat /confdia/version '; done)
	
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$loja, PDVs:$tpvs, IP:$ip$i, Vesao da loja:$versao" >> $log
			
			#existe=$(sqlite3 db_tpvs.db \ "SELECT COUNT(Loja) AS total FROM tb_dados_loja_I WHERE Loja = '$loja';")
			existe=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(Loja) AS total FROM tb_dados_loja WHERE Loja = '$loja';")
			if [ $existe != 0 ]; then
				echo "$(date +%Y%m%d"-"%H%M%S):Loja ja existe no banco de dados, atualizando dados no banco.Retorno($existe)" >> $log
				echo -e "\033[01;32mLoja ja existe no banco de dados, atualizando dados.\033[00;37m"
				data=$(date +%d"-"%m"-"%Y)
				contador=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT contador FROM tb_dados_loja WHERE Loja = '$loja';")
				soma=$(($contador + 1 ))
				#echo "UPDATE tb_dados_loja_I SET Qntd_Tpvs = '$tpvs', Versao = '$versao', IP = '$ip$i', CONTADOR = '$soma', Data = '$data' WHERE Loja = '$loja';" >> arquivo_update_dados_loja.SQL
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_dados_loja SET Qntd_Tpvs = '$tpvs', Versao = '$versao', IP = '$ip$i', CONTADOR = '$soma', Data = '$data' WHERE Loja = '$loja';"
				retorn=$?
				if [ $retorn != 0 ]; then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Retorno($retorno)" >> $log
					echo -e "\033[01;31mFalha ao gravar dados no banco de dados.\033[00;37m"
				else
					echo "$(date +%Y%m$d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Retorno($retorno)" >> $log
					echo -e "\033[01;32mDados gravados com sucesso no banco de dados.\033[00;37m"
				fi
			else
				#Passamos as informacoes obtids para o banco de dados
				#echo "INSERT INTO tb_dados_loja_I ( Loja, Qntd_Tpvs, Versao, IP, CONTADOR, Data ) VALUES ('$loja', '$tpvs', '$versao', '$ip$i', 1, '$data');" >> arquivo_dados_loja.SQL
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_dados_loja_I ( Loja, Qntd_Tpvs, Versao, IP, CONTADOR, Data ) VALUES ('$loja', '$tpvs', '$versao', '$ip$i', 1, '$data');"
				retorno=$?
					if [ $retorno != 0 ]; then
						echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Retorno($retorno)" >> $log
						echo -e "\033[01;31mFalha ao gravar dados no banco de dados.\033[00;37m"
					else
						echo "$(date +%Y%m$d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Retorno($retorno)" >> $log
						echo -e "\033[01;32mDados gravados com sucesso no banco de dados.\033[00;37m"
					fi

				fi
			fi
			((i++))
		done

	i=0
    while (( i < 254 ))
      do
      ip=$IP5
			resultado=$(val_comunicacao)
			ret=$?
			if [ $ret != 0 ]; then
				echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $log
			else
				loja=$(for l in $i ; do ssh $ip$l ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
				tpvs=$(for x in $i ; do ssh $ip$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
				versao=$(for v in $i ; do ssh $ip$v 'cat /confdia/version '; done)
	
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$loja, PDVs:$tpvs, IP:$ip$i, Vesao da loja:$versao" >> $log
			
			#existe=$(sqlite3 db_tpvs.db \ "SELECT COUNT(Loja) AS total FROM tb_dados_loja_I WHERE Loja = '$loja';")
			existe=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(Loja) AS total FROM tb_dados_loja WHERE Loja = '$loja';")
			if [ $existe != 0 ]; then
				echo "$(date +%Y%m%d"-"%H%M%S):Loja ja existe no banco de dados, atualizando dados no banco.Retorno($existe)" >> $log
				echo -e "\033[01;32mLoja ja existe no banco de dados, atualizando dados.\033[00;37m"
				data=$(date +%d"-"%m"-"%Y)
				contador=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT contador FROM tb_dados_loja WHERE Loja = '$loja';")
				soma=$(($contador + 1 ))
				#echo "UPDATE tb_dados_loja_I SET Qntd_Tpvs = '$tpvs', Versao = '$versao', IP = '$ip$i', CONTADOR = '$soma', Data = '$data' WHERE Loja = '$loja';" >> arquivo_update_dados_loja.SQL
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_dados_loja SET Qntd_Tpvs = '$tpvs', Versao = '$versao', IP = '$ip$i', CONTADOR = '$soma', Data = '$data' WHERE Loja = '$loja';"
				retorn=$?
				if [ $retorn != 0 ]; then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Retorno($retorno)" >> $log
					echo -e "\033[01;31mFalha ao gravar dados no banco de dados.\033[00;37m"
				else
					echo "$(date +%Y%m$d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Retorno($retorno)" >> $log
					echo -e "\033[01;32mDados gravados com sucesso no banco de dados.\033[00;37m"
				fi
			else
				#Passamos as informacoes obtids para o banco de dados
				#echo "INSERT INTO tb_dados_loja_I ( Loja, Qntd_Tpvs, Versao, IP, CONTADOR, Data ) VALUES ('$loja', '$tpvs', '$versao', '$ip$i', 1, '$data');" >> arquivo_dados_loja.SQL
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_dados_loja_I ( Loja, Qntd_Tpvs, Versao, IP, CONTADOR, Data ) VALUES ('$loja', '$tpvs', '$versao', '$ip$i', 1, '$data');"
				retorno=$?
					if [ $retorno != 0 ]; then
						echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Retorno($retorno)" >> $log
						echo -e "\033[01;31mFalha ao gravar dados no banco de dados.\033[00;37m"
					else
						echo "$(date +%Y%m$d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Retorno($retorno)" >> $log
						echo -e "\033[01;32mDados gravados com sucesso no banco de dados.\033[00;37m"
					fi

				fi
			fi
			((i++))
		done

	i=0
    while (( i < 254 ))
      do
      ip=$IP6
			resultado=$(val_comunicacao)
			ret=$?
			if [ $ret != 0 ]; then
				echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $log
			else
				loja=$(for l in $i ; do ssh $ip$l ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
				tpvs=$(for x in $i ; do ssh $ip$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
				versao=$(for v in $i ; do ssh $ip$v 'cat /confdia/version '; done)
	
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$loja, PDVs:$tpvs, IP:$ip$i, Vesao da loja:$versao" >> $log
			
			#existe=$(sqlite3 db_tpvs.db \ "SELECT COUNT(Loja) AS total FROM tb_dados_loja_I WHERE Loja = '$loja';")
			existe=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(Loja) AS total FROM tb_dados_loja WHERE Loja = '$loja';")
			if [ $existe != 0 ]; then
				echo "$(date +%Y%m%d"-"%H%M%S):Loja ja existe no banco de dados, atualizando dados no banco.Retorno($existe)" >> $log
				echo -e "\033[01;32mLoja ja existe no banco de dados, atualizando dados.\033[00;37m"
				data=$(date +%d"-"%m"-"%Y)
				contador=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT contador FROM tb_dados_loja WHERE Loja = '$loja';")
				soma=$(($contador + 1 ))
				#echo "UPDATE tb_dados_loja_I SET Qntd_Tpvs = '$tpvs', Versao = '$versao', IP = '$ip$i', CONTADOR = '$soma', Data = '$data' WHERE Loja = '$loja';" >> arquivo_update_dados_loja.SQL
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_dados_loja SET Qntd_Tpvs = '$tpvs', Versao = '$versao', IP = '$ip$i', CONTADOR = '$soma', Data = '$data' WHERE Loja = '$loja';"
				retorn=$?
				if [ $retorn != 0 ]; then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Retorno($retorno)" >> $log
					echo -e "\033[01;31mFalha ao gravar dados no banco de dados.\033[00;37m"
				else
					echo "$(date +%Y%m$d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Retorno($retorno)" >> $log
					echo -e "\033[01;32mDados gravados com sucesso no banco de dados.\033[00;37m"
				fi
			else
				#Passamos as informacoes obtids para o banco de dados
				#echo "INSERT INTO tb_dados_loja_I ( Loja, Qntd_Tpvs, Versao, IP, CONTADOR, Data ) VALUES ('$loja', '$tpvs', '$versao', '$ip$i', 1, '$data');" >> arquivo_dados_loja.SQL
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_dados_loja_I ( Loja, Qntd_Tpvs, Versao, IP, CONTADOR, Data ) VALUES ('$loja', '$tpvs', '$versao', '$ip$i', 1, '$data');"
				retorno=$?
					if [ $retorno != 0 ]; then
						echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Retorno($retorno)" >> $log
						echo -e "\033[01;31mFalha ao gravar dados no banco de dados.\033[00;37m"
					else
						echo "$(date +%Y%m$d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Retorno($retorno)" >> $log
						echo -e "\033[01;32mDados gravados com sucesso no banco de dados.\033[00;37m"
					fi

				fi
			fi
			((i++))
		done

	i=0
    while (( i < 254 ))
      do
      ip=$IP7
			resultado=$(val_comunicacao)
			ret=$?
			if [ $ret != 0 ]; then
				echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $log
			else
				loja=$(for l in $i ; do ssh $ip$l ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
				tpvs=$(for x in $i ; do ssh $ip$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
				versao=$(for v in $i ; do ssh $ip$v 'cat /confdia/version '; done)
	
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$loja, PDVs:$tpvs, IP:$ip$i, Vesao da loja:$versao" >> $log
			
			#existe=$(sqlite3 db_tpvs.db \ "SELECT COUNT(Loja) AS total FROM tb_dados_loja_I WHERE Loja = '$loja';")
			existe=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(Loja) AS total FROM tb_dados_loja WHERE Loja = '$loja';")
			if [ $existe != 0 ]; then
				echo "$(date +%Y%m%d"-"%H%M%S):Loja ja existe no banco de dados, atualizando dados no banco.Retorno($existe)" >> $log
				echo -e "\033[01;32mLoja ja existe no banco de dados, atualizando dados.\033[00;37m"
				data=$(date +%d"-"%m"-"%Y)
				contador=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT contador FROM tb_dados_loja WHERE Loja = '$loja';")
				soma=$(($contador + 1 ))
				#echo "UPDATE tb_dados_loja_I SET Qntd_Tpvs = '$tpvs', Versao = '$versao', IP = '$ip$i', CONTADOR = '$soma', Data = '$data' WHERE Loja = '$loja';" >> arquivo_update_dados_loja.SQL
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_dados_loja SET Qntd_Tpvs = '$tpvs', Versao = '$versao', IP = '$ip$i', CONTADOR = '$soma', Data = '$data' WHERE Loja = '$loja';"
				retorn=$?
				if [ $retorn != 0 ]; then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Retorno($retorno)" >> $log
					echo -e "\033[01;31mFalha ao gravar dados no banco de dados.\033[00;37m"
				else
					echo "$(date +%Y%m$d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Retorno($retorno)" >> $log
					echo -e "\033[01;32mDados gravados com sucesso no banco de dados.\033[00;37m"
				fi
			else
				#Passamos as informacoes obtids para o banco de dados
				#echo "INSERT INTO tb_dados_loja_I ( Loja, Qntd_Tpvs, Versao, IP, CONTADOR, Data ) VALUES ('$loja', '$tpvs', '$versao', '$ip$i', 1, '$data');" >> arquivo_dados_loja.SQL
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_dados_loja_I ( Loja, Qntd_Tpvs, Versao, IP, CONTADOR, Data ) VALUES ('$loja', '$tpvs', '$versao', '$ip$i', 1, '$data');"
				retorno=$?
					if [ $retorno != 0 ]; then
						echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Retorno($retorno)" >> $log
						echo -e "\033[01;31mFalha ao gravar dados no banco de dados.\033[00;37m"
					else
						echo "$(date +%Y%m$d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Retorno($retorno)" >> $log
						echo -e "\033[01;32mDados gravados com sucesso no banco de dados.\033[00;37m"
					fi

				fi
			fi
			((i++))
		done

	i=0
    while (( i < 254 ))
      do
      ip=$IP8
			resultado=$(val_comunicacao)
			ret=$?
			if [ $ret != 0 ]; then
				echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $log
			else
				loja=$(for l in $i ; do ssh $ip$l ' . /confdia/bin/setvari ; echo "${NUMETIEN}"' ; done)
				tpvs=$(for x in $i ; do ssh $ip$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
				versao=$(for v in $i ; do ssh $ip$v 'cat /confdia/version '; done)
	
			echo "$(date +%Y%m%d"-"%H%M%S):Dados obtidos da Loja:$loja, PDVs:$tpvs, IP:$ip$i, Vesao da loja:$versao" >> $log
			
			#existe=$(sqlite3 db_tpvs.db \ "SELECT COUNT(Loja) AS total FROM tb_dados_loja_I WHERE Loja = '$loja';")
			existe=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(Loja) AS total FROM tb_dados_loja WHERE Loja = '$loja';")
			if [ $existe != 0 ]; then
				echo "$(date +%Y%m%d"-"%H%M%S):Loja ja existe no banco de dados, atualizando dados no banco.Retorno($existe)" >> $log
				echo -e "\033[01;32mLoja ja existe no banco de dados, atualizando dados.\033[00;37m"
				data=$(date +%d"-"%m"-"%Y)
				contador=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT contador FROM tb_dados_loja WHERE Loja = '$loja';")
				soma=$(($contador + 1 ))
				#echo "UPDATE tb_dados_loja_I SET Qntd_Tpvs = '$tpvs', Versao = '$versao', IP = '$ip$i', CONTADOR = '$soma', Data = '$data' WHERE Loja = '$loja';" >> arquivo_update_dados_loja.SQL
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_dados_loja SET Qntd_Tpvs = '$tpvs', Versao = '$versao', IP = '$ip$i', CONTADOR = '$soma', Data = '$data' WHERE Loja = '$loja';"
				retorn=$?
				if [ $retorn != 0 ]; then
					echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Retorno($retorno)" >> $log
					echo -e "\033[01;31mFalha ao gravar dados no banco de dados.\033[00;37m"
				else
					echo "$(date +%Y%m$d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Retorno($retorno)" >> $log
					echo -e "\033[01;32mDados gravados com sucesso no banco de dados.\033[00;37m"
				fi
			else
				#Passamos as informacoes obtids para o banco de dados
				#echo "INSERT INTO tb_dados_loja_I ( Loja, Qntd_Tpvs, Versao, IP, CONTADOR, Data ) VALUES ('$loja', '$tpvs', '$versao', '$ip$i', 1, '$data');" >> arquivo_dados_loja.SQL
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_dados_loja_I ( Loja, Qntd_Tpvs, Versao, IP, CONTADOR, Data ) VALUES ('$loja', '$tpvs', '$versao', '$ip$i', 1, '$data');"
				retorno=$?
					if [ $retorno != 0 ]; then
						echo "$(date +%Y%m%d"-"%H%M%S):Falha ao gravar dados no banco de dados.Retorno($retorno)" >> $log
						echo -e "\033[01;31mFalha ao gravar dados no banco de dados.\033[00;37m"
					else
						echo "$(date +%Y%m$d"-"%H%M%S):Dados gravados com sucesso no banco de dados.Retorno($retorno)" >> $log
						echo -e "\033[01;32mDados gravados com sucesso no banco de dados.\033[00;37m"
					fi

				fi
			fi
			((i++))
		done
}
main
