#!/bin/bash


LOG="error.getdatetime.log"
RELATORIO="relatorio_getdatetime.txt"

function valida_comunicacao(){

	 ssh -o ConnectTimeout=1 -p10001 $IP$i exit
	 
}

function main(){
	
	IP1="10.106.235."
	IP2="10.106.102."
	
	i=0
	while(( i < 150))
		do
			IP=$IP1
			CONEXAO=$(valida_comunicacao)
			RETURN=$?
			if [ $RETURN != 0 ]; then
				echo "$(date +%Y%m%d-%H%M%S.%s):TEST_CONN:Error ao conectar com IP => $IP$i ." >> $LOG
				echo "$(date +%Y%m%d-%H%M%S.%s):TEST_CONN:Retorno => $RETURN ." >> $LOG
			else
				echo "$(date +%Y%m%d-%H%M%S.%s):TEST_CONN:Conexão realizado com sucesso com o IP => $IP$i ." >> $LOG
				echo "$(date +%Y%m%d-%H%M%S.%s):TEST_CONN:Retorno => $RETURN ." >> $LOG
				NUM_TPVS=$(for x in $i;do ssh -p10001 $IP$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
				RETURN=$?
				if [ $RETURN != 0 ]; then
					echo "$(date +%Y%m%d-%H%M%S.%s):GET_NUMTPVS:Error ao obter dados do IP => $IP$i ." >> $LOG
					echo "$(date +%Y%m%d-%H%M%S.%s):GET_NUMTPVS:Retorno => $RETURN ." >> $LOG
				else
					echo "$(date +%Y%m%d-%H%M%S.%s):GET_NUMTPVS:Dados obtidos com sucesso do IP => $IP$i [$NUM_TPVS] ." >> $LOG
					echo "$(date +%Y%m%d-%H%M%S.%s):GET_NUMTPVS:Retorno => $RETURN ." >> $LOG
					for y in $(seq 1 $NUM_TPVS) ; do echo "$(date +%Y%m%d)""|""$(date +%H%M%S)""|""$(ssh -o ConnectTimeout=1 -p1000$y $IP$i '. /confdia/bin/setvari ; echo "${NUMETIEN}""|""${NUMECAJA}""|""$(date +%Y%m%d)""|""$(date +%H%M%S)" ')" ; done >> $RELATORIO
					(valida_comunicacao)
				fi
			fi		
	((i++))
	done
	
	i=0
	while(( i < 100))
		do
			IP=$IP2
			CONEXAO=$(valida_comunicacao)
			RETURN=$?
			if [ $RETURN != 0 ]; then
				echo "$(date +%Y%m%d-%H%M%S.%s):TEST_CONN:Error ao conectar com IP => $IP$i ." >> $LOG
				echo "$(date +%Y%m%d-%H%M%S.%s):TEST_CONN:Retorno => $RETURN ." >> $LOG
			else
				echo "$(date +%Y%m%d-%H%M%S.%s):TEST_CONN:Conexão realizado com sucesso com o IP => $IP$i ." >> $LOG
				echo "$(date +%Y%m%d-%H%M%S.%s):TEST_CONN:Retorno => $RETURN ." >> $LOG
				NUM_TPVS=$(for x in $i;do ssh -p10001 $IP$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
				RETURN=$?
				if [ $RETURN != 0 ]; then
					echo "$(date +%Y%m%d-%H%M%S.%s):GET_NUMTPVS:Error ao obter dados do IP => $IP$i ." >> $LOG
					echo "$(date +%Y%m%d-%H%M%S.%s):GET_NUMTPVS:Retorno => $RETURN ." >> $LOG
				else
					echo "$(date +%Y%m%d-%H%M%S.%s):GET_NUMTPVS:Dados obtidos com sucesso do IP => $IP$i [$NUM_TPVS] ." >> $LOG
					for y in $(seq 1 $NUM_TPVS) ; do  echo "$(date +%Y%m%d)""|""$(date +%H%M%S)""|""$(ssh -o ConnectTimeout=1 -p1000$y $IP$i '. /confdia/bin/setvari ; echo "${NUMETIEN}""|""${NUMECAJA}""|""$(date +%Y%m%d)""|""$(date +%H%M%S)" ')" ; done >> $RELATORIO
					(valida_comunicacao)
				fi
			fi		
	((i++))
	done


}

main