#!/bin/bash
#
# Script para obter dados de equipamentos nas lojas ( shop, tpv, cpu, codempresa, codfilial, coditpv, tipo, franquiciado, numnfe, ambiente, impressora, firmware)
# Autor: Walter Moura
# Data Criação: 2019-04-30
# Data Modoficação: 2019-05-12
# Versão : 1.0.0
# 


function valida_comunicacao(){
    sshpass -p root ssh -o ConnectionAttempts=3 -o ConnectTimeout=1 -p10001 $ip$i exit
}

function main(){
    LOCAL="/home/diabrasil/Developers_TPVs/"
    LOG="error.getlistadototal.log"
    REL="listado_total_tiendas.txt"
    IP0="10.105.188."
    IP1="10.105.189."
    IP2="10.105.194."
    IP3="10.105.190."
    IP4="10.106.235."
    IP5="10.106.101."
    IP6="10.106.102."
    IP7="10.106.103."
    IP8="10.106.104."
    data=$(date +%y%m%d)

    # Criar o arquivo de listado mantendo sempre o anterior
    # até que a contagem de arquivos seja igual a 10
    if [ -e $REL ]; then
        COUNT=$(ls -ltr $REL* | wc -l)  
        if [ $COUNT -ge 10 ]; then
            rm $REL.1
            for i in $(seq 1 9); do mv $REL.$(( $i + 1 )) $REL.$i ; done
            mv $REL $REL.10
        else
            mv $REL $REL.$COUNT
        fi
    else    
        touch $REL
    fi

    #IP0
    i=0
    while (( i < 254 ))
        do
            ip=$IP0
            resultado=$(valida_comunicacao)
            ret=$?
            if [ $ret != 0 ]; then
                echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $LOG
            else
                tpvs=$(for x in $i ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1  $ip$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
		for y in $(seq 1 $tpvs) ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1 $ip$i -p1000$y ' . /confdia/bin/setvari ; echo "$NUMETIEN""|""$NUMECAJA""|""$(cat /confdia/version)""|""$(/usr/bin/detectpos)""|""$CODIEMPR""|""$CODIFILIAL""|""$CODIPDV""|""$(if [ -z $NUM_FRANQUICIADO ]; then echo "PROPRIA""|""0" ; else echo "FRANQUIA""|""$NUM_FRANQUICIADO";fi)""|""$NUMESERI""|""$(if [ "$TIPO_IMPR" == "DR700" ] && [ $SAT -eq 1 ] ; then echo "SAT"; elif [ "$TIPO_IMPR" == "DR700" ] || [ "$TIPO_IMPR" == "TM88" ] && [ $SAT -eq 0 ]; then echo "NFCe" ; else echo "MF";fi)""|""$(if [ "$TIPO_IMPR" == "DR700" ]; then z=$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/Auditoria_DUAL.txt | grep "#### IMPRESSORA:" | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-3) ; if [ "$z" == " D" ]; then echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed -e "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | awk -F "|" "{print $1;}")" ; else echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed -e "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | awk -F "|" "{print $1;}")" ; fi ;elif [ "$TIPO_IMPR" == "TM88" ]; then echo "TM88""|""0" ; else echo "FS700""|""0" ;fi)"'; done >> $REL
	     fi
		((i++))
    done

    #IP1
    i=0
    while (( i < 254 ))
        do
            ip=$IP1
            resultado=$(valida_comunicacao)
            ret=$?
            if [ $ret != 0 ]; then
                echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $LOG
            else
                tpvs=$(for x in $i ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1  $ip$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
		for y in $(seq 1 $tpvs) ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1 $ip$i -p1000$y ' . /confdia/bin/setvari ; echo "$NUMETIEN""|""$NUMECAJA""|""$(cat /confdia/version)""|""$(/usr/bin/detectpos)""|""$CODIEMPR""|""$CODIFILIAL""|""$CODIPDV""|""$(if [ -z $NUM_FRANQUICIADO ]; then echo "PROPRIA""|""0" ; else echo "FRANQUIA""|""$NUM_FRANQUICIADO";fi)""|""$NUMESERI""|""$(if [ "$TIPO_IMPR" == "DR700" ] && [ $SAT -eq 1 ] ; then echo "SAT"; elif [ "$TIPO_IMPR" == "DR700" ] || [ "$TIPO_IMPR" == "TM88" ] && [ $SAT -eq 0 ]; then echo "NFCe" ; else echo "MF";fi)""|""$(if [ "$TIPO_IMPR" == "DR700" ]; then z=$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/Auditoria_DUAL.txt | grep "#### IMPRESSORA:" | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-3) ; if [ "$z" == " D" ]; then echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed -e "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | awk -F "|" "{print $1;}")" ; else echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed -e "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | awk -F "|" "{print $1;}")" ; fi ;elif [ "$TIPO_IMPR" == "TM88" ]; then echo "TM88""|""0" ; else echo "FS700""|""0" ;fi)"'; done >> $REL
	     fi
		((i++))
    done

    #IP2
    i=197
    while (( i < 254 ))
        do
            ip=$IP2
            resultado=$(valida_comunicacao)
            ret=$?
            if [ $ret != 0 ]; then
                echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $LOG
            else
                tpvs=$(for x in $i ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1  $ip$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
		for y in $(seq 1 $tpvs) ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1 $ip$i -p1000$y ' . /confdia/bin/setvari ; echo "$NUMETIEN""|""$NUMECAJA""|""$(cat /confdia/version)""|""$(/usr/bin/detectpos)""|""$CODIEMPR""|""$CODIFILIAL""|""$CODIPDV""|""$(if [ -z $NUM_FRANQUICIADO ]; then echo "PROPRIA""|""0" ; else echo "FRANQUIA""|""$NUM_FRANQUICIADO";fi)""|""$NUMESERI""|""$(if [ "$TIPO_IMPR" == "DR700" ] && [ $SAT -eq 1 ] ; then echo "SAT"; elif [ "$TIPO_IMPR" == "DR700" ] || [ "$TIPO_IMPR" == "TM88" ] && [ $SAT -eq 0 ]; then echo "NFCe" ; else echo "MF";fi)""|""$(if [ "$TIPO_IMPR" == "DR700" ]; then z=$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/Auditoria_DUAL.txt | grep "#### IMPRESSORA:" | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-3) ; if [ "$z" == " D" ]; then echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed -e "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | awk -F "|" "{print $1;}")" ; else echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed -e "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | awk -F "|" "{print $1;}")" ; fi ;elif [ "$TIPO_IMPR" == "TM88" ]; then echo "TM88""|""0" ; else echo "FS700""|""0" ;fi)"'; done >> $REL
	     fi
		((i++))
    done

    #IP3
    i=0
    while (( i < 254 ))
        do
            ip=$IP3
            resultado=$(valida_comunicacao)
            ret=$?
            if [ $ret != 0 ]; then
                echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $LOG
            else
                tpvs=$(for x in $i ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1  $ip$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
		for y in $(seq 1 $tpvs) ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1 $ip$i -p1000$y ' . /confdia/bin/setvari ; echo "$NUMETIEN""|""$NUMECAJA""|""$(cat /confdia/version)""|""$(/usr/bin/detectpos)""|""$CODIEMPR""|""$CODIFILIAL""|""$CODIPDV""|""$(if [ -z $NUM_FRANQUICIADO ]; then echo "PROPRIA""|""0" ; else echo "FRANQUIA""|""$NUM_FRANQUICIADO";fi)""|""$NUMESERI""|""$(if [ "$TIPO_IMPR" == "DR700" ] && [ $SAT -eq 1 ] ; then echo "SAT"; elif [ "$TIPO_IMPR" == "DR700" ] || [ "$TIPO_IMPR" == "TM88" ] && [ $SAT -eq 0 ]; then echo "NFCe" ; else echo "MF";fi)""|""$(if [ "$TIPO_IMPR" == "DR700" ]; then z=$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/Auditoria_DUAL.txt | grep "#### IMPRESSORA:" | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-3) ; if [ "$z" == " D" ]; then echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed -e "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | awk -F "|" "{print $1;}")" ; else echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed -e "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | awk -F "|" "{print $1;}")" ; fi ;elif [ "$TIPO_IMPR" == "TM88" ]; then echo "TM88""|""0" ; else echo "FS700""|""0" ;fi)"'; done >> $REL
	     fi
		((i++))
    done

    #IP4
    i=0
    while (( i < 254 ))
        do
            ip=$IP4
            resultado=$(valida_comunicacao)
            ret=$?
            if [ $ret != 0 ]; then
                echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $LOG
            else
                tpvs=$(for x in $i ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1  $ip$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
		for y in $(seq 1 $tpvs) ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1 $ip$i -p1000$y ' . /confdia/bin/setvari ; echo "$NUMETIEN""|""$NUMECAJA""|""$(cat /confdia/version)""|""$(/usr/bin/detectpos)""|""$CODIEMPR""|""$CODIFILIAL""|""$CODIPDV""|""$(if [ -z $NUM_FRANQUICIADO ]; then echo "PROPRIA""|""0" ; else echo "FRANQUIA""|""$NUM_FRANQUICIADO";fi)""|""$NUMESERI""|""$(if [ "$TIPO_IMPR" == "DR700" ] && [ $SAT -eq 1 ] ; then echo "SAT"; elif [ "$TIPO_IMPR" == "DR700" ] || [ "$TIPO_IMPR" == "TM88" ] && [ $SAT -eq 0 ]; then echo "NFCe" ; else echo "MF";fi)""|""$(if [ "$TIPO_IMPR" == "DR700" ]; then z=$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/Auditoria_DUAL.txt | grep "#### IMPRESSORA:" | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-3) ; if [ "$z" == " D" ]; then echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed -e "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | awk -F "|" "{print $1;}")" ; else echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed -e "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | awk -F "|" "{print $1;}")" ; fi ;elif [ "$TIPO_IMPR" == "TM88" ]; then echo "TM88""|""0" ; else echo "FS700""|""0" ;fi)"'; done >> $REL
	     fi
		((i++))
    done

    #IP5
    i=0
    while (( i < 254 ))
        do
            ip=$IP5
            resultado=$(valida_comunicacao)
            ret=$?
            if [ $ret != 0 ]; then
                echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $LOG
            else
                tpvs=$(for x in $i ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1  $ip$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
		for y in $(seq 1 $tpvs) ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1 $ip$i -p1000$y ' . /confdia/bin/setvari ; echo "$NUMETIEN""|""$NUMECAJA""|""$(cat /confdia/version)""|""$(/usr/bin/detectpos)""|""$CODIEMPR""|""$CODIFILIAL""|""$CODIPDV""|""$(if [ -z $NUM_FRANQUICIADO ]; then echo "PROPRIA""|""0" ; else echo "FRANQUIA""|""$NUM_FRANQUICIADO";fi)""|""$NUMESERI""|""$(if [ "$TIPO_IMPR" == "DR700" ] && [ $SAT -eq 1 ] ; then echo "SAT"; elif [ "$TIPO_IMPR" == "DR700" ] || [ "$TIPO_IMPR" == "TM88" ] && [ $SAT -eq 0 ]; then echo "NFCe" ; else echo "MF";fi)""|""$(if [ "$TIPO_IMPR" == "DR700" ]; then z=$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/Auditoria_DUAL.txt | grep "#### IMPRESSORA:" | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-3) ; if [ "$z" == " D" ]; then echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed -e "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | awk -F "|" "{print $1;}")" ; else echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed -e "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | awk -F "|" "{print $1;}")" ; fi ;elif [ "$TIPO_IMPR" == "TM88" ]; then echo "TM88""|""0" ; else echo "FS700""|""0" ;fi)"'; done >> $REL
	     fi
		((i++))
    done

    #IP6
    i=0
    while (( i < 254 ))
        do
            ip=$IP6
            resultado=$(valida_comunicacao)
            ret=$?
            if [ $ret != 0 ]; then
                echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $LOG
            else
                tpvs=$(for x in $i ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1  $ip$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
		for y in $(seq 1 $tpvs) ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1 $ip$i -p1000$y ' . /confdia/bin/setvari ; echo "$NUMETIEN""|""$NUMECAJA""|""$(cat /confdia/version)""|""$(/usr/bin/detectpos)""|""$CODIEMPR""|""$CODIFILIAL""|""$CODIPDV""|""$(if [ -z $NUM_FRANQUICIADO ]; then echo "PROPRIA""|""0" ; else echo "FRANQUIA""|""$NUM_FRANQUICIADO";fi)""|""$NUMESERI""|""$(if [ "$TIPO_IMPR" == "DR700" ] && [ $SAT -eq 1 ] ; then echo "SAT"; elif [ "$TIPO_IMPR" == "DR700" ] || [ "$TIPO_IMPR" == "TM88" ] && [ $SAT -eq 0 ]; then echo "NFCe" ; else echo "MF";fi)""|""$(if [ "$TIPO_IMPR" == "DR700" ]; then z=$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/Auditoria_DUAL.txt | grep "#### IMPRESSORA:" | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-3) ; if [ "$z" == " D" ]; then echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed -e "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | awk -F "|" "{print $1;}")" ; else echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed -e "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | awk -F "|" "{print $1;}")" ; fi ;elif [ "$TIPO_IMPR" == "TM88" ]; then echo "TM88""|""0" ; else echo "FS700""|""0" ;fi)"'; done >> $REL
	     fi
		((i++))
    done

    #IP7
    i=0
    while (( i < 254 ))
        do
            ip=$IP7
            resultado=$(valida_comunicacao)
            ret=$?
            if [ $ret != 0 ]; then
                echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $LOG
            else
                tpvs=$(for x in $i ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1  $ip$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
		for y in $(seq 1 $tpvs) ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1 $ip$i -p1000$y ' . /confdia/bin/setvari ; echo "$NUMETIEN""|""$NUMECAJA""|""$(cat /confdia/version)""|""$(/usr/bin/detectpos)""|""$CODIEMPR""|""$CODIFILIAL""|""$CODIPDV""|""$(if [ -z $NUM_FRANQUICIADO ]; then echo "PROPRIA""|""0" ; else echo "FRANQUIA""|""$NUM_FRANQUICIADO";fi)""|""$NUMESERI""|""$(if [ "$TIPO_IMPR" == "DR700" ] && [ $SAT -eq 1 ] ; then echo "SAT"; elif [ "$TIPO_IMPR" == "DR700" ] || [ "$TIPO_IMPR" == "TM88" ] && [ $SAT -eq 0 ]; then echo "NFCe" ; else echo "MF";fi)""|""$(if [ "$TIPO_IMPR" == "DR700" ]; then z=$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/Auditoria_DUAL.txt | grep "#### IMPRESSORA:" | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-3) ; if [ "$z" == " D" ]; then echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed -e "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | awk -F "|" "{print $1;}")" ; else echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed -e "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | awk -F "|" "{print $1;}")" ; fi ;elif [ "$TIPO_IMPR" == "TM88" ]; then echo "TM88""|""0" ; else echo "FS700""|""0" ;fi)"'; done >> $REL
	     fi
		((i++))
    done

    #IP8
    i=0
    while (( i < 254 ))
        do
            ip=$IP8
            resultado=$(valida_comunicacao)
            ret=$?
            if [ $ret != 0 ]; then
                echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $LOG
            else
                tpvs=$(for x in $i ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1  $ip$x ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
		for y in $(seq 1 $tpvs) ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1 $ip$i -p1000$y ' . /confdia/bin/setvari ; echo "$NUMETIEN""|""$NUMECAJA""|""$(cat /confdia/version)""|""$(/usr/bin/detectpos)""|""$CODIEMPR""|""$CODIFILIAL""|""$CODIPDV""|""$(if [ -z $NUM_FRANQUICIADO ]; then echo "PROPRIA""|""0" ; else echo "FRANQUIA""|""$NUM_FRANQUICIADO";fi)""|""$NUMESERI""|""$(if [ "$TIPO_IMPR" == "DR700" ] && [ $SAT -eq 1 ] ; then echo "SAT"; elif [ "$TIPO_IMPR" == "DR700" ] || [ "$TIPO_IMPR" == "TM88" ] && [ $SAT -eq 0 ]; then echo "NFCe" ; else echo "MF";fi)""|""$(if [ "$TIPO_IMPR" == "DR700" ]; then z=$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/Auditoria_DUAL.txt | grep "#### IMPRESSORA:" | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-3) ; if [ "$z" == " D" ]; then echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed -e "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | awk -F "|" "{print $1;}")" ; else echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed -e "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | awk -F "|" "{print $1;}")" ; fi ;elif [ "$TIPO_IMPR" == "TM88" ]; then echo "TM88""|""0" ; else echo "FS700""|""0" ;fi)"'; done >> $REL
	     fi
		((i++))
    done

}

main
