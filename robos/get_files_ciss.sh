#!/bin/bash -x

# returnToWarehouse_req
# returnReceipt_req
# salesReceipt_req
#
#

BD="/root/srv_remoto/srv_remoto.db"

function group_tiendas(){
	ARR_LOJAS="/root/srv_remoto/arquivos_read/lojas_ciss.txt"
	while read loja;
	do
		progs=("$loja")
	done < $ARR_LOJAS
	lojas=(${progs[0]})
}

function group_datas(){
	DAT=`echo "$(date +%Y%m%d -d "-1 days")"`
	echo $DAT > .arq_data.txt
	while read dt;
	do
		progss=("$dt")
	done < .arq_data.txt
	dts=(${progss[0]})
}
function valida_comunicacao(){
	sshpass -p root ssh -o ConnectTimeout=1 -p 10001 -l root $ip exit
}

function main(){
	
	arq_1="returnToWarehouse_req.xml"
	arq_2="returnReceipt_req.xml"
	arq_3="salesReceipt_req.xml"
	SAVE_TGZ="/root/srv_remoto/tgz/"
	LOG="/root/srv_remoto/log/error.filesciss.log"
	group_tiendas
	i=0

	while [ $i != ${#lojas[@]} ]
	do
		if [ "$x" == "0" ]; then
			echo "LOJA:"${lojas[$x]}"."
		else		
			shop=`printf "%05d" ${lojas[$i]}`
			echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_IP:Consulta loja:['$shop']." >> $LOG 
			exist=`echo "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$shop';" | sqlite3 $BD`
			if [ $exist -eq 1 ]; then
				echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_IP:Consulta para a loja:['$shop'] realizada com sucesso['$exist']." >> $LOG
				ip=`echo "SELECT ip FROM tb_ip WHERE loja LIKE '$shop';" | sqlite3 $BD`
				group_datas
				k=0
				while [ $k != ${#dts[@]} ]
				do
					conn=(valida_comunicacao)
					if [ $? != 0 ]; then
						echo "$(date +%Y%m%d-%H%M%S.%s):TEST_CONN:Erro ao conectar com a loja::['$shop']." >> $LOG
					else
						touch .send_data
						chmod 777 .send_data
						echo "DATA_FILE=${dts[$k]}" > .send_data
						echo "LOJA=$shop" >> .send_data
						echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_DATE:Check data para a loja:['$shop']." >> $LOG
						sshpass -p root scp -P10001 .send_data root@$ip:/root/
						if [ $? -eq 0 ]; then
							echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_DATE:Data verificado com sucesso para a loja:['$shop']." >> $LOG
							echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Verificando se existe os arquvios para loja:['$shop']." >> $LOG
							count=`sshpass -p root ssh $ip -p10001 -l root '. .send_data; ls -ltr /confdia/ws/ciss/output/${DATA_FILE}*{returnToWarehouse_req.xml,returnReceipt_req.xml,salesReceipt_req.xml}	| wc -l'`
							if [ $count -ge 1 ]; then
								echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Arquivos vericados com sucesso para loja:['$shop']." >> $LOG
								echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Foram encontrados ['$count'] para a loja:['$shop']." >> $LOG
								sshpass -p root ssh $ip -p10001 -l root ' . .send_data; cd /confdia/ws/ciss/; mkdir support ; mv output/${DATA_FILE}*returnToWarehouse_req.xml support/ ; mv output/${DATA_FILE}*returnReceipt_req.xml support/; mv output/${DATA_FILE}*salesReceipt_req.xml support/ '
							fi 
						else
							echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_DATE:Erro ao enviar data loja:['$shop']." >> $LOG
						fi
					fi
					let "k = k +1"
				done
				echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Count arquivos loja:['$shop']." >> $LOG
				count2=`sshpass -p root ssh $ip -p10001 -l root '. .send_data; ls -ltr /confdia/ws/ciss/support/$(date +%Y%m%d -d "-1 days")*{returnToWarehouse_req.xml,returnReceipt_req.xml,salesReceipt_req.xml}	| wc -l'`
				echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Resultado count [$count2] loja:['$shop']." >> $LOG
				if [ $count2 -ge 1 ]; then
					echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_PROC:Excutando processo, gerar araquivo tgz e mover os arquivos para pesados para a loja:['$shop']." >> $LOG
					sshpass -p root ssh $ip -p10001 -l root ' . .send_data; cd /confdia/ws/ciss/support/ ; tar -czvf ${LOJA}_$(date +%Y%m%d).tgz $(date +%Y%m%d -d "-1 days")*{returnToWarehouse_req.xml,returnReceipt_req.xml,salesReceipt_req.xml} ; mv ${LOJA}_$(date +%Y%m%d).tgz /root/ ; mkdir /pesados/bkp_xml_ciss/ ; mv $(date +%Y%m%d -d "-1 days")*returnToWarehouse_req.xml /pesados/bkp_xml_ciss/ ; mv $(date +%Y%m%d -d "-1 days")*returnReceipt_req.xml /pesados/bkp_xml_ciss/ ; mv $(date +%Y%m%d -d "-1 days")*salesReceipt_req.xml /pesados/bkp_xml_ciss/'
					echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Check se o arquivo tgz foi criado com sucesso para a loja:['$shop']." >> $LOG
					file_exist=`sshpass -p root ssh $ip -p10001 -l root '. .send_data ; ls -ltr ${LOJA}_$(date +%Y%m%d).tgz | wc -l'`
					if [ $file_exist -ge 1 ]; then
						echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_TGZ:Arquivo tgz encontrado para a loja:['$shop']." >> $LOG
						echo "$(date +%Y%m%d-%H%M%S.%s):COPY_TGZ:Copiando arquivo tgz da loja:['$shop']." >> $LOG
						sshpass -p root scp -P10001 root@$ip:/root/${shop}_$(date +%Y%m%d).tgz $SAVE_TGZ
						if [ $? != 0 ]; then
							echo "$(date +%Y%m%d-%H%M%S.%s):COPY_TGZ:Erro ao copiar tgz da loja:['$shop']." >> $LOG
						else
							echo "$(date +%Y%m%d-%H%M%S.%s):DEL_TGZ:Deletar arquivo tgz da loja:['$shop']." >> $LOG
							sshpass -p root ssh $ip -p10001 -l root '. .send_data ; rm -vf ${LOJA}_$(date +%Y%m%d).tgz'
							if [ $? -eq 0 ]; then
								echo "$(date +%Y%m%d-%H%M%S.%s):DEL_TGZ:TGZ deletado com sucesso da loja:['$shop']." >> $LOG
							else
								echo "$(date +%Y%m%d-%H%M%S.%s):DEL_TGZ:Erro ao deletar TGZ da loja:['$shop']." >> $LOG
							fi
						fi
					else
						echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_TGZ:Arquivo tgz não disponivel para a loja:['$shop']." >> $LOG
					fi
				else
					echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Não existe arquivos para a loja:['$shop']." >> $LOG
				fi
			else
				echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Loja:['$shop'] não encontrada no banco de dados." >> $LOG
			fi

		fi
		let "i = i +1"
	done
}
main
