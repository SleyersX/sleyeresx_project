#!/bin/bash

IP0="10.105.188."
IP1="10.105.189."
IP2="10.105.194."
IP3="10.105.190."
IP4="10.106.235."
IP5="10.106.112."
IP6="10.106.101."
IP7="10.106.102."
IP8="10.106.103."
IP9="10.106.104."
REL="/root/srv_remoto/arquivos_read/relatorio_master_escritorio.txt"
REL2="/root/srv_remoto/arquivos_read/relatorio_master_detalhado.txt"

function fnValidaConexao(){
    
    sshpass -p root ssh -o ConnectTimeout=1 -p10001 -l root $ip$i exit
    RET=$?
    return $RET 

}

function fnNumeroLoja(){

    NUMLOJA=`sshpass -p root ssh -o ConnectTimeout=1 -p10001 -l root $ip$i ' . /confdia/bin/setvari ; echo "${NUMETIEN}" '`
    RET=$?
    return $RET

}

function fnTestaCOM1(){ 

    STCOM1=`sshpass -p root ssh -o ConnectTimeout=1 -p10001 -l root $ip$i 'tts -d /dev/ttyS0 > /tmp/.stcom.log 2>&1 ; cat /tmp/.stcom.log'`
    RET=$?
    return $RET
    
}

function fnTestaCOM4(){

    STCOM4=`sshpass -p root ssh -o ConnectTimeout=1 -p10001 -l root $ip$i 'tts -d /dev/ttyS3 > /tmp/.stcom.log 2>&1 ; cat /tmp/.stcom.log'`
    RET=$?
    return $RET

}

function fnTesteCOM5(){

    STCOM5=`sshpass -p root ssh -o ConnectTimeout=1 -p10001 -l root $ip$i 'tts -d /dev/ttyS4 > /tmp/.stcom.log 2>&1 ; cat /tmp/.stcom.log'`
    RET=$?
    return $RET

}

function main(){

    #IP0
    i=0
    while (( i < 254 ))
    do
        ip=$IP0
        fnValidaConexao
        RET=$?
        if [ $RET != 0 ]; then
            echo "Error ao conectar com o IP -> ['$ip$i'] - Err -> ['$RET']."
        else
            fnNumeroLoja
            RET=$?
            if [ $RET != 0 ]; then
                echo "Erro ao obter número da loja."
            else
                fnTestaCOM1
                RET=$?
                if [ $RET != 0 ]; then
                    echo "Erro ao obter status da COM1."
                else
                    STCOM1CONN=`echo $STCOM1 | cut -d ":" -f2`
                    if [ "$STCOM1CONN" == "NO" ]; then
                        fnTestaCOM4
                        RET=$?
                        if [ $RET != 0 ]; then
                            echo "Erro ao obter status da COM4."
                        else
                            STCOM4CONN=`echo $STCOM4 | cut -d ":" -f2`
                            fnTesteCOM5
                            RET=$?
                            if [ $RET != 0 ]; then
                                echo "Erro ao obter status da COM4."
                            else
                                STCOM5CONN=`echo $STCOM5 | cut -d ":" -f2`
                                echo "$NUMLOJA""|""$STCOM1CONN""|""$STCOM4CONN""|""$STCOM5CONN" >> $REL2
                                if [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then                                
                                    echo "$NUMLOJA""|""0" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL 
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                fi
                            fi
                        fi
                    fi 
                fi
            fi
        fi
        ((i++))
    done
	
    #IP1
    i=0
    while (( i < 254 ))
    do
        ip=$IP1
        fnValidaConexao
        RET=$?
        if [ $RET != 0 ]; then
            echo "Error ao conectar com o IP -> ['$ip$i'] - Err -> ['$RET']."
        else
            fnNumeroLoja
            RET=$?
            if [ $RET != 0 ]; then
                echo "Erro ao obter número da loja."
            else
                fnTestaCOM1
                RET=$?
                if [ $RET != 0 ]; then
                    echo "Erro ao obter status da COM1."
                else
                    STCOM1CONN=`echo $STCOM1 | cut -d ":" -f2`
                    if [ "$STCOM1CONN" == "NO" ]; then
                        fnTestaCOM4
                        RET=$?
                        if [ $RET != 0 ]; then
                            echo "Erro ao obter status da COM4."
                        else
                            STCOM4CONN=`echo $STCOM4 | cut -d ":" -f2`
                            fnTesteCOM5
                            RET=$?
                            if [ $RET != 0 ]; then
                                echo "Erro ao obter status da COM4."
                            else
                                STCOM5CONN=`echo $STCOM5 | cut -d ":" -f2`
                                echo "$NUMLOJA""|""$STCOM1CONN""|""$STCOM4CONN""|""$STCOM5CONN" >> $REL2
                                if [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then                                
                                    echo "$NUMLOJA""|""0" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL 
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                fi
                            fi
                        fi
                    fi 
                fi
            fi
        fi
        ((i++))
    done

    #IP2
    i=0
    while (( i < 254 ))
    do
        ip=$IP2
        fnValidaConexao
        RET=$?
        if [ $RET != 0 ]; then
            echo "Error ao conectar com o IP -> ['$ip$i'] - Err -> ['$RET']."
        else
            fnNumeroLoja
            RET=$?
            if [ $RET != 0 ]; then
                echo "Erro ao obter número da loja."
            else
                fnTestaCOM1
                RET=$?
                if [ $RET != 0 ]; then
                    echo "Erro ao obter status da COM1."
                else
                    STCOM1CONN=`echo $STCOM1 | cut -d ":" -f2`
                    if [ "$STCOM1CONN" == "NO" ]; then
                        fnTestaCOM4
                        RET=$?
                        if [ $RET != 0 ]; then
                            echo "Erro ao obter status da COM4."
                        else
                            STCOM4CONN=`echo $STCOM4 | cut -d ":" -f2`
                            fnTesteCOM5
                            RET=$?
                            if [ $RET != 0 ]; then
                                echo "Erro ao obter status da COM4."
                            else
                                STCOM5CONN=`echo $STCOM5 | cut -d ":" -f2`
                                echo "$NUMLOJA""|""$STCOM1CONN""|""$STCOM4CONN""|""$STCOM5CONN" >> $REL2
                                if [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then                                
                                    echo "$NUMLOJA""|""0" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL 
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                fi
                            fi
                        fi
                    fi 
                fi
            fi
        fi
        ((i++))
    done

    #IP3
    i=0
    while (( i < 254 ))
    do
        ip=$IP3
        fnValidaConexao
        RET=$?
        if [ $RET != 0 ]; then
            echo "Error ao conectar com o IP -> ['$ip$i'] - Err -> ['$RET']."
        else
            fnNumeroLoja
            RET=$?
            if [ $RET != 0 ]; then
                echo "Erro ao obter número da loja."
            else
                fnTestaCOM1
                RET=$?
                if [ $RET != 0 ]; then
                    echo "Erro ao obter status da COM1."
                else
                    STCOM1CONN=`echo $STCOM1 | cut -d ":" -f2`
                    if [ "$STCOM1CONN" == "NO" ]; then
                        fnTestaCOM4
                        RET=$?
                        if [ $RET != 0 ]; then
                            echo "Erro ao obter status da COM4."
                        else
                            STCOM4CONN=`echo $STCOM4 | cut -d ":" -f2`
                            fnTesteCOM5
                            RET=$?
                            if [ $RET != 0 ]; then
                                echo "Erro ao obter status da COM4."
                            else
                                STCOM5CONN=`echo $STCOM5 | cut -d ":" -f2`
                                echo "$NUMLOJA""|""$STCOM1CONN""|""$STCOM4CONN""|""$STCOM5CONN" >> $REL2
                                if [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then                                
                                    echo "$NUMLOJA""|""0" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL 
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                fi
                            fi
                        fi
                    fi 
                fi
            fi
        fi
        ((i++))
    done
	
    #IP4
    i=0
    while (( i < 254 ))
    do
        ip=$IP4
        fnValidaConexao
        RET=$?
        if [ $RET != 0 ]; then
            echo "Error ao conectar com o IP -> ['$ip$i'] - Err -> ['$RET']."
        else
            fnNumeroLoja
            RET=$?
            if [ $RET != 0 ]; then
                echo "Erro ao obter número da loja."
            else
                fnTestaCOM1
                RET=$?
                if [ $RET != 0 ]; then
                    echo "Erro ao obter status da COM1."
                else
                    STCOM1CONN=`echo $STCOM1 | cut -d ":" -f2`
                    if [ "$STCOM1CONN" == "NO" ]; then
                        fnTestaCOM4
                        RET=$?
                        if [ $RET != 0 ]; then
                            echo "Erro ao obter status da COM4."
                        else
                            STCOM4CONN=`echo $STCOM4 | cut -d ":" -f2`
                            fnTesteCOM5
                            RET=$?
                            if [ $RET != 0 ]; then
                                echo "Erro ao obter status da COM4."
                            else
                                STCOM5CONN=`echo $STCOM5 | cut -d ":" -f2`
                                echo "$NUMLOJA""|""$STCOM1CONN""|""$STCOM4CONN""|""$STCOM5CONN" >> $REL2
                                if [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then                                
                                    echo "$NUMLOJA""|""0" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL 
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                fi
                            fi
                        fi
                    fi 
                fi
            fi
        fi
        ((i++))
    done
	
    #IP5
    i=0
    while (( i < 254 ))
    do
        ip=$IP5
        fnValidaConexao
        RET=$?
        if [ $RET != 0 ]; then
            echo "Error ao conectar com o IP -> ['$ip$i'] - Err -> ['$RET']."
        else
            fnNumeroLoja
            RET=$?
            if [ $RET != 0 ]; then
                echo "Erro ao obter número da loja."
            else
                fnTestaCOM1
                RET=$?
                if [ $RET != 0 ]; then
                    echo "Erro ao obter status da COM1."
                else
                    STCOM1CONN=`echo $STCOM1 | cut -d ":" -f2`
                    if [ "$STCOM1CONN" == "NO" ]; then
                        fnTestaCOM4
                        RET=$?
                        if [ $RET != 0 ]; then
                            echo "Erro ao obter status da COM4."
                        else
                            STCOM4CONN=`echo $STCOM4 | cut -d ":" -f2`
                            fnTesteCOM5
                            RET=$?
                            if [ $RET != 0 ]; then
                                echo "Erro ao obter status da COM4."
                            else
                                STCOM5CONN=`echo $STCOM5 | cut -d ":" -f2`
                                echo "$NUMLOJA""|""$STCOM1CONN""|""$STCOM4CONN""|""$STCOM5CONN" >> $REL2
                                if [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then                                
                                    echo "$NUMLOJA""|""0" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL 
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                fi
                            fi
                        fi
                    fi 
                fi
            fi
        fi
        ((i++))
    done

    #IP6
    i=0
    while (( i < 254 ))
    do
        ip=$IP6
        fnValidaConexao
        RET=$?
        if [ $RET != 0 ]; then
            echo "Error ao conectar com o IP -> ['$ip$i'] - Err -> ['$RET']."
        else
            fnNumeroLoja
            RET=$?
            if [ $RET != 0 ]; then
                echo "Erro ao obter número da loja."
            else
                fnTestaCOM1
                RET=$?
                if [ $RET != 0 ]; then
                    echo "Erro ao obter status da COM1."
                else
                    STCOM1CONN=`echo $STCOM1 | cut -d ":" -f2`
                    if [ "$STCOM1CONN" == "NO" ]; then
                        fnTestaCOM4
                        RET=$?
                        if [ $RET != 0 ]; then
                            echo "Erro ao obter status da COM4."
                        else
                            STCOM4CONN=`echo $STCOM4 | cut -d ":" -f2`
                            fnTesteCOM5
                            RET=$?
                            if [ $RET != 0 ]; then
                                echo "Erro ao obter status da COM4."
                            else
                                STCOM5CONN=`echo $STCOM5 | cut -d ":" -f2`
                                echo "$NUMLOJA""|""$STCOM1CONN""|""$STCOM4CONN""|""$STCOM5CONN" >> $REL2
                                if [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then                                
                                    echo "$NUMLOJA""|""0" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL 
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                fi
                            fi
                        fi
                    fi 
                fi
            fi
        fi
        ((i++))
    done
	
    #IP7
    i=0
    while (( i < 254 ))
    do
        ip=$IP7
        fnValidaConexao
        RET=$?
        if [ $RET != 0 ]; then
            echo "Error ao conectar com o IP -> ['$ip$i'] - Err -> ['$RET']."
        else
            fnNumeroLoja
            RET=$?
            if [ $RET != 0 ]; then
                echo "Erro ao obter número da loja."
            else
                fnTestaCOM1
                RET=$?
                if [ $RET != 0 ]; then
                    echo "Erro ao obter status da COM1."
                else
                    STCOM1CONN=`echo $STCOM1 | cut -d ":" -f2`
                    if [ "$STCOM1CONN" == "NO" ]; then
                        fnTestaCOM4
                        RET=$?
                        if [ $RET != 0 ]; then
                            echo "Erro ao obter status da COM4."
                        else
                            STCOM4CONN=`echo $STCOM4 | cut -d ":" -f2`
                            fnTesteCOM5
                            RET=$?
                            if [ $RET != 0 ]; then
                                echo "Erro ao obter status da COM4."
                            else
                                STCOM5CONN=`echo $STCOM5 | cut -d ":" -f2`
                                echo "$NUMLOJA""|""$STCOM1CONN""|""$STCOM4CONN""|""$STCOM5CONN" >> $REL2
                                if [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then                                
                                    echo "$NUMLOJA""|""0" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL 
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                fi
                            fi
                        fi
                    fi 
                fi
            fi
        fi
        ((i++))
    done
	
    #IP8
    i=0
    while (( i < 254 ))
    do
        ip=$IP8
        fnValidaConexao
        RET=$?
        if [ $RET != 0 ]; then
            echo "Error ao conectar com o IP -> ['$ip$i'] - Err -> ['$RET']."
        else
            fnNumeroLoja
            RET=$?
            if [ $RET != 0 ]; then
                echo "Erro ao obter número da loja."
            else
                fnTestaCOM1
                RET=$?
                if [ $RET != 0 ]; then
                    echo "Erro ao obter status da COM1."
                else
                    STCOM1CONN=`echo $STCOM1 | cut -d ":" -f2`
                    if [ "$STCOM1CONN" == "NO" ]; then
                        fnTestaCOM4
                        RET=$?
                        if [ $RET != 0 ]; then
                            echo "Erro ao obter status da COM4."
                        else
                            STCOM4CONN=`echo $STCOM4 | cut -d ":" -f2`
                            fnTesteCOM5
                            RET=$?
                            if [ $RET != 0 ]; then
                                echo "Erro ao obter status da COM4."
                            else
                                STCOM5CONN=`echo $STCOM5 | cut -d ":" -f2`
                                echo "$NUMLOJA""|""$STCOM1CONN""|""$STCOM4CONN""|""$STCOM5CONN" >> $REL2
                                if [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then                                
                                    echo "$NUMLOJA""|""0" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL 
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                fi
                            fi
                        fi
                    fi 
                fi
            fi
        fi
        ((i++))
    done
	
    #IP9
    i=0
    while (( i < 254 ))
    do
        ip=$IP9
        fnValidaConexao
        RET=$?
        if [ $RET != 0 ]; then
            echo "Error ao conectar com o IP -> ['$ip$i'] - Err -> ['$RET']."
        else
            fnNumeroLoja
            RET=$?
            if [ $RET != 0 ]; then
                echo "Erro ao obter número da loja."
            else
                fnTestaCOM1
                RET=$?
                if [ $RET != 0 ]; then
                    echo "Erro ao obter status da COM1."
                else
                    STCOM1CONN=`echo $STCOM1 | cut -d ":" -f2`
                    if [ "$STCOM1CONN" == "NO" ]; then
                        fnTestaCOM4
                        RET=$?
                        if [ $RET != 0 ]; then
                            echo "Erro ao obter status da COM4."
                        else
                            STCOM4CONN=`echo $STCOM4 | cut -d ":" -f2`
                            fnTesteCOM5
                            RET=$?
                            if [ $RET != 0 ]; then
                                echo "Erro ao obter status da COM4."
                            else
                                STCOM5CONN=`echo $STCOM5 | cut -d ":" -f2`
                                echo "$NUMLOJA""|""$STCOM1CONN""|""$STCOM4CONN""|""$STCOM5CONN" >> $REL2
                                if [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then                                
                                    echo "$NUMLOJA""|""0" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL 
                                elif [ "$STCOM1CONN" == "NO" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "YES" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "NO" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                elif [ "$STCOM1CONN" == "YES" ] && [ "$STCOM4CONN" == "YES" ] && [ "$STCOM5CONN" == "NO" ]; then
                                    echo "$NUMLOJA""|""1" >> $REL
                                fi
                            fi
                        fi
                    fi 
                fi
            fi
        fi
        ((i++))
    done
}

if [ -e $REL ]; then
    mv $REL $REL.old
    mv $REL2 $REL2.old
fi

main
