#!/bin/bash
#
#Autor      : Walter Moura(wam001br)
#Data       : 2019-02-23
#Modificado : 2019-02-23
#
#

function valida_comunicacao(){

	ssh -o ConnectTimeout=1 $IP$I -p10001 exit

}
function main(){

	LOG="/root/srv_remoto/log/error.getnetport.log"
	REL="/root/srv_remoto/arquivos_read/relatorio_netport.txt"
	IP0="10.105.188."
	IP1="10.105.189."
	IP2="10.105.194."
	IP3="10.106.235."
	IP4="10.106.101."
	IP5="10.106.102."
	IP6="10.106.104."
	IP7="10.105.190."
	IP8="10.106.103."
	
	I=0
	while (( I < 254 ))
	do
		IP=$IP0
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOG
		else
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Conexão com sucesso IP:$IP$I.Return => $RETURN ." >> $LOG
			ssh -o ConnectTimeout=1 -p10001 $IP$I '. /confdia/bin/setvari ; echo "${NUMETIEN}""|""$(ping -q -w5 -c1 10.10.10.16 > /dev/null && echo 1 || echo 0 )""|""$(ping -q -w5 -c1 10.10.10.36 > /dev/null && echo 1 || echo 0 )"' >> $REL  
			RETURN=$?
			if [ $RETURN != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):EXEC_CMD:Erro ao executar comando:$IP$I.Return => $RETURN ." >> $LOG
			else
				echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Comando executado com sucesso:$IP$I.Return => $RETURN ." >> $LOG
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP1
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOG
		else
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Conexão com sucesso IP:$IP$I.Return => $RETURN ." >> $LOG
			ssh -o ConnectTimeout=1 -p10001 $IP$I '. /confdia/bin/setvari ; echo "${NUMETIEN}""|""$(ping -q -w5 -c1 10.10.10.16 > /dev/null && echo 1 || echo 0 )""|""$(ping -q -w5 -c1 10.10.10.36 > /dev/null && echo 1 || echo 0 )"' >> $REL  
			RETURN=$?
			if [ $RETURN != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):EXEC_CMD:Erro ao executar comando:$IP$I.Return => $RETURN ." >> $LOG
			else
				echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Comando executado com sucesso:$IP$I.Return => $RETURN ." >> $LOG
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP2
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOG
		else
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Conexão com sucesso IP:$IP$I.Return => $RETURN ." >> $LOG
			ssh -o ConnectTimeout=1 -p10001 $IP$I '. /confdia/bin/setvari ; echo "${NUMETIEN}""|""$(ping -q -w5 -c1 10.10.10.16 > /dev/null && echo 1 || echo 0 )""|""$(ping -q -w5 -c1 10.10.10.36 > /dev/null && echo 1 || echo 0 )"' >> $REL  
			RETURN=$?
			if [ $RETURN != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):EXEC_CMD:Erro ao executar comando:$IP$I.Return => $RETURN ." >> $LOG
			else
				echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Comando executado com sucesso:$IP$I.Return => $RETURN ." >> $LOG
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP3
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOG
		else
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Conexão com sucesso IP:$IP$I.Return => $RETURN ." >> $LOG
			ssh -o ConnectTimeout=1 -p10001 $IP$I '. /confdia/bin/setvari ; echo "${NUMETIEN}""|""$(ping -q -w5 -c1 10.10.10.16 > /dev/null && echo 1 || echo 0 )""|""$(ping -q -w5 -c1 10.10.10.36 > /dev/null && echo 1 || echo 0 )"' >> $REL  
			RETURN=$?
			if [ $RETURN != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):EXEC_CMD:Erro ao executar comando:$IP$I.Return => $RETURN ." >> $LOG
			else
				echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Comando executado com sucesso:$IP$I.Return => $RETURN ." >> $LOG
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP4
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOG
		else
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Conexão com sucesso IP:$IP$I.Return => $RETURN ." >> $LOG
			ssh -o ConnectTimeout=1 -p10001 $IP$I '. /confdia/bin/setvari ; echo "${NUMETIEN}""|""$(ping -q -w5 -c1 10.10.10.16 > /dev/null && echo 1 || echo 0 )""|""$(ping -q -w5 -c1 10.10.10.36 > /dev/null && echo 1 || echo 0 )"' >> $REL  
			RETURN=$?
			if [ $RETURN != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):EXEC_CMD:Erro ao executar comando:$IP$I.Return => $RETURN ." >> $LOG
			else
				echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Comando executado com sucesso:$IP$I.Return => $RETURN ." >> $LOG
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP5
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOG
		else
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Conexão com sucesso IP:$IP$I.Return => $RETURN ." >> $LOG
			ssh -o ConnectTimeout=1 -p10001 $IP$I '. /confdia/bin/setvari ; echo "${NUMETIEN}""|""$(ping -q -w5 -c1 10.10.10.16 > /dev/null && echo 1 || echo 0 )""|""$(ping -q -w5 -c1 10.10.10.36 > /dev/null && echo 1 || echo 0 )"' >> $REL  
			RETURN=$?
			if [ $RETURN != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):EXEC_CMD:Erro ao executar comando:$IP$I.Return => $RETURN ." >> $LOG
			else
				echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Comando executado com sucesso:$IP$I.Return => $RETURN ." >> $LOG
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP6
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOG
		else
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Conexão com sucesso IP:$IP$I.Return => $RETURN ." >> $LOG
			ssh -o ConnectTimeout=1 -p10001 $IP$I '. /confdia/bin/setvari ; echo "${NUMETIEN}""|""$(ping -q -w5 -c1 10.10.10.16 > /dev/null && echo 1 || echo 0 )""|""$(ping -q -w5 -c1 10.10.10.36 > /dev/null && echo 1 || echo 0 )"' >> $REL  
			RETURN=$?
			if [ $RETURN != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):EXEC_CMD:Erro ao executar comando:$IP$I.Return => $RETURN ." >> $LOG
			else
				echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Comando executado com sucesso:$IP$I.Return => $RETURN ." >> $LOG
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP7
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOG
		else
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Conexão com sucesso IP:$IP$I.Return => $RETURN ." >> $LOG
			ssh -o ConnectTimeout=1 -p10001 $IP$I '. /confdia/bin/setvari ; echo "${NUMETIEN}""|""$(ping -q -w5 -c1 10.10.10.16 > /dev/null && echo 1 || echo 0 )""|""$(ping -q -w5 -c1 10.10.10.36 > /dev/null && echo 1 || echo 0 )"' >> $REL  
			RETURN=$?
			if [ $RETURN != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):EXEC_CMD:Erro ao executar comando:$IP$I.Return => $RETURN ." >> $LOG
			else
				echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Comando executado com sucesso:$IP$I.Return => $RETURN ." >> $LOG
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP8
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOG
		else
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Conexão com sucesso IP:$IP$I.Return => $RETURN ." >> $LOG
			ssh -o ConnectTimeout=1 -p10001 $IP$I '. /confdia/bin/setvari ; echo "${NUMETIEN}""|""$(ping -q -w5 -c1 10.10.10.16 > /dev/null && echo 1 || echo 0 )""|""$(ping -q -w5 -c1 10.10.10.36 > /dev/null && echo 1 || echo 0 )"' >> $REL  
			RETURN=$?
			if [ $RETURN != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):EXEC_CMD:Erro ao executar comando:$IP$I.Return => $RETURN ." >> $LOG
			else
				echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Comando executado com sucesso:$IP$I.Return => $RETURN ." >> $LOG
			fi
		fi
		((I++))
	done

}
main
