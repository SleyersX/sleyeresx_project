#!/bin/bash

function valida_comunicacao(){

    ssh -o ConnectTimeout=1 -p10001 $IP$i exit
}

function main(){
	IP1="10.105.190."
	IP2="10.106.103."
	REL="list_print_ecf.txt"
	LOG="error.getlistprintecf.log"
	# Criar o arquivo de listado mantendo sempre o anterior
    # até que a contagem de arquivos seja igual a 10
    if [ -e $REL ]; then
        COUNT=$(ls -ltr $REL* | wc -l)  
        if [ $COUNT -ge 10 ]; then
            rm $REL.1
            for i in $(seq 1 9); do mv $REL.$(( $i + 1 )) $REL.$i ; done
            mv $REL $REL.10
        else
            mv $REL $REL.$COUNT
        fi
    else    
        touch $REL
    fi

    i=0
    while(( i < 254 ))
    do
    	IP=$IP1
    	RESULT=$(valida_comunicacao)
    	RETURN=$?
    	if [ $RETURN != 0 ]; then
    		echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $LOG
    	else
    		ecf=$(for x in $i ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1  $IP$x ' . /confdia/bin/setvari ; echo "${TIPO_IMPR}"' ; done)
    		if [ "$ecf" == "FISCAL" ]; then
    			tpvs=$(for z in $i ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1  $IP$z ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
    			for y in $(seq 1 $tpvs); do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1 $IP$i -p1000$y '. /confdia/bin/setvari ; echo "$NUMETIEN""|""$NUMECAJA""|""$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*Auditoria_ECF.txt | grep "fnRetornarInformacao_ECF(78, DR" | tail -n1 | sed "s/.\{60\}//" | sed "s/)//")"' ; done >> $REL
    		fi
    	fi
    	((i++))
    done

    i=0
    while(( i < 254 ))
    do
    	IP=$IP2
    	RESULT=$(valida_comunicacao)
    	RETURN=$?
    	if [ $RETURN != 0 ]; then
    		echo "$(date +%Y%m%d"-"%H%M%S):Nao foi possivel conectar ao IP:$ip$i" >> $LOG
    	else
    		ecf=$(for x in $i ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1  $IP$x ' . /confdia/bin/setvari ; echo "${TIPO_IMPR}"' ; done)
    		if [ "$ecf" == "FISCAL" ]; then
    			tpvs=$(for z in $i ; do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1  $IP$z ' . /confdia/bin/setvari ; echo "${NUMETPVS}"' ; done)
    			for y in $(seq 1 $tpvs); do sshpass -p root ssh -o ConnectionAttempts=1 -o ConnectTimeout=1 $IP$i -p1000$y '. /confdia/bin/setvari ; echo "$NUMETIEN""|""$NUMECAJA""|""$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*Auditoria_ECF.txt | grep "fnRetornarInformacao_ECF(78, DR" | tail -n1 | sed "s/.\{60\}//" | sed "s/)//")"' ; done >> $REL
    		fi
    	fi
    	((i++))
    done		
}
main