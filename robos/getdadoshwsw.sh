#!/bin/bash
# Programa gerar um relatório com dados das lojas por HW e SW
# 
# 
# Author: Walter Moura
# Data Criação: 15/11/2019
# Modificado por: Walter Moura
# Data Modificação: 15/11/2019

# Varáveis Globais
BD="/root/srv_remoto/srv_remoto.db"
PATH_USER="/root/srv_remoto"
ARQSHOPS="/root/srv_remoto/arquivos_read/list_tiendas.txt"
LOG="/root/srv_remoto/log/error.getdadoshwsw.log"
REL="/root/srv_remoto/arquivos_read/relatorio_hwsw.txt"
PATH_REL="/root/srv_remoto/arquivos_read"
ARQNAME="relatorio_hwsw.txt"
ARQARRAYFILES="root/srv_remoto/arquivos_read/array_fileshwsw.txt"

function main(){
    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Inicinando o programa." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Iniciando função ['fnGroupShops']." >> $LOG
    fnCheckRelatorio
    RETURN=$?
    if [ $RETURN -eq 0 ]; then
        fnGroupShops
        echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Verificamos o retorno da função ['fnGroupShops']." >> $LOG
        if [ $RETURN -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Função ['fnGroupShops'] executado com sucesso [$RETURN]." >> $LOG
            i=0
            echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Inicializando array de lojas." >> $LOG
            while [ $i != ${#ARRAYSHOPS[@]} ]
            do
                if [[ ${ARRAYSHOPS[i]} = ?(+|-)+([0-9]) ]]; then
                    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Valor ['${ARRAYSHOPS[i]}'] é do tipo número, incluímos 0 a esquerda." >> $LOG
                    SHOP=${ARRAYSHOPS[i]}
                else
                    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Valor ['${ARRAYSHOPS[i]}'] não é do tipo númerico, será ignorado." >> $LOG 
                fi
                fnGetIP
                if [ $RETURN -eq 0 ]; then
                    fnValidaComunicacao
                    if [ $RETURN -eq 0 ]; then
                        for x in $(seq 1 $tpvs); do sshpass -p root ssh -o ConnectTimeout=1 -p1000$x $ip -l root '. /confdia/bin/setvari ; 
                            IFS="|"; 
                            x=`cat /srv/Debian6.0/srv/servidorDFW/ficcaje/Auditoria_SAT.txt | grep -i "Resposta com sucesso." | tail -n1 | sed "s/.\{31\}//" | cut -d "|" -f1` ; 
                            if [ -z $x ]; then 
                                echo "0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0" 
                            else 
                                cat /srv/Debian6.0/srv/servidorDFW/ficcaje/Auditoria_SAT.txt | grep -i "Resposta com sucesso." | tail -n1 | sed "s/.\{31\}//" | cut -d "|" -f6,7,8,9,10,11,12,13,14,16,17,19,20,26,27
                            fi | while read serie rede ip mac mask gw dns1 dns2 status sdcard ocupado firmware layaut dativacao dfativacao ; 
                                    do 
                                        echo "$NUMETIEN""|""$NUMECAJA""|""$(cat /confdia/version | cut -c 4-21)""|""$(/usr/bin/detectpos)""|""$CODIEMPR""|""$CODIFILIAL""|""$CODIPDV""|""$(if [ -z $NUM_FRANQUICIADO ]; then echo "$MASTERFRANQUICIA""|""0" ; else echo "$MASTERFRANQUICIA""|""$NUM_FRANQUICIADO";fi)""|""$NUMESERI""|""$(if [ "$TIPO_IMPR" == "DR700" ] && [ $SAT -eq 1 ] ; then echo "SAT"; elif [ "$TIPO_IMPR" == "DR700" ] || [ "$TIPO_IMPR" == "TM88" ] && [ $SAT -eq 0 ]; then echo "NFCe" ; else echo "MF" ; fi)""|""$serie""|""$rede""|""$ip""|""$mac""|""$mask""|""$gw""|""$dns1""|""$dns2""|""$status""|""$sdcard""|""$ocupado""|""$firmware""|""$layaut""|""$dativacao""|""$dfativacao""|""$(if [ "$TIPO_IMPR" == "DR700" ]; then z=`cat /srv/Debian6.0/srv/servidorDFW/ficcaje/Auditoria_DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-2` ; if [ "$z" ==  "D" ] ; then echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | cut -d "|" -f1)""|""$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" |  cut -d "|" -f2)"; else echo "$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" |  cut -d "|" -f1)""|""$(cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" |  cut -d "|" -f2)"; fi ;elif [ "$TIPO_IMPR" == "TM88" ]; then echo "TM88""|""0";else echo "FS700""|""0" ; fi)"; 
                                    done';
                            RETURN=$?
                            if [ $RETURN != 0 ]; then
                                echo "$SHOP""|""0$x""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0""|""0"
                            fi 
                        done >> $REL
                        RETURN=$?
                        if [ $RETURN != 0 ]; then
                            echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Erro ao executar comando para a loja -> ['$SHOP']:['$ip']." >> $LOG
                        fi
                    else
                        echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Erro ao executar comando para a loja -> ['$SHOP']:['$ip']." >> $LOG
                    fi
                else
                    echo "$(date +%Y%m%d-%H%M%S.%s):MIAN:Erro ao obter ip para a loja -> ['$SHOP']." >> $LOG
                fi
                let "i = i +1"
            done
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Função ['fnGroupShops'] executada com erro ['$RETURN']." >> $LOG
        fi
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Função ['fnCheckRelatorio'] executada com erro ['$RETURN']." >> $LOG
    fi
}

function fnGroupShops(){
    if [ -e $ARQSHOPS ]; then
        while read idshop;
            do
                progress=("$idshop")
        done < $ARQSHOPS
        ARRAYSHOPS=(${progress[0]})
        RETURN=0
    else
        RETURN=1
    fi
    return $RETURN
}

function fnGetIP(){
    EXISTE=`echo "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
    if [ $EXISTE -eq 1 ]; then
        ip=`echo "SELECT ip FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
        tpvs=`echo "SELECT tpvs FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
        RETURN=0
    else
        RETURN=1
    fi
    return $RETURN
}

function fnValidaComunicacao(){
    sshpass -p root ssh -o ConnectTimeout=1 -p10001 -l root $ip exit
    RETURN=$?
    if [ $RETURN -eq 0 ]; then
        RETURN=0
    else
        RETURN=1
    fi
    return $RETURN
}

function fnCheckRelatorio(){

    echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Iniciando a função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Verifica se existe o diretorio ['$PATH_REL']." >> $LOG
    if [ -e $PATH_REL ]; then
        RETURN$?
        echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Diretório OK." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Verifica se existe o arquivo ['$ARQNAME']." >> $LOG
        if [ -e $REL ]; then
            RETURN$?
            echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Arquivo OK." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Verifica a quntidades de arquivos [$ARQNAME] dentro do diretorio ['$PATH_REL']." >> $LOG
            COUNTBKP=`ls -ltr $PATH_REL/$ARQNAME* | wc -l`
            RETURN$?
            echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Quantidade de arquivos econtrados -> ['$COUNTBKP']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Testamos se o valor retornado é maior que 0." >> $LOG
            if [ $COUNTBKP -eq 0 ]; then
                RETURN$?
                echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Valor retornado igual a 0." >> $LOG
                echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Movendo ['$REL'] -> ['$REL.0']." >> $LOG
                mv $REL $REL.0
                RETURN$?
            else
                RETURN$?
                echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Valor retornado maior que 0." >> $LOG
                echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Verificamos se valor retornado é igual a 100." >> $LOG
                if [ $COUNTBKP -eq 11 ]; then
                    RETURN$?
                    echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Valor retornado igual a 10." >> $LOG
                    cd $PATH_REL
                    RETURN$?
                    FILESTEMP=`ls -tr $ARQNAME*`
                    RETURN$?
                    echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Preenchemos a variável ['FILESTEMP'] com um ['ls $ARQNAME*'] ." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:FILESTEMP -> ['$FILESTEMP']." >> $LOG
                    echo $FILESTEMP > $ARQARRAYFILES
                    RETURN$?
                    echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Passamos os valores da varável ['FILESTEMP'] -> ['$ARQARRAYFILES']." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Iniciamos um while read n arquivo ['$ARQARRAYFILES'] e gravamos cada valor em um array." >> $LOG
                    while read filename;
                        do
                            progs=("$filename")
                    done < $ARQARRAYFILES
                    ARRAYFILES=(${progs[0]})
                    RETURN$?
                    echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:ARRAY Files -> ['$ARRAYFILES']." >> $LOG
                    i=0
                    x=1
                    echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Iniciamos um while no array ['ARRAYFILES']" >> $LOG
                    while [ $i != ${#ARRAYFILES[@]} ]
                    do
                        if [ $i -eq 0 ]; then
                            echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Removendo o arquivo ['${ARRAYFILES[i]}']." >> $LOG
                            rm -f ${ARRAYFILES[i]}
                            echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Renomendo o arquivo ['${ARRAYFILES[x]}'] -> ['${ARRAYFILES[i]}']." >> $LOG
                            mv -f ${ARRAYFILES[x]} ${ARRAYFILES[i]}
                        else
                            echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Renomendo o arquivo ['${ARRAYFILES[x]}'] -> ['${ARRAYFILES[i]}']." >> $LOG
                            mv -f ${ARRAYFILES[x]} ${ARRAYFILES[i]}
                        fi
                        let "i = i +1"
                        let "x = x +1"
                        if [ $i -eq 10 ]; then
                            break
                        fi 
                    done
                    RETURN$?
                    echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Movendo o arquivo ['$ARQRET'] -> ['$PATH_BKP/$ARQNAME.10']." >> $LOG
                    mv $REL $REL.10
                    RETURN$?
                else
                    echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Valor é menor que 10." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Movendo o arquivo ['$REL'] -> ['$REL.$COUNTBKP']." >> $LOG    
                    mv $REL $REL.$COUNTBKP
                    RETURN$?
                fi
            fi
        fi
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Diretório NOK." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Criando diretório ['$PATH_REL']." >> $LOG
        cd $PATH_USER
        RETURN$?
        echo "$(date +%Y%m%d-%H%M%S.%s):CHECKRELATORIO:Movendo o arquivo ['$REL'] -> ['$REL.0']." >> $LOG
        mv $REL $REL.0
        RETURN$?
    fi
    cd $PATH_USER
    RETURN=$?

    return $RETURN
}

set -x
if [ -e $PATH_USER/log/getdadoshwsw.log ]; then
    cp $PATH_USER/log/getdadoshwsw.log $PATH_USER/getdadoshwsw.log.1
fi
exec 2> $PATH_USER/log/getdadoshwsw.log
main