#!/bin/bash
#====================================================
# Autor            : Walter Moura
# Data Criação     : 2019-02-20
# Data Modificação : 2019-02-20
#
# Scpript para desativar a mensagem de status da 
# impressora de cupom fidelização na rede de lojas.
# Mudar IMPRCUPO=1 e CUPOPARA=1 ambos para 0
# 
# Para caso seja reintalado, faço uma validação
# do IMPRCUPO atual no pdv
#
#=====================================================

#Variáveis
LOG="/root/srv_remoto/log/error.impfindelizacao.log"
BD="/root/srv_remoto/srv_remoto.db"
function group_shop(){

	while read loja; 
	do
		progs=("$loja")
	done < /root/srv_remoto/arquivos_read/list_lojas.txt
	tiendas=(${progs[0]})
}

function group_ip(){

	while read ip; 
	do
		progss=("$ip")
	done < /root/srv_remoto/arquivos_read/list_ip.txt
	ips=(${progss[0]})
}

function valida_comunicacao(){

    ssh -o ConnectTimeout=1 ${ips[$i]} exit

}

function main(){

    group_shop
    group_ip
    i=0
    while [ $i != ${#tiendas[@]} ]
    do
        if [ "$x" == "0" ]; then
            echo "LOJA:"${tiendas[$x]}"."
        else
            d_desativacao=$(sqlite3 $BD \ "SELECT MAX(data) FROM tb_imp_fidelizacao WHERE loja LIKE '"${tiendas[$i]}"'; ")
            d_atual=$(date +%Y%m%d)
            if [ -n "$d_desativacao" ]; then
                if [ $d_atual -eq $d_desativacao ] || [ $d_atual -ge $d_desativacao ]; then
                    conn=$(valida_comunicacao)
                    return=$?
                    if [ $return != 0 ]; then
                        echo "$(date +%Y%m%d-%H%M%S.%s):CONN_SHOP:Falha ao conectar com a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                        echo "$(date +%Y%m%d-%H%M%S.%s):CONN_SHOP:Return=>'$return'." >> $LOG
                    else
                        echo "$(date +%Y%m%d-%H%M%S.%s):CONN_SHOP:Conexão com sucesso com a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                        echo "$(date +%Y%m%d-%H%M%S.%s):CONN_SHOP:Return=>'$return'." >> $LOG
                        n_tpvs=$(ssh ${ips[$i]} '. /confdia/bin/setvari ; echo "${NUMETPVS}"')
                        return=$?
                        if [ $return != 0 ]; then
                            echo "$(date +%Y%m%d-%H%M%S.%s):GET_TPVS:Falha ao conectar com a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                            echo "$(date +%Y%m%d-%H%M%S.%s):GET_TPVS:Return=>'$return'." >> $LOG
                        else
                            imprcupo=$(ssh ${ips[$i]} '. /confdia/bin/setvari ; echo "${IMPRCUPO}"')
                            return=$?
                            if [ $return != 0 ]; then
                                echo "$(date +%Y%m%d-%H%M%S.%s):GET_IMPR:Falha ao conectar com a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                                echo "$(date +%Y%m%d-%H%M%S.%s):GET_IMPR:Return=>'$return'." >> $LOG
                            else
                                if [ $imprcupo -eq 0 ]; then
                                    echo "$(date +%Y%m%d-%H%M%S.%s):GET_IMPR:Ação não necessária para a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                                    echo "$(date +%Y%m%d-%H%M%S.%s):GET_IMPR:Return=>'$return'." >> $LOG
                                else
                                    echo "$(date +%Y%m%d-%H%M%S.%s):GET_TPVS:Dados obtidos com sucesso loja:['${tiendas[$i]}'].['${ips[$i]}'][$n_tpvs]." >> $LOG
                                    echo "$(date +%Y%m%d-%H%M%S.%s):GET_TPVS:Return=>'$return'." >> $LOG
                                    exec_cmd=$(for y in $(seq 1 $n_tpvs); do if [ $y -eq 2 ] 
                                        then 
                                            echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Ignorando o caixa 2." >> $LOG ; 
                                        else 
                                            ssh ${ips[$i]} -p1000$y ' sed -e 's/CUPOPARA=1/CUPOPARA=0/' -e 's/IMPRCUPO=1/IMPRCUPO=0/' /confdia/bin/setvari > /confdia/bin/setvari.new ; mv /confdia/bin/setvari.new /confdia/bin/setvari ' ; 
                                        fi ; 
                                    done)
                                    return=$?
                                    if [ $return != 0 ]; then
                                        echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Falha ao executar comando na loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                                        echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Return=>'$return'." >> $LOG
                                    else
                                        echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Comando executado com sucesso na loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                                        echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Return=>'$return'." >> $LOG
                                    fi
                                fi
                            fi
                        fi
                    fi
                else
                    echo "$(date +%Y%m%d-%H%M%S.%s):GET_DATE:Data para a loja ${tiendas[$i]}.['${ips[$i]}'] maior que a data atual." >> $LOG
                fi
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):GET_CMD:Loja ${tiendas[$i]}.['${ips[$i]}'] sem data de desatiação da impressora." >> $LOG
            fi
        fi
        let "i = i +1"
    done
}

main
