#!/bin/bash
#====================================================
# Autor            : Walter Moura
# Data Criação     : 2019-02-20
# Data Modificação : 2019-07-04
#
# Scpript para desativar a mensagem de status da 
# impressora de cupom fidelização na rede de lojas.
# Mudar IMPRCUPO=1 e CUPOPARA=1 ambos para 0
# 
# Para caso seja reintalado, faço uma validação
# do IMPRCUPO atual no pdv
#
#=====================================================

#Variáveis
LOG="/root/srv_remoto/log/error.impfidelizacao.log"
BD="/root/srv_remoto/srv_remoto.db"
ARRAY_LOJAS="/root/srv_remoto/arquivos_read/list_tiendas.txt"
ARRAY_IPS="/root/srv_remoto/arquivos_read/list_ipss.txt"
function group_shop(){

	while read loja; 
	do
		progs=("$loja")
	     done < $ARRAY_LOJAS 
	tiendas=(${progs[0]})
}

function group_ip(){

	while read ip; 
	do
		progss=("$ip")
	done < $ARRAY_IPS
	ips=(${progss[0]})
}

function valida_comunicacao(){

  sshpass -p root ssh -o ConnectTimeout=1 ${ips[$i]} exit

}

function main(){

    group_shop
    group_ip
    i=0
    while [ $i != ${#tiendas[@]} ]
    do
        if [ "$x" == "0" ]; then
            echo "LOJA:"${tiendas[$x]}"."
        else
            d_desativacao=$(sqlite3 $BD \ "SELECT MAX(data) FROM tb_imp_fidelizacao WHERE loja LIKE '"${tiendas[$i]}"'; ")
            d_atual=$(date +%Y%m%d)
            if [ -n "$d_desativacao" ]; then
                if [ $d_atual -eq $d_desativacao ] || [ $d_atual -ge $d_desativacao ]; then
                    conn=$(valida_comunicacao)
                    return=$?
                    if [ $return != 0 ]; then
                        echo "$(date +%Y%m%d-%H%M%S.%s):CONN_SHOP:Falha ao conectar com a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                        echo "$(date +%Y%m%d-%H%M%S.%s):CONN_SHOP:Return=>'$return'." >> $LOG
                    else
                        echo "$(date +%Y%m%d-%H%M%S.%s):CONN_SHOP:Conexão com sucesso com a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                        echo "$(date +%Y%m%d-%H%M%S.%s):CONN_SHOP:Return=>'$return'." >> $LOG
                        n_tpvs=$(sshpass -p root ssh -o ConnectTimeout=1 ${ips[$i]} '. /confdia/bin/setvari ; echo "${NUMETPVS}"')
                        return=$?
                        if [ $return != 0 ]; then
                            echo "$(date +%Y%m%d-%H%M%S.%s):GET_TPVS:Falha ao conectar com a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                            echo "$(date +%Y%m%d-%H%M%S.%s):GET_TPVS:Return=>'$return'." >> $LOG
                        else
                            franquia=$(sshpass -p root ssh -o ConnectTimeout=1 ${ips[$i]} '. /confdia/bin/setvari ; echo "${TIPOTIEN}"')
                            returnt=$?
                            if [ $returnt -eq 1 ]; then
                                echo "$(date +%Y%m%d-%H%M%S.%s):TIPO_TIEN:Falha ao conectar com a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                                echo "$(date +%Y%m%d-%H%M%S.%s):TIPO_TIEN:Return=>'$returnt'." >> $LOG
                            else
                                if [ $franquia -eq 01 ]; then
                                    echo "$(date +%Y%m%d-%H%M%S.%s):TIPO_TIEN:Loja:['${tiendas[$i]}'].['${ips[$i]}'] Franquia." >> $LOG
                                    echo "$(date +%Y%m%d-%H%M%S.%s):TIPO_TIEN:Return=>'$returnt'." >> $LOG
                                    echo "$(date +%Y%m%d-%H%M%S.%s):GET_TPVS:Dados obtidos com sucesso loja:['${tiendas[$i]}'].['${ips[$i]}'][$n_tpvs]." >> $LOG
                                    echo "$(date +%Y%m%d-%H%M%S.%s):GET_TPVS:Return=>'$return'." >> $LOG
                                    echo "$(for y in $(seq 1 $n_tpvs); do if [ $y -eq 2 ] 
                                        then 
                                            sshpass -p root ssh -o ConnectTimeout=1 ${ips[$i]} -p1000$y ' . /confdia/bin/setvari ; 
                                            if [ ${IMPRCUPO} -eq 1 ] ; then
                                                echo "$(date +%Y%m%d-%H%M%S.%s):GET_IMPR:Ação necessária para a loja:['${tiendas[$i]}'].['${ips[$i]}'].['1000$y']."
                                                sed -e 's/CUPOPARA=1/CUPOPARA=0/' -e 's/IMPRCUPO=1/IMPRCUPO=0/' /confdia/bin/setvari > /confdia/bin/setvari.new ; mv /confdia/bin/setvari.new /confdia/bin/setvari;
                                            else
                                                echo "$(date +%Y%m%d-%H%M%S.%s):GET_IMPR:Ação não necessária para a loja:['${tiendas[$i]}'].['${ips[$i]}'].['1000$y']." ;
                                            fi' ; 
                                        else 
                                            sshpass -p root ssh -o ConnectTimeout=1 ${ips[$i]} -p1000$y ' . /confdia/bin/setvari ; 
                                            if [ ${IMPRCUPO} -eq 1 ] ; then
                                                echo "$(date +%Y%m%d-%H%M%S.%s):GET_IMPR:Ação necessária para a loja:['${tiendas[$i]}'].['${ips[$i]}'].['1000$y']."
                                                sed -e 's/CUPOPARA=1/CUPOPARA=0/' -e 's/IMPRCUPO=1/IMPRCUPO=0/' /confdia/bin/setvari > /confdia/bin/setvari.new ; mv /confdia/bin/setvari.new /confdia/bin/setvari;
                                            else
                                                echo "$(date +%Y%m%d-%H%M%S.%s):GET_IMPR:Ação não necessária para a loja:['${tiendas[$i]}'].['${ips[$i]}'].['1000$y']." ;
                                            fi' ; 
                                        fi ; 
                                    done)" >> $LOG
                                    return=$?
                                    if [ $return != 0 ]; then
                                            echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Falha ao executar comando na loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                                            echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Return=>'$return'." >> $LOG
                                        else
                                            echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Comando executado com sucesso na loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                                            echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Return=>'$return'." >> $LOG
                                    fi
                                else
                                    if [ ${tiendas[$i]} -eq 09999 ]; then
                                        echo "$(date +%Y%m%d-%H%M%S.%s):TIPO_TIEN:Loja:['${tiendas[$i]}'].['${ips[$i]}'] Especial Presidente." >> $LOG
                                        echo "$(date +%Y%m%d-%H%M%S.%s):TIPO_TIEN:Loja:['${tiendas[$i]}'].['${ips[$i]}'] Propria." >> $LOG
                                        echo "$(date +%Y%m%d-%H%M%S.%s):TIPO_TIEN:Return=>'$returnt'." >> $LOG
                                        echo "$(date +%Y%m%d-%H%M%S.%s):GET_TPVS:Dados obtidos com sucesso loja:['${tiendas[$i]}'].['${ips[$i]}'][$n_tpvs]." >> $LOG
                                        echo "$(date +%Y%m%d-%H%M%S.%s):GET_TPVS:Return=>'$return'." >> $LOG
                                        echo "$(for y in $(seq 1 $n_tpvs); do if [ $y -eq 0 ] 
                                                then 
                                                    echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Ignorando o caixa 2." >> $LOG ; 
                                                else 
                                                    sshpass -p root ssh -o ConnectTimeout=1 ${ips[$i]} -p1000$y ' . /confdia/bin/setvari ; 
                                                    if [ ${IMPRCUPO} -eq 1 ] ; then
                                                        echo "$(date +%Y%m%d-%H%M%S.%s):GET_IMPR:Ação necessária para a loja:['${tiendas[$i]}'].['${ips[$i]}'].['1000$y']."
                                                            sed -e 's/CUPOPARA=1/CUPOPARA=0/' -e 's/IMPRCUPO=1/IMPRCUPO=0/' /confdia/bin/setvari > /confdia/bin/setvari.new ; mv /confdia/bin/setvari.new /confdia/bin/setvari;
                                                    else
                                                        echo "$(date +%Y%m%d-%H%M%S.%s):GET_IMPR:Ação não necessária para a loja:['${tiendas[$i]}'].['${ips[$i]}'].['1000$y']." ;
                                                    fi' ; 
                                                fi ; 
                                            done)" >> $LOG
                                            return=$?
                                            if [ $return != 0 ]; then
                                                    echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Falha ao executar comando na loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                                                    echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Return=>'$return'." >> $LOG
                                                else
                                                    echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Comando executado com sucesso na loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                                                    echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Return=>'$return'." >> $LOG
                                            fi
                                    else
                                        echo "$(date +%Y%m%d-%H%M%S.%s):TIPO_TIEN:Loja:['${tiendas[$i]}'].['${ips[$i]}'] Propria." >> $LOG
                                        echo "$(date +%Y%m%d-%H%M%S.%s):TIPO_TIEN:Return=>'$returnt'." >> $LOG
                                        echo "$(date +%Y%m%d-%H%M%S.%s):GET_TPVS:Dados obtidos com sucesso loja:['${tiendas[$i]}'].['${ips[$i]}'][$n_tpvs]." >> $LOG
                                        echo "$(date +%Y%m%d-%H%M%S.%s):GET_TPVS:Return=>'$return'." >> $LOG
                                        echo "$(for y in $(seq 1 $n_tpvs); do sshpass -p root ssh -o ConnectTimeout=1 ${ips[$i]} -p1000$y ' . /confdia/bin/setvari ; if [ ${IMPRCUPO} -eq 1 ] ; then
                                                    echo "$(date +%Y%m%d-%H%M%S.%s):GET_IMPR:Ação necessária para a loja:['${tiendas[$i]}'].['${ips[$i]}'].['1000$y']."
                                                    sed -e 's/CUPOPARA=1/CUPOPARA=0/' -e 's/IMPRCUPO=1/IMPRCUPO=0/' /confdia/bin/setvari > /confdia/bin/setvari.new ; mv /confdia/bin/setvari.new /confdia/bin/setvari;
                                                else
                                                    echo "$(date +%Y%m%d-%H%M%S.%s):GET_IMPR:Ação não necessária para a loja:['${tiendas[$i]}'].['${ips[$i]}'].['1000$y'].";
                                                fi' ; 
                                        done)" >> $LOG
                                        return=$?
                                        if [ $return != 0 ]; then
                                                echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Falha ao executar comando na loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                                                echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Return=>'$return'." >> $LOG
                                        else
                                                echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Comando executado com sucesso na loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                                                echo "$(date +%Y%m%d-%H%M%S.%s):EXEC_CMD:Return=>'$return'." >> $LOG
                                        fi
                                    fi
                                fi
                            fi

                        fi
                    fi
                else
                    echo "$(date +%Y%m%d-%H%M%S.%s):GET_DATE:Data para a loja ${tiendas[$i]}.['${ips[$i]}'] maior que a data atual." >> $LOG
                fi
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):GET_CMD:Loja ${tiendas[$i]}.['${ips[$i]}'] sem data de desativação da impressora." >> $LOG
            fi
        fi
        let "i = i +1"
    done
}
main
