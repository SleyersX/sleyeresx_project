#!/bin/bash
#====================================================#
# Autor             : Walter Moura
# Data Criacao      : 2020-05-13
# Data Modificacao  : 2020-05-13
#
#
#=====================================================#

LOG="/root/srv_remoto/log/error.install-monitor-sat.log"
FILE1="/root/srv_remoto/tgz/monitor-sat.tgz"

function group_shop(){

	while read loja; 
	do
		progs=("$loja")
	     done < /root/srv_remoto/arquivos_read/list_tiendas.txt 
	tiendas=(${progs[0]})
}

function group_ip(){

	while read ip; 
	do
		progss=("$ip")
	done < /root/srv_remoto/arquivos_read/list_ipss.txt
	ips=(${progss[0]})
}

function valida_comunicacao(){

  sshpass -p root ssh -o ConnectTimeout=1 ${ips[$i]} exit

}

function main(){

	group_shop
	group_ip
	i=0
	while [ $i != ${#tiendas[@]} ]
	do
		if [ "$x" == "0" ]; then
			echo "LOJA:"${tiendas[$x]}"."
		else
			d_envio=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT DATE_FORMAT(MAX(date_send), '%Y%m%d') FROM tb_install_monitor_sat WHERE shop = '"${tiendas[$i]}"' AND date_send IS NOT NULL")
			d_instalacao=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT DATE_FORMAT(MAX(date_install), '%Y%m%d') FROM tb_install_monitor_sat WHERE shop = '"${tiendas[$i]}"' AND date_install IS NOT NULL")
			d_atual=$(date +%Y%m%d)
			if [ -n "$d_envio" ]; then
				if [ $d_atual -eq $d_envio ] || [ $d_atual -ge $d_envio ]; then
					conn=$(valida_comunicacao)
					return=$?
					if [ $return != 0 ]; then
						echo "$(date +%Y%m%d-%H%M%S.%s):CONN_SHOP:Falha ao conectar com a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
						echo "$(date +%Y%m%d-%H%M%S.%s):CONN_SHOP:Return => '$return'." >> $LOG
					else
						echo "$(date +%Y%m%d-%H%M%S.%s):CONN_SHOP:Conectado com sucesso a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
						echo "$(date +%Y%m%d-%H%M%S.%s):CONN_SHOP:Return => '$return'." >> $LOG
						n_tpvs=$(sshpass -p root ssh -o ConnectTimeout=1 ${ips[$i]} '. /confdia/bin/setvari; echo "${NUMETPVS}"')
						for y in $(seq 1 $n_tpvs); do sshpass -p root ssh -o ConnectTimeout=1 ${ips[$i]} -p1000$y '. /confdia/bin/setvari ; if [ -e "/confdia/descargas/monitor-sat.tgz" ]; then echo "${NUMECAJA}""|""1"; else echo "${NUMECAJA}""|""0" ; fi' ; done >> resultado.txt
						return=$?
						if [ $return -eq 0 ]; then
							echo "${d_instalacao}" > data_install.txt
							z=0
							echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILE:OK para a loja ['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
							t=`awk -F "|" '{print $1 ;}' resultado.txt | while read line ; do printf "%s\t" $line; done ` 
							s=`awk -F "|" '{print $2 ;}' resultado.txt | while read line ; do printf "%s\t" $line; done `
							echo $t > tmp_tpv.txt 
							echo $s > tmp_st.txt
							while read tp; 
							do
								prog=("$tp")
							done < tmp_tpv.txt
							array_tpv=(${prog[0]})
							while read sts; 
							do
								pro=("$sts")
							done < tmp_st.txt
							array_st=(${pro[0]})
							k=0
							while [ $k != ${#array_tpv[@]} ]
							do
								if [ "$j" == "0" ]; then
									echo "Caixa:"${array_tpv[$j]}"."
								else
									if [ ${array_st[$k]} -eq 0 ]; then
										echo "$(date +%Y%m%d-%H%M%S.%s):SEND_FILE:Enviando para a loja ['${tiendas[$i]}'].['${array_tpv[$k]}'].['${ips[$i]}']." >> $LOG
										sshpass -p root scp -P100${array_tpv[$k]} $FILE1 root@${ips[$i]}:/confdia/descargas/
										return=$?
										if [ $return -eq 0 ]; then
											echo "$(date +%Y%m%d-%H%M%S.%s):SEND_FILE:Arquivo enviado para a loja ['${tiendas[$i]}'].['${array_tpv[$k]}'].['${ips[$i]}']." >> $LOG
											sshpass -p root scp -P100${array_tpv[$k]} data_install.txt root@${ips[$i]}:/tmp/
											return=$?
											if [ $return -eq 0 ]; then
												if [ $d_atual -eq $d_instalacao ] || [ $d_atual -ge $d_instalacao ]; then
													sshpass -p root ssh -l root -p100${array_tpv[$k]} ${ips[$i]} 'tar -xzvf /confdia/descargas/monitor-sat.tgz -C /'
													z=$(( $z + 1 ))
                                                    mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_install_monitor_sat SET tpvs = '$z' WHERE shop = '${tiendas[$i]}';" 
													return=$?
													if [ $return -eq 0 ]; then
														echo "$(date +%Y%m%d-%H%M%S.%s):ACT_BD:Loja ['${tiendas[$i]}'].['${ips[$i]}'] atualizada com sucesso no banco de dados." >> $LOG
														sshpass -p root ssh -l root -p100${array_tpv[$k]} ${ips[$i]} '/etc/init.d/watchdog-monitor-sat start'
														return=$?
														if [ $return -eq 0 ]; then
															echo "$(date +%Y%m%d-%H%M%S.%s):START_SERVICE:Loja ['${tiendas[$i]}'].['${ips[$i]}'] serviço inicializado com sucesso." >> $LOG
														else
															echo "$(date +%Y%m%d-%H%M%S.%s):START_SERVICE:Loja ['${tiendas[$i]}'].['${ips[$i]}'] erro ao inicializar serviço." >> $LOG
														fi
													else
														echo "$(date +%Y%m%d-%H%M%S.%s):ACT_BD:Falha ao atualizar loja ['${tiendas[$i]}'].['${ips[$i]}'] no banco de dados." >> $LOG
													fi 
												else
													echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_DATE:Data ['$d_envio'] de instalação para a loja ['${tiendas[$i]}'].['${ips[$i]}'] maior que a data atual." >> $LOG
												fi
											fi
										else
											echo "$(date +%Y%m%d-%H%M%S.%s):SEND_FILE:Falha ao enviar para a loja ['${tiendas[$i]}'].['${array_tpv[$k]}'].['${ips[$i]}']." >> $LOG
										fi
									else
										echo "$(date +%Y%m%d-%H%M%S.%s):SEND_FILE:Arquivo já existe para a loja ['${tiendas[$i]}'].['${array_tpv[$k]}'].['${ips[$i]}']." >> $LOG
										WATCHDOG=`sshpass -p root ssh -l root -p100${array_tpv[$k]} ${ips[$i]} 'ls -ltr /etc/init.d/watchdog-monitor-sat | wc -l'`
										if [ $WATCHDOG -eq 0 ]; then
											echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILE:Monitor SAT não ativada para a loja ['${tiendas[$i]}'].['${array_tpv[$k]}'].['${ips[$i]}']." >> $LOG
											echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_DATE:Verificando data de instalação para a loja ['${tiendas[$i]}'].['${array_tpv[$k]}'].['${ips[$i]}']." >> $LOG
											if [ $d_atual -eq $d_instalacao ] || [ $d_atual -ge $d_instalacao ]; then
												sshpass -p root ssh -l root -p100${array_tpv[$k]} ${ips[$i]} 'tar -xzvf /confdia/descargas/monitor-sat.tgz -C /'
												z=$(( $z + 1 ))
												mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_install_monitor_sat SET tpvs = '$z' WHERE shop = '${tiendas[$i]}';" 
												return=$?
												if [ $return -eq 0 ]; then
													echo "$(date +%Y%m%d-%H%M%S.%s):ACT_BD:Loja ['${tiendas[$i]}'].['${ips[$i]}'] atualizada com sucesso no banco de dados." >> $LOG
													sshpass -p root ssh -l root -p100${array_tpv[$k]} ${ips[$i]} '/etc/init.d/watchdog-monitor-sat start'
													return=$?
													if [ $return -eq 0 ]; then
														echo "$(date +%Y%m%d-%H%M%S.%s):START_SERVICE:Loja ['${tiendas[$i]}'].['${ips[$i]}'] serviço inicializado com sucesso." >> $LOG
													else
														echo "$(date +%Y%m%d-%H%M%S.%s):START_SERVICE:Loja ['${tiendas[$i]}'].['${ips[$i]}'] erro ao inicializar serviço." >> $LOG
													fi
												else
													echo "$(date +%Y%m%d-%H%M%S.%s):ACT_BD:Falha ao atualizar loja ['${tiendas[$i]}'].['${ips[$i]}'] no banco de dados." >> $LOG
												fi 
											else
												echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_DATE:Data ['$d_envio'] de instalação para a loja ['${tiendas[$i]}'].['${ips[$i]}'] maior que a data atual." >> $LOG
											fi
										else
											echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILE:Monitor SAT já está ativada para a loja ['${tiendas[$i]}'].['${array_tpv[$k]}'].['${ips[$i]}']." >> $LOG
                                            z=$(( $z + 1 ))
                                            mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_install_monitor_sat SET tpvs = '$z' WHERE shop = '${tiendas[$i]}';" 
                                            return=$?
                                            if [ $return -eq 0 ]; then
                                                echo "$(date +%Y%m%d-%H%M%S.%s):ACT_BD:Loja ['${tiendas[$i]}'].['${ips[$i]}'] atualizada com sucesso no banco de dados." >> $LOG
												sshpass -p root ssh -l root -p100${array_tpv[$k]} ${ips[$i]} '/etc/init.d/watchdog-monitor-sat start'
												return=$?
												if [ $return -eq 0 ]; then
													echo "$(date +%Y%m%d-%H%M%S.%s):START_SERVICE:Loja ['${tiendas[$i]}'].['${ips[$i]}'] serviço inicializado com sucesso." >> $LOG
												else
													echo "$(date +%Y%m%d-%H%M%S.%s):START_SERVICE:Loja ['${tiendas[$i]}'].['${ips[$i]}'] erro ao inicializar serviço." >> $LOG
												fi
                                            else
                                                echo "$(date +%Y%m%d-%H%M%S.%s):ACT_BD:Falha ao atualizar loja ['${tiendas[$i]}'].['${ips[$i]}'] no banco de dados." >> $LOG
                                            fi
										fi	
									fi
									let "k = k +1"
								fi
							done
							return=$?
							if [ $return -eq 0 ]; then
								rm -vf resultado.txt
								rm -vf tmp_st.txt
								rm -vf tmp_tpv.txt
							fi
                            ntpvs_install=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT tpvs FROM tb_install_monitor_sat WHERE shop = '${tiendas[$i]}'"`
                            if [ $ntpvs_install -eq $n_tpvs ]; then
                                echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Todos os PDVs loja ['${tiendas[$i]}'].['${ips[$i]}'] estão com Monitor SAT instalado." >> $LOG
                                mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_install_monitor_sat SET status = 'Instalado' WHERE shop = '${tiendas[$i]}';" 
                                return=$?
                                if [ $return -eq 0 ]; then
                                    echo "$(date +%Y%m%d-%H%M%S.%s):ACT_BD:Loja ['${tiendas[$i]}'].['${ips[$i]}'] atualizada com sucesso no banco de dados." >> $LOG
                                else
                                    echo "$(date +%Y%m%d-%H%M%S.%s):ACT_BD:Falha ao atualizar loja ['${tiendas[$i]}'].['${ips[$i]}'] no banco de dados." >> $LOG
                                fi
                            else
                                echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Ainda há PDVs pendente para a loja ['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
                            fi   
						else
							echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILE:Erro ao detectar arquivo para a loja ['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
						fi
					fi
				else
					echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_DATE:Data ['$d_envio'] de envio para a loja ['${tiendas[$i]}'].['${ips[$i]}'] maior que a data atual." >> $LOG
				fi
			else
				echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_DATE:Loja ['${tiendas[$i]}'].['${ips[$i]}'] nao possui data de instalacao.)" >> $LOG
			fi
		let "i = i +1"
	fi
done

}
main