#!/bin/bash
#====================================================#
# Autor             : Walter Moura
# Data Criacao      : 2019-08-06
# Data Modificacao  : 2019-08-06
#
#
#=====================================================#

#Variaveis

LOG="/root/srv_remoto/log/error.tcgertec2-0.log"
BD="/root/srv_remoto/srv_remoto.db"
FILE1="/root/srv_remoto/tgz/tcgertec2-0.tgz"
function group_shop(){

	while read loja; 
	do
		progs=("$loja")
	     done < /root/srv_remoto/arquivos_read/list_tiendas.txt 
	tiendas=(${progs[0]})
}

function group_ip(){

	while read ip; 
	do
		progss=("$ip")
	done < /root/srv_remoto/arquivos_read/list_ipss.txt
	ips=(${progss[0]})
}

function valida_comunicacao(){

  sshpass -p root ssh -o ConnectTimeout=1 ${ips[$i]} exit

}

function main(){

	group_shop
	group_ip
	i=0
	while [ $i != ${#tiendas[@]} ]
	do
		if [ "$x" == "0" ]; then
			echo "LOJA:"${tiendas[$x]}"."
		else
			d_instalacao=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT DATE_FORMAT(MAX(fecha_envio), '%Y%m%d') FROM tb_verificador WHERE tienda = '"${tiendas[$i]}"' AND versao = '2' AND fecha_envio IS NOT NULL")
			d_atual=$(date +%Y%m%d)
			if [ -n "$d_instalacao" ]; then
				if [ $d_atual -eq $d_instalacao ] || [ $d_atual -ge $d_instalacao ]; then
					conn=$(valida_comunicacao)
					return=$?
					if [ $return != 0 ]; then
						echo "$(date +%Y%m%d-%H%M%S.%s):CONN_SHOP:Falha ao conectar com a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
						echo "$(date +%Y%m%d-%H%M%S.%s):CONN_SHOP:Return => '$return'." >> $LOG
					else
						echo "$(date +%Y%m%d-%H%M%S.%s):CONN_SHOP:Conectado com sucesso a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
						echo "$(date +%Y%m%d-%H%M%S.%s):CONN_SHOP:Return => '$return'." >> $LOG
						file_exist=$(sshpass -p root ssh -o ConnectTimeout=1 ${ips[$i]} 'if [ -e "/confdia/descargas/tcgertec2-0.tgz" ]; then echo 1; else echo 0 ; fi')
						return=$?
						if [ $return != 0 ]; then
								echo "$(date +%Y%m%d-%H%M%S.%s):FILE_EXIST:Erro ao buscar arquivo:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
								echo "$(date +%Y%m%d-%H%M%S.%s):FILE_EXIST:Return => '$return'." >> $LOG
						else
							echo "$(date +%Y%m%d-%H%M%S.%s):FILE_EXIST:Busca arquivo realizado com sucesso:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
							echo "$(date +%Y%m%d-%H%M%S.%s):FILE_EXIST:Return => '$return'." >> $LOG
							if [ $file_exist -eq 1 ]; then
								echo "$(date +%Y%m%d-%H%M%S.%s):FILE_EXIST:Arquivo já existe para a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
								status=$(sshpass -p root ssh -l root -p10001 ${ips[$i]} 'if [ -e "/etc/init.d/TCgertec" ]; then echo 1; else echo 0 ; fi')
								if [ $status -eq 0 ];then
									echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:TCgertec já está instalada para a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
								else
									echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:Instalando TCgertec para a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
									sshpass -p root ssh -p10001 root@${ips[$i]} '/etc/init.d/TCgertec stop'
									sshpass -p root ssh -p10001 root@${ips[$i]} 'rm -vf /etc/init.d/TCgertec'
									sshpass -p root ssh -l root -p10001 ${ips[$i]} 'tar -xzvf /confdia/descargas/tcgertec2-0.tgz -C /'
									return=$?
									if [ $return -eq 0 ]; then
										echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:TCgertec instalado com sucesso para a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
										echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:Return => '$return'." >> $LOG
									else
										echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:Falha ao instalar TCgertec para a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
										echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:Return => '$return'." >> $LOG
									fi
									sshpass -p root ssh -p10001 -l root ${ips[$i]} 'bash -x /root/TCgertec.sh restart'
									return=$?
									if [ $return -eq 0 ]; then
										echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_START:TCgertec iniciado com sucesso para a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
										echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_START:Return => '$return'." >> $LOG
									else
										echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_START:Falha ao iniciar TCgertec para a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
										echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_START:Return => '$return'." >> $LOG
									fi
								fi
							else
								echo "$(date +%Y%m%d-%H%M%S.%s):FILE_EXIST:Arquivo não existe para a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
								echo "$(date +%Y%m%d-%H%M%S.%s):FILE_EXIST:Enviando arquivo:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
								sshpass -p root ssh -p10001 root@${ips[$i]} '/etc/init.d/TCgertec stop'
								sshpass -p root ssh -p10001 root@${ips[$i]} 'rm -vf /etc/init.d/TCgertec'
								sshpass -p root scp -P10001 $FILE1 root@${ips[$i]}:/confdia/descargas/
								return=$?
								if [ $return -eq 0 ]; then
									echo "$(date +%Y%m%d-%H%M%S.%s):SEND_FILE:Arquivo enviado com sucesso:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
									echo "$(date +%Y%m%d-%H%M%S.%s):SEND_FILE:Return => '$return'." >> $LOG
									sshpass -p root ssh -p10001 -l root ${ips[$i]} 'tar -xzvf /confdia/descargas/tcgertec2-0.tgz -C /'
									return=$?
									if [ $return -eq 0 ]; then
										echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:TCgertec instalado com sucesso para a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
										echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:Return => '$return'." >> $LOG
									else
										echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:Falha ao instalar TCgertec para a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
										echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_INSTALL:Return => '$return'." >> $LOG
									fi
									sshpass -p root ssh -p10001 -l root ${ips[$i]} 'bash -x /root/TCgertec.sh restart'
									return=$?
									if [ $return -eq 0 ]; then
										echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_START:TCgertec iniciado com sucesso para a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
										echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_START:Return => '$return'." >> $LOG
									else
										echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_START:Falha ao iniciar TCgertec para a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
										echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_START:Return => '$return'." >> $LOG
									fi
								else
									echo "$(date +%Y%m%d-%H%M%S.%s):SEND_FILE:Falha ao enviar arquivo para a loja:['${tiendas[$i]}'].['${ips[$i]}']." >> $LOG
									echo "$(date +%Y%m%d-%H%M%S.%s):SEND_FILE:Return => '$return'." >> $LOG
								fi
							fi
						fi
					fi
				else
					echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_DATE:Data ['$d_instalacao'] para loja ['${tiendas[$i]}'].['${ips[$i]}'] maior que a data atual." >> $LOG
				fi
			else
				echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_DATE:Loja ['${tiendas[$i]}'].['${ips[$i]}'] nao possui data de instalacao.)" >> $LOG
			fi
		let "i = i +1"
	fi
done

}
main
