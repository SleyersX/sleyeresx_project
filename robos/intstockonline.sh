#!/bin/bash
#
# Criado por: Walter Moura(wam001br)
# Data Criação: 24.03.2020
# Modificado por: Walter Moura
# Data moficação: 25.03.2020
#
# Serviço para gerar um listado com stock online das lojas, com corte de 15/30 min e enviar os dados via FTP
# a um servidor de banco de dados, se encarregara de ler os dados e fazer o input no banco de dados.
#
# Version 1.0
# Ler as seguintes tabelas do PDV:
# - MAEARTI1
# - Family
# - SubFamily
# - Section
# - ACUARTI1

. /confdia/bin/setvari

PATH_USER="/root/outquery"
BD="/confdia/movil/catalog.db"
OUTFILE="$PATH_USER/list_stock"
NAMEFILE="list_stock"
SHOP=${NUMETIEN}
PATH_BKP="$PATH_USER/backup"
ARQARRAYFILES=".temp_files.txt"
OUTQUERY="$PATH_USER/.query_stock"

function main(){

	check_files
	query_items
	ajust_query

}

function check_files(){

	echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Verificando se o diretorio ['$PATH_USER'] existe."
	if [ -e $PATH_USER ]; then
		echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Diretorio ['$PATH_USER'] já existe."
	else
		echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Diretorio ['$PATH_USER'] não existe."
		mkdir outquery
		RETURN=$?
		if [ $RETURN -eq 0 ]; then
			echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Diretorio ['$PATH_USER'] criado com sucesso."
		else
			echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Ocorreu um erro ao criar o diretorio ['$PATH_USER']."
		fi
	fi

	if [ -e $PATH_BKP ]; then
		COUNTBKP=`ls -ltr $PATH_BKP/$NAMEFILE* | wc -l`
		if [ $COUNTBKP -eq 0 ]; then
			mv -vf $OUTFILE* $PATH_BKP/$NAMEFILE.0
		else
			if [ $COUNTBKP -eq 101 ]; then
				cd $PATH_BKP
				FILESTEMP=`ls -tr $NAMEFILE*`
				echo $FILESTEMP > $ARQARRAYFILES
				while read filename;
				do
					progress=("$filename")
				done < $ARQARRAYFILES
				ARQARRAYFILES=(${progress[0]})
				i=0
				x=1
				while [ $i != ${#ARQARRAYFILES[@]} ]
				do
					if [ $i -eq 0 ]; then
						rm -vf ${ARQARRAYFILES[i]}
						mv -vf ${ARQARRAYFILES[x]} ${ARQARRAYFILES[i]}
					else
						mv -vf ${ARQARRAYFILES[x]} ${ARQARRAYFILES[i]}
					fi
					let "i = i +1"
					let "x = x +1"
					if [ $i -eq 100 ]; then
						break
					fi
				done
				mv -vf $OUTFILE* $PATH_BKP/$NAMEFILE.100
			else
				mv -vf $OUTFILE* $PATH_BKP/$NAMEFILE.$COUNTBKP
			fi
		fi
	else
		cd $PATH_USER
		mkdir backup
		mv -vf $OUTFILE* $PATH_BKP/$NAMEFILE.0
	fi
	cd $PATH_USER
}

function query_items(){

	echo	"SELECT 
				MAEARTI1.Codi, 
				MAEARTI1.Desc, 
				printf('%.2f', MAEARTI1.Pvp_0_) AS PVP_Tarifa, 
				printf('%.2f', MAEARTI1.Pvp_4_) AS PVP_NFidelizado, 
				printf('%.2f', MAEARTI1.Pvp_2_) AS PVP_Fidelizado, 
				Section.Description AS Secao, 
				Family.Description AS Familia, 
				SubFamily.Description AS SubFamilia, 
				MAEARTI1.TipoTrat, 
				printf('%d', ACUARTI1.StocActuUnid) AS StockActUnid, 
				printf('%.4f', ACUARTI1.StocActuKilo) AS StockActKilo 
			FROM 
				(MAEARTI1 
					INNER JOIN ACUARTI1 ON MAEARTI1.Codi LIKE ACUARTI1.Codi) 
					INNER JOIN Family ON (MAEARTI1.TipoFami LIKE Family.FamilyID ) 
					INNER JOIN SubFamily ON (MAEARTI1.SubFami LIKE SubFamily.SubFamilyID) 
					INNER JOIN Section ON (MAEARTI1.CodiSecc LIKE Section.SectionID)
			;" | sqlite3 $BD -noheader > $OUTQUERY
}

function ajust_query(){

	DATA_QUERY=$(date +%Y%m%d-%H%M%S)
	echo "Shop""|""Date""|""Codi""|""Desc""|""PVP_Tarifa""|""PVP_NFidelizado""|""PVP_Fidelizado""|""Secao""|""Familia""|""SubFamilia""|""TipoTrat""|""StockActUnid""|""StockActKilo" > $OUTFILE
	IFS="|"
	while read Codi Desc PVP_Tarifa PVP_NFidelizado PVP_Fidelizado Secao Familia SubFamilia TipoTrat StockActUnid StockActKilo;
	do
		if [ $TipoTrat -eq 0 ]; then
			echo "$SHOP""|""$DATA_QUERY""|""$Codi""|""$Desc""|""$PVP_Tarifa""|""$PVP_NFidelizado""|""$PVP_Fidelizado""|""$Secao""|""$Familia""|""$SubFamilia""|""$TipoTrat""|""$StockActUnid""|""0" >> $OUTFILE
		elif [ $TipoTrat -eq 1 ]; then
			echo "$SHOP""|""$DATA_QUERY""|""$Codi""|""$Desc""|""$PVP_Tarifa""|""$PVP_NFidelizado""|""$PVP_Fidelizado""|""$Secao""|""$Familia""|""$SubFamilia""|""$TipoTrat""|""0""|""$StockActKilo" >> $OUTFILE
		fi
	done < $OUTQUERY

}

function send_file(){

	echo "Em desenvolvimento...."
}

main