#!/bin/bash
#
#Autor      : Walter Moura(wam001br)
#Data       : 2018-12-28
#Modificado : 2018-12-28
#
#

function valida_comunicacao(){

	ssh -o ConnectTimeout=1 $IP$I -p10002 exit

}
function main(){

	LOG="/root/srv_remoto/log/error.ajusttpv2.log"
	IP0="10.105.188."
	IP1="10.105.189."
	IP2="10.105.194."
	IP3="10.106.235."
	IP4="10.106.101."
	IP5="10.106.102."
	IP6="10.106.104."
	
	I=0
	while (( I < 2 ))
	do
		IP=$IP0
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOG
		else
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Conexão com sucesso IP:$IP$I.Return => $RETURN ." >> $LOG
			ajust=$(ssh -o ConnectTimeout=1 -p10002 $IP$I 'sed -e 's/CUPOPARA=0/CUPOPARA=1/' -e 's/IMPRCUPO=0/IMPRCUPO=1/' /confdia/bin/setvari > /confdia/bin/setvari.new ; mv /confdia/bin/setvari.new /confdia/bin/setvari ')
			RETURN=$?
			if [ $RETURN != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):EXEC_CMD:Erro ao executar comando:$IP$I.Return => $RETURN ." >> $LOG
			else
				echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Comando executado com sucesso:$IP$I.Return => $RETURN ." >> $LOG
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 2 ))
	do
		IP=$IP1
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOG
		else
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Conexão com sucesso IP:$IP$I.Return => $RETURN ." >> $LOG
			ajust=$(ssh -o ConnectTimeout=1 -p10002 $IP$I 'sed -e 's/CUPOPARA=0/CUPOPARA=1/' -e 's/IMPRCUPO=0/IMPRCUPO=1/' /confdia/bin/setvari > /confdia/bin/setvari.new ; mv /confdia/bin/setvari.new /confdia/bin/setvari ')
			RETURN=$?
			if [ $RETURN != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):EXEC_CMD:Erro ao executar comando:$IP$I.Return => $RETURN ." >> $LOG
			else
				echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Comando executado com sucesso:$IP$I.Return => $RETURN ." >> $LOG
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 2 ))
	do
		IP=$IP2
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOG
		else
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Conexão com sucesso IP:$IP$I.Return => $RETURN ." >> $LOG
			ajust=$(ssh -o ConnectTimeout=1 -p10002 $IP$I 'sed -e 's/CUPOPARA=0/CUPOPARA=1/' -e 's/IMPRCUPO=0/IMPRCUPO=1/' /confdia/bin/setvari > /confdia/bin/setvari.new ; mv /confdia/bin/setvari.new /confdia/bin/setvari ')
			RETURN=$?
			if [ $RETURN != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):EXEC_CMD:Erro ao executar comando:$IP$I.Return => $RETURN ." >> $LOG
			else
				echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Comando executado com sucesso:$IP$I.Return => $RETURN ." >> $LOG
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP3
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOG
		else
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Conexão com sucesso IP:$IP$I.Return => $RETURN ." >> $LOG
			ajust=$(ssh -o ConnectTimeout=1 -p10002 $IP$I 'sed -e 's/CUPOPARA=0/CUPOPARA=1/' -e 's/IMPRCUPO=0/IMPRCUPO=1/' /confdia/bin/setvari > /confdia/bin/setvari.new ; mv /confdia/bin/setvari.new /confdia/bin/setvari ')
			RETURN=$?
			if [ $RETURN != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):EXEC_CMD:Erro ao executar comando:$IP$I.Return => $RETURN ." >> $LOG
			else
				echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Comando executado com sucesso:$IP$I.Return => $RETURN ." >> $LOG
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP4
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOG
		else
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Conexão com sucesso IP:$IP$I.Return => $RETURN ." >> $LOG
			ajust=$(ssh -o ConnectTimeout=1 -p10002 $IP$I 'sed -e 's/CUPOPARA=0/CUPOPARA=1/' -e 's/IMPRCUPO=0/IMPRCUPO=1/' /confdia/bin/setvari > /confdia/bin/setvari.new ; mv /confdia/bin/setvari.new /confdia/bin/setvari ')
			RETURN=$?
			if [ $RETURN != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):EXEC_CMD:Erro ao executar comando:$IP$I.Return => $RETURN ." >> $LOG
			else
				echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Comando executado com sucesso:$IP$I.Return => $RETURN ." >> $LOG
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP5
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOG
		else
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Conexão com sucesso IP:$IP$I.Return => $RETURN ." >> $LOG
			ajust=$(ssh -o ConnectTimeout=1 -p10002 $IP$I 'sed -e 's/CUPOPARA=0/CUPOPARA=1/' -e 's/IMPRCUPO=0/IMPRCUPO=1/' /confdia/bin/setvari > /confdia/bin/setvari.new ; mv /confdia/bin/setvari.new /confdia/bin/setvari ')
			RETURN=$?
			if [ $RETURN != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):EXEC_CMD:Erro ao executar comando:$IP$I.Return => $RETURN ." >> $LOG
			else
				echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Comando executado com sucesso:$IP$I.Return => $RETURN ." >> $LOG
			fi
		fi
		((I++))
	done

	I=0
	while (( I < 254 ))
	do
		IP=$IP6
		RESULT=$(valida_comunicacao)
		RETURN=$?
		if [ $RETURN != 0 ];then
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Nao foi possivel conectar ao IP:$IP$I.Return => $RETURN ." >> $LOG
		else
			echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Conexão com sucesso IP:$IP$I.Return => $RETURN ." >> $LOG
			ajust=$(ssh -o ConnectTimeout=1 -p10002 $IP$I 'sed -e 's/CUPOPARA=0/CUPOPARA=1/' -e 's/IMPRCUPO=0/IMPRCUPO=1/' /confdia/bin/setvari > /confdia/bin/setvari.new ; mv /confdia/bin/setvari.new /confdia/bin/setvari ')
			RETURN=$?
			if [ $RETURN != 0 ];then
				echo "$(date +%Y%m%d"-"%H%M%S):EXEC_CMD:Erro ao executar comando:$IP$I.Return => $RETURN ." >> $LOG
			else
				echo "$(date +%Y%m%d"-"%H%M%S):CONN_SHOP:Comando executado com sucesso:$IP$I.Return => $RETURN ." >> $LOG
			fi
		fi
		((I++))
	done

}
main
