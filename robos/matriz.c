#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main(){

    // Variaveis

    int matriz_1[3][2], matriz_2[2][2], matriz_3[3][2];
    int result[9], z;
    char val[10];

    // Mensagem usuário
    printf("\033[01;32m");
    printf("++++ DESENHO MATRIZ ++++\n");
    printf("\033[01;33m");
    printf("Matrizes de entrada de dados:\n");
    printf("\nMATRIZ-1\tMATRIZ-2\n[x][x]\t\t[x][x]\n[x][x]\t\t[x][x]\n[x][x]\n");
    printf("\nMatriz com o resultado da multiplicação entre a Matriz-1 e Matriz-2:\n");
    printf("\nMATRIZ-3\n[x][x]\n[x][x]\n[x][x]\n");
    printf("\033[00;37m");
    printf("\n\033[01;32m ### Multiplicação entre Matrizes - Dados MATRIZ-1 ###\033[00;37m\n");


    // Loop para pegar os valores para matriz 1
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 2; j++){
            printf("Entre com números inteiros para alimentar a MATRIZ-1 [%d][%d]:", i,j);
            scanf("%d",&matriz_1[i][j]);
            fflush(stdin);
        }
    }

    // Loop para pegar os valores para matriz 2
    printf("\n\033[01;32m ### Multiplicação entre Matrizes - Dados MATRIZ-2 ###\033[00;37m\n");
    for(int i = 0; i < 2; i++){
        for(int j = 0; j < 2; j++){
            printf("Entre com números inteiros para alimentar a MATRIZ-2 [%d][%d]:", i,j);
            scanf("%d",&matriz_2[i][j]);
            fflush(stdin);
        }
    }
    
    // Loop que faz a multiplicação entre as tabelas, e guada os valores em um vetor
    for(int i = 0 ; i < 3; i++){
        for(int j = 0 ; j < 2; j++){        
            if(i==0 && j==0){
                result[0] = (matriz_1[i][j] * matriz_2[i][j]) + (matriz_1[i][j+1] * matriz_2[i+1][j]);
                result[1] = (matriz_1[i][j] * matriz_2[i][j+1]) + (matriz_1[i][j+1] * matriz_2[j+1][j+1]);
            }else if(i==1 && j==0){
                result[2] = (matriz_1[i][j] * matriz_2[i-1][j]) + (matriz_1[i][j+1] * matriz_2[i][j]);
                result[3] = (matriz_1[i][j] * matriz_2[i-1][j+1]) + (matriz_1[i][j+1] * matriz_2[i][j+1]);
            }else if(i==1 && j==1){
                result[4] = (matriz_1[i+1][j-1] * matriz_2[i-1][j-1]) + (matriz_1[i+1][j] * matriz_2[i-1][j+1]);
                result[5] = (matriz_1[i+1][j-1] * matriz_2[i-1][j]) + (matriz_1[i+1][j] * matriz_2[i][j]);
            }
        }
    }

    // Loop que passa os valores guardados no vetor para matriz 3
    z=0;
    for(int x = 0; x < 3; x++){     
        for(int y = 0; y < 2; y++){
            matriz_3[x][y] = result[z];
             z++;
        }     
    }

    // Exibimos o resultado para usuário
    printf("\033[01;32m");
    printf("\n++++ RESULTADO ++++\n");
    printf("\033[01;34m");
    printf("MATRIZ-3\n[%02d][%02d]\n[%02d][%02d]\n[%02d][%02d]\n", matriz_3[0][0], matriz_3[0][1], matriz_3[1][0], matriz_3[1][1], matriz_3[2][0], matriz_3[2][1]);
    printf("\033[00;37m");
    return 0;

}