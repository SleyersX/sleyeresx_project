#!/bin/bash
# Autor: Walter Moura
# Data Criação: 11/10/2019
# Data Modificação: 16/10/2019
# Modificado por: Walter Moura
#
# Versão 0.01
# for i in /confdia/logscomu/20190823*CommsGN.tgz; do echo "::$i" ; tar xzf $i -O confdia/comunica/8056.DGZ | gunzip | fold  -bw140 | grep '^09080563' ; done >> saida.txt
#cat saida.txt | sed '1d' | sed 's/.\{8\}//' | cut -c 1-120 > new_saida.txt
#while read linha; do CODI1=$(echo $linha | cut -c 1-6); PVP1=$(echo $linha | cut -c 7-15) ; CODI2=$(echo $linha | cut -c 41-46); PVP2=$(echo $linha | cut -c 47-55) ; CODI3=$(echo $linha |cut -c 81-86 ); PVP3=$(echo $linha | cut -c 87-95) ; printf "CODIGO:%s PVP:%3.f\nCODIGO:%s PVP:%3.f\nCODIGO:%s PVP:%3.f\n" $CODI1 $PVP1 $CODI2 $PVP2 $CODI3 $PVP3;done < new_saida.txt


BD="/"

function valida_conexao(){
	sshpass -p root ssh -l root -p10001 $ip exit
}

function testa_caractere(){	
	if [[ $data = ?(+|-)+([0-9]) ]]; then
		if [ $sizedata -ge 9 ] || [ $sizedata -le 7 ]; then
			RETURN=-2
		else
			RETURN=0
			valida_file
		fi
	else
		RETURN=-1
	fi
}

function valida_file(){
	echo "Estou aqui"
}

function trata_error(){
	# Codigos de error
	# -1 Um dos caracteres informados não é um número inteiro
	# -2 Informe até 8 caracteres

	case $RETURN in
		-1)
			echo "Um dos caracteres informados não é um número inteiro."
		;;
		-2)
			echo "Informe até 8 caracteres."
		;;
		*)
			echo ""
		;;
	esac
}

function get_ip(){

}

function main(){
	echo "Ler transacao 09 do GN de um dia especifico"
	read -p "Informe a data a buscar no arquivo:" data

	[ "$data" ] || { echo "Nao informada a data a ser pesquisada."; exit 1; }

	sizedata=`echo ${#data}`
	testa_caractere
	if [ $RETURN != 0 ]; then
		trata_error
	else
		echo "Estou aqui."
	fi	
}

main
