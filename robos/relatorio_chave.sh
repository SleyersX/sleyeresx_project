#!/bin/bash
#Script para criar arquivo de chaves formato linux
#Criado por wam001br

CHAVE="chave.txt"
CFE="CFeKey.txt"
LOG="registro.log"
ARQ="relatorio_chave.txt"

function main()
{
	ConsultarChave
}

function ConsultarChave()
	{
		psw="pafstore"
		usr="pafstore"
		srv="pafbackup"
		pt=22
		i=8000
		while (( i < 8200 )) 
			do
				echo "$(date +%Y%m%d-%H%M%S.%s):Função consulta de lojas('$i')." >> $LOG
				busca=$(sshpass -p $psw scp  -P $pt $usr@$srv://D//CLAVES_SAT//D$i.zip . )
				ret=$?
				if [ $ret != 0 ]; then
					echo -e "\033[01;33mLoja nao econtrada no servidor de chaves SAT.\033[00;37m"
					echo "$(date +%Y%m%d-%H%M%S.%s):Loja não encontrada no servidor de chaves SAT." >> $LOG
					sleep 1
					busca_I=$(sshpass -p $psw scp -P $pt $usr@$srv://D//CLAVES_NFCE//D$i.zip . )
					ret_II=$?
						if [ $ret_II != 0 ]; then
							echo -e "\033[01;33mLoja nao encontrada no servidor de chave NFCe.\033[00;37m"
							echo "$(date +%Y%m%d-%H%M%S.%s):Loja não encontrada no servidor de chaves NFCe." >> $LOG
							sleep 1
						else
							echo -e "\033[01;32mLoja encontrada no servidor de chaves NFCe.\033[00;37m"
							echo "$(date +%Y%m%d-%H%M%S.%s):Loja encontrada no servidor de chaves NFCe." >> $LOG
							unzip D$i.zip
							exibir_cfe=$(cat $CFE)
							
							sleep 1
							exibir_token=$(cat $CHAVE )
							
							echo "$(date +%Y%m%d-%H%M%S.%s):Chaves encontradas('$i' '$exibir_cfe' '$exibir_token')." >> $LOG
							echo "NFCe|$i|$exibir_cfe|$exibir_token" >> $ARQ
							sleep 1
											
							rm -f D$i.zip
							rm -f chave.txt
							rm -f CFeKey.txt
							sleep 1
							echo -n
						fi
					else
						echo -e "\033[01;32mLoja econtrada no servidor de chaves SAT.\033[00;37m"
						echo "$(date +%Y%m%d-%H%M%S.%s):Loja encontrada no servidor de chaves SAT." >> $LOG
						unzip D$i.zip
						exibir_cfe=$(cat $CFE)
						
						sleep 1
						exibir_ac=$(cat $CHAVE)
						
						sleep 1
						echo "$(date +%Y%m%d-%H%M%S.%s):Chaves encontradas('$i' '$exibir_cfe' '$exibir_ac')." >> $LOG
						echo "SAT|$i|$exibir_cfe|$exibir_ac" >> $ARQ
						rm -f D$i.zip
						rm -f chave.txt
						rm -f CFeKey.txt
						
						ESPEBAT="ESPEBAT.BAT"
						if [ -e $ESPEBAT ]; then
							rm -f $ESPEBAT
						fi
						sleep 1
						echo -n
					fi
				((i++))
			done
}
main
