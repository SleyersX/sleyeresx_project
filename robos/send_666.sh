#!/bin/bash

LOG="/root/srv_remoto/log/error.send666.log"
ARQSHOPS="/root/srv_remoto/texto/aux_list_lojas_666.txt"
PASS="root"
USER="root"
FILESEND="/root/srv_remoto/tgz/666"
BD="/root/srv_remoto/srv_remoto.db"

function fnGroupLojas(){

    echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Inciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Verificamos se arquivo ['$ARQTEMP'] existe." >> $LOG
    if [ -e $ARQSHOPS ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Arquivo OK." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Iniciamos um while read no arquivo ['$ARQSHOPS'] e gravamos cada valor em um array." >> $LOG
        while read idshop;
            do
                progress=("$idshop")
        done < $ARQSHOPS
        ARRAYSHOPS=(${progress[0]})
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:ARRAY Shops -> ['$ARRAYSHOPS']." >> $LOG
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Arquivo NOK." >> $LOG
    fi
            
}

function fnGetIP(){
    
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Verificamos se a loja ['$SHOP'] existe no banco ['$BD']." >> $LOG
    EXISTE=`echo "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Retorno existe ['$EXISTE']." >> $LOG
    if [ $EXISTE -eq 1 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Loja ['$SHOP'] existe, buscamos o IP." >> $LOG
        ip=`echo "SELECT ip FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Retorno IP -> ['$ip']." >> $LOG
        RET=0
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Loja ['$SHOP'] não econtrada no banco de dados." >> $LOG
        RET=1
    fi

}

function fnValidaComunicacao(){

    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VALIDACONEXAO:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VALIDACONEXAO:Validando conexão ['$SHOP:$ip']." >> $LOG
    sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip exit

}

function fnSendFile666(){

    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:FNSENDFILE666:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:FNSENDFILE666:Enviando o arquivo ['$FILESEND'] -> ['$SHOP:$ip']." >> $LOG
    sshpass -p $PASS scp -P10001 $FILESEND $USER@$ip:/confdia/bin/
    if [ $? -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:FNSENDFILE666:Arquivo enviado ['$FILESEND'] -> ['$SHOP:$ip'] com sucesso." >> $LOG
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:FNSENDFILE666:Erro ao enviar ['$FILESEND'] -> ['$SHOP:$ip']." >> $LOG
    fi

}

function main(){

    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Inicio programa." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Inciando os menus." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Iniciando a função ['GroupShop']." >> $LOG
    fnGroupLojas
    i=0
    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Iniciamos um while no array ['ARRAYSHOPS']" >> $LOG
    while [ $i != ${#ARRAYSHOPS[@]} ]
    do
        echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Verificamos se o valor ['${ARRAYSHOPS[i]}'] é do tipo númerico." >> $LOG
        if [[ ${ARRAYSHOPS[i]} = ?(+|-)+([0-9]) ]]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Valor ['${ARRAYSHOPS[i]}'] é do tipo número, incluímos 0 a esquerda." >> $LOG
            SHOP=`printf "%05d" ${ARRAYSHOPS[i]}`
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Valor ['${ARRAYSHOPS[i]}'] não é do tipo númerico, será ignorado." >> $LOG
        fi
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Iniciando a função ['fnGetIP']." >> $LOG
        fnGetIP
        if [ $RET -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Iniciando a função ['fnValidaComunicacao']." >> $LOG
            fnValidaComunicacao
            if [ $? -eq 0 ]; then
                echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Iniciando a função ['fnSendFile666']." >> $LOG
                fnSendFile666
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Comando não executado -> ['$?']." >> $LOG
            fi
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Erro ao obter ip da loja ['${ARRAYSHOPS[i]}']." >> $LOG
        fi
        let "i = i +1"
    done

}

main