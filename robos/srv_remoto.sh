#!/bin/bash
#
# Autor: Walter Moura
# Data Criacao: 2019.06.26
# Data Modificacao: 2019.08.16
# Versao: 0.03

# Variaveis Globais
BD="/root/srv_remoto/srv_remoto.db"
LOG1="export.log"
delimit="|"
FUNC=0
RESP=0
function main(){

# Chamar menus
clear
echo "+-------------------------------------------------------------------+"
echo "| Remote System Install                          $(date +%Y.%m.%d-%H:%M:%S)|"
echo "+-------------------------------------------------------------------+"
echo "| 01. Install Brother                                               |"
echo "| 02. Install TCGertec 2.0(VS s/ De/Por)                            |"
echo "| 03. Install TCGertec 3.0(VS c/ De/Por)                            |"
echo "| 04. Disabled Printer TM88                                         |"
echo "| 05. Delete files actualPAF                                        |"
echo "| 06. Active Oferta Menos Venda                                     |"
echo "| 99. Exit/Quit                                                     |"
echo "+-------------------------------------------------------------------+"
read -p "                                                    Change Option: " opmenu
echo "+-------------------------------------------------------------------+"

# Case para chamadas de cada menu
case $opmenu in
    "1"|"01")
            echo "                     +++ INSTALL BROTHER +++"
            sleep 3
            FUNC=1
            menuPrinterBrother
    ;;
    "2"|"02")
            echo "                   +++ INSTALL TCGERTEC 2.0 +++"
            sleep 3
            FUNC=2
            menuTCGertec2
    ;;
    "3"|"03")
            echo "                   +++ INSTALL TCGERTEC 3.0 +++"
            sleep 3
            FUNC=3
            menuTCGertec3
    ;;
    "4"|"04")
            echo "                    +++ DISABLED EPSON TM88 +++"
            sleep 3
            FUNC=4
            menuDisabledTM88
    ;;
    "5"|"05")
            echo "                   +++ DELETE FILES ACTUALPAF +++"
            sleep 3
            FUNC=5
            menuDeleteActualPAF
    ;;
    "6"|"06")
            echo "                 +++ ACTIVE OFERTA MENOS VENDA +++"
            sleep 3
            FUNC=6
            menuOfertaMenosVenda
    ;;
    "99")
            echo "                       +++ EXIT/QUIT +++"
            sleep 1
            exit 1
    ;;
    *)
            echo -e "                     \033[01;33m+++ Opção inválida +++\033[00;37m"
            sleep 2
            main
esac        

}

function menuPrinterBrother(){

# Construir Menu
clear
echo "+-------------------------------------------------------------------+"
echo "| System Install Brother                         $(date +%Y.%m.%d-%H:%M:%S)|"
echo "+-------------------------------------------------------------------+"
echo "| 01. Enabled Store                                                 |"
echo "| 02. Enabled multiple Stores                                       |"
echo "| 03. Store Inquiry                                                 |"
echo "| 04. Disabled Store                                                |"
echo "| 05. Generate remote shipping/installation program record          |"
echo "| 06. Extract data from the database                                |"
echo "| 07. Forced Installation                                           |"
echo "| 08. View System Log                                               |"
echo "| 99. Exit/Quit                                                     |"
echo "+-------------------------------------------------------------------+"
read -p "                                                    Change option: " opcao
echo "+-------------------------------------------------------------------+"

# Construir case para receber a resposta do usuário
case $opcao in
    "01"|"1") # Ativar Loja
        echo "                       +++ ATIVAR LOJA +++"
        AtivarLoja
    ;;
    "02"|"2") # Ativar varias Lojas
        echo "                   +++ ATIVAR VARIAS LOJAS +++"
        AtivarVariasLoja
    ;;
    "03"|"3") # Consultar Loja
        echo "                     +++ CONSULTAR LOJA +++"
        ConsultarLoja
    ;;
    "04"|"4") # Desativar Loja
        echo "                     +++ DESATIVAR LOJA +++"
        DesativarLoja
    ;;
    "05"|"5") # Gerar Log
        echo "                       +++ GERAR LOG +++"
        GerarLog
    ;;
    "06"|"6") # Extrair Dados
        echo "                      +++ EXTRAIR DADOS +++"
        ExtrairDados
    ;;
    "07"|"7") # Forçar instalação
        echo "                     +++ FORÇAR INSTALAÇÃO +++"
        ForcedOneShop
    ;;
    "08"|"8") # Visualizar log do sistema
        echo "                    +++ VISUALIZAR LOG APLICAÇÃO +++"
        ViewSystemLog
    ;;
    "99") # Exit/Quit
        echo "                       +++ EXIT/QUIT +++"
        sleep 2
        main
    ;;
    *) # Opção inválida
        echo -e "                     \033[01;33m+++ Opção inválida +++\033[00;37m"
        sleep 2
        menuPrinterBrother
esac

}

function menuTCGertec2(){

# Construir Menu
clear
echo "+-------------------------------------------------------------------+"
echo "| System Install TCGertec 2.0                    $(date +%Y.%m.%d-%H:%M:%S)|"
echo "+-------------------------------------------------------------------+"
echo "| 01. Enabled Store                                                 |"
echo "| 02. Enabled multiple Stores                                       |"
echo "| 03. Store Inquiry                                                 |"
echo "| 04. Disabled Store                                                |"
echo "| 05. Generate remote shipping/installation program record          |"
echo "| 06. Extract data from the database                                |"
echo "| 07. Forced Installation                                           |"
echo "| 08. View System Log                                               |"
echo "| 99. Exit/Quit                                                     |"
echo "+-------------------------------------------------------------------+"
read -p "                                                    Change option: " opcao
echo "+-------------------------------------------------------------------+"

# Construir case para receber a resposta do usuário
case $opcao in
    "01"|"1") # Ativar Loja
        echo "                       +++ ATIVAR LOJA +++"
        AtivarLoja
    ;;
    "02"|"2") # Ativar varias Lojas
        echo "                   +++ ATIVAR VARIAS LOJAS +++"
        AtivarVariasLoja
    ;;
    "03"|"3") # Consultar Loja
        echo "                     +++ CONSULTAR LOJA +++"
        ConsultarLoja
    ;;
    "04"|"4") # Desativar Loja
        echo "                     +++ DESATIVAR LOJA +++"
        DesativarLoja
    ;;
    "05"|"5") # Gerar Log
        echo "                       +++ GERAR LOG +++"
        GerarLog
    ;;
    "06"|"6") # Extrair Dados
        echo "                      +++ EXTRAIR DADOS +++"
        ExtrairDados
    ;;
    "07"|"7") # Forçar instalação
        echo "                     +++ FORÇAR INSTALAÇÃO +++"
        ForcedOneShop
    ;;
    "08"|"8") # Visualizar log do sistema
        echo "                    +++ VISUALIZAR LOG APLICAÇÃO +++"
        ViewSystemLog
    ;;
    "99") # Exit/Quit
        echo "                       +++ EXIT/QUIT +++"
        sleep 2
        main
    ;;
    *) # Opção inválida
        echo -e "                     \033[01;33m+++ Opção inválida +++\033[00;37m"
        sleep 2
        menuTCGertec2
esac

}

function menuTCGertec3(){

# Construir Menu
clear
echo "+-------------------------------------------------------------------+"
echo "| System Install TCGertec 3.0                    $(date +%Y.%m.%d-%H:%M:%S)|"
echo "+-------------------------------------------------------------------+"
echo "| 01. Enabled Store                                                 |"
echo "| 02. Enabled multiple Stores                                       |"
echo "| 03. Store Inquiry                                                 |"
echo "| 04. Disabled Store                                                |"
echo "| 05. Generate remote shipping/installation program record          |"
echo "| 06. Extract data from the database                                |"
echo "| 07. Forced Installation                                           |"
echo "| 08. View System Log                                               |"
echo "| 99. Exit/Quit                                                     |"
echo "+-------------------------------------------------------------------+"
read -p "                                                    Change option: " opcao
echo "+-------------------------------------------------------------------+"

# Construir case para receber a resposta do usuário
case $opcao in
    "01"|"1") # Ativar Loja
        echo "                       +++ ATIVAR LOJA +++"
        AtivarLoja
    ;;
    "02"|"2") # Ativar varias Lojas
        echo "                   +++ ATIVAR VARIAS LOJAS +++"
        AtivarVariasLoja
    ;;
    "03"|"3") # Consultar Loja
        echo "                     +++ CONSULTAR LOJA +++"
        ConsultarLoja
    ;;
    "04"|"4") # Desativar Loja
        echo "                     +++ DESATIVAR LOJA +++"
        DesativarLoja
    ;;
    "05"|"5") # Gerar Log
        echo "                       +++ GERAR LOG +++"
        GerarLog
    ;;
    "06"|"6") # Extrair Dados
        echo "                      +++ EXTRAIR DADOS +++"
        ExtrairDados
    ;;
    "07"|"7") # Forçar instalação
        echo "                     +++ FORÇAR INSTALAÇÃO +++"
        ForcedOneShop
    ;;
    "08"|"8") # Visualizar log do sistema
        echo "                    +++ VISUALIZAR LOG APLICAÇÃO +++"
        ViewSystemLog
    ;;
    "99") # Exit/Quit
        echo "                       +++ EXIT/QUIT +++"
        sleep 2
        main
    ;;
    *) # Opção inválida
        echo -e "                     \033[01;33m+++ Opção inválida +++\033[00;37m"
        sleep 2
        menuTCGertec3
esac

}

function menuDisabledTM88(){

# Construir Menu
clear
echo "+-------------------------------------------------------------------+"
echo "| System Disabled TM88                           $(date +%Y.%m.%d-%H:%M:%S)|"
echo "+-------------------------------------------------------------------+"
echo "| 01. Enabled Store                                                 |"
echo "| 02. Enabled multiple Stores                                       |"
echo "| 03. Store Inquiry                                                 |"
echo "| 04. Disabled Store                                                |"
echo "| 05. Generate remote shipping/installation program record          |"
echo "| 06. Extract data from the database                                |"
echo "| 07. Forced Installation                                           |"
echo "| 08. View System Log                                               |"
echo "| 99. Exit/Quit                                                     |"
echo "+-------------------------------------------------------------------+"
read -p "                                                    Change option: " opcao
echo "+-------------------------------------------------------------------+"

# Construir case para receber a resposta do usuário
case $opcao in
    "01"|"1") # Ativar Loja
        echo "                       +++ ATIVAR LOJA +++"
        AtivarLoja
    ;;
    "02"|"2") # Ativar varias Lojas
        echo "                   +++ ATIVAR VARIAS LOJAS +++"
        AtivarVariasLoja
    ;;
    "03"|"3") # Consultar Loja
        echo "                     +++ CONSULTAR LOJA +++"
        ConsultarLoja
    ;;
    "04"|"4") # Desativar Loja
        echo "                     +++ DESATIVAR LOJA +++"
        DesativarLoja
    ;;
    "05"|"5") # Gerar Log
        echo "                       +++ GERAR LOG +++"
        GerarLog
    ;;
    "06"|"6") # Extrair Dados
        echo "                      +++ EXTRAIR DADOS +++"
        ExtrairDados
    ;;
    "07"|"7") # Forçar instalação
        echo "                     +++ FORÇAR INSTALAÇÃO +++"
        ForcedOneShop
    ;;
    "08"|"8") # Visualizar log do sistema
        echo "                    +++ VISUALIZAR LOG APLICAÇÃO +++"
        ViewSystemLog
    ;;
    "99") # Exit/Quit
        echo "                       +++ EXIT/QUIT +++"
        sleep 2
        main
    ;;
    *) # Opção inválida
        echo -e "                     \033[01;33m+++ Opção inválida +++\033[00;37m"
        sleep 2
        menuDisabledTM88
esac

}

function menuDeleteActualPAF(){

# Construir Menu
clear
echo "+-------------------------------------------------------------------+"
echo "| System Delete Files actualPAF                  $(date +%Y.%m.%d-%H:%M:%S)|"
echo "+-------------------------------------------------------------------+"
echo "| 01. Enabled Store                                                 |"
echo "| 02. Enabled multiple Stores                                       |"
echo "| 03. Store Inquiry                                                 |"
echo "| 04. Disabled Store                                                |"
echo "| 05. Generate remote shipping/installation program record          |"
echo "| 06. Extract data from the database                                |"
echo "| 07. Forced Installation                                           |"
echo "| 08. View System Log                                               |"
echo "| 99. Exit/Quit                                                     |"
echo "+-------------------------------------------------------------------+"
read -p "                                                    Change option: " opcao
echo "+-------------------------------------------------------------------+"

# Construir case para receber a resposta do usuário
case $opcao in
    "01"|"1") # Ativar Loja
        echo "                       +++ ATIVAR LOJA +++"
        AtivarLoja
    ;;
    "02"|"2") # Ativar varias Lojas
        echo "                   +++ ATIVAR VARIAS LOJAS +++"
        AtivarVariasLoja
    ;;
    "03"|"3") # Consultar Loja
        echo "                     +++ CONSULTAR LOJA +++"
        ConsultarLoja
    ;;
    "04"|"4") # Desativar Loja
        echo "                     +++ DESATIVAR LOJA +++"
        DesativarLoja
    ;;
    "05"|"5") # Gerar Log
        echo "                       +++ GERAR LOG +++"
        GerarLog
    ;;
    "06"|"6") # Extrair Dados
        echo "                      +++ EXTRAIR DADOS +++"
        ExtrairDados
    ;;
    "07"|"7") # Forçar instalação
        echo "                     +++ FORÇAR INSTALAÇÃO +++"
        ForcedOneShop
    ;;
    "08"|"8") # Visualizar log do sistema
        echo "                    +++ VISUALIZAR LOG APLICAÇÃO +++"
        ViewSystemLog
    ;;
    "99") # Exit/Quit
        echo "                       +++ EXIT/QUIT +++"
        sleep 2
        main
    ;;
    *) # Opção inválida
        echo -e "                     \033[01;33m+++ Opção inválida +++\033[00;37m"
        sleep 2
        menuDeleteActualPAF
esac

}

function menuDeleteActualPAF(){

# Construir Menu
clear
echo "+-------------------------------------------------------------------+"
echo "| System Delete Files actualPAF                  $(date +%Y.%m.%d-%H:%M:%S)|"
echo "+-------------------------------------------------------------------+"
echo "| 01. Enabled Store                                                 |"
echo "| 02. Enabled multiple Stores                                       |"
echo "| 03. Store Inquiry                                                 |"
echo "| 04. Disabled Store                                                |"
echo "| 05. Generate remote shipping/installation program record          |"
echo "| 06. Extract data from the database                                |"
echo "| 07. Forced Installation                                           |"
echo "| 08. View System Log                                               |"
echo "| 99. Exit/Quit                                                     |"
echo "+-------------------------------------------------------------------+"
read -p "                                                    Change option: " opcao
echo "+-------------------------------------------------------------------+"

# Construir case para receber a resposta do usuário
case $opcao in
    "01"|"1") # Ativar Loja
        echo "                       +++ ATIVAR LOJA +++"
        AtivarLoja
    ;;
    "02"|"2") # Ativar varias Lojas
        echo "                   +++ ATIVAR VARIAS LOJAS +++"
        AtivarVariasLoja
    ;;
    "03"|"3") # Consultar Loja
        echo "                     +++ CONSULTAR LOJA +++"
        ConsultarLoja
    ;;
    "04"|"4") # Desativar Loja
        echo "                     +++ DESATIVAR LOJA +++"
        DesativarLoja
    ;;
    "05"|"5") # Gerar Log
        echo "                       +++ GERAR LOG +++"
        GerarLog
    ;;
    "06"|"6") # Extrair Dados
        echo "                      +++ EXTRAIR DADOS +++"
        ExtrairDados
    ;;
    "07"|"7") # Forçar instalação
        echo "                     +++ FORÇAR INSTALAÇÃO +++"
        ForcedOneShop
    ;;
    "08"|"8") # Visualizar log do sistema
        echo "                    +++ VISUALIZAR LOG APLICAÇÃO +++"
        ViewSystemLog
    ;;
    "99") # Exit/Quit
        echo "                       +++ EXIT/QUIT +++"
        sleep 2
        main
    ;;
    *) # Opção inválida
        echo -e "                     \033[01;33m+++ Opção inválida +++\033[00;37m"
        sleep 2
        menuDeleteActualPAF
esac

}

function menuOfertaMenosVenda(){

# Construir Menu
clear
echo "+-------------------------------------------------------------------+"
echo "| System Oferta Menos Venda                      $(date +%Y.%m.%d-%H:%M:%S)|"
echo "+-------------------------------------------------------------------+"
echo "| 01. Enabled Store                                                 |"
echo "| 02. Enabled multiple Stores                                       |"
echo "| 03. Store Inquiry                                                 |"
echo "| 04. Disabled Store                                                |"
echo "| 05. Generate remote shipping/installation program record          |"
echo "| 06. Extract data from the database                                |"
echo "| 07. Forced Installation                                           |"
echo "| 08. View System Log                                               |"
echo "| 99. Exit/Quit                                                     |"
echo "+-------------------------------------------------------------------+"
read -p "                                                    Change option: " opcao
echo "+-------------------------------------------------------------------+"

# Construir case para receber a resposta do usuário
case $opcao in
    "01"|"1") # Ativar Loja
        echo "                       +++ ATIVAR LOJA +++"
        AtivarLoja
    ;;
    "02"|"2") # Ativar varias Lojas
        echo "                   +++ ATIVAR VARIAS LOJAS +++"
        AtivarVariasLoja
    ;;
    "03"|"3") # Consultar Loja
        echo "                     +++ CONSULTAR LOJA +++"
        ConsultarLoja
    ;;
    "04"|"4") # Desativar Loja
        echo "                     +++ DESATIVAR LOJA +++"
        DesativarLoja
    ;;
    "05"|"5") # Gerar Log
        echo "                       +++ GERAR LOG +++"
        GerarLog
    ;;
    "06"|"6") # Extrair Dados
        echo "                      +++ EXTRAIR DADOS +++"
        ExtrairDados
    ;;
    "07"|"7") # Forçar instalação
        echo "                     +++ FORÇAR INSTALAÇÃO +++"
        ForcedOneShop
    ;;
    "08"|"8") # Visualizar log do sistema
        echo "                    +++ VISUALIZAR LOG APLICAÇÃO +++"
        ViewSystemLog
    ;;
    "99") # Exit/Quit
        echo "                       +++ EXIT/QUIT +++"
        sleep 2
        main
    ;;
    *) # Opção inválida
        echo -e "                     \033[01;33m+++ Opção inválida +++\033[00;37m"
        sleep 2
        menuDeleteActualPAF
esac

}

function AtivarLoja(){

# Solicitamos ao usuario que informe a loja e data de instação
read -p "Informe a loja: " loja
# Validadmos se as variáveis estão vazias
[ "$loja" ] || { echo -e "\033[01;31m ERROR: Não foi informado a loja.\033[00;37m" ; sleep 1 ; main ; }
# Validamos a quantidade de caracteres informados
lj=`echo ${#loja}`
if [ $lj -ge 5 ]; then
    echo "Informe até 4 caracteres númericos. Ex.: 2, 1000, 9527, 165."
    sleep 1
    main
fi
# Validamos se os campos informados são númericos
if [[ $loja = ?(+|-)+([0-9]) ]]; then
    echo ""
else
    echo "Ops, valor informado não é um número inteiro!"
    sleep 1
    main
fi

if [ $FUNC -eq 5 ]; then

    read -p "1. Ativar [Y/N] ] ? " Ativar
    # Validadmos se as variáveis estão vazias
    [ "$Ativar" ] || { echo -e "\033[01;31m ERROR: Não foi informada uma resposta.\033[00;37m" ; sleep 1 ; main ; }
    # Validamos a quantidade de caracteres informados
    case $Ativar in
        "Y"|"y"|"s"|"S")
            RESP=1   
        ;;
        "N"|"n")
            RESP=0
        ;;
        *)
        echo "ERROR: Resposta não identificada."
        sleep 2
        main
    esac
else
    if [ $FUNC -eq 1 ]; then
        read -p "Informe a data de envio (YYYYMMDD): " dt_enivo
        # Validadmos se as variáveis estão vazias
        [ "$dt_enivo" ] || { echo -e "\033[01;31m ERROR: Não foi informada a data.\033[00;37m" ; sleep 1 ; main ; }
        # Validamos se os campos informados são númericos
        if [[ $dt_enivo = ?(+|-)+([0-9]) ]]; then
            echo ""
        else
            echo "Ops, valor informado não é um número inteiro!"
            sleep 1
            main
        fi
        # Validamos a quantidade de caracteres informados
        dt=`echo ${#dt_enivo}`
        if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
            echo "Informe até 8 caracteres númericos. Ex.: 20190101."
            sleep 1
            main
        fi
        read -p "Informe a data de instalação (YYYYMMDD): " data
        # Validadmos se as variáveis estão vazias
        [ "$data" ] || { echo -e "\033[01;31m ERROR: Não foi informada a data.\033[00;37m" ; sleep 1 ; main ; }
        # Validamos se os campos informados são númericos
        if [[ $data = ?(+|-)+([0-9]) ]]; then
            echo ""
        else
            echo "Ops, valor informado não é um número inteiro!"
            sleep 1
            main
        fi
        # Validamos a quantidade de caracteres informados
        dt=`echo ${#data}`
        if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
            echo "Informe até 8 caracteres númericos. Ex.: 20190101."
            sleep 1
            main
        fi
    elif [ $FUNC -eq 6 ]; then
        read -p "Informe a data de envio (YYYYMMDD): " dt_enivo
        # Validadmos se as variáveis estão vazias
        [ "$dt_enivo" ] || { echo -e "\033[01;31m ERROR: Não foi informada a data.\033[00;37m" ; sleep 1 ; main ; }
        # Validamos se os campos informados são númericos
        if [[ $dt_enivo = ?(+|-)+([0-9]) ]]; then
            echo ""
        else
            echo "Ops, valor informado não é um número inteiro!"
            sleep 1
            main
        fi
        # Validamos a quantidade de caracteres informados
        dt=`echo ${#dt_enivo}`
        if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
            echo "Informe até 8 caracteres númericos. Ex.: 20190101."
            sleep 1
            main
        fi
        read -p "Informe a data de instalação (YYYYMMDD): " data
        # Validadmos se as variáveis estão vazias
        [ "$data" ] || { echo -e "\033[01;31m ERROR: Não foi informada a data.\033[00;37m" ; sleep 1 ; main ; }
        # Validamos se os campos informados são númericos
        if [[ $data = ?(+|-)+([0-9]) ]]; then
            echo ""
        else
            echo "Ops, valor informado não é um número inteiro!"
            sleep 1
            main
        fi
        # Validamos a quantidade de caracteres informados
        dt=`echo ${#data}`
        if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
            echo "Informe até 8 caracteres númericos. Ex.: 20190101."
            sleep 1
            main
        fi
    else
        read -p "Informe a data de instalação (YYYYMMDD): " data
        # Validadmos se as variáveis estão vazias
        [ "$data" ] || { echo -e "\033[01;31m ERROR: Não foi informada a data.\033[00;37m" ; sleep 1 ; main ; }
        # Validamos a quantidade de caracteres informados
        dt=`echo ${#data}`
        if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
            echo "Informe até 8 caracteres númericos. Ex.: 20190101."
            sleep 1
            main
        fi
        # Validamos se os campos informados são númericos
        if [[ $data = ?(+|-)+([0-9]) ]]; then
            echo ""
        else
            echo "Ops, valor informado não é um número inteiro!"
            sleep 1
            main
        fi
    fi

fi

# Colocamos 0 a esquerda para o número da loja
loja=`printf "%05d" $loja`

# Inserimos os valores no banco de dados
if [ $FUNC -eq 1 ]; then

    exist=`echo "SELECT COUNT(id) FROM tb_printer_brother WHERE loja = '$loja';" | sqlite3 $BD`
    if [ $exist -eq 0 ]; then
        sqlite3 $BD \
                        "INSERT INTO tb_printer_brother (loja, data, data_instalacao) VALUES ('$loja', '$dt_enivo', '$data')"
        if [ $? != 0 ]; then
            echo "ERROR: Falha ao inserir dados no banco de dados."
            sleep 1
            menuPrinterBrother
        else
            echo "Dados salvos com sucesso."
            sleep 1
            menuPrinterBrother
        fi
    else
        sqlite3 $BD \
                        "UPDATE tb_printer_brother SET data = '$dt_enivo', data_instalacao = '$data' WHERE ID=(SELECT MAX(ID) FROM tb_printer_brother WHERE loja LIKE '$loja')"
        if [ $? != 0 ]; then
            echo "ERROR: Falha ao inserir dados no banco de dados."
            sleep 1
            menuPrinterBrother
        else
            echo "Dados salvos com sucesso."
            sleep 1
            menuPrinterBrother
        fi
    fi

elif [ $FUNC -eq 2 ]; then

    exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$loja';" | sqlite3 $BD`
    if [ $exist -eq 0 ]; then
        sqlite3 $BD \
                        "INSERT INTO tb_verificador (tienda, fecha_envio, versao) VALUES ('$loja', '$data', '2')"
        if [ $? != 0 ]; then
            echo "ERROR: Falha ao inserir dados no banco de dados."
            sleep 1
            menuTCGertec2
        else
            echo "Dados salvos com sucesso."
            sleep 1
            menuTCGertec2
        fi
    else
        sqlite3 $BD \
                        "UPDATE tb_verificador SET fecha_envio = '$data', versao = '2' WHERE ID=(SELECT MAX(ID) FROM tb_verificador WHERE tienda = '$loja')"
        if [ $? != 0 ]; then
            echo "ERROR: Falha ao inserir dados no banco de dados."
            sleep 1
            menuTCGertec2
        else
            echo "Dados salvos com sucesso."
            sleep 1
            menuTCGertec2
        fi
    fi

elif [ $FUNC -eq 3 ]; then

    exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$loja';" | sqlite3 $BD`
    if [ $exist -eq 0 ]; then
        sqlite3 $BD \
                        "INSERT INTO tb_verificador (tienda, fecha_envio, versao) VALUES ('$loja', '$data', '3')"
        if [ $? != 0 ]; then
            echo "ERROR: Falha ao inserir dados no banco de dados."
            sleep 1
            menuTCGertec3
        else
            echo "Dados salvos com sucesso."
            sleep 1
            menuTCGertec3
        fi
    else
        sqlite3 $BD \
                        "UPDATE tb_verificador SET fecha_envio = '$data', versao = '3' WHERE ID=(SELECT MAX(ID) FROM tb_verificador WHERE tienda = '$loja')"
        if [ $? != 0 ]; then
            echo "ERROR: Falha ao inserir dados no banco de dados."
            sleep 1
            menuTCGertec3
        else
            echo "Dados salvos com sucesso."
            sleep 1
            menuTCGertec3
        fi
    fi

elif [ $FUNC -eq 4 ]; then
     
    exist=`echo "SELECT COUNT(id) FROM tb_imp_fidelizacao WHERE loja = '$loja';" | sqlite3 $BD`
    if [ $exist -eq 0 ]; then
        sqlite3 $BD \
                        "INSERT INTO tb_imp_fidelizacao (loja, data) VALUES ('$loja', '$data')"
        if [ $? != 0 ]; then
            echo "ERROR: Falha ao inserir dados no banco de dados."
            sleep 1
            menuDisabledTM88
        else
            echo "Dados salvos com sucesso."
            sleep 1
            menuDisabledTM88
        fi
    else
        sqlite3 $BD \
                        "UPDATE tb_imp_fidelizacao SET data = '$data' WHERE ID=(SELECT MAX(ID) FROM tb_imp_fidelizacao WHERE loja LIKE '$loja')"
        if [ $? != 0 ]; then
            echo "ERROR: Falha ao inserir dados no banco de dados."
            sleep 1
            menuDisabledTM88
        else
            echo "Dados salvos com sucesso."
            sleep 1
            menuDisabledTM88
        fi
    fi

elif [ $FUNC -eq 5 ]; then
     
    exist=`echo "SELECT COUNT(id) FROM tb_actual_paf WHERE tienda = '$loja';" | sqlite3 $BD`
    if [ $exist -eq 0 ]; then
        sqlite3 $BD \
                        "INSERT INTO tb_actual_paf (tienda, active) VALUES ('$loja', '$RESP')"
        if [ $? != 0 ]; then
            echo "ERROR: Falha ao inserir dados no banco de dados."
            sleep 1
            menuDeleteActualPAF
        else
            echo "Dados salvos com sucesso."
            sleep 1
            menuDeleteActualPAF
        fi
    else
        sqlite3 $BD \
                        "UPDATE tb_actual_paf SET active = '$RESP' WHERE ID=(SELECT MAX(ID) FROM tb_actual_paf WHERE tienda LIKE '$loja')"
        if [ $? != 0 ]; then
            echo "ERROR: Falha ao inserir dados no banco de dados."
            sleep 1
            menuDeleteActualPAF
        else
            echo "Dados salvos com sucesso."
            sleep 1
            menuDeleteActualPAF
        fi
    fi
elif [ $FUNC -eq 6 ]; then

    exist=`echo "SELECT COUNT(id) FROM tb_install_oferta WHERE tienda = '$loja';" | sqlite3 $BD`
    if [ $exist -eq 0 ]; then
        sqlite3 $BD \
                        "INSERT INTO tb_install_oferta (tienda, n_tpvs,data_instalacao, data_envio, status) VALUES ('$loja', '0','$data', '$dt_enivo', 'pendente')"
        if [ $? != 0 ]; then
            echo "ERROR: Falha ao inserir dados no banco de dados."
            sleep 1
            menuOfertaMenosVenda
        else
            echo "Dados salvos com sucesso."
            sleep 1
            menuOfertaMenosVenda
        fi
    else
        sqlite3 $BD \
                        "UPDATE tb_install_oferta SET data_instalacao = '$data', data_envio = '$dt_enivo' WHERE ID=(SELECT MAX(ID) FROM tb_install_oferta WHERE tienda = '$loja')"
        if [ $? != 0 ]; then
            echo "ERROR: Falha ao inserir dados no banco de dados."
            sleep 1
            menuOfertaMenosVenda
        else
            echo "Dados salvos com sucesso."
            sleep 1
            menuOfertaMenosVenda
        fi
    fi

fi

}

function AtivarVariasLoja(){
    
    echo "02.1 Ler TXT"
    echo "02.2 Lojas em sequência"
    echo "99   Exit/Quit"
    read -p "" metodo
    
    case $metodo in
        "02.1"|"2.1") # Ler arquivo de texto
            echo "*** LER ARQUIVO DE TEXTO ***"
            sleep 1
            echo "Arquivo deve se chamar arq_srv_remoto.txt"
            echo "                             Loja |Data_Envio|Data_instalacao"
            echo "Estrura de cada linha do txt 00000|00000000|00000000"
            echo "02.1.1 Informe o delimitador [ Default='|']"
            read -p "       Disponiveis [ '|' ',' ';' '-' ] :" delimitador
            # Validadmos se as variáveis estão vazias
            [ "$delimitador" ] || { echo -e "\033[01;31m ERROR: Delimitador não informado, será colocado o valor DEFAULT.\033[00;37m" ; $delimit="|"; }
            case $delimitador in
                "|") # Selecionado PIPER
                    delimit="|"
                    LerTXT
                ;;
                ",") # Selecionado virgula
                    delimit=","
                    LerTXT
                ;;
                ";") # Selecionado ponto e virgula
                    delimit=";"
                    LerTXT
                ;;
                "-") # Selecionado Hifen
                    delimit="-"
                    LerTXT
                ;;
                *)
                    echo "Delimitador informado, não foi reconhecido."
                    sleep 1
                    AtivarVariasLoja
            esac
      
        ;;
        "02.2"|"2.2") # Lojas em sequencia
            echo "*** LOJAS EM SEQUENCIA ***"
            LojasEmSeq
            sleep 1
        ;;
        "99") # Exit/Quit
            sleep 1
            main
        ;;
        *)
            echo "Opção inválida."
            AtivarLoja
    esac  
}

function LerTXT(){

    echo "*** VALIDANDO ARQUIVO DE TEXTO ***"
    sleep 1
    ARQ="arq_srv_remoto.txt"
    ARQ_SQL="arq_srv_remoto.SQL"

    if [ -e $ARQ ]; then
        nlinhas=`cat $ARQ | wc -l`
        nlinhasc=`cat $ARQ | grep "$delimit" | wc -l`
        if [ $nlinhas -eq 0 ]; then
            echo -e "\033[01;31mDetectado erro na estrutura do txt.\033[00;37m"
            sleep 1
            main
        fi
        if [ $nlinhas != $nlinhasc ]; then
            echo -e "\033[01;31mDetectado erro na estrutura do txt.\033[00;37m"
            sleep 1
            main
        else
            echo "*** LENDO ARQUIVO DE TEXTO ***"
            sleep 1
            IFS="$delimit"
            i=0
            while read shop dtinstall denvio;
            do
                let "i = i +1"
                if [[ $shop = ?(+|-)+([0-9]) ]]; then
                    echo ""
                else
                    echo -e "\033[01;31mOps, valor informado não é um número inteiro!\033[00;37m"
                    if [ -e $ARQ_SQL ]; then
                        rm -vf $ARQ_SQL
                    fi
                    sleep 1
                    break
                    main
                fi
                if [[ $dtinstall = ?(+|-)+([0-9]) ]]; then
                    echo ""
                else
                    echo -e "\033[01;31mOps, valor informado não é um número inteiro!\033[00;37m"
                    if [ -e $ARQ_SQL ]; then
                        rm -vf $ARQ_SQL
                    fi
                    sleep 1
                    break
                    main
                fi
                if [[ $denvio = ?(+|-)+([0-9]) ]]; then
                    echo ""
                else
                    echo -e "\033[01;31mOps, valor informado não é um número inteiro!\033[00;37m"
                    if [ -e $ARQ_SQL ]; then
                        rm -vf $ARQ_SQL
                    fi
                    sleep 1
                    break
                    main
                fi
                shop=`printf "%05d" $shop`
                dt=`echo ${#dtinstall}`
                dtt=`echo ${#denvio}`
                if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
                    echo -e "\033[01;31mDetectado erro na data, linha [$i], revise o txt.\033[00;37m"
                    if [ -e $ARQ_SQL ]; then
                        rm -vf $ARQ_SQL
                    fi
                    sleep 1
                    break
                    main
                elif [ $dtt -ge 9 ] || [ $dtt -le 7 ]; then
                    echo -e "\033[01;31mDetectado erro na data, linha [$i], revise o txt.\033[00;37m"
                    if [ -e $ARQ_SQL ]; then
                        rm -vf $ARQ_SQL
                    fi
                    sleep 1
                    break
                    main
                else
                    if [ $FUNC -eq 1 ]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_printer_brother WHERE loja = '$shop';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            echo "INSERT INTO tb_printer_brother (loja, data, data_instalacao) VALUES ( '$shop', '$dtinstall', '$denvio');" >> $ARQ_SQL
                        else
                            echo "UPDATE tb_printer_brother SET data = '$dtinstall', data_instalacao = '$denvio' WHERE loja = '$shop';" >> $ARQ_SQL
                        fi
                    elif [ $FUNC -eq 2 ]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            echo "INSERT INTO tb_verificador (tienda, fecha_envio, versao) VALUES ( '$shop', '$dtinstall', '2');" >> $ARQ_SQL
                        else
                            echo "UPDATE tb_verificador SET fecha_envio = '$dtinstall', versao = '2' WHERE tienda = '$shop';" >> $ARQ_SQL
                        fi
                    elif [ $FUNC -eq 3 ]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            echo "INSERT INTO tb_verificador (tienda, fecha_envio, versao) VALUES ( '$shop', '$dtinstall', '3');" >> $ARQ_SQL
                        else
                            echo "UPDATE tb_verificador SET fecha_envio = '$dtinstall', versao = '3' WHERE tienda = '$shop';" >> $ARQ_SQL
                        fi
                    elif [ $FUNC -eq 4 ]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_imp_fidelizacao WHERE loja = '$shop';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            echo "INSERT INTO tb_imp_fidelizacao (loja, data) VALUES ( '$shop', '$dtinstall');" >> $ARQ_SQL
                        else
                            echo "UPDATE tb_imp_fidelizacao SET data = '$dtinstall' WHERE ID=(SELECT MAX(ID) FROM tb_imp_fidelizacao WHERE tienda LIKE '$shop');" >> $ARQ_SQL
                        fi
                    elif [ $FUNC -eq 5]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_actual_paf WHERE tienda = '$shop';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            echo "INSERT INTO tb_actual_paf (tienda, active) VALUES ( '$shop', '1');" >> $ARQ_SQL
                        else
                            echo "UPDATE tb_actual_paf SET active = '1' WHERE tienda = '$shop';" >> $ARQ_SQL
                        fi
                    elif [ $FUNC -eq 6 ]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_install_oferta WHERE tienda = '$shop';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            echo "INSERT INTO tb_install_oferta (tienda, n_tpvs, data_instalacao, data_envio) VALUES ( '$shop', '0','$dtinstall', '$denvio');" >> $ARQ_SQL
                        else
                            echo "UPDATE tb_install_oferta SET data_instalacao = '$dtinstall', data_envio = '$denvio' WHERE tienda = '$shop';" >> $ARQ_SQL
                        fi
                    fi
                fi
                [ "$?" = "0" ] && echo -e "\033[01;32mLoja ['$shop'].['$dtinstall'] gravada com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gravar loja no banco de dedos.Err[$?].\033[00;37m"
            done < $ARQ
            if [ -e $ARQ_SQL ]; then    
                sqlite3 $BD < $ARQ_SQL
            fi
        fi
        if [ -e $ARQ_SQL ]; then
            rm -vf $ARQ_SQL
        fi
    else
        echo "Arquivo ['$ARQ'] não foi encontrado no diretorio atual."
    fi
    main
}

function LojasEmSeq(){

    ARQ_SQL="arq_srvremoto.SQL"
    ARQ_VAR="array_lojas.txt"
    echo "Informe as lojas em sequencia separados por um espaço, a data inserida será igual amanhã:"
    read -p "Digite o número das loja: " shops
    # Validadmos se as variáveis estão vazias
    [ "$shops" ] || { echo -e "\033[01;31m ERROR: Não foi detectado nenhum caracter"; sleep 1 ; main ;}
    echo $shops > $ARQ_VAR
    # Call função array 
    group_shop
    i=0
    dtinstall=`date +%Y%m%d -d '+1 days'`
    dtenvio=`date +%Y%m%d -d '+2 days'`
    while [ $i != ${#arrshops[@]} ]
    do
        if [ "$x" == "0" ];then
            echo "Loja: "${arrshops[$x]}"."
        else
            loja="${arrshops[$i]}"
            lj=`echo ${#loja}`
            if [ $lj -ge 5 ]; then
                echo "Informe até 4 caracteres númericos, o valor [${arrshops[$i]}] está sendo ignorado."
            else
                 # Validamos se os campos informados são númericos
                if [[ $loja = ?(+|-)+([0-9]) ]]; then
                    shop=`printf "%05d" ${arrshops[$i]}`
                    if [ $FUNC -eq  1 ]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_printer_brother WHERE loja = '$shop';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            echo "INSERT INTO tb_printer_brother (loja, data, data_instalacao) VALUES ( '$shop', '$dtinstall', '$dtenvio');" >> $ARQ_SQL
                        else
                            echo "UPDATE tb_printer_brother SET data = '$dtinstall', data_instalacao = '$dtenvio' WHERE loja = '$shop';" >> $ARQ_SQL
                        fi
                    elif [ $FUNC -eq 2 ]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            echo "INSERT INTO tb_verificador (tienda, fecha_envio, versao) VALUES ( '$shop', '$dtinstall', '2');" >> $ARQ_SQL
                        else
                            echo "UPDATE tb_verificador SET fecha_envio = '$dtinstall', versao = '2' WHERE tienda = '$shop';" >> $ARQ_SQL
                        fi
                    elif [ $FUNC -eq 3 ]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            echo "INSERT INTO tb_verificador (tienda, fecha_envio, versao) VALUES ( '$shop', '$dtinstall', '3');" >> $ARQ_SQL
                        else
                            echo "UPDATE tb_verificador SET fecha_envio = '$dtinstall', versao = '3' WHERE tienda = '$shop';" >> $ARQ_SQL
                        fi
                    elif [ $FUNC -eq 4 ]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_imp_fidelizacao WHERE loja = '$shop';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            echo "INSERT INTO tb_imp_fidelizacao (loja, data) VALUES ( '$shop', '$dtinstall');" >> $ARQ_SQL
                        else
                            echo "UPDATE tb_imp_fidelizacao SET data = '$dtinstall' WHERE ID=(SELECT MAX(ID) FROM tb_imp_fidelizacao WHERE tienda LIKE '$shop');" >> $ARQ_SQL
                        fi
                    elif [ $FUNC -eq 5 ]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_actual_paf WHERE tienda = '$shop';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            echo "INSERT INTO tb_actual_paf (tienda, active) VALUES ( '$shop', '1');" >> $ARQ_SQL
                        else
                            echo "UPDATE tb_actual_paf SET active = '1' WHERE tienda = '$shop';" >> $ARQ_SQL
                        fi
                    elif [ $FUNC -eq 6 ]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_install_oferta WHERE tienda = '$shop';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            echo "INSERT INTO tb_install_oferta (tienda, n_tpvs, data_instalacao, data_envio) VALUES ( '$shop', '0', '$dtinstall', '$dtenvio');" >> $ARQ_SQL
                        else
                            echo "UPDATE tb_install_oferta SET data_instalacao = '$dtinstall', data_envio = '$dtenvio' WHERE tienda = '$shop';" >> $ARQ_SQL
                        fi
                    fi
                else
                    echo "Ops, valor [${arrshops[$i]}] informado não é um número inteiro, e está sendo ignorado."
                fi
            fi
        fi
    let "i = i +1"
    done
    sqlite3 $BD < $ARQ_SQL
    [ "$?" = "0" ] && echo -e "\033[01;32mLojas gravadas com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gravar loja no banco de dedos.Err[$?].\033[00;37m"
    rm -f $ARQ_VAR
    rm -f $ARQ_SQL
    main
}

function group_shop(){

    ARQ_ARRAY="array_lojas.txt"
	while read lojas; 
	do
        progs=("$lojas")
	done < $ARQ_ARRAY
	arrshops=(${progs[0]})

}

function ConsultarLoja(){

    if [ $FUNC -eq 1 ] || [ $FUNC -eq 2 ] || [ $FUNC -eq 3 ] || [ $FUNC -eq 4 ]; then
        echo "03.1 Consultar por loja"
        echo "03.2 Consultar por data de instalação"
    elif [ $FUNC -eq 5 ]; then
        echo "03.1 Consultar por loja"
    fi
    read -p "Digite a opção: " opcao
    case $opcao in
        "03.1"|"3.1"|"03.01")
            read -p "Digite o número das loja: " shop
            # Validadmos se as variáveis estão vazias
            [ "$shop" ] || { echo -e "\033[01;31m ERROR: Não foi detectado nenhum caracter"; sleep 1 ; main ; }
            # Validamos a quantidade de caracteres informados
            lj=`echo ${#shop}`
            if [ $lj -ge 5 ]; then
                echo "Informe até 4 caracteres númericos. Ex.: 2, 1000, 9527, 165."
                sleep 1
                main
            fi
            # Validamos se os campos informados são númericos
            if [[ $shop = ?(+|-)+([0-9]) ]]; then
                if [ $FUNC -eq 1 ]; then
                    shop=`printf "%05d" $shop`
                    exist=`echo "SELECT COUNT(id) FROM tb_printer_brother WHERE loja = '$shop';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        echo "Loja [$shop] não foi encontrada no banco de dados."
                        sleep 5
                        menuPrinterBrother
                    else
                        echo "SELECT * FROM tb_printer_brother WHERE loja = '$shop';" | sqlite3 $BD
                        sleep 5
                        [ "$?" = "0" ] && echo -e "" || echo -e "\033[01;31mFalha durante a consulta loja [$shop].Err[$?].\033[00;37m"
                        menuPrinterBrother
                    fi
                elif [ $FUNC -eq 2 ]; then
                    shop=`printf "%05d" $shop`
                    exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        echo "Loja [$shop] não foi encontrada no banco de dados."
                        sleep 5
                        menuTCGertec2
                    else
                        echo "SELECT * FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD
                        [ "$?" = "0" ] && echo -e "" || echo -e "\033[01;31mFalha durante a consulta loja [$shop].Err[$?].\033[00;37m"
                        sleep 5
                        menuTCGertec2
                    fi
                elif [ $FUNC -eq 3 ]; then
                    shop=`printf "%05d" $shop`
                    exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        echo "Loja [$shop] não foi encontrada no banco de dados."
                        sleep 5
                        menuTCGertec3
                    else
                        echo "SELECT * FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD
                        [ "$?" = "0" ] && echo -e "" || echo -e "\033[01;31mFalha durante a consulta loja [$shop].Err[$?].\033[00;37m"
                        sleep 5
                        menuTCGertec3
                    fi
                elif [ $FUNC -eq 4 ]; then
                    shop=`printf "%05d" $shop`
                    exist=`echo "SELECT COUNT(id) FROM tb_imp_fidelizacao WHERE loja = '$shop';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        echo "Loja [$shop] não foi encontrada no banco de dados."
                        sleep 5
                        menuDisabledTM88
                    else
                        echo "SELECT * FROM tb_imp_fidelizacao WHERE loja = '$shop';" | sqlite3 $BD
                        [ "$?" = "0" ] && echo -e "" || echo -e "\033[01;31mFalha durante a consulta loja [$shop].Err[$?].\033[00;37m"
                        sleep 5
                        menuDisabledTM88
                    fi
                elif [ $FUNC -eq 5 ]; then
                    shop=`printf "%05d" $shop`
                    exist=`echo "SELECT COUNT(id) FROM tb_actual_paf WHERE tienda = '$shop';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        echo "Loja [$shop] não foi encontrada no banco de dados."
                        sleep 5
                        menuDeleteActualPAF
                    else
                        echo "SELECT * FROM tb_actual_paf WHERE tienda = '$shop';" | sqlite3 $BD
                        [ "$?" = "0" ] && echo -e "" || echo -e "\033[01;31mFalha durante a consulta loja [$shop].Err[$?].\033[00;37m"
                        sleep 5
                        menuDeleteActualPAF
                    fi
                elif [ $FUNC -eq 6 ]; then
                    shop=`printf "%05d" $shop`
                    exist=`echo "SELECT COUNT(id) FROM tb_install_oferta WHERE tienda = '$shop';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        echo "Loja [$shop] não foi encontrada no banco de dados."
                        sleep 5
                        menuOfertaMenosVenda
                    else
                        echo "SELECT * FROM tb_install_oferta WHERE tienda = '$shop';" | sqlite3 $BD
                        [ "$?" = "0" ] && echo -e "" || echo -e "\033[01;31mFalha durante a consulta loja [$shop].Err[$?].\033[00;37m"
                        sleep 5
                        menuOfertaMenosVenda
                    fi
                fi
            else
                echo "Ops, valor informado não é um número inteiro!"
                sleep 1
                main
            fi
        ;;
        "03.2"|"03.02"|"3.2")
            read -p "Informe a data de instação (YYYYMMDD): " datinstalacao
            # Validadmos se as variáveis estão vazias
            [ "$datinstalacao" ] || { echo -e "\033[01;31m ERROR: Não foi detectado nenhum caracter"; sleep 1 ; main ; }
            # Validamos a quantidade de caracteres informados
            dt=`echo ${#datinstalacao}`
            if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
                echo "Informe até 8 caracteres númericos. Ex.: 20190101."
                sleep 1
                main
            fi
            # Validamos se os campos informados são númericos
            if [[ $datinstalacao = ?(+|-)+([0-9]) ]]; then
                if [ $FUNC -eq 1 ]; then
                    exist=`echo "SELECT COUNT(id) FROM tb_printer_brother WHERE data = '$datinstalacao';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        echo "Não foi encontrada nenhuma loja no banco de dados com esta data[$datinstalacao]."
                        sleep 5
                        menuPrinterBrother
                    else
                        echo "SELECT * FROM tb_printer_brother WHERE data = '$datinstalacao';" | sqlite3 $BD
                        [ "$?" = "0" ] && echo -e "" || echo -e "\033[01;31mFalha durante a consulta loja [$shop].Err[$?].\033[00;37m"
                        sleep 5
                        menuPrinterBrother
                    fi
                elif [ $FUNC -eq 2 ]; then
                    exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE fecha_envio = '$datinstalacao';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        echo "Não foi encontrada nenhuma loja no banco de dados com esta data[$datinstalacao]."
                        sleep 5
                        menuTCGertec2
                    else
                        echo "SELECT * FROM tb_verificador WHERE fecha_envio = '$datinstalacao';" | sqlite3 $BD
                        [ "$?" = "0" ] && echo -e "" || echo -e "\033[01;31mFalha durante a consulta loja [$shop].Err[$?].\033[00;37m"
                        sleep 5
                        menuTCGertec2
                    fi
                elif [ $FUNC -eq 3 ]; then
                    exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE fecha_envio = '$datinstalacao';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        echo "Não foi encontrada nenhuma loja no banco de dados com esta data[$datinstalacao]."
                        sleep 5
                        menuTCGertec3
                    else
                        echo "SELECT * FROM tb_verificador WHERE fecha_envio = '$datinstalacao';" | sqlite3 $BD
                        [ "$?" = "0" ] && echo -e "" || echo -e "\033[01;31mFalha durante a consulta loja [$shop].Err[$?].\033[00;37m"
                        sleep 5
                        menuTCGertec3
                    fi
                elif [ $FUNC -eq 4 ]; then
                    exist=`echo "SELECT COUNT(id) FROM tb_imp_fidelizacao WHERE data = '$datinstalacao';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        echo "Não foi encontrada nenhuma loja no banco de dados com esta data[$datinstalacao]."
                        sleep 5
                        menuPrinterBrother
                    else
                        echo "SELECT * FROM tb_imp_fidelizacao WHERE data = '$datinstalacao';" | sqlite3 $BD
                        [ "$?" = "0" ] && echo -e "" || echo -e "\033[01;31mFalha durante a consulta loja [$shop].Err[$?].\033[00;37m"
                        sleep 5
                        menuPrinterBrother
                    fi
                elif [ $FUNC -eq 6 ]; then
                    exist=`echo "SELECT COUNT(id) FROM tb_install_oferta WHERE data_instalacao = '$datinstalacao';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        echo "Não foi encontrada nenhuma loja no banco de dados com esta data[$datinstalacao]."
                        sleep 5
                        menuOfertaMenosVenda
                    else
                        echo "SELECT * FROM tb_install_oferta WHERE data_instalacao = '$datinstalacao';" | sqlite3 $BD
                        [ "$?" = "0" ] && echo -e "" || echo -e "\033[01;31mFalha durante a consulta loja [$shop].Err[$?].\033[00;37m"
                        sleep 5
                        menuOfertaMenosVenda
                    fi
                fi
            else
                echo "Ops, valor informado não é um número inteiro!"
                sleep 1
                main
            fi
        ;;
        *)
            echo "Opção Inválida."
    esac

}

function DesativarLoja(){

    read -p "Digite o número das loja: " shop
    # Validadmos se as variáveis estão vazias
    [ "$shop" ] || { echo -e "\033[01;31m ERROR: Não foi detectado nenhum caracter"; sleep 1 ; main ; }
    # Validamos a quantidade de caracteres informados
    lj=`echo ${#shop}`
    if [ $lj -ge 5 ]; then
        echo "Informe até 4 caracteres númericos. Ex.: 2, 1000, 9527, 165."
        sleep 1
        main
    fi
    # Validamos se os campos informados são númericos
    if [[ $shop = ?(+|-)+([0-9]) ]]; then
        if [ $FUNC -eq 1 ]; then
            shop=`printf "%05d" $shop`
            exist=`echo "SELECT COUNT(id) FROM tb_printer_brother WHERE loja = '$shop';" | sqlite3 $BD`
            if [ $exist -eq 0 ]; then
                echo "Loja não foi encontrada no banco de dados."
            else
                echo "UPDATE tb_printer_brother SET data = '99999999', data_instalacao = '99999999' WHERE loja = '$shop';" | sqlite3 $BD
                [ "$?" = "0" ] && echo -e "\033[01;32mDados para loja ['$shop'].['99999999'] gravada com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gravar loja no banco de dedos.Err[$?].\033[00;37m"
            fi
        elif [ $FUNC -eq 2 ]; then
            shop=`printf "%05d" $shop`
            exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD`
            if [ $exist -eq 0 ]; then
                echo "Loja não foi encontrada no banco de dados."
            else
                echo "UPDATE tb_verificador SET fecha_envio = '99999999' WHERE tienda = '$shop';" | sqlite3 $BD
                [ "$?" = "0" ] && echo -e "\033[01;32mDados para loja ['$shop'].['99999999'] gravada com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gravar loja no banco de dedos.Err[$?].\033[00;37m"
            fi
        elif [ $FUNC -eq 3 ]; then
            shop=`printf "%05d" $shop`
            exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD`
            if [ $exist -eq 0 ]; then
                echo "Loja não foi encontrada no banco de dados."
            else
                echo "UPDATE tb_verificador SET fecha_envio = '99999999' WHERE tienda = '$shop';" | sqlite3 $BD
                [ "$?" = "0" ] && echo -e "\033[01;32mDados para loja ['$shop'].['99999999'] gravada com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gravar loja no banco de dedos.Err[$?].\033[00;37m"
            fi
        elif [ $FUNC -eq 4 ]; then
            shop=`printf "%05d" $shop`
            exist=`echo "SELECT COUNT(id) FROM tb_imp_fidelizacao WHERE loja = '$shop';" | sqlite3 $BD`
            if [ $exist -eq 0 ]; then
                echo "Loja não foi encontrada no banco de dados."
            else
                echo "UPDATE tb_imp_fidelizacao SET data = '99999999' WHERE loja = '$shop';" | sqlite3 $BD
                [ "$?" = "0" ] && echo -e "\033[01;32mDados para loja ['$shop'].['99999999'] gravada com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gravar loja no banco de dedos.Err[$?].\033[00;37m"
            fi
        elif [ $FUNC -eq 5 ]; then
            shop=`printf "%05d" $shop`
            exist=`echo "SELECT COUNT(id) FROM tb_actual_paf WHERE tienda = '$shop';" | sqlite3 $BD`
            if [ $exist -eq 0 ]; then
                echo "Loja não foi encontrada no banco de dados."
            else
                echo "UPDATE tb_actual_paf SET active = '0' WHERE tienda = '$shop';" | sqlite3 $BD
                [ "$?" = "0" ] && echo -e "\033[01;32mDados para loja ['$shop'].['99999999'] gravada com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gravar loja no banco de dedos.Err[$?].\033[00;37m"
            fi
        elif [ $FUNC -eq 6 ]; then
            shop=`printf "%05d" $shop`
            exist=`echo "SELECT COUNT(id) FROM tb_install_oferta WHERE tienda = '$shop';" | sqlite3 $BD`
            if [ $exist -eq 0 ]; then
                echo "Loja não foi encontrada no banco de dados."
            else
                echo "UPDATE tb_install_oferta SET data_instalacao = '99999999', data_envio = '99999999' WHERE tienda = '$shop';" | sqlite3 $BD
                [ "$?" = "0" ] && echo -e "\033[01;32mDados para loja ['$shop'].['99999999'] gravada com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gravar loja no banco de dedos.Err[$?].\033[00;37m"
            fi
        fi
    else
        echo "Ops, valor informado não é um número inteiro!"
        sleep 1
        main
    fi

}

function GerarLog(){

    if [ $FUNC -eq 1 ]; then
        LOG="/root/srv_remoto/log/error.printerbrother.log"
    elif [ $FUNC -eq 2 ]; then
        LOG="/root/srv_remoto/log/error.tcgertec.log"
    elif [ $FUNC -eq 3 ]; then
        LOG="/root/srv_remoto/log/error.tcgertec.log"
    elif [ $FUNC -eq 4 ]; then
        LOG="/root/srv_remoto/log/error.impfidelizacao.log"
    elif [ $FUNC -eq 5 ]; then
        LOG="/root/srv_remoto/log/error.actualpaf.log"
    elif [ $FUNC -eq 6 ]; then
        LOG="/root/srv_remoto/log/error.oferta.log"
    fi
    echo "05.1 Gerar log por Loja"
    echo "05.2 Gerar log por data"
    read -p "Escolha opção: " fator
    case $fator in
        "05.1"|"05.01"|"5.1")
            read -p "Digite o número das loja: " shop
            # Validadmos se as variáveis estão vazias
            [ "$shop" ] || { echo -e "\033[01;31m ERROR: Não foi detectado nenhum caracter"; sleep 1 ; main ; }
            # Validamos a quantidade de caracteres informados
            lj=`echo ${#shop}`
            if [ $lj -ge 5 ]; then
                echo "Informe até 4 caracteres númericos. Ex.: 2, 1000, 9527, 165."
                sleep 1
                main
            fi
            # Validamos se os campos informados são númericos
            if [[ $shop = ?(+|-)+([0-9]) ]]; then
                if [ $FUNC -eq 1 ]; then
                    shop=`printf "%05d" $shop`
                    cat $LOG | grep -i "Loja.*$shop" > $LOG1
                    cat $LOG | grep -i "Loja.*$shop" | less
                    [ "$?" = "0" ] && echo -e "\033[01;32mLog gerado com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gerar Log.Err[$?].\033[00;37m"
                elif [ $FUNC -eq 2 ]; then
                    shop=`printf "%05d" $shop`
                    cat $LOG | grep -i "Loja.*$shop" > $LOG1
                    cat $LOG | grep -i "Loja.*$shop" | less
                    [ "$?" = "0" ] && echo -e "\033[01;32mLog gerado com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gerar Log.Err[$?].\033[00;37m"
                elif [ $FUNC -eq 3 ]; then
                    shop=`printf "%05d" $shop`
                    cat $LOG | grep -i "Loja.*$shop" > $LOG1
                    cat $LOG | grep -i "Loja.*$shop" | less
                    [ "$?" = "0" ] && echo -e "\033[01;32mLog gerado com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gerar Log.Err[$?].\033[00;37m"
                elif [ $FUNC -eq 4 ]; then
                    shop=`printf "%05d" $shop`
                    cat $LOG | grep -i "Loja.*$shop" > $LOG1
                    cat $LOG | grep -i "Loja.*$shop" | less
                    [ "$?" = "0" ] && echo -e "\033[01;32mLog gerado com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gerar Log.Err[$?].\033[00;37m"
                elif [ $FUNC -eq 5 ]; then
                    shop=`printf "%05d" $shop`
                    cat $LOG | grep -i "Loja.*$shop" > $LOG1
                    cat $LOG | grep -i "Loja.*$shop" | less
                    [ "$?" = "0" ] && echo -e "\033[01;32mLog gerado com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gerar Log.Err[$?].\033[00;37m"
                elif [ $FUNC -eq 6 ]; then
                    shop=`printf "%05d" $shop`
                    cat $LOG | grep -i "Loja.*$shop" > $LOG1
                    cat $LOG | grep -i "Loja.*$shop" | less
                    [ "$?" = "0" ] && echo -e "\033[01;32mLog gerado com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gerar Log.Err[$?].\033[00;37m"
                fi
            else
                echo "Ops, valor informado não é um número inteiro!"
                sleep 1
                main
            fi

        ;;
        "05.2"|"05.02"|"5.2")
            read -p "Informe a data de instação (YYYYMMDD): " datinstalacao
            # Validadmos se as variáveis estão vazias
            [ "$datinstalacao" ] || { echo -e "\033[01;31m ERROR: Não foi detectado nenhum caracter"; sleep 1 ; main ; }
            # Validamos a quantidade de caracteres informados
            dt=`echo ${#datinstalacao}`
            if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
                echo "Informe até 8 caracteres númericos. Ex.: 20190101."
                sleep 1
                main
            fi
            # Validamos se os campos informados são númericos
            if [[ $datinstalacao = ?(+|-)+([0-9]) ]]; then
                if [ $FUNC -eq 1 ]; then
                    cat $LOG | grep -i "^$datinstalacao" > $LOG1
                    cat $LOG | grep -i "^$datinstalacao" | less
                    [ "$?" = "0" ] && echo -e "\033[01;32mLog gerado com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gerar Log.Err[$?].\033[00;37m"
                elif [ $FUNC -eq 2 ]; then
                    cat $LOG | grep -i "^$datinstalacao" > $LOG1
                    cat $LOG | grep -i "^$datinstalacao" | less
                    [ "$?" = "0" ] && echo -e "\033[01;32mLog gerado com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gerar Log.Err[$?].\033[00;37m"
                elif [ $FUNC -eq 3 ]; then
                    cat $LOG | grep -i "^$datinstalacao" > $LOG1
                    cat $LOG | grep -i "^$datinstalacao" | less
                    [ "$?" = "0" ] && echo -e "\033[01;32mLog gerado com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gerar Log.Err[$?].\033[00;37m"
                elif [ $FUNC -eq 4 ]; then
                    cat $LOG | grep -i "^$datinstalacao" > $LOG1
                    cat $LOG | grep -i "^$datinstalacao" | less
                    [ "$?" = "0" ] && echo -e "\033[01;32mLog gerado com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gerar Log.Err[$?].\033[00;37m"
                elif [ $FUNC -eq 5 ]; then
                    cat $LOG | grep -i "^$datinstalacao" > $LOG1
                    cat $LOG | grep -i "^$datinstalacao" | less
                    [ "$?" = "0" ] && echo -e "\033[01;32mLog gerado com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gerar Log.Err[$?].\033[00;37m"
                elif [ $FUNC -eq 6 ]; then
                    cat $LOG | grep -i "^$datinstalacao" > $LOG1
                    cat $LOG | grep -i "^$datinstalacao" | less
                    [ "$?" = "0" ] && echo -e "\033[01;32mLog gerado com sucesso.\033[00;37m" || echo -e "\033[01;31mErro ao gerar Log.Err[$?].\033[00;37m"
                fi
            else
                echo "Ops, valor informado não é um número inteiro!"
                sleep 1
                main
            fi
        ;;
        *)
            echo "Opção inválida."
    esac
}

function ExtrairDados(){

    echo "06.1 Extrair dados completo "
    echo "06.2 Extrair por data de instalação"
    read -p "Escolha opção: " op
    case $op in
            "06.1"|"6.1"|"06.01")
                if [ $FUNC -eq 1 ]; then
                    sqlite3 $BD  ".output relatorio_completo.txt" \ "SELECT * FROM tb_printer_brother;"
                    [ "$?" = "0" ] && echo -e "\033[01;32mRelatorio gerado com sucesso.\033[00;37m"; cat relatorio_completo.txt | less  || echo -e "\033[01;31mErro ao gerar relatorio.Err[$?].\033[00;37m"
                elif [ $FUNC -eq 2 ]; then
                    sqlite3 $BD  ".output relatorio_completo.txt" \ "SELECT * FROM tb_verificador;"
                    [ "$?" = "0" ] && echo -e "\033[01;32mRelatorio gerado com sucesso.\033[00;37m"; cat relatorio_completo.txt | less  || echo -e "\033[01;31mErro ao gerar relatorio.Err[$?].\033[00;37m"
                elif [ $FUNC -eq 3 ]; then
                    sqlite3 $BD  ".output relatorio_completo.txt" \ "SELECT * FROM tb_verificador;"
                    [ "$?" = "0" ] && echo -e "\033[01;32mRelatorio gerado com sucesso.\033[00;37m"; cat relatorio_completo.txt | less  || echo -e "\033[01;31mErro ao gerar relatorio.Err[$?].\033[00;37m"
                elif [ $FUNC -eq 4 ]; then
                    sqlite3 $BD  ".output relatorio_completo.txt" \ "SELECT * FROM tb_imp_fedelizacao;"
                    [ "$?" = "0" ] && echo -e "\033[01;32mRelatorio gerado com sucesso.\033[00;37m"; cat relatorio_completo.txt | less  || echo -e "\033[01;31mErro ao gerar relatorio.Err[$?].\033[00;37m"
                elif [ $FUNC -eq 5 ]; then
                    sqlite3 $BD  ".output relatorio_completo.txt" \ "SELECT * FROM tb_actual_paf;"
                    [ "$?" = "0" ] && echo -e "\033[01;32mRelatorio gerado com sucesso.\033[00;37m"; cat relatorio_completo.txt | less  || echo -e "\033[01;31mErro ao gerar relatorio.Err[$?].\033[00;37m"
                elif [ $FUNC -eq 6 ]; then
                    sqlite3 $BD  ".output relatorio_completo.txt" \ "SELECT * FROM tb_install_oferta;"
                    [ "$?" = "0" ] && echo -e "\033[01;32mRelatorio gerado com sucesso.\033[00;37m"; cat relatorio_completo.txt | less  || echo -e "\033[01;31mErro ao gerar relatorio.Err[$?].\033[00;37m"
                fi
            ;;
            "06.2"|"6.2"|"06.02")
                read -p "Informe a data inicial (YYYYMMDD): " dinstalacao
                # Validadmos se as variáveis estão vazias
                [ "$dinstalacao" ] || { echo -e "\033[01;31m ERROR: Não foi detectado nenhum caracter"; sleep 1 ; main ; }
                # Validamos a quantidade de caracteres informados
                dt=`echo ${#dinstalacao}`
                if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
                    echo "Informe até 8 caracteres númericos. Ex.: 20190101."
                    sleep 1
                    main
                fi
                read -p "Informe a data final (YYYYMMDD): " dtinstalacao
                # Validadmos se as variáveis estão vazias
                [ "$dtinstalacao" ] || { echo -e "\033[01;31m ERROR: Não foi detectado nenhum caracter"; sleep 1 ; main ; }
                # Validamos a quantidade de caracteres informados
                dt=`echo ${#dtinstalacao}`
                if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
                    echo "Informe até 8 caracteres númericos. Ex.: 20190101."
                    sleep 1
                    main
                fi
                # Validamos se os campos informados são númericos
                if [[ $dinstalacao = ?(+|-)+([0-9]) ]]; then
                    if [[ $dtinstalacao = ?(+|-)+([0-9]) ]]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_printer_brother WHERE data LIKE '$dinstalacao';" | sqlite3 $BD`
                        if [ $exist != 0 ]; then
                            if [ $FUNC -eq 1 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_printer_brother WHERE data BETWEEN '$dinstalacao' AND '$dtinstalacao';"
                                [ "$?" = "0" ] && echo -e "\033[01;32mRelatorio gerado com sucesso.\033[00;37m"; cat relatorio_por_periodo.txt | less  || echo -e "\033[01;31mErro ao gerar relatorio.Err[$?].\033[00;37m"
                            elif [ $FUNC -eq 2 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_verificador WHERE fecha_envio BETWEEN '$dinstalacao' AND '$dtinstalacao';"
                                [ "$?" = "0" ] && echo -e "\033[01;32mRelatorio gerado com sucesso.\033[00;37m"; cat relatorio_por_periodo.txt | less  || echo -e "\033[01;31mErro ao gerar relatorio.Err[$?].\033[00;37m"
                            elif [ $FUNC -eq 3 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_verificador WHERE fecha_envio BETWEEN '$dinstalacao' AND '$dtinstalacao';"
                                [ "$?" = "0" ] && echo -e "\033[01;32mRelatorio gerado com sucesso.\033[00;37m"; cat relatorio_por_periodo.txt | less  || echo -e "\033[01;31mErro ao gerar relatorio.Err[$?].\033[00;37m"
                            elif [ $FUNC -eq 4 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_imp_fidelizacao WHERE data BETWEEN '$dinstalacao' AND '$dtinstalacao';"
                                [ "$?" = "0" ] && echo -e "\033[01;32mRelatorio gerado com sucesso.\033[00;37m"; cat relatorio_por_periodo.txt | less  || echo -e "\033[01;31mErro ao gerar relatorio.Err[$?].\033[00;37m"
                            elif [ $FUNC -eq 5 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_actual_paf WHERE status LIKE 'ACTIVE';"
                                [ "$?" = "0" ] && echo -e "\033[01;32mRelatorio gerado com sucesso.\033[00;37m"; cat relatorio_por_periodo.txt | less  || echo -e "\033[01;31mErro ao gerar relatorio.Err[$?].\033[00;37m"
                            elif [ $FUNC -eq 6 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_install_oferta WHERE data_instalacao BETWEEN '$dinstalacao AND '$dtinstalacao';"
                                [ "$?" = "0" ] && echo -e "\033[01;32mRelatorio gerado com sucesso.\033[00;37m"; cat relatorio_por_periodo.txt | less  || echo -e "\033[01;31mErro ao gerar relatorio.Err[$?].\033[00;37m"
                            fi
                        else
                            if [ $FUNC -eq 1 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_printer_brother WHERE data BETWEEN '$dinstalacao' AND '$dtinstalacao';"
                                [ "$?" = "0" ] && echo -e "\033[01;32mRelatorio gerado com sucesso.\033[00;37m"; cat relatorio_por_periodo.txt | less  || echo -e "\033[01;31mErro ao gerar relatorio.Err[$?].\033[00;37m"
                            elif [ $FUNC -eq 2 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_verificador WHERE fecha_envio BETWEEN '$dinstalacao' AND '$dtinstalacao';"
                                [ "$?" = "0" ] && echo -e "\033[01;32mRelatorio gerado com sucesso.\033[00;37m"; cat relatorio_por_periodo.txt | less  || echo -e "\033[01;31mErro ao gerar relatorio.Err[$?].\033[00;37m"
                            elif [ $FUNC -eq 3 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_verificador WHERE fecha_envio BETWEEN '$dinstalacao' AND '$dtinstalacao';"
                                [ "$?" = "0" ] && echo -e "\033[01;32mRelatorio gerado com sucesso.\033[00;37m"; cat relatorio_por_periodo.txt | less  || echo -e "\033[01;31mErro ao gerar relatorio.Err[$?].\033[00;37m"
                            elif [ $FUNC -eq 4 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_imp_fidelizacao WHERE data BETWEEN '$dinstalacao' AND '$dtinstalacao';"
                                [ "$?" = "0" ] && echo -e "\033[01;32mRelatorio gerado com sucesso.\033[00;37m"; cat relatorio_por_periodo.txt | less  || echo -e "\033[01;31mErro ao gerar relatorio.Err[$?].\033[00;37m"
                            elif [ $FUNC -eq 5 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_actual_paf WHERE status LIKE 'ACTIVE';"
                            elif [ $FUNC -eq 6 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_install_oferta WHERE data_instalacao BETWEEN '$dinstalacao AND '$dtinstalacao';"
                                [ "$?" = "0" ] && echo -e "\033[01;32mRelatorio gerado com sucesso.\033[00;37m"; cat relatorio_por_periodo.txt | less  || echo -e "\033[01;31mErro ao gerar relatorio.Err[$?].\033[00;37m"
                            fi
                        fi
                    else
                        echo "Ops, valor informado não é um número inteiro!"
                        sleep 1
                        main
                    fi
                else
                    echo "Ops, valor informado não é um número inteiro!"
                    sleep 1
                    main
                fi
            ;;
            *)
                echo "Opção inválida."
    esac
}

function ForcedOneShop(){

# Solicitamos ao usuario que informe a loja e data de instação
read -p "Informe a loja: " loja
# Validadmos se as variáveis estão vazias
[ "$loja" ] || { echo -e "\033[01;31m ERROR: Não foi informado a loja.\033[00;37m" ; sleep 1 ; main ; }
# Validamos a quantidade de caracteres informados
lj=`echo ${#loja}`
if [ $lj -ge 5 ]; then
    echo "Informe até 4 caracteres númericos. Ex.: 2, 1000, 9527, 165."
    sleep 1
    main
fi
# Validamos se os campos informados são númericos
if [[ $loja = ?(+|-)+([0-9]) ]]; then
    echo ""
else
    echo "Ops, valor informado não é um número inteiro!"
    sleep 1
    main
fi
loja=`printf "%05d" $loja`
EXISTE=`echo "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$loja';" | sqlite3 $BD`

if [ $EXISTE -eq 1 ]; then
    # GetIP
    ip=`echo "SELECT ip FROM tb_ip WHERE loja LIKE '$loja';" | sqlite3 $BD`
else
    echo "Loja não encontrada no banco de dados."
    read -p "Deseja informar o IP, manualmente? [s/Y/n/N] " decisao
    case $decisao in
        "s"|"S"|"y"|"Y")
            read -p "Digite IP (XXX.XXX.XXX.XXX)= " ipss
            # Validadmos se as variáveis estão vazias
            [ "$ipss" ] || { echo -e "\033[01;31m ERROR: Não foi informado informado.\033[00;37m" ; sleep 1 ; menuPrinterBrother ; }
            ip=$ipss
        ;;
        "n"|"N")
            menuPrinterBrother  
        ;;
        *)
            echo "Opção inválida."
            menuPrinterBrother
    esac

fi

sshpass -p root ssh -o ConnectTimeout=1 $ip exit
return=$?
if [ $return -eq 0 ]; then
    if [ $FUNC -eq 1 ]; then
        FILE1="/root/srv_remoto/tgz/configuracaoBrother.tgz"
        FILE2="/root/srv_remoto/tgz/666"
        sshpass -p root scp -P10001 $FILE1 root@$ip:/confdia/descargas/
		sshpass -p root scp -P10001 $FILE2 root@$ip:/confdia/bin/
        sshpass -p root ssh -p10001 -l root $ip 'tar -xzvf /confdia/descargas/configuracaoBrother.tgz -C /'
		return=$?
        if [ $return -eq 0 ]; then
            echo "Impressora Brother instalado com sucesso."
            sleep 5
            datae=`date +%Y%m%d -d '+1 days'`
            datai=`date +%Y%m%d -d '+2 days'`
            exist=`echo "SELECT COUNT(id) FROM tb_printer_brother WHERE loja = '$loja';" | sqlite3 $BD`
            if [ $exist -eq 0 ]; then
                sqlite3 $BD \
                            "INSERT INTO tb_printer_brother (loja, data, data_instalacao) VALUES ('$loja', '$datae', '$datai')"
                if [ $? != 0 ]; then
                    echo "ERROR: Falha ao inserir dados no banco de dados."
                    sleep 1
                    menuPrinterBrother
                else
                    echo "Dados salvos com sucesso."
                    sleep 1
                    menuPrinterBrother
                fi
            else
                sqlite3 $BD \
                        "UPDATE tb_printer_brother SET data = '$datae', data_instalacao = '$datai' WHERE ID=(SELECT MAX(ID) FROM tb_printer_brother WHERE loja LIKE '$loja')"
                if [ $? != 0 ]; then
                    echo "ERROR: Falha ao inserir dados no banco de dados."
                    sleep 1
                    menuPrinterBrother
                else
                    echo "Dados salvos com sucesso."
                    sleep 1
                    menuPrinterBrother
                fi
            fi
        else
            echo "Falha ao instalar Brother."
            sleep 5
            menuPrinterBrother
        fi
    elif [ $FUNC -eq 2 ]; then
        FILE1="/root/srv_remoto/tgz/tcgertec2-0.tgz"
        sshpass -p root ssh -p10001 root@$ip '/etc/init.d/TCgertec stop'
        sshpass -p root ssh -p10001 root@$ip 'rm -vf /etc/init.d/TCgertec'
        sshpass -p root scp -P10001 $FILE1 root@$ip:/confdia/descargas/
        sshpass -p root ssh -p10001 -l root $ip 'tar -xzvf /confdia/descargas/tcgertec2-0.tgz -C /'
        return=$?
        if [ $return -eq 0 ]; then
            echo "TCGertec instalado com sucesso."
            sshpass -p root ssh -p10001 -l root $ip 'bash -x /root/TCgertec.sh restart'
            sleep 5
            datae=`date +%Y%m%d -d '+1 days'`
            datai=`date +%Y%m%d -d '+2 days'`
            exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$loja';" | sqlite3 $BD`
            if [ $exist -eq 0 ]; then
                sqlite3 $BD \
                                "INSERT INTO tb_verificador (tienda, fecha_envio, versao) VALUES ('$loja', '$datae', '2')"
                if [ $? != 0 ]; then
                    echo "ERROR: Falha ao inserir dados no banco de dados."
                    sleep 1
                    menuTCGertec2
                else
                    echo "Dados salvos com sucesso."
                    sleep 1
                    menuTCGertec2
                fi
            else
                sqlite3 $BD \
                                "UPDATE tb_verificador SET fecha_envio = '$datae', versao = '2' WHERE ID=(SELECT MAX(ID) FROM tb_verificador WHERE tienda = '$loja')"
                if [ $? != 0 ]; then
                    echo "ERROR: Falha ao inserir dados no banco de dados."
                    sleep 1
                    menuTCGertec2
                else
                    echo "Dados salvos com sucesso."
                    sleep 1
                    menuTCGertec2
                fi
            fi
        else
            echo "Falha ao instalar TCGertec."
            sleep 5
            menuTCGertec2
        fi
    elif [ $FUNC -eq 3 ]; then
        FILE1="/root/srv_remoto/tgz/tcgertec3-0.tgz"
        sshpass -p root ssh -p10001 root@$ip '/etc/init.d/TCgertec stop'
        sshpass -p root ssh -p10001 root@$ip 'rm -vf /etc/init.d/TCgertec'
        sshpass -p root scp -P10001 $FILE1 root@$ip:/confdia/descargas/
        sshpass -p root ssh -p10001 -l root $ip 'tar -xzvf /confdia/descargas/tcgertec3-0.tgz -C /'
        return=$?
        if [ $return -eq 0 ]; then
            echo "TCGertec instalado com sucesso."
            sshpass -p root ssh -p10001 -l root $ip 'bash -x /root/TCgertec.sh restart'
            sleep 5
            datae=`date +%Y%m%d -d '+1 days'`
            datai=`date +%Y%m%d -d '+2 days'`
            exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$loja';" | sqlite3 $BD`
            if [ $exist -eq 0 ]; then
                sqlite3 $BD \
                                "INSERT INTO tb_verificador (tienda, fecha_envio, versao) VALUES ('$loja', '$datae', '3')"
                if [ $? != 0 ]; then
                    echo "ERROR: Falha ao inserir dados no banco de dados."
                    sleep 1
                    menuTCGertec3
                else
                    echo "Dados salvos com sucesso."
                    sleep 1
                    menuTCGertec3
                fi
            else
                sqlite3 $BD \
                                "UPDATE tb_verificador SET fecha_envio = '$datae', versao = '3' WHERE ID=(SELECT MAX(ID) FROM tb_verificador WHERE tienda = '$loja')"
                if [ $? != 0 ]; then
                    echo "ERROR: Falha ao inserir dados no banco de dados."
                    sleep 1
                    menuTCGertec3
                else
                    echo "Dados salvos com sucesso."
                    sleep 1
                    menuTCGertec3
                fi
            fi
        else
            echo "Falha ao instalar TCGertec."
            sleep 5
            menuTCGertec3
        fi
    elif [ $FUNC -eq 4 ]; then
        echo "07.1 Especificar PDV"
        echo "07.2 Todos PDVs"
        read -p "Opção: " opcion
        case $opcion in
            "7.1"|"07.01"|"07.1")
                read -p "Informe o número do caixa: " pdv
                [ "$pdv" ] || { echo -e "\033[01;31m ERROR: Não foi informado informado.\033[00;37m" ; sleep 1 ; menuDisabledTM88 ; }
                # Validamos se os campos informados são númericos
                if [[ $pdv = ?(+|-)+([0-9]) ]]; then
                    echo ""
                else
                    echo "Ops, valor informado não é um número inteiro!"
                    sleep 1
                    main
                fi
                ntpv=`echo ${#pdv}`
                echo $ntpv
                sleep 1
                if [ $ntpv -ge 2 ]; then
                    echo "Informe até 1 caracteres númericos. Ex.: 1, 2, 9."
                    sleep 1
                    main
                fi
                
                if [ $pdv -ge 1 ]; then
                    sshpass -p root ssh -o ConnectTimeout=1 $ip -p1000$pdv exit
                    return=$?
                    if [ $return -eq 0 ]; then
                        sshpass -p root ssh -o ConnectTimeout=1 $ip -p1000$pdv ' . /confdia/bin/setvari ; if [ ${IMPRCUPO} -eq 1 ] ;then
                            echo "Impressora TM-88 desativada com sucesso:['$loja'].['$ip'].['1000$pdv'].";
                            sed -e 's/CUPOPARA=1/CUPOPARA=0/' -e 's/IMPRCUPO=1/IMPRCUPO=0/' /confdia/bin/setvari > /confdia/bin/setvari.new ; mv /confdia/bin/setvari.new /confdia/bin/setvari;
                        else
                            echo "Ação não necessária para a loja:['$loja'].['$ip'].['1000$pdv'].";
                        fi'
                        datae=`date +%Y%m%d -d '+1 days'`
                        datai=`date +%Y%m%d -d '+2 days'`
                        exist=`echo "SELECT COUNT(id) FROM tb_imp_fidelizacao WHERE loja = '$loja';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            sqlite3 $BD \
                                            "INSERT INTO tb_imp_fidelizacao (loja, data) VALUES ('$loja', '$datae')"
                            if [ $? != 0 ]; then
                                echo "ERROR: Falha ao inserir dados no banco de dados."
                                sleep 1
                                menuDisabledTM88
                            else
                                echo "Dados salvos com sucesso."
                                sleep 1
                                menuDisabledTM88
                            fi
                        else
                            sqlite3 $BD \
                                            "UPDATE tb_imp_fidelizacao SET data = '$datae' WHERE ID=(SELECT MAX(ID) FROM tb_imp_fidelizacao WHERE loja LIKE '$loja')"
                            if [ $? != 0 ]; then
                                echo "ERROR: Falha ao inserir dados no banco de dados."
                                sleep 1
                                menuDisabledTM88
                            else
                                echo "Dados salvos com sucesso."
                                sleep 1
                                menuDisabledTM88
                            fi
                        fi
                    else
                        echo "Erro ao conectar com a loja."
                        sleep 5
                        menuDisabledTM88
                    fi
                else
                    echo "Valor informado deve ser maior que 0."
                    sleep 5
                    menuDisabledTM88
                fi
            ;;
            "7.2"|"07.02"|"07.2")
                sshpass -p root ssh -o ConnectTimeout=1 $ip -p10001 exit
                return=$?
                if [ $return -eq 0 ]; then
                    n_tpvs=$(sshpass -p root ssh -o ConnectTimeout=1 $ip -p10001 '. /confdia/bin/setvari ; echo "${NUMETPVS}"')
                    return=$?
                    if [ $return -eq 0 ]; then
                        for y in $(seq 1 $n_tpvs); do sshpass -p root ssh -o ConnectTimeout=1 $ip -p1000$y ' . /confdia/bin/setvari ; if [ ${IMPRCUPO} -eq 1 ] ; then
                            echo "Impressora TM-88 desativada com sucesso:['$loja'].['$ip'].['1000$y'].";
                            sed -e 's/CUPOPARA=1/CUPOPARA=0/' -e 's/IMPRCUPO=1/IMPRCUPO=0/' /confdia/bin/setvari > /confdia/bin/setvari.new ; mv /confdia/bin/setvari.new /confdia/bin/setvari;
                        else
                            echo "Ação não necessária para a loja:['$loja'].['$ip'].['1000$y'].";
                        fi' ; 
                        done
                        datae=`date +%Y%m%d -d '+1 days'`
                        datai=`date +%Y%m%d -d '+2 days'`
                        exist=`echo "SELECT COUNT(id) FROM tb_imp_fidelizacao WHERE loja = '$loja';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            sqlite3 $BD \
                                            "INSERT INTO tb_imp_fidelizacao (loja, data) VALUES ('$loja', '$datae')"
                            if [ $? != 0 ]; then
                                echo "ERROR: Falha ao inserir dados no banco de dados."
                                sleep 1
                                menuDisabledTM88
                            else
                                echo "Dados salvos com sucesso."
                                sleep 1
                                menuDisabledTM88
                            fi
                        else
                            sqlite3 $BD \
                                            "UPDATE tb_imp_fidelizacao SET data = '$datae' WHERE ID=(SELECT MAX(ID) FROM tb_imp_fidelizacao WHERE loja LIKE '$loja')"
                            if [ $? != 0 ]; then
                                echo "ERROR: Falha ao inserir dados no banco de dados."
                                sleep 1
                                menuDisabledTM88
                            else
                                echo "Dados salvos com sucesso."
                                sleep 1
                                menuDisabledTM88
                            fi
                        fi
                    else
                        echo "Erro ao obter número de pdvs."
                        sleep 5
                        menuDisabledTM88
                    fi
                else
                    echo "Erro ao conectar com a loja."
                    sleep 5
                    menuDisabledTM88
                fi
            ;;
            *)
                echo "Opção inválida."
                menuDisabledTM88
        esac
    elif [ $FUNC -eq 5 ]; then
        sshpass -p root ssh -o ConnectTimeout=1 -l root $ip -p10001 exit
        return=$?
        if [ $return -eq 0 ]; then
            actualpaf=$(sshpass -p root ssh -l root $ip -p10001 'ls -ltr /confdia/actualPAF/ | wc -l ')
            return=$?
            if [ $return -eq 0 ]; then
                if [ $actualpaf -gt 1000 ]; then
					sshpass -p root ssh -l root $ip -p10001 'rm /confdia/actualPAF/*txt'
					return=$?
					if [ $return -eq 0 ]; then
                        echo "Arquivos deletados com sucesso."
                        sleep 1
                        datae=`date +%Y%m%d -d '+1 days'`
                        datai=`date +%Y%m%d -d '+2 days'`
                        exist=`echo "SELECT COUNT(id) FROM tb_actual_paf WHERE tienda = '$loja';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            sqlite3 $BD \
                                            "INSERT INTO tb_actual_paf (tienda, active) VALUES ('$loja', '1')"
                            if [ $? != 0 ]; then
                                echo "ERROR: Falha ao inserir dados no banco de dados."
                                sleep 1
                                menuDeleteActualPAF
                            else
                                echo "Dados salvos com sucesso."
                                sleep 1
                                menuDeleteActualPAF
                            fi
                        else
                            sqlite3 $BD \
                                            "UPDATE tb_actual_paf SET active = '1' WHERE ID=(SELECT MAX(ID) FROM tb_actual_paf WHERE tienda LIKE '$loja')"
                            if [ $? != 0 ]; then
                                echo "ERROR: Falha ao inserir dados no banco de dados."
                                sleep 1
                                menuDeleteActualPAF
                            else
                                echo "Dados salvos com sucesso."
                                sleep 1
                                menuDeleteActualPAF
                            fi
                        fi
                    else
                        echo "Erro ao deletar arquivos."
                        sleep 5
                        menuDeleteActualPAF
                    fi
                else
                    echo "Quantidade de arquivos menor que 1000"
                    sleep 5 
                    menuDeleteActualPAF
                fi
            else
                echo "Erro ao obter quantidade de do diretorio /confdia/actualPAF/"
                sleep 5
                menuDeleteActualPAF
            fi
        else
            echo "Erro ao conectar com a loja."
            sleep 5
            menuDeleteActualPAF
        fi
    elif [ $FUNC -eq 6 ]; then
        echo "07.1 Especificar PDV"
        echo "07.2 Todos PDVs"
        read -p "Opção: " opcion
        case $opcion in
            "7.1"|"07.01"|"07.1")
                read -p "Informe o número do caixa: " pdv
                [ "$pdv" ] || { echo -e "\033[01;31m ERROR: Não foi informado informado.\033[00;37m" ; sleep 1 ; menuDisabledTM88 ; }
                # Validamos se os campos informados são númericos
                if [[ $pdv = ?(+|-)+([0-9]) ]]; then
                    echo ""
                else
                    echo "Ops, valor informado não é um número inteiro!"
                    sleep 1
                    main
                fi
                ntpv=`echo ${#pdv}`
                echo $ntpv
                sleep 1
                if [ $ntpv -ge 2 ]; then
                    echo "Informe até 1 caracteres númericos. Ex.: 1, 2, 9."
                    sleep 1
                    main
                fi
                
                if [ $pdv -ge 1 ]; then
                    sshpass -p root ssh -o ConnectTimeout=1 $ip -p1000$pdv exit
                    return=$?
                    if [ $return -eq 0 ]; then
                        FILE1="/root/srv_remoto/tgz/ofertaMenosVenda.tgz"
                        sshpass -p root scp -P1000$pdv $FILE1 root@$ip:/confdia/descargas/
                        sshpass -p root ssh -p1000$pdv -l root $ip 'tar -xzvf /confdia/descargas/ofertaMenosVenda.tgz -C /'
                        return=$?
                        if [ $return -eq 0 ]; then
                            echo "Oferta Menos Venda configurada com sucesso."
                            datae=`date +%Y%m%d -d '+1 days'`
                            datai=`date +%Y%m%d -d '+2 days'`
                            sleep 5
                            if [ $exist -eq 0 ]; then
                                sqlite3 $BD \
                                                "INSERT INTO tb_install_oferta (tienda, n_tpvs,data_instalacao, data_envio, status) VALUES ('$loja', '0','$datai', '$datae', 'pendente')"
                                if [ $? != 0 ]; then
                                    echo "ERROR: Falha ao inserir dados no banco de dados."
                                    sleep 1
                                    menuOfertaMenosVenda
                                else
                                    echo "Dados salvos com sucesso."
                                    sleep 1
                                    menuOfertaMenosVenda
                                fi
                            else
                                sqlite3 $BD \
                                                "UPDATE tb_install_oferta SET data_instalacao = '$datai', data_envio = '$datae' WHERE ID=(SELECT MAX(ID) FROM tb_install_oferta WHERE tienda = '$loja')"
                                if [ $? != 0 ]; then
                                    echo "ERROR: Falha ao inserir dados no banco de dados."
                                    sleep 1
                                    menuOfertaMenosVenda
                                else
                                    echo "Dados salvos com sucesso."
                                    sleep 1
                                    menuOfertaMenosVenda
                                fi
                            fi
                        else
                            echo "Falha ao configurar Oferta Menos Venda."
                            sleep 5
                            menuOfertaMenosVenda
                        fi
                    else
                        echo "Erro ao conectar com a loja."
                        sleep 5
                        menuOfertaMenosVenda
                    fi
                else
                    echo "Valor informado deve ser maior que 0."
                    sleep 5
                    menuOfertaMenosVenda
                fi
            ;;
            "7.2"|"07.02"|"07.2")
                sshpass -p root ssh -o ConnectTimeout=1 $ip -p10001 exit
                return=$?
                if [ $return -eq 0 ]; then
                    n_tpvs=$(sshpass -p root ssh -o ConnectTimeout=1 $ip -p10001 '. /confdia/bin/setvari ; echo "${NUMETPVS}"')
                    return=$?
                    if [ $return -eq 0 ]; then
                        FILE1="/root/srv_remoto/tgz/ofertaMenosVenda.tgz"
                        for z in $(seq 1 $n_tpvs); do sshpass -p root scp -P1000$z $FILE1 root@$ip:/confdia/descargas/; done
                        for k in $(seq 1 $n_tpvs); do sshpass -p root ssh -p1000$z -l root $ip 'tar -xzvf /confdia/descargas/ofertaMenosVenda.tgz -C /'; done
                        return=$?
                        if [ $return -eq 0 ]; then
                            echo "Oferta Menos Venda configurada com sucesso."
                            sleep 5
                            datae=`date +%Y%m%d -d '+1 days'`
                            datai=`date +%Y%m%d -d '+2 days'`
                            sleep 5
                            if [ $exist -eq 0 ]; then
                                sqlite3 $BD \
                                                "INSERT INTO tb_install_oferta (tienda, n_tpvs,data_instalacao, data_envio, status) VALUES ('$loja', '0','$datai', '$datae', 'pendente')"
                                if [ $? != 0 ]; then
                                    echo "ERROR: Falha ao inserir dados no banco de dados."
                                    sleep 1
                                    menuOfertaMenosVenda
                                else
                                    echo "Dados salvos com sucesso."
                                    sleep 1
                                    menuOfertaMenosVenda
                                fi
                            else
                                sqlite3 $BD \
                                                "UPDATE tb_install_oferta SET data_instalacao = '$datai', data_envio = '$datae' WHERE ID=(SELECT MAX(ID) FROM tb_install_oferta WHERE tienda = '$loja')"
                                if [ $? != 0 ]; then
                                    echo "ERROR: Falha ao inserir dados no banco de dados."
                                    sleep 1
                                    menuOfertaMenosVenda
                                else
                                    echo "Dados salvos com sucesso."
                                    sleep 1
                                    menuOfertaMenosVenda
                                fi
                            fi
                        else
                            echo "Falha ao configurar Oferta Menos Venda."
                            sleep 5
                            menuOfertaMenosVenda
                        fi
                    else
                        echo "Erro ao obter número de pdvs."
                        sleep 5
                        menuOfertaMenosVenda
                    fi
                else
                    echo "Erro ao conectar com a loja."
                    sleep 5
                    menuOfertaMenosVenda
                fi
            ;;
            *)
                echo "Opção inválida."
                menuOfertaMenosVenda
        esac
    fi
else
    echo "Falha ao conectar com a loja."
    exit 1
fi
}

function ViewSystemLog(){
    if [ $FUNC -eq  1 ]; then
        LOG="/root/srv_remoto/log/install_printer_brother.log"
        if [ -e $LOG ]; then
            cat $LOG | less
            menuPrinterBrother
        else
            echo "Log do sistema não econtrado."
            menuPrinterBrother
        fi
    elif [ $FUNC -eq 2 ]; then
        LOG="/root/srv_remoto/log/install_tcgertec2.0.log"
        if [ -e $LOG ]; then
            cat $LOG | less
            menuTCGertec2
        else
            echo "Log do sistema não econtrado."
            menuTCGertec2
        fi
    elif [ $FUNC -eq 3 ]; then
        LOG="/root/srv_remoto/log/install_tcgertec3.0.log"
        if [ -e $LOG ]; then
            cat $LOG | less
            menuTCGertec3
        else
            echo "Log do sistema não econtrado."
            menuTCGertec3
        fi
    elif [ $FUNC -eq 4 ]; then
        LOG="/root/srv_remoto/log/imp_fidelizacao3.log"
        if [ -e $LOG ]; then
            cat $LOG | less
            menuDisabledTM88
        else
            echo "Log do sistema não econtrado."
            menuDisabledTM88
        fi
    elif [ $FUNC -eq 5 ]; then
        LOG="/root/srv_remoto/log/actual_paf.log"
        if [ -e $LOG ]; then
            cat $LOG | less
            menuDeleteActualPAF
        else
            echo "Log do sistema não econtrado."
            menuDeleteActualPAF
        fi
    elif [ $FUNC -eq 6 ]; then
        LOG="/root/srv_remoto/log/install_ofer_venda.log"
        if [ -e $LOG ]; then
            cat $LOG | less
            menuOfertaMenosVenda
        else
            echo "Log do sistema não econtrado."
            menuOfertaMenosVenda
        fi
    fi
}

main