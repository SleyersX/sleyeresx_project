#!/bin/bash
#
# Autor: Walter Moura
# Data Criacao: 2019.06.26
# Data Modificacao: 2019.08.17
# Versao: 0.04

# Variaveis Globais
BD="/root/srv_remoto/srv_remoto.db"
LOG1="export.log"
delimit="|"
FUNC=0
RESP=0
function main(){

opmenu=$(dialog --no-cancel  --title "Remote System Install" --menu "System change: " 0 0 0 01 "Install Brother" 02 "Install TCGertec-2.0" 03 "Install TCGertec-3.0" 04 "Disabled Printer TM88" 05 "Delete Files actualPAF" 06 "Oferta Menos Venda" 99 "Exit\Quit" --stdout)

# Case para chamadas de cada menu
case $opmenu in
    "1"|"01")
            FUNC=1
            sleep 1
            menuPrinterBrother
    ;;
    "2"|"02")
            FUNC=2
            sleep 1
            menuTCGertec2
    ;;
    "3"|"03")
            FUNC=3
            sleep 1
            menuTCGertec3
    ;;
    "4"|"04")
            FUNC=4
            sleep 1
            menuDisabledTM88
    ;;
    "5"|"05")
            FUNC=5
            sleep 1
            menuDeleteActualPAF
    ;;
    "6"|"06")
            FUNC=6
            sleep 1
            menuOfertaMenosVenda
    ;;
    "99")
            clear
            exit 1
    ;;
    *)
            dialog --title  "Remote System Install" --msgbox "55-Opção inválida." 5 30
            sleep 1
            main
esac        

}

function menuPrinterBrother(){

# Construir Menu
clear
opcao=$(dialog --title "Remote System Install - Printer Brother" --ok-label "Avançar" --cancel-label "Voltar" --radiolist "Escolha uma opção:" 0 0 0 01 "Enabled Store" ON 02 "Enabled Multiple Stores" OFF 03 "Store Inquiry" OFF 04 "Disabled Store" OFF 05 "Generate remote shipping/installation program record" OFF 06 "Extract data from the database" OFF 07 "Forced Installation" OFF 08 "View System Log" OFF 99 "Exit\Quit" OFF --stdout)

# Construir case para receber a resposta do usuário
case $opcao in
    "01"|"1") # Ativar Loja
        AtivarLoja
    ;;
    "02"|"2") # Ativar varias Lojas
        AtivarVariasLoja
    ;;
    "03"|"3") # Consultar Loja
        ConsultarLoja
    ;;
    "04"|"4") # Desativar Loja
        DesativarLoja
    ;;
    "05"|"5") # Gerar Log
        GerarLog
    ;;
    "06"|"6") # Extrair Dados
        ExtrairDados
    ;;
    "07"|"7") # Forçar instalação
        ForcedOneShop
    ;;
    "08"|"8") # Visualizar log do sistema
        ViewSystemLog
    ;;
    "99") # Exit/Quit
        sleep 1
        main
    ;;
    "")
        sleep 1
        main
    ;;
    *) # Opção inválida
        dialog --title  "Remote System Install" --msgbox "Opção inválida: $opcao" 5 30
        sleep 1
        menuPrinterBrother
esac

}

function menuTCGertec2(){

# Construir Menu
clear
opcao=$(dialog --title "Remote System Install - TCgertec 2.0" --ok-label "Avançar" --cancel-label "Voltar" --radiolist "Escolha uma opção:" 0 0 0 01 "Enabled Store" ON 02 "Enabled Multiple Stores" OFF 03 "Store Inquiry" OFF 04 "Disabled Store" OFF 05 "Generate remote shipping/installation program record" OFF 06 "Extract data from the database" OFF 07 "Forced Installation" OFF 08 "View System Log" OFF 99 "Exit\Quit" OFF --stdout)

# Construir case para receber a resposta do usuário
case $opcao in
    "01"|"1") # Ativar Loja
        AtivarLoja
    ;;
    "02"|"2") # Ativar varias Lojas
        AtivarVariasLoja
    ;;
    "03"|"3") # Consultar Loja
        ConsultarLoja
    ;;
    "04"|"4") # Desativar Loja
        DesativarLoja
    ;;
    "05"|"5") # Gerar Log
        GerarLog
    ;;
    "06"|"6") # Extrair Dados
        ExtrairDados
    ;;
    "07"|"7") # Forçar instalação
        ForcedOneShop
    ;;
    "08"|"8") # Visualizar log do sistema
        ViewSystemLog
    ;;
    "99") # Exit/Quit
        sleep 1
        main
    ;;
    "")
        sleep 1
        main
    ;;
    *) # Opção inválida
        dialog --title  "Remote System Install" --msgbox "Opção inválida: $opcao" 5 30
        sleep 1
        menuTCGertec2
esac

}

function menuTCGertec3(){

# Construir Menu
clear
opcao=$(dialog --title "Remote System Install - TCGertec 3.0" --ok-label "Avançar" --cancel-label "Voltar" --radiolist "Escolha uma opção:" 0 0 0 01 "Enabled Store" ON 02 "Enabled Multiple Stores" OFF 03 "Store Inquiry" OFF 04 "Disabled Store" OFF 05 "Generate remote shipping/installation program record" OFF 06 "Extract data from the database" OFF 07 "Forced Installation" OFF 08 "View System Log" OFF 99 "Exit\Quit" OFF --stdout)


# Construir case para receber a resposta do usuário
case $opcao in
    "01"|"1") # Ativar Loja
        AtivarLoja
    ;;
    "02"|"2") # Ativar varias Lojas
        AtivarVariasLoja
    ;;
    "03"|"3") # Consultar Loja
        ConsultarLoja
    ;;
    "04"|"4") # Desativar Loja
        DesativarLoja
    ;;
    "05"|"5") # Gerar Log
        GerarLog
    ;;
    "06"|"6") # Extrair Dados
        ExtrairDados
    ;;
    "07"|"7") # Forçar instalação
        ForcedOneShop
    ;;
    "08"|"8") # Visualizar log do sistema
        ViewSystemLog
    ;;
    "99") # Exit/Quit
        sleep 1
        main
    ;;
    "")
        sleep 1
        main
    ;;
    *) # Opção inválida
        dialog --title  "Remote System Install" --msgbox "Opção inválida: $opcao" 5 30
        sleep 1
        menuTCGertec3
esac

}

function menuDisabledTM88(){

# Construir Menu
clear
opcao=$(dialog --title "Remote System Install - Disabled TM88" --ok-label "Avançar" --cancel-label "Voltar" --radiolist "Escolha uma opção:" 0 0 0 01 "Enabled Store" ON 02 "Enabled Multiple Stores" OFF 03 "Store Inquiry" OFF 04 "Disabled Store" OFF 05 "Generate remote shipping/installation program record" OFF 06 "Extract data from the database" OFF 07 "Forced Installation" OFF 08 "View System Log" OFF 99 "Exit\Quit" OFF --stdout)

# Construir case para receber a resposta do usuário
case $opcao in
    "01"|"1") # Ativar Loja
        AtivarLoja
    ;;
    "02"|"2") # Ativar varias Lojas
        AtivarVariasLoja
    ;;
    "03"|"3") # Consultar Loja
        ConsultarLoja
    ;;
    "04"|"4") # Desativar Loja
        DesativarLoja
    ;;
    "05"|"5") # Gerar Log
        GerarLog
    ;;
    "06"|"6") # Extrair Dados
        ExtrairDados
    ;;
    "07"|"7") # Forçar instalação
        ForcedOneShop
    ;;
    "08"|"8") # Visualizar log do sistema
        ViewSystemLog
    ;;
    "99") # Exit/Quit
        sleep 1
        main
    ;;
    "")
        sleep 1
        main
    ;;
    *) # Opção inválida
        dialog --title  "Remote System Install" --msgbox "Opção inválida: $opcao" 5 30
        sleep 1
        menuDisabledTM88
esac

}

function menuDeleteActualPAF(){

# Construir Menu
clear
opcao=$(dialog --title "Remote System Install - Delete actualPAF" --ok-label "Avançar" --cancel-label "Voltar" --radiolist "Escolha uma opção:" 0 0 0 01 "Enabled Store" ON 02 "Enabled Multiple Stores" OFF 03 "Store Inquiry" OFF 04 "Disabled Store" OFF 05 "Generate remote shipping/installation program record" OFF 06 "Extract data from the database" OFF 07 "Forced Installation" OFF 08 "View System Log" OFF 99 "Exit\Quit" OFF --stdout)

# Construir case para receber a resposta do usuário
case $opcao in
    "01"|"1") # Ativar Loja
        AtivarLoja
    ;;
    "02"|"2") # Ativar varias Lojas
        AtivarVariasLoja
    ;;
    "03"|"3") # Consultar Loja
        ConsultarLoja
    ;;
    "04"|"4") # Desativar Loja
        DesativarLoja
    ;;
    "05"|"5") # Gerar Log
        GerarLog
    ;;
    "06"|"6") # Extrair Dados
        ExtrairDados
    ;;
    "07"|"7") # Forçar instalação
        ForcedOneShop
    ;;
    "08"|"8") # Visualizar log do sistema
        ViewSystemLog
    ;;
    "99") # Exit/Quit
        sleep 1
        main
    ;;
    "")
        sleep 1
        main
    ;;
    *) # Opção inválida
        dialog --title  "Remote System Install" --msgbox "Opção inválida: $opcao" 5 30
        sleep 1
        menuDeleteActualPAF
esac

}

function menuOfertaMenosVenda(){

# Construir Menu
clear
opcao=$(dialog --title "Remote System Install - Oferta Menos Venda" --ok-label "Avançar" --cancel-label "Voltar" --radiolist "Escolha uma opção:" 0 0 0 01 "Enabled Store" ON 02 "Enabled Multiple Stores" OFF 03 "Store Inquiry" OFF 04 "Disabled Store" OFF 05 "Generate remote shipping/installation program record" OFF 06 "Extract data from the database" OFF 07 "Forced Installation" OFF 08 "View System Log" OFF 99 "Exit\Quit" OFF --stdout)

# Construir case para receber a resposta do usuário 
case $opcao in
    "01"|"1") # Ativar Loja
        AtivarLoja
    ;;
    "02"|"2") # Ativar varias Lojas
        AtivarVariasLoja
    ;;
    "03"|"3") # Consultar Loja
        ConsultarLoja
    ;;
    "04"|"4") # Desativar Loja
        DesativarLoja
    ;;
    "05"|"5") # Gerar Log
        GerarLog
    ;;
    "06"|"6") # Extrair Dados
        ExtrairDados
    ;;
    "07"|"7") # Forçar instalação
        ForcedOneShop
    ;;
    "08"|"8") # Visualizar log do sistema
        ViewSystemLog
    ;;
    "99") # Exit/Quit
        sleep 1
        main
    ;;
    "")
        sleep 1
        main
    ;;
    *) # Opção inválida
        dialog --title  "Remote System Install" --msgbox "Opção inválida: $opcao" 5 30
        sleep 1
        menuOfertaMenosVenda
esac

}

function AtivarLoja(){

# Solicitamos ao usuario que informe a loja e data de instação
loja=$(dialog --stdout --title "Remote Systema" --form "Ativar lojas" 11 50 0 "Informe a loja:" 1 1 "" 1 16 6 4)
# Validadmos se as variáveis estão vazias
[ "$loja" ] || { dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Não foi informado a loja." 5 70 ; sleep 1 ; main ; }
# Validamos a quantidade de caracteres informados
lj=`echo ${#loja}`
if [ $lj -ge 5 ]; then
    dialog --ok-label "OK" --title "Remote System" --msgbox "Informe até 4 caracteres númericos. Ex.: 2, 1000, 9527, 165." 5 70
    sleep 1
    main
fi
# Validamos se os campos informados são númericos
if [[ $loja = ?(+|-)+([0-9]) ]]; then
    echo ""
else
    dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Ops, valor informado não é um número inteiro" 5 70
    sleep 1
    main
fi

if [ $FUNC -eq 5 ]; then

    Ativar=$(dialog --stdout --ok-label "Avançar" --no-cancel --title "Remote System" --form "1.1 Ativar lojas" 11 50 0 "1.1 Ativar [Y/N] ] ?" 1 1 "" 1 16 5 1)
    # Validadmos se as variáveis estão vazias
    [ "$Ativar" ] || { dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Não foi informada uma resposta." 5 70 ; sleep 1 ; main ; }
    # Validamos a quantidade de caracteres informados
    case $Ativar in
        "Y"|"y"|"s"|"S")
            RESP=1   
        ;;
        "N"|"n")
            RESP=0
        ;;
        *)
        dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Resposta não identificada." 5 70
        sleep 2
        main
    esac
else
    if [ $FUNC -eq 1 ]; then
        dt_enivo=$(dialog --stdout --no-cancel --ok-label "Confirmar" --date-format "%Y%m%d" --calendar "Selecione a data de envio:" 0 0)
        # Validadmos se as variáveis estão vazias
        [ "$dt_enivo" ] || { dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Não foi informada a data." 5 70; sleep 1 ; main ; }
        # Validamos se os campos informados são númericos
        if [[ $dt_enivo = ?(+|-)+([0-9]) ]]; then
            echo ""
        else
            dialog --ok-label "OK" --title "Remote System" --msgbox "Ops, valor informado não é um número inteiro." 5 70
            sleep 1
            main
        fi
        # Validamos a quantidade de caracteres informados
        dt=`echo ${#dt_enivo}`
        if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
            dialog --ok-label "OK" --title "Remote System" --msgbox "Informe até 8 caracteres númericos. Ex.: 20190101." 5 70
            sleep 1
            main
        fi
        data=$(dialog --stdout --no-cancel --ok-label "Confirmar" --date-format "%Y%m%d" --calendar "Selecione a data de instalação:" 0 0)
        # Validadmos se as variáveis estão vazias
        [ "$data" ] || { dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Não foi informada a data." 5 70 ; sleep 1 ; main ; }
        # Validamos se os campos informados são númericos
        if [[ $data = ?(+|-)+([0-9]) ]]; then
            echo ""
        else
            dialog --ok-label "OK" --title "Remote System" --msgbox "Ops, valor informado não é um número inteiro." 5 70
            sleep 1
            main
        fi
        # Validamos a quantidade de caracteres informados
        dt=`echo ${#data}`
        if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
            dialog --ok-label "OK" --title "Remote System" --msgbox "Ops, valor informado não é um número inteiro." 5 70
            sleep 1
            main
        fi
    elif [ $FUNC -eq 6 ]; then
        dt_enivo=$(dialog --stdout --no-cancel --ok-label "Confirmar" --date-format "%Y%m%d" --calendar "Selecione a data de envio:" 0 0)
        # Validadmos se as variáveis estão vazias
        [ "$dt_enivo" ] || { dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Não foi informada a data." 5 70; sleep 1 ; main ; }
        # Validamos se os campos informados são númericos
        if [[ $dt_enivo = ?(+|-)+([0-9]) ]]; then
            echo ""
        else
            dialog --ok-label "OK" --title "Remote System" --msgbox "Ops, valor informado não é um número inteiro." 5 70
            sleep 1
            main
        fi
        # Validamos a quantidade de caracteres informados
        dt=`echo ${#dt_enivo}`
        if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
            dialog --ok-label "OK" --title "Remote System" --msgbox "Informe até 8 caracteres númericos. Ex.: 20190101." 5 70
            sleep 1
            main
        fi
        data=$(dialog --stdout --no-cancel --ok-label "Confirmar" --date-format "%Y%m%d" --calendar "Selecione a data de instalação:" 0 0)
        # Validadmos se as variáveis estão vazias
        [ "$data" ] || { dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Não foi informada a data." 5 70 ; sleep 1 ; main ; }
        # Validamos se os campos informados são númericos
        if [[ $data = ?(+|-)+([0-9]) ]]; then
            echo ""
        else
            dialog --ok-label "OK" --title "Remote System" --msgbox "Ops, valor informado não é um número inteiro." 5 70
            sleep 1
            main
        fi
        # Validamos a quantidade de caracteres informados
        dt=`echo ${#data}`
        if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
            dialog --ok-label "OK" --title "Remote System" --msgbox "Ops, valor informado não é um número inteiro." 5 70
            sleep 1
            main
        fi
    else
        data=$(dialog --stdout --no-cancel --ok-label "Confirmar" --date-format "%Y%m%d" --calendar "Selecione a data de instalação:" 0 0)
        # Validadmos se as variáveis estão vazias
        [ "$data" ] || { dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Não foi informada a data." 5 70 ; sleep 1 ; main ; }
        # Validamos se os campos informados são númericos
        if [[ $data = ?(+|-)+([0-9]) ]]; then
            echo ""
        else
            dialog --ok-label "OK" --title "Remote System" --msgbox "Ops, valor informado não é um número inteiro." 5 70
            sleep 1
            main
        fi
        # Validamos a quantidade de caracteres informados
        dt=`echo ${#data}`
        if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
            dialog --ok-label "OK" --title "Remote System" --msgbox "Ops, valor informado não é um número inteiro." 5 70
            sleep 1
            main
        fi
    fi

fi

# Colocamos 0 a esquerda para o número da loja
loja=`printf "%05d" $loja`

# Inserimos os valores no banco de dados
if [ $FUNC -eq 1 ]; then

    exist=`echo "SELECT COUNT(id) FROM tb_printer_brother WHERE loja = '$loja';" | sqlite3 $BD`
    if [ $exist -eq 0 ]; then
        sqlite3 $BD \
                        "INSERT INTO tb_printer_brother (loja, data, data_instalacao) VALUES ('$loja', '$dt_enivo', '$data')"
        if [ $? != 0 ]; then
            dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Falha ao inserir dados no banco de dados." 5 70
            sleep 1
            menuPrinterBrother
        else
            dialog --ok-label "OK" --title "Remote System" --msgbox "Dados salvos com sucesso." 5 70
            sleep 1
            menuPrinterBrother
        fi
    else
        sqlite3 $BD \
                        "UPDATE tb_printer_brother SET data = '$dt_enivo', data_instalacao = '$data' WHERE ID=(SELECT MAX(ID) FROM tb_printer_brother WHERE loja LIKE '$loja')"
        if [ $? != 0 ]; then
            dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Falha ao inserir dados no banco de dados." 5 70
            sleep 1
            menuPrinterBrother
        else
            dialog --ok-label "OK" --title "Remote System" --msgbox "Dados salvos com sucesso." 5 70
            sleep 1
            menuPrinterBrother
        fi
    fi

elif [ $FUNC -eq 2 ]; then

    exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$loja';" | sqlite3 $BD`
    if [ $exist -eq 0 ]; then
        sqlite3 $BD \
                        "INSERT INTO tb_verificador (tienda, fecha_envio, versao) VALUES ('$loja', '$data', '2')"
        if [ $? != 0 ]; then
            dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Falha ao inserir dados no banco de dados." 5 70
            sleep 1
            menuTCGertec2
        else
            dialog --ok-label "OK" --title "Remote System" --msgbox "Dados salvos com sucesso." 5 70
            sleep 1
            menuTCGertec2
        fi
    else
        sqlite3 $BD \
                        "UPDATE tb_verificador SET fecha_envio = '$data', versao = '2' WHERE ID=(SELECT MAX(ID) FROM tb_verificador WHERE tienda = '$loja')"
        if [ $? != 0 ]; then
            dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Falha ao inserir dados no banco de dados." 5 70
            sleep 1
            menuTCGertec2
        else
            dialog --ok-label "OK" --title "Remote System" --msgbox "Dados salvos com sucesso." 5 70
            sleep 1
            menuTCGertec2
        fi
    fi

elif [ $FUNC -eq 3 ]; then

    exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$loja';" | sqlite3 $BD`
    if [ $exist -eq 0 ]; then
        sqlite3 $BD \
                        "INSERT INTO tb_verificador (tienda, fecha_envio, versao) VALUES ('$loja', '$data', '3')"
        if [ $? != 0 ]; then
            dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Falha ao inserir dados no banco de dados." 5 70
            sleep 1
            menuTCGertec3
        else
            dialog --ok-label "OK" --title "Remote System" --msgbox "Dados salvos com sucesso." 5 70
            sleep 1
            menuTCGertec3
        fi
    else
        sqlite3 $BD \
                        "UPDATE tb_verificador SET fecha_envio = '$data', versao = '3' WHERE ID=(SELECT MAX(ID) FROM tb_verificador WHERE tienda = '$loja')"
        if [ $? != 0 ]; then
            dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Falha ao inserir dados no banco de dados." 5 70
            sleep 1
            menuTCGertec3
        else
            dialog --ok-label "OK" --title "Remote System" --msgbox "Dados salvos com sucesso." 5 70
            sleep 1
            menuTCGertec3
        fi
    fi

elif [ $FUNC -eq 4 ]; then
     
    exist=`echo "SELECT COUNT(id) FROM tb_imp_fidelizacao WHERE loja = '$loja';" | sqlite3 $BD`
    if [ $exist -eq 0 ]; then
        sqlite3 $BD \
                        "INSERT INTO tb_imp_fidelizacao (loja, data) VALUES ('$loja', '$data')"
        if [ $? != 0 ]; then
            dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Falha ao inserir dados no banco de dados." 5 70
            sleep 1
            menuDisabledTM88
        else
            dialog --ok-label "OK" --title "Remote System" --msgbox "Dados salvos com sucesso." 5 70
            sleep 1
            menuDisabledTM88
        fi
    else
        sqlite3 $BD \
                        "UPDATE tb_imp_fidelizacao SET data = '$data' WHERE ID=(SELECT MAX(ID) FROM tb_imp_fidelizacao WHERE loja LIKE '$loja')"
        if [ $? != 0 ]; then
            dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Falha ao inserir dados no banco de dados." 5 70
            sleep 1
            menuDisabledTM88
        else
            dialog --ok-label "OK" --title "Remote System" --msgbox "Dados salvos com sucesso." 5 70
            sleep 1
            menuDisabledTM88
        fi
    fi

elif [ $FUNC -eq 5 ]; then
     
    exist=`echo "SELECT COUNT(id) FROM tb_actual_paf WHERE tienda = '$loja';" | sqlite3 $BD`
    if [ $exist -eq 0 ]; then
        sqlite3 $BD \
                        "INSERT INTO tb_actual_paf (tienda, active) VALUES ('$loja', '$RESP')"
        if [ $? != 0 ]; then
            dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Falha ao inserir dados no banco de dados." 5 70
            sleep 1
            menuDeleteActualPAF
        else
            dialog --ok-label "OK" --title "Remote System" --msgbox "Dados salvos com sucesso." 5 70
            sleep 1
            menuDeleteActualPAF
        fi
    else
        sqlite3 $BD \
                        "UPDATE tb_actual_paf SET active = '$RESP' WHERE ID=(SELECT MAX(ID) FROM tb_actual_paf WHERE tienda LIKE '$loja')"
        if [ $? != 0 ]; then
            dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Falha ao inserir dados no banco de dados." 5 70
            sleep 1
            menuDeleteActualPAF
        else
            dialog --ok-label "OK" --title "Remote System" --msgbox "Dados salvos com sucesso." 5 70
            sleep 1
            menuDeleteActualPAF
        fi
    fi
elif [ $FUNC -eq 6 ]; then

    exist=`echo "SELECT COUNT(id) FROM tb_install_oferta WHERE tienda = '$loja';" | sqlite3 $BD`
    if [ $exist -eq 0 ]; then
        sqlite3 $BD \
                        "INSERT INTO tb_install_oferta (tienda, n_tpvs,data_instalacao, data_envio, status) VALUES ('$loja', '0','$data', '$dt_enivo', 'pendente')"
        if [ $? != 0 ]; then
            dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Falha ao inserir dados no banco de dados." 5 70
            sleep 1
            menuOfertaMenosVenda
        else
            dialog --ok-label "OK" --title "Remote System" --msgbox "Dados salvos com sucesso." 5 70
            sleep 1
            menuOfertaMenosVenda
        fi
    else
        sqlite3 $BD \
                        "UPDATE tb_install_oferta SET data_instalacao = '$data', data_envio = '$dt_enivo' WHERE ID=(SELECT MAX(ID) FROM tb_install_oferta WHERE tienda = '$loja')"
        if [ $? != 0 ]; then
            dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Falha ao inserir dados no banco de dados." 5 70
            sleep 1
            menuOfertaMenosVenda
        else
            dialog --ok-label "OK" --title "Remote System" --msgbox "Dados salvos com sucesso." 5 70
            sleep 1
            menuOfertaMenosVenda
        fi
    fi

fi

}

function AtivarVariasLoja(){
    
    metodo=$(dialog --title "Remote System Install - Ativar Varias Lojas" --ok-label "Avançar" --cancel-label "Voltar" --radiolist "Escolha uma opção:" 0 0 0 02.1 "Ler TXT" ON 02.2 "Lojas em sequencia" OFF 99 "Exit\Quit" OFF --stdout)
    case $metodo in
        "02.1"|"2.1") # Ler arquivo de texto
            dialog --sleep 2 --title "Remote System" --infobox "\n*** LER ARQUIVO DE TEXTO ***" 5 70
            sleep 1
            delimitador=$(dialog --stdout --ok-label "Avançar" --no-cancel --title "Remote System" --form "** LER ARQUIVO DE TEXTO ***" 13 65 0 "Arquivo deve se chamar arq_srv_remoto.txt" 1 1 "" 1 16 0 0 "Loja |Data_Envio|Data_instalacao" 2 1 "" 1 16 0 0 "00000|00000000|00000000 <- Estrura de cada linha do txt." 3 1 "" 1 16 0 0 "02.1.1 Informe o delimitador [ Default='|']" 4 1 "" 1 16 0 0 "                   Disponiveis [ '|' ',' ';' '-' ] :" 5 1 "" 5 53 5 1)
            # Validadmos se as variáveis estão vazias
            [ "$delimitador" ] || { dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Delimitador não informado, será colocado o valor DEFAULT." 5 70 ; $delimit="|"; }
            case $delimitador in
                "|") # Selecionado PIPER
                    delimit="|"
                    LerTXT
                ;;
                ",") # Selecionado virgula
                    delimit=","
                    LerTXT
                ;;
                ";") # Selecionado ponto e virgula
                    delimit=";"
                    LerTXT
                ;;
                "-") # Selecionado Hifen
                    delimit="-"
                    LerTXT
                ;;
                *)
                    dialog --ok-label "OK" --title "Remote System" --msgbox "Delimitador informado, não foi reconhecido." 5 70
                    sleep 1
                    AtivarVariasLoja
            esac
      
        ;;
        "02.2"|"2.2") # Lojas em sequencia
            dialog --sleep 2 --title "Remote System" --infobox "\n*** LOJAS EM SEQUENCIA ***" 5 70
            LojasEmSeq
            sleep 1
        ;;
        "99") # Exit/Quit
            sleep 1
            main
        ;;
        *)
            dialog --sleep 2 --title "Remote System" --infobox "\nOpção inválida." 5 70
            AtivarLoja
    esac  
}

function LerTXT(){

    dialog --sleep 2 --title "Remote System" --infobox "\n*** VALIDANDO ARQUIVO DE TEXTO ***" 5 70
    sleep 1
    ARQ="arq_srv_remoto.txt"
    ARQ_SQL="arq_srv_remoto.SQL"
    ARQ_TMP="tmp_srv_remoto.txt"

    if [ -e $ARQ ]; then
        dialog --ok-label "Confirmar" --cancel-label "Voltar" --title "Ativar varias lojas apartir de um TXT" --backtitle "Read/Edit arquivo TXT" --editbox $ARQ 0 0 2> $ARQ_TMP
        if [ $? -eq 0 ]; then
            mv -f $ARQ_TMP $ARQ
            if [ -e $ARQ ]; then
                nlinhas=`cat $ARQ | wc -l`
                nlinhasc=`cat $ARQ | grep "$delimit" | wc -l`
                if [ $nlinhas -eq 0 ]; then
                    dialog --sleep 2 --title "Remote System" --infobox "\nDetectado erro na estrutura do txt." 5 70
                    sleep 1
                    main
                fi
                if [ $nlinhas != $nlinhasc ]; then
                    dialog --sleep 2 --title "Remote System" --infobox "\nDetectado erro na estrutura do txt." 5 70
                    sleep 1
                    main
                else
                    dialog --sleep 2 --title "Remote System" --infobox "\n*** LENDO ARQUIVO DE TEXTO ***" 5 70
                    sleep 1
                    IFS="$delimit"
                    i=0
                    while read shop dtinstall denvio;
                    do
                        let "i = i +1"
                        if [[ $shop = ?(+|-)+([0-9]) ]]; then
                            echo ""
                        else
                            dialog --sleep 2 --title "Remote System" --infobox "\nOps, valor informado não é um número inteiro." 5 70
                            if [ -e $ARQ_SQL ]; then
                                rm -vf $ARQ_SQL
                            fi
                            sleep 1
                            break
                            main
                        fi
                        if [[ $dtinstall = ?(+|-)+([0-9]) ]]; then
                            echo ""
                        else
                            dialog --sleep 2 --title "Remote System" --infobox "\nOps, valor informado não é um número inteiro." 5 70
                            if [ -e $ARQ_SQL ]; then
                                rm -vf $ARQ_SQL
                            fi
                            sleep 1
                            break
                            main
                        fi
                        if [[ $denvio = ?(+|-)+([0-9]) ]]; then
                            echo ""
                        else
                            dialog --sleep 2 --title "Remote System" --infobox "\nOps, valor informado não é um número inteiro." 5 70
                            if [ -e $ARQ_SQL ]; then
                                rm -vf $ARQ_SQL
                            fi
                            sleep 1
                            break
                            main
                        fi
                        shop=`printf "%05d" $shop`
                        dt=`echo ${#dtinstall}`
                        dtt=`echo ${#denvio}`
                        if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
                            dialog --sleep 2 --title "Remote System" --infobox "\nDetectado erro na data, linha [$i], revise o txt." 5 70
                            if [ -e $ARQ_SQL ]; then
                                rm -vf $ARQ_SQL
                            fi
                            sleep 1
                            break
                            main
                        elif [ $dtt -ge 9 ] || [ $dtt -le 7 ]; then
                            dialog --sleep 2 --title "Remote System" --infobox "\nDetectado erro na data, linha [$i], revise o txt." 5 70
                            if [ -e $ARQ_SQL ]; then
                                rm -vf $ARQ_SQL
                            fi
                            sleep 1
                            break
                            main
                        else
                            if [ $FUNC -eq 1 ]; then
                                exist=`echo "SELECT COUNT(id) FROM tb_printer_brother WHERE loja = '$shop';" | sqlite3 $BD`
                                if [ $exist -eq 0 ]; then
                                    echo "INSERT INTO tb_printer_brother (loja, data, data_instalacao) VALUES ( '$shop', '$dtinstall', '$denvio');" >> $ARQ_SQL
                                else
                                    echo "UPDATE tb_printer_brother SET data = '$dtinstall', data_instalacao = '$denvio' WHERE loja = '$shop';" >> $ARQ_SQL
                                fi
                            elif [ $FUNC -eq 2 ]; then
                                exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD`
                                if [ $exist -eq 0 ]; then
                                    echo "INSERT INTO tb_verificador (tienda, fecha_envio, versao) VALUES ( '$shop', '$dtinstall', '2');" >> $ARQ_SQL
                                else
                                    echo "UPDATE tb_verificador SET fecha_envio = '$dtinstall', versao = '2' WHERE tienda = '$shop';" >> $ARQ_SQL
                                fi
                            elif [ $FUNC -eq 3 ]; then
                                exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD`
                                if [ $exist -eq 0 ]; then
                                    echo "INSERT INTO tb_verificador (tienda, fecha_envio, versao) VALUES ( '$shop', '$dtinstall', '3');" >> $ARQ_SQL
                                else
                                    echo "UPDATE tb_verificador SET fecha_envio = '$dtinstall', versao = '3' WHERE tienda = '$shop';" >> $ARQ_SQL
                                fi
                            elif [ $FUNC -eq 4 ]; then
                                exist=`echo "SELECT COUNT(id) FROM tb_imp_fidelizacao WHERE loja = '$shop';" | sqlite3 $BD`
                                if [ $exist -eq 0 ]; then
                                    echo "INSERT INTO tb_imp_fidelizacao (loja, data) VALUES ( '$shop', '$dtinstall');" >> $ARQ_SQL
                                else
                                    echo "UPDATE tb_imp_fidelizacao SET data = '$dtinstall' WHERE ID=(SELECT MAX(ID) FROM tb_imp_fidelizacao WHERE tienda LIKE '$shop');" >> $ARQ_SQL
                                fi
                            elif [ $FUNC -eq 5]; then
                                exist=`echo "SELECT COUNT(id) FROM tb_actual_paf WHERE tienda = '$shop';" | sqlite3 $BD`
                                if [ $exist -eq 0 ]; then
                                    echo "INSERT INTO tb_actual_paf (tienda, active) VALUES ( '$shop', '1');" >> $ARQ_SQL
                                else
                                    echo "UPDATE tb_actual_paf SET active = '1' WHERE tienda = '$shop';" >> $ARQ_SQL
                                fi
                            elif [ $FUNC -eq 6 ]; then
                                exist=`echo "SELECT COUNT(id) FROM tb_install_oferta WHERE tienda = '$shop';" | sqlite3 $BD`
                                if [ $exist -eq 0 ]; then
                                    echo "INSERT INTO tb_install_oferta (tienda, n_tpvs, data_instalacao, data_envio, status) VALUES ( '$shop', '0','$denvio', '$dtinstall', 'pendente');" >> $ARQ_SQL
                                else
                                    echo "UPDATE tb_install_oferta SET data_instalacao = '$denvio', data_envio = '$dtinstall' WHERE tienda = '$shop';" >> $ARQ_SQL
                                fi
                            fi
                        fi
                        [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLoja ['$shop'].['$dtinstall'] gravada com sucesso." 5 70 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gravar loja no banco de dedos.Err[$?]." 5 70
                    done < $ARQ
                    if [ -e $ARQ_SQL ]; then    
                        sqlite3 $BD < $ARQ_SQL
                    fi
                fi
                if [ -e $ARQ_SQL ]; then
                    rm -vf $ARQ_SQL
                    rm -f $ARQ_TMP
                fi
            else
                dialog --ok-label "OK" --title "Remote System" --msgbox "Arquivo ['$ARQ'] não foi encontrado no diretorio atual." 5 70
            fi
        elif [ $? -eq 1 ]; then
            if [ $FUNC -eq 1 ]; then
                menuPrinterBrother
            elif [ $FUNC -eq 2 ]; then
                menuTCGertec2
            elif [ $FUNC -eq 3 ]; then
                menuTCGertec3
            elif [ $FUNC -eq 4 ]; then
                menuDisabledTM88
            elif [ $FUNC -eq 5 ]; then
                menuDeleteActualPAF
            elif [ $FUNC -eq 6 ]; then
                menuOfertaMenosVenda
            fi
        elif [ $? -eq 255 ]; then
            dialog --ok-label "OK" --title "Remote System" --msgbox "Operação cancelada.\nRetornando para o menu principal." 5 70
            main
        fi
    else
        dialog --ok-label "OK" --title "Remote System" --msgbox "Arquivo não encontrado no diretório atual." 5 70
    fi
    main
}

function LojasEmSeq(){

    ARQ_SQL="arq_srvremoto.SQL"
    ARQ_VAR="array_lojas.txt"
    shops=$(dialog --stdout --ok-label "Avançar" --no-cancel --title "Remote System" --form "** LOJAS EM SEQUENCIA ***" 10 100 0 "Informe as lojas em sequencia separados por um espaço, a data inserida será igual amanhã." 1 1 "" 1 16 0 0 "Digite o número das loja: " 2 1 "" 3 1 250 250 )
    # Validadmos se as variáveis estão vazias
    [ "$shops" ] || { dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Não foi detectado nenhum caracter." 5 70 ; sleep 1 ; main ;}
    echo $shops > $ARQ_VAR
    # Call função array 
    group_shop
    i=0
    dtinstall=`date +%Y%m%d -d '+1 days'`
    dtenvio=`date +%Y%m%d -d '+2 days'`
    while [ $i != ${#arrshops[@]} ]
    do
        if [ "$x" == "0" ];then
            dialog --sleep 1 --title "Remote System" --infobox "\nLoja: "${arrshops[$x]}"." 5 70
        else
            loja="${arrshops[$i]}"
            lj=`echo ${#loja}`
            if [ $lj -ge 5 ]; then
                dialog --sleep 1 --title "Informe até 4 caracteres númericos, o valor [${arrshops[$i]}] está sendo ignorado." 5 70
            else
                 # Validamos se os campos informados são númericos
                if [[ $loja = ?(+|-)+([0-9]) ]]; then
                    shop=`printf "%05d" ${arrshops[$i]}`
                    if [ $FUNC -eq  1 ]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_printer_brother WHERE loja = '$shop';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            echo "INSERT INTO tb_printer_brother (loja, data, data_instalacao) VALUES ( '$shop', '$dtinstall', '$dtenvio');" >> $ARQ_SQL
                        else
                            echo "UPDATE tb_printer_brother SET data = '$dtinstall', data_instalacao = '$dtenvio' WHERE loja = '$shop';" >> $ARQ_SQL
                        fi
                    elif [ $FUNC -eq 2 ]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            echo "INSERT INTO tb_verificador (tienda, fecha_envio, versao) VALUES ( '$shop', '$dtinstall', '2');" >> $ARQ_SQL
                        else
                            echo "UPDATE tb_verificador SET fecha_envio = '$dtinstall', versao = '2' WHERE tienda = '$shop';" >> $ARQ_SQL
                        fi
                    elif [ $FUNC -eq 3 ]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            echo "INSERT INTO tb_verificador (tienda, fecha_envio, versao) VALUES ( '$shop', '$dtinstall', '3');" >> $ARQ_SQL
                        else
                            echo "UPDATE tb_verificador SET fecha_envio = '$dtinstall', versao = '3' WHERE tienda = '$shop';" >> $ARQ_SQL
                        fi
                    elif [ $FUNC -eq 4 ]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_imp_fidelizacao WHERE loja = '$shop';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            echo "INSERT INTO tb_imp_fidelizacao (loja, data) VALUES ( '$shop', '$dtinstall');" >> $ARQ_SQL
                        else
                            echo "UPDATE tb_imp_fidelizacao SET data = '$dtinstall' WHERE ID=(SELECT MAX(ID) FROM tb_imp_fidelizacao WHERE tienda LIKE '$shop');" >> $ARQ_SQL
                        fi
                    elif [ $FUNC -eq 5 ]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_actual_paf WHERE tienda = '$shop';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            echo "INSERT INTO tb_actual_paf (tienda, active) VALUES ( '$shop', '1');" >> $ARQ_SQL
                        else
                            echo "UPDATE tb_actual_paf SET active = '1' WHERE tienda = '$shop';" >> $ARQ_SQL
                        fi
                    elif [ $FUNC -eq 6 ]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_install_oferta WHERE tienda = '$shop';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            echo "INSERT INTO tb_install_oferta (tienda, n_tpvs, data_instalacao, data_envio, status) VALUES ( '$shop', '0', '$dtinstall', '$dtenvio', 'pendente');" >> $ARQ_SQL
                        else
                            echo "UPDATE tb_install_oferta SET data_instalacao = '$dtinstall', data_envio = '$dtenvio' WHERE tienda = '$shop';" >> $ARQ_SQL
                        fi
                    fi
                else
                    dialog --sleep 2 --title "Remote System" --infobox "\nOps, valor [${arrshops[$i]}] informado não é um número inteiro, e está sendo ignorado." 5 70
                fi
            fi
        fi
    let "i = i +1"
    done
    sqlite3 $BD < $ARQ_SQL
    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLoja ['$shop'].['$dtinstall'] gravada com sucesso." 5 70 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gravar loja no banco de dedos.Err[$?]." 5 70
    rm -f $ARQ_VAR
    rm -f $ARQ_SQL
    main
}

function group_shop(){

    ARQ_ARRAY="array_lojas.txt"
	while read lojas; 
	do
        progs=("$lojas")
	done < $ARQ_ARRAY
	arrshops=(${progs[0]})

}

function ConsultarLoja(){

    if [ $FUNC -eq 1 ] || [ $FUNC -eq 2 ] || [ $FUNC -eq 3 ] || [ $FUNC -eq 4 ] || [ $FUNC -eq 6 ]; then
        opcao=$(dialog --stdout --ok-label "Avançar" --no-cancel --title "Remote System" --form "CONSULTAR LOJAS" 10 75 0 "03.1 Consultar por loja" 1 1 "" 1 16 0 0 "03.2 Consultar por data de instalação" 2 1 "" 1 1 0 0 "Digite a opção: " 3 1 "" 3 16 5 5)
    elif [ $FUNC -eq 5 ]; then
        opcao=$(dialog --stdout --ok-label "Avançar" --no-cancel --title "Remote System" --form "CONSULTAR LOJAS" 10 75 0 "03.1 Consultar por loja" 1 1 "" 1 16 0 0 "Digite a opção: " 2 1 "" 3 16 5 5)
    fi
    case $opcao in
        "03.1"|"3.1"|"03.01")
            shop=$(dialog --stdout --ok-label "Avançar" --no-cancel --title "Remote System" --form "CONSULTAR LOJAS" 10 75 0 "Digite o número da loja:" 1 1 "" 3 16 4 5)
            # Validadmos se as variáveis estão vazias
            [ "$shop" ] || { dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Não foi detectado nenhum caracter." 5 70 ; sleep 1 ; main ; }
            # Validamos a quantidade de caracteres informados
            lj=`echo ${#shop}`
            if [ $lj -ge 5 ]; then
                dialog --sleep 2 --title "Remote System" --infobox "\nInforme até 4 caracteres númericos. Ex.: 2, 1000, 9527, 165." 5 70 
                sleep 1
                main
            fi
            # Validamos se os campos informados são númericos
            if [[ $shop = ?(+|-)+([0-9]) ]]; then
                if [ $FUNC -eq 1 ]; then
                    shop=`printf "%05d" $shop`
                    exist=`echo "SELECT COUNT(id) FROM tb_printer_brother WHERE loja = '$shop';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        dialog --sleep 2 --title "Remote System" --infobox "\nLoja [$shop] não foi encontrada no banco de dados." 5 70
                        sleep 5
                        menuPrinterBrother
                    else
                        dialog --no-cancel --ok-label "Voltar" --title "Remote System" --msgbox "\n$(echo "SELECT loja AS Loja, data AS Data_Envio, data_instalacao AS Data_Instalação FROM tb_printer_brother WHERE loja = '$shop';" | sqlite3 $BD --header --line)" 6 90
                        sleep 5
                        [ "$?" = "0" ] && sleep 1 || dialog --sleep 2 --title "Remote System" --infobox "\nFalha durante a consulta loja [$shop].Err[$?]." 5 70
                        menuPrinterBrother
                    fi
                elif [ $FUNC -eq 2 ]; then
                    shop=`printf "%05d" $shop`
                    exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        dialog --sleep 2 --title "Remote System" --infobox "\nLoja [$shop] não foi encontrada no banco de dados." 5 70
                        sleep 5
                        menuTCGertec2
                    else
                        dialog --no-cancel --ok-label "Voltar" --title "Remote System" --msgbox "\n$(echo "SELECT tienda as Loja, fecha_envio AS Data_Envio, versao AS Versão FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD --header --line)" 6 60
                        sleep 5
                        [ "$?" = "0" ] && sleep 1 || dialog --sleep 2 --title "Remote System" --infobox "\nFalha durante a consulta loja [$shop].Err[$?]." 5 70
                        menuTCGertec2
                    fi
                elif [ $FUNC -eq 3 ]; then
                    shop=`printf "%05d" $shop`
                    exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        dialog --sleep 2 --title "Remote System" --infobox "\nLoja [$shop] não foi encontrada no banco de dados." 5 70
                        sleep 5
                        menuTCGertec2
                    else
                        dialog --no-cancel --ok-label "Voltar" --title "Remote System" --msgbox "\n$(echo "SELECT tienda as Loja, fecha_envio AS Data_Envio, versao AS Versão FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD --header --line)" 6 60
                        sleep 5
                        [ "$?" = "0" ] && sleep 1 || dialog --sleep 2 --title "Remote System" --infobox "\nFalha durante a consulta loja [$shop].Err[$?]." 5 70
                        menuTCGertec2
                    fi
                elif [ $FUNC -eq 4 ]; then
                    shop=`printf "%05d" $shop`
                    exist=`echo "SELECT COUNT(id) FROM tb_imp_fidelizacao WHERE loja = '$shop';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        dialog --sleep 2 --title "Remote System" --infobox "\nLoja [$shop] não foi encontrada no banco de dados." 0 0
                        sleep 5
                        menuDisabledTM88
                    else
                        dialog --no-cancel --ok-label "Voltar" --title "Remote System" --msgbox "\n$(echo "SELECT loja AS Loja, data AS Data_Instalação FROM tb_imp_fidelizacao WHERE loja = '$shop';" | sqlite3 $BD --header --line)" 6 60
                        [ "$?" = "0" ] && sleep 1 || dialog --sleep 2 --title "Remote System" --infobox "\nFalha durante a consulta loja [$shop].Err[$?]." 5 70
                        sleep 5
                        menuDisabledTM88
                    fi
                elif [ $FUNC -eq 5 ]; then
                    shop=`printf "%05d" $shop`
                    exist=`echo "SELECT COUNT(id) FROM tb_actual_paf WHERE tienda = '$shop';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        dialog --sleep 2 --title "Remote System" --infobox "\nLoja [$shop] não foi encontrada no banco de dados." 5 70
                        sleep 5
                        menuDeleteActualPAF
                    else
                        dialog --no-cancel --ok-label "Voltar" --title "Remote System" --msgbox "\n$(echo "SELECT tienda AS Loja, active AS Active FROM tb_actual_paf WHERE tienda = '$shop';" | sqlite3 $BD --header --line)" 6 40
                        [ "$?" = "0" ] && sleep 1 || dialog --sleep 2 --title "Remote System" --infobox "\nFalha durante a consulta loja [$shop].Err[$?]." 5 70
                        sleep 5
                        menuDeleteActualPAF
                    fi
                elif [ $FUNC -eq 6 ]; then
                    shop=`printf "%05d" $shop`
                    exist=`echo "SELECT COUNT(id) FROM tb_install_oferta WHERE tienda = '$shop';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        dialog --sleep 2 --title "Remote System" --infobox "\nLoja [$shop] não foi encontrada no banco de dados." 5 70
                        sleep 5
                        menuOfertaMenosVenda
                    else
                        dialog --no-cancel --ok-label "Voltar" --title "Remote System" --msgbox "\n$(echo "SELECT tienda AS Loja, n_tpvs AS N_Pdvs_Ativo, data_envio AS Data_Envio, data_instalacao AS Data_Instalação FROM tb_install_oferta WHERE tienda = '$shop';" | sqlite3 $BD --header --line)" 6 110
                        [ "$?" = "0" ] && sleep 1 || dialog --sleep 2 --title "Remote System" --infobox "\nFalha durante a consulta loja [$shop].Err[$?]." 5 70
                        sleep 5
                        menuOfertaMenosVenda
                    fi
                fi
            else
                dialog --sleep 2 --title "Remote System" --infobox "\nOps, valor informado não é um número inteiro." 5 70
                sleep 1
                main
            fi
        ;;
        "03.2"|"03.02"|"3.2")
            datinstalacao=$(dialog --stdout --no-cancel --ok-label "Confirmar" --date-format "%Y%m%d" --calendar "Selecione a data de envio:" 0 0)
            # Validadmos se as variáveis estão vazias
            [ "$datinstalacao" ] || { dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Não foi detectado nenhum caracter." 5 70 ; sleep 1 ; main ; }
            # Validamos a quantidade de caracteres informados
            dt=`echo ${#datinstalacao}`
            if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
                dialog --sleep 2 --title "Remote System" --infobox "\nInforme até 8 caracteres númericos. Ex.: 20190101." 5 70
                sleep 1
                main
            fi
            # Validamos se os campos informados são númericos
            if [[ $datinstalacao = ?(+|-)+([0-9]) ]]; then
                if [ $FUNC -eq 1 ]; then
                    exist=`echo "SELECT COUNT(id) FROM tb_printer_brother WHERE data = '$datinstalacao';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        dialog --sleep 2 --title "Remote System" --infobox "\nNão foi encontrada nenhuma loja no banco de dados com esta data[$datinstalacao]." 5 70
                        sleep 5
                        menuPrinterBrother
                    else
                        dialog --no-cancel --ok-label "Voltar" --title "Remote System" --msgbox "Consulta de lojas por data:\n$(echo "SELECT loja AS Loja, data AS Data_Envio, data_instalacao AS Data_Instalação FROM tb_printer_brother WHERE data = '$datinstalacao';" | sqlite3 $BD --header --line)" $exist 90
                        [ "$?" = "0" ] && sleep 1 || dialog --sleep 2 --title "Remote System" --infobox "\nFalha durante a consulta loja [$datinstalacao].Err[$?]." 5 70
                        sleep 5
                        menuPrinterBrother
                    fi
                elif [ $FUNC -eq 2 ]; then
                    exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE fecha_envio = '$datinstalacao';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        dialog --sleep 2 --title "Remote System" --infobox "\nNão foi encontrada nenhuma loja no banco de dados com esta data[$datinstalacao]." 5 70
                        menuTCGertec2
                    else
                        dialog --no-cancel --ok-label "Voltar" --title "Remote System" --msgbox "\n$(echo "SELECT tienda as Loja, fecha_envio AS Data_Envio, versao AS Versão FROM tb_verificador WHERE fecha_envio = '$datinstalacao';" | sqlite3 $BD --header --line)" $exist 90
                        [ "$?" = "0" ] && sleep 1 || dialog --sleep 2 --title "Remote System" --infobox "\nFalha durante a consulta loja [$datinstalacao].Err[$?]." 5 70
                        sleep 5
                        menuTCGertec2
                    fi
                elif [ $FUNC -eq 3 ]; then
                    exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE fecha_envio = '$datinstalacao';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        dialog --sleep 2 --title "Remote System" --infobox "\nNão foi encontrada nenhuma loja no banco de dados com esta data[$datinstalacao]." 5 70
                        sleep 5
                        menuTCGertec3
                    else
                        dialog --no-cancel --ok-label "Voltar" --title "Remote System" --msgbox "\n$(echo "SELECT tienda as Loja, fecha_envio AS Data_Envio, versao AS Versão FROM tb_verificador WHERE fecha_envio = '$datinstalacao';" | sqlite3 $BD --header --line)" $exist 90
                        [ "$?" = "0" ] && sleep 1 || dialog --sleep 2 --title "Remote System" --infobox "\nFalha durante a consulta loja [$datinstalacao].Err[$?]." 5 70
                        sleep 5
                        menuTCGertec3
                    fi
                elif [ $FUNC -eq 4 ]; then
                    exist=`echo "SELECT COUNT(id) FROM tb_imp_fidelizacao WHERE data = '$datinstalacao';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        dialog --sleep 2 --title "Remote System" --infobox "\nNão foi encontrada nenhuma loja no banco de dados com esta data[$datinstalacao]." 5 70
                        sleep 5
                        menuDisabledTM88
                    else
                        dialog --no-cancel --ok-label "Voltar" --title "Remote System" --msgbox "\n$(echo "SELECT loja AS Loja, data AS Data_Instalação FROM tb_imp_fidelizacao WHERE data = '$datinstalacao';" | sqlite3 $BD --header --line)" 6 60
                        [ "$?" = "0" ] && sleep 1 || dialog --sleep 2 --title "Remote System" --infobox "\nFalha durante a consulta loja [$datinstalacao].Err[$?]." 5 70
                        sleep 5
                        menuDisabledTM88
                    fi
                elif [ $FUNC -eq 6 ]; then
                    exist=`echo "SELECT COUNT(id) FROM tb_install_oferta WHERE data_instalacao = '$datinstalacao';" | sqlite3 $BD`
                    if [ $exist -eq 0 ]; then
                        dialog --sleep 2 --title "Remote System" --infobox "\nNão foi encontrada nenhuma loja no banco de dados com esta data[$datinstalacao]." 5 70
                        sleep 5
                        menuOfertaMenosVenda
                    else
                        dialog --no-cancel --ok-label "Voltar" --title "Remote System" --msgbox "\n$(echo "SELECT tienda AS Loja, n_tpvs AS N_Pdvs_Ativo, data_envio AS Data_Envio, data_instalacao AS Data_Instalação FROM tb_install_oferta WHERE data_instalacao = '$datinstalacao';" | sqlite3 $BD --header --line)" 6 110
                        [ "$?" = "0" ] && sleep 1 || dialog --sleep 2 --title "Remote System" --infobox "\nFalha durante a consulta loja [$datinstalacao].Err[$?]." 5 70
                        sleep 5
                        menuOfertaMenosVenda
                    fi
                fi
            else
                dialog --sleep 2 --title "Remote System" --infobox "\nFOps, valor informado não é um número inteiro." 5 70
                sleep 1
                main
            fi
        ;;
        *)
            dialog --sleep 2 --title "Remote System" --infobox "\nOpção Inválida." 5 70
    esac

}

function DesativarLoja(){

    shop=$(dialog --stdout --title "Remote Systema" --form "Desativar lojas" 11 50 0 "Informe a loja:" 1 1 "" 1 16 6 4)
    # Validadmos se as variáveis estão vazias
    [ "$shop" ] || { dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Não foi detectado nenhum caracter." 5 70; sleep 1 ; main ; }
    # Validamos a quantidade de caracteres informados
    lj=`echo ${#shop}`
    if [ $lj -ge 5 ]; then
        dialog --sleep 2 --title "Remote System" --infobox "\nInforme até 4 caracteres númericos. Ex.: 2, 1000, 9527, 165." 5 70
        sleep 1
        main
    fi
    # Validamos se os campos informados são númericos
    if [[ $shop = ?(+|-)+([0-9]) ]]; then
        if [ $FUNC -eq 1 ]; then
            shop=`printf "%05d" $shop`
            exist=`echo "SELECT COUNT(id) FROM tb_printer_brother WHERE loja = '$shop';" | sqlite3 $BD`
            if [ $exist -eq 0 ]; then
                dialog --sleep 2 --title "Remote System" --infobox "\nLoja não foi encontrada no banco de dados." 5 70
            else
                echo "UPDATE tb_printer_brother SET data = '99999999', data_instalacao = '99999999' WHERE loja = '$shop';" | sqlite3 $BD
                [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nDados para loja ['$shop'].['99999999'] gravada com sucesso." 5 70 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gravar loja no banco de dedos.Err[$?]." 5 70
            fi
        elif [ $FUNC -eq 2 ]; then
            shop=`printf "%05d" $shop`
            exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD`
            if [ $exist -eq 0 ]; then
                 dialog --sleep 2 --title "Remote System" --infobox "\nLoja não foi encontrada no banco de dados." 5 70
            else
                echo "UPDATE tb_verificador SET fecha_envio = '99999999' WHERE tienda = '$shop';" | sqlite3 $BD
                [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nDados para loja ['$shop'].['99999999'] gravada com sucesso." 5 70 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gravar loja no banco de dedos.Err[$?]." 5 70
            fi
        elif [ $FUNC -eq 3 ]; then
            shop=`printf "%05d" $shop`
            exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$shop';" | sqlite3 $BD`
            if [ $exist -eq 0 ]; then
                dialog --sleep 2 --title "Remote System" --infobox "\nLoja não foi encontrada no banco de dados." 5 70
            else
               [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nDados para loja ['$shop'].['99999999'] gravada com sucesso." 5 70 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gravar loja no banco de dedos.Err[$?]." 5 70
            fi
        elif [ $FUNC -eq 4 ]; then
            shop=`printf "%05d" $shop`
            exist=`echo "SELECT COUNT(id) FROM tb_imp_fidelizacao WHERE loja = '$shop';" | sqlite3 $BD`
            if [ $exist -eq 0 ]; then
                dialog --sleep 2 --title "Remote System" --infobox "\nLoja não foi encontrada no banco de dados." 5 70
            else
                [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nDados para loja ['$shop'].['99999999'] gravada com sucesso." 5 70 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gravar loja no banco de dedos.Err[$?]." 5 70
            fi
        elif [ $FUNC -eq 5 ]; then
            shop=`printf "%05d" $shop`
            exist=`echo "SELECT COUNT(id) FROM tb_actual_paf WHERE tienda = '$shop';" | sqlite3 $BD`
            if [ $exist -eq 0 ]; then
                dialog --sleep 2 --title "Remote System" --infobox "\nLoja não foi encontrada no banco de dados." 5 70
            else
                [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nDados para loja ['$shop'].['99999999'] gravada com sucesso." 5 70 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gravar loja no banco de dedos.Err[$?]." 5 70
            fi
        elif [ $FUNC -eq 6 ]; then
            shop=`printf "%05d" $shop`
            exist=`echo "SELECT COUNT(id) FROM tb_install_oferta WHERE tienda = '$shop';" | sqlite3 $BD`
            if [ $exist -eq 0 ]; then
                dialog --sleep 2 --title "Remote System" --infobox "\nLoja não foi encontrada no banco de dados." 5 70
            else
                echo "UPDATE tb_install_oferta SET data_instalacao = '99999999', data_envio = '99999999' WHERE tienda = '$shop';" | sqlite3 $BD
                [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nDados para loja ['$shop'].['99999999'] gravada com sucesso." 5 70 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gravar loja no banco de dedos.Err[$?]." 5 70
            fi
        fi
    else
        dialog --sleep 2 --title "Remote System" --infobox "\nOps, valor informado não é um número inteiro." 5 70
        sleep 1
        main
    fi

}

function GerarLog(){

    if [ $FUNC -eq 1 ]; then
        LOG="/root/srv_remoto/log/error.printerbrother.log"
    elif [ $FUNC -eq 2 ]; then
        LOG="/root/srv_remoto/log/error.tcgertec.log"
    elif [ $FUNC -eq 3 ]; then
        LOG="/root/srv_remoto/log/error.tcgertec.log"
    elif [ $FUNC -eq 4 ]; then
        LOG="/root/srv_remoto/log/error.impfidelizacao.log"
    elif [ $FUNC -eq 5 ]; then
        LOG="/root/srv_remoto/log/error.actualpaf.log"
    elif [ $FUNC -eq 6 ]; then
        LOG="/root/srv_remoto/log/error.oferta.log"
    fi
    fator=$(dialog --stdout --ok-label "Avançar" --no-cancel --title "Remote System" --form "GERAR LOGS" 10 75 0 "05.1 Gerar Log por loja" 1 1 "" 1 16 0 0 "05.2 Gerar Log por Data de Instalação" 2 1 "" 1 1 0 0 "Digite a opção: " 3 1 "" 3 16 5 5)
    case $fator in
        "05.1"|"05.01"|"5.1")
            shop=$(dialog --stdout --title "Remote Systema" --form "Gerar Log por Loja" 11 50 0 "Informe a loja:" 1 1 "" 1 16 6 4)
            # Validadmos se as variáveis estão vazias
            [ "$shop" ] || { dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Não foi detectado nenhum caracter." 5 70 ; sleep 1 ; main ; }
            # Validamos a quantidade de caracteres informados
            lj=`echo ${#shop}`
            if [ $lj -ge 5 ]; then
                dialog --sleep 2 --title "Remote System" --infobox "\nInforme até 4 caracteres númericos. Ex.: 2, 1000, 9527, 165." 5 70
                sleep 1
                main
            fi
            # Validamos se os campos informados são númericos
            if [[ $shop = ?(+|-)+([0-9]) ]]; then
                if [ $FUNC -eq 1 ]; then
                    shop=`printf "%05d" $shop`
                    cat $LOG | grep -i "Loja.*$shop" > $LOG1
                    dialog --title "Teste arquivo de texto" --textbox $LOG1 0 0
                    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLog gerado com sucesso." 5 70 ||dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar Log.Err[$?]." 5 70
                elif [ $FUNC -eq 2 ]; then
                    shop=`printf "%05d" $shop`
                    cat $LOG | grep -i "Loja.*$shop" > $LOG1
                    dialog --title "Teste arquivo de texto" --textbox $LOG1 0 0
                    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLog gerado com sucesso." 5 70 ||dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar Log.Err[$?]." 5 70
                elif [ $FUNC -eq 3 ]; then
                    shop=`printf "%05d" $shop`
                    cat $LOG | grep -i "Loja.*$shop" > $LOG1
                    dialog --title "Teste arquivo de texto" --textbox $LOG1 0 0
                    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLog gerado com sucesso." 5 70 ||dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar Log.Err[$?]." 5 70
                elif [ $FUNC -eq 4 ]; then
                    shop=`printf "%05d" $shop`
                    cat $LOG | grep -i "Loja.*$shop" > $LOG1
                    dialog --title "Teste arquivo de texto" --textbox $LOG1 0 0
                    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLog gerado com sucesso." 5 70 ||dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar Log.Err[$?]." 5 70
                elif [ $FUNC -eq 5 ]; then
                    shop=`printf "%05d" $shop`
                    cat $LOG | grep -i "Loja.*$shop" > $LOG1
                    dialog --title "Teste arquivo de texto" --textbox $LOG1 0 0
                    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLog gerado com sucesso." 5 70 ||dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar Log.Err[$?]." 5 70
                elif [ $FUNC -eq 6 ]; then
                    shop=`printf "%05d" $shop`
                    cat $LOG | grep -i "Loja.*$shop" > $LOG1
                    dialog --title "Teste arquivo de texto" --textbox $LOG1 0 0
                    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLog gerado com sucesso." 5 70 ||dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar Log.Err[$?]." 5 70
                fi
            else
                dialog --sleep 2 --title "Remote System" --infobox "\nOps, valor informado não é um número inteiro." 5 70
                sleep 1
                main
            fi

        ;;
        "05.2"|"05.02"|"5.2")
            datinstalacao=$(dialog --stdout --no-cancel --ok-label "Confirmar" --date-format "%Y%m%d" --calendar "Selecione a data de envio:" 0 0)
            # Validadmos se as variáveis estão vazias
            [ "$datinstalacao" ] || { dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Não foi detectado nenhum caracter." 5 70 ; sleep 1 ; main ; }
            # Validamos a quantidade de caracteres informados
            dt=`echo ${#datinstalacao}`
            if [ $dt -ge 9 ] || [ $dt -le 7 ]; then               
                dialog --sleep 2 --title "Remote System" --infobox "\nInforme até 8 caracteres númericos. Ex.: 20190101." 5 70
                sleep 1
                main
            fi
            # Validamos se os campos informados são númericos
            if [[ $datinstalacao = ?(+|-)+([0-9]) ]]; then
                if [ $FUNC -eq 1 ]; then
                    cat $LOG | grep -i "^$datinstalacao" > $LOG1
                    dialog --title "Teste arquivo de texto" --textbox $LOG1 0 0
                    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLog gerado com sucesso." 5 70 ||dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar Log.Err[$?]." 5 70
                elif [ $FUNC -eq 2 ]; then
                    cat $LOG | grep -i "^$datinstalacao" > $LOG1
                    dialog --title "Teste arquivo de texto" --textbox $LOG1 0 0
                    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLog gerado com sucesso." 5 70 ||dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar Log.Err[$?]." 5 70
                elif [ $FUNC -eq 3 ]; then
                    cat $LOG | grep -i "^$datinstalacao" > $LOG1
                    dialog --title "Teste arquivo de texto" --textbox $LOG1 0 0
                    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLog gerado com sucesso." 5 70 ||dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar Log.Err[$?]." 5 70
                elif [ $FUNC -eq 4 ]; then
                    cat $LOG | grep -i "^$datinstalacao" > $LOG1
                    dialog --title "Teste arquivo de texto" --textbox $LOG1 0 0
                    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLog gerado com sucesso." 5 70 ||dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar Log.Err[$?]." 5 70
                elif [ $FUNC -eq 5 ]; then
                    cat $LOG | grep -i "^$datinstalacao" > $LOG1
                    dialog --title "Teste arquivo de texto" --textbox $LOG1 0 0
                    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLog gerado com sucesso." 5 70 ||dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar Log.Err[$?]." 5 70
                elif [ $FUNC -eq 6 ]; then
                    cat $LOG | grep -i "^$datinstalacao" > $LOG1
                    dialog --title "Teste arquivo de texto" --textbox $LOG1 0 0
                    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLog gerado com sucesso." 5 70 ||dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar Log.Err[$?]." 5 70
                fi
            else
                dialog --sleep 2 --title "Remote System" --infobox "\nOps, valor informado não é um número inteiro." 5 70
                sleep 1
                main
            fi
        ;;
        *)
             dialog --sleep 2 --title "Remote System" --infobox "\nOpção inválida." 5 70
    esac
}

function ExtrairDados(){

    op=$(dialog --stdout --ok-label "Avançar" --no-cancel --title "Remote System" --form "EXTRAIR DADOS DO BANCO DE DADOS" 10 75 0 "06.1 Extrair dados por loja" 1 1 "" 1 16 0 0 "06.2 Extrair por Data de Instalação" 2 1 "" 1 1 0 0 "Digite a opção: " 3 1 "" 3 16 5 5)
    case $op in
            "06.1"|"6.1"|"06.01")
                if [ $FUNC -eq 1 ]; then
                    sqlite3 $BD  ".output relatorio_completo.txt" \ "SELECT * FROM tb_printer_brother;"
                    LOG2=$(cat relatorio_completo.txt)
                    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLog gerado com sucesso." dialog --title "Relatório banco de dados" --textbox $LOG2 0 0 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar Log.Err[$?]." 5 70
                elif [ $FUNC -eq 2 ]; then
                    sqlite3 $BD  ".output relatorio_completo.txt" \ "SELECT * FROM tb_verificador;"
                    LOG2=$(cat relatorio_completo.txt)
                    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLog gerado com sucesso." dialog --title "Relatório banco de dados" --textbox $LOG2 0 0 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar Log.Err[$?]." 5 70
                elif [ $FUNC -eq 3 ]; then
                    sqlite3 $BD  ".output relatorio_completo.txt" \ "SELECT * FROM tb_verificador;"
                    LOG2=$(cat relatorio_completo.txt)
                    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLog gerado com sucesso." dialog --title "Relatório banco de dados" --textbox $LOG2 0 0 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar Log.Err[$?]." 5 70
                elif [ $FUNC -eq 4 ]; then
                    sqlite3 $BD  ".output relatorio_completo.txt" \ "SELECT * FROM tb_imp_fedelizacao;"
                    LOG2=$(cat relatorio_completo.txt)
                    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLog gerado com sucesso." dialog --title "Relatório banco de dados" --textbox $LOG2 0 0 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar Log.Err[$?]." 5 70
                elif [ $FUNC -eq 5 ]; then
                    sqlite3 $BD  ".output relatorio_completo.txt" \ "SELECT * FROM tb_actual_paf;"
                    LOG2=$(cat relatorio_completo.txt)
                    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLog gerado com sucesso." dialog --title "Relatório banco de dados" --textbox $LOG2 0 0 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar Log.Err[$?]." 5 70
                elif [ $FUNC -eq 6 ]; then
                    sqlite3 $BD  ".output relatorio_completo.txt" \ "SELECT * FROM tb_install_oferta;"
                    LOG2=$(cat relatorio_completo.txt)
                    [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nLog gerado com sucesso." dialog --title "Relatório banco de dados" --textbox $LOG2 0 0 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar Log.Err[$?]." 5 70
                fi
            ;;
            "06.2"|"6.2"|"06.02")
                dinstalacao=$(dialog --stdout --no-cancel --ok-label "Confirmar" --date-format "%Y%m%d" --calendar "Selecione a data inicial:" 0 0)
                # Validadmos se as variáveis estão vazias
                [ "$dinstalacao" ] || { dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Não foi detectado nenhum caracter." 5 70 ; sleep 1 ; main ; }
                # Validamos a quantidade de caracteres informados
                dt=`echo ${#dinstalacao}`
                if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
                    dialog --sleep 2 --title "Remote System" --infobox "\nInforme até 8 caracteres númericos. Ex.: 20190101." 5 70
                    sleep 1
                    main
                fi
                
                dtinstalacao=$(dialog --stdout --no-cancel --ok-label "Confirmar" --date-format "%Y%m%d" --calendar "Selecione a data final:" 0 0)
                # Validadmos se as variáveis estão vazias
                [ "$dtinstalacao" ] || { dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Não foi detectado nenhum caracter." 5 70 ; sleep 1 ; main ; }
                # Validamos a quantidade de caracteres informados
                dt=`echo ${#dtinstalacao}`
                if [ $dt -ge 9 ] || [ $dt -le 7 ]; then
                    dialog --sleep 2 --title "Remote System" --infobox "\nInforme até 8 caracteres númericos. Ex.: 20190101." 5 70
                    sleep 1
                    main
                fi
                # Validamos se os campos informados são númericos
                if [[ $dinstalacao = ?(+|-)+([0-9]) ]]; then
                    if [[ $dtinstalacao = ?(+|-)+([0-9]) ]]; then
                        exist=`echo "SELECT COUNT(id) FROM tb_printer_brother WHERE data LIKE '$dinstalacao';" | sqlite3 $BD`
                        if [ $exist != 0 ]; then
                            if [ $FUNC -eq 1 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_printer_brother WHERE data BETWEEN '$dinstalacao' AND '$dtinstalacao';"
                                LOG2=$(cat relatorio_por_periodo.txt)
                                [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nRelatorio gerado com sucesso." 5 70 ; dialog --title "Relatório banco de dados" --textbox $LOG2 0 0 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar relatorio.Err[$?]." 5 70
                            elif [ $FUNC -eq 2 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_verificador WHERE fecha_envio BETWEEN '$dinstalacao' AND '$dtinstalacao';"
                                LOG2=$(cat relatorio_por_periodo.txt)
                                [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nRelatorio gerado com sucesso." 5 70 ; dialog --title "Relatório banco de dados" --textbox $LOG2 0 0 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar relatorio.Err[$?]." 5 70
                            elif [ $FUNC -eq 3 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_verificador WHERE fecha_envio BETWEEN '$dinstalacao' AND '$dtinstalacao';"
                                LOG2=$(cat relatorio_por_periodo.txt)
                                [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nRelatorio gerado com sucesso." 5 70 ; dialog --title "Relatório banco de dados" --textbox $LOG2 0 0 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar relatorio.Err[$?]." 5 70
                            elif [ $FUNC -eq 4 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_imp_fidelizacao WHERE data BETWEEN '$dinstalacao' AND '$dtinstalacao';"
                                LOG2=$(cat relatorio_por_periodo.txt)
                                [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nRelatorio gerado com sucesso." 5 70 ; dialog --title "Relatório banco de dados" --textbox $LOG2 0 0 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar relatorio.Err[$?]." 5 70
                            elif [ $FUNC -eq 5 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_actual_paf WHERE status LIKE 'ACTIVE';"
                                LOG2=$(cat relatorio_por_periodo.txt)
                                [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nRelatorio gerado com sucesso." 5 70 ; dialog --title "Relatório banco de dados" --textbox $LOG2 0 0 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar relatorio.Err[$?]." 5 70
                            elif [ $FUNC -eq 6 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_install_oferta WHERE data_instalacao BETWEEN '$dinstalacao AND '$dtinstalacao';"
                                LOG2=$(cat relatorio_por_periodo.txt)
                                [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nRelatorio gerado com sucesso." 5 70 ; dialog --title "Relatório banco de dados" --textbox $LOG2 0 0 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar relatorio.Err[$?]." 5 70
                            fi
                        else
                            if [ $FUNC -eq 1 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_printer_brother WHERE data BETWEEN '$dinstalacao' AND '$dtinstalacao';"
                                LOG2=$(cat relatorio_por_periodo.txt)
                                [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nRelatorio gerado com sucesso." 5 70 ; dialog --title "Relatorio banco de dados" --textbox $LOG2 0 0 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar relatorio.Err[$?]." 5 70
                            elif [ $FUNC -eq 2 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_verificador WHERE fecha_envio BETWEEN '$dinstalacao' AND '$dtinstalacao';"
                                LOG2=$(cat relatorio_por_periodo.txt)
                                [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nRelatorio gerado com sucesso." 5 70 ; dialog --title "Relatório banco de dados" --textbox $LOG2 0 0 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar relatorio.Err[$?]." 5 70
                            elif [ $FUNC -eq 3 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_verificador WHERE fecha_envio BETWEEN '$dinstalacao' AND '$dtinstalacao';"
                                LOG2=$(cat relatorio_por_periodo.txt)
                                [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nRelatorio gerado com sucesso." 5 70 ; dialog --title "Relatorio banco de dados" --textbox $LOG2 0 0 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar relatorio.Err[$?]." 5 70
                            elif [ $FUNC -eq 4 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_imp_fidelizacao WHERE data BETWEEN '$dinstalacao' AND '$dtinstalacao';"
                                LOG2=$(cat relatorio_por_periodo.txt)
                                [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nRelatorio gerado com sucesso." 5 70 ; dialog --title "Relatorio banco de dados" --textbox $LOG2 0 0 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar relatorio.Err[$?]." 5 70
                            elif [ $FUNC -eq 5 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_actual_paf WHERE status LIKE 'ACTIVE';"
                                LOG2=$(cat relatorio_por_periodo.txt)
                                [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nRelatorio gerado com sucesso." 5 70 ; dialog --title "Relatorio banco de dados" --textbox $LOG2 0 0 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar relatorio.Err[$?]." 5 70
                            elif [ $FUNC -eq 6 ]; then
                                sqlite3 $BD  ".output relatorio_por_periodo.txt" \ "SELECT * FROM tb_install_oferta WHERE data_instalacao BETWEEN '$dinstalacao AND '$dtinstalacao';"
                                LOG2=$(cat relatorio_por_periodo.txt)
                                [ "$?" = "0" ] && dialog --sleep 2 --title "Remote System" --infobox "\nRelatorio gerado com sucesso." 5 70 ; dialog --title "Relatorio banco de dados" --textbox $LOG2 0 0 || dialog --sleep 2 --title "Remote System" --infobox "\nErro ao gerar relatorio.Err[$?]." 5 70
                            fi
                        fi
                    else
                        dialog --sleep 2 --title "Remote System" --infobox "\nOps, valor informado não é um número inteiro." 5 70
                        sleep 1
                        main
                    fi
                else
                    dialog --sleep 2 --title "Remote System" --infobox "\nOps, valor informado não é um número inteiro." 5 70
                    sleep 1
                    main
                fi
            ;;
            *)
                dialog --sleep 2 --title "Remote System" --infobox "\nOpção inválida." 5 70
                main
    esac
}

function ForcedOneShop(){

# Solicitamos ao usuario que informe a loja e data de instação
loja=$(dialog --stdout --title "Remote Systema" --form "Forced One Shop" 11 50 0 "Informe a loja:" 1 1 "" 1 16 6 4)
# Validadmos se as variáveis estão vazias
[ "$loja" ] || { dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Não foi detectado nenhum caracter." 5 70 ; sleep 1 ; main ; }
# Validamos a quantidade de caracteres informados
lj=`echo ${#loja}`
if [ $lj -ge 5 ]; then
    dialog --sleep 2 --title "Remote System" --infobox "\nInforme até 4 caracteres númericos. Ex.: 2, 1000, 9527, 165." 5 70
    sleep 1
    main
fi
# Validamos se os campos informados são númericos
if [[ $loja = ?(+|-)+([0-9]) ]]; then
    echo ""
else
    dialog --sleep 2 --title "Remote System" --infobox "\nOps, valor informado não é um número inteiro." 5 70
    sleep 1
    main
fi
loja=`printf "%05d" $loja`
EXISTE=`echo "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$loja';" | sqlite3 $BD`

if [ $EXISTE -eq 1 ]; then
    # GetIP
    ip=`echo "SELECT ip FROM tb_ip WHERE loja LIKE '$loja';" | sqlite3 $BD`
else
    dialog --sleep 2 --title "Remote System" --infobox "\nLoja não encontrada no banco de dados." 5 70
    dialog --stdout --title "Forced Instalation" --yesno "Deseja informar o ip manualmente?" 0 0
    decisao=$?
    case $decisao in
        "0")
            read -p " " ipss
            ipss=$(dialog --stdout --title "Remote System" --form "Forced Instalation" 11 50 0 "Digite IP (XXX.XXX.XXX.XXX)=" 1 1 "" 1 16 6 4)
            # Validadmos se as variáveis estão vazias
            [ "$ipss" ] || { dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Não foi detectado nenhum caracter." 5 70 ; sleep 1 ; main ; }
            ip=$ipss
        ;;
        "1"|"255")
            main  
        ;;
        *)
            dialog --sleep 2 --title "Remote System" --infobox "\nOpção inválida." 5 70
            main
    esac

fi

sshpass -p root ssh -o ConnectTimeout=1 $ip exit
return=$?
if [ $return -eq 0 ]; then
    if [ $FUNC -eq 1 ]; then
        FILE1="/root/srv_remoto/tgz/configuracaoBrother.tgz"
        FILE2="/root/srv_remoto/tgz/666"
        sshpass -p root scp -P10001 $FILE1 root@$ip:/confdia/descargas/
		sshpass -p root scp -P10001 $FILE2 root@$ip:/confdia/bin/
        sshpass -p root ssh -p10001 -l root $ip 'tar -xzvf /confdia/descargas/configuracaoBrother.tgz -C /'
		return=$?
        if [ $return -eq 0 ]; then
            dialog --sleep 2 --title "Remote System" --infobox "\nImpressora Brother instalado com sucesso." 5 70
            sleep 5
            datae=`date +%Y%m%d -d '+1 days'`
            datai=`date +%Y%m%d -d '+2 days'`
            exist=`echo "SELECT COUNT(id) FROM tb_printer_brother WHERE loja = '$loja';" | sqlite3 $BD`
            if [ $exist -eq 0 ]; then
                sqlite3 $BD \
                            "INSERT INTO tb_printer_brother (loja, data, data_instalacao) VALUES ('$loja', '$datae', '$datai')"
                if [ $? != 0 ]; then
                    dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Falha ao inserir dados no banco de dados." 5 70
                    sleep 1
                    menuPrinterBrother
                else
                    dialog --sleep 2 --title "Remote System" --infobox "\nDados salvos com sucesso." 5 70
                    sleep 1
                    menuPrinterBrother
                fi
            else
                sqlite3 $BD \
                        "UPDATE tb_printer_brother SET data = '$datae', data_instalacao = '$datai' WHERE ID=(SELECT MAX(ID) FROM tb_printer_brother WHERE loja LIKE '$loja')"
                if [ $? != 0 ]; then
                    dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Falha ao inserir dados no banco de dados." 5 70
                    sleep 1
                    menuPrinterBrother
                else
                    dialog --sleep 2 --title "Remote System" --infobox "\nDados salvos com sucesso." 5 70
                    sleep 1
                    menuPrinterBrother
                fi
            fi
        else
            dialog --sleep 2 --title "Remote System" --infobox "\nFalha ao instalar Brother." 5 70
            sleep 5
            menuPrinterBrother
        fi
    elif [ $FUNC -eq 2 ]; then
        FILE1="/root/srv_remoto/tgz/tcgertec2-0.tgz"
        sshpass -p root ssh -p10001 root@$ip '/etc/init.d/TCgertec stop'
        sshpass -p root ssh -p10001 root@$ip 'rm -vf /etc/init.d/TCgertec'
        sshpass -p root scp -P10001 $FILE1 root@$ip:/confdia/descargas/
        sshpass -p root ssh -p10001 -l root $ip 'tar -xzvf /confdia/descargas/tcgertec2-0.tgz -C /'
        return=$?
        if [ $return -eq 0 ]; then
            dialog --sleep 2 --title "Remote System" --infobox "\nTCGertec instalado com sucesso." 5 70
            sshpass -p root ssh -p10001 -l root $ip 'bash -x /root/TCgertec.sh restart'
            sleep 5
            datae=`date +%Y%m%d -d '+1 days'`
            datai=`date +%Y%m%d -d '+2 days'`
            exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$loja';" | sqlite3 $BD`
            if [ $exist -eq 0 ]; then
                sqlite3 $BD \
                                "INSERT INTO tb_verificador (tienda, fecha_envio, versao) VALUES ('$loja', '$datae', '2')"
                if [ $? != 0 ]; then
                    dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Falha ao inserir dados no banco de dados." 5 70
                    sleep 1
                    menuTCGertec2
                else
                    dialog --sleep 2 --title "Remote System" --infobox "\nDados salvos com sucesso." 5 70
                    sleep 1
                    menuTCGertec2
                fi
            else
                sqlite3 $BD \
                                "UPDATE tb_verificador SET fecha_envio = '$datae', versao = '2' WHERE ID=(SELECT MAX(ID) FROM tb_verificador WHERE tienda = '$loja')"
                if [ $? != 0 ]; then
                    dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Falha ao inserir dados no banco de dados." 5 70
                    sleep 1
                    menuTCGertec2
                else
                    dialog --sleep 2 --title "Remote System" --infobox "\nDados salvos com sucesso." 5 70
                    sleep 1
                    menuTCGertec2
                fi
            fi
        else
            dialog --sleep 2 --title "Remote System" --infobox "\nFalha ao instalar TCGertec." 5 70
            sleep 5
            menuTCGertec2
        fi
    elif [ $FUNC -eq 3 ]; then
        FILE1="/root/srv_remoto/tgz/tcgertec3-0.tgz"
        sshpass -p root ssh -p10001 root@$ip '/etc/init.d/TCgertec stop'
        sshpass -p root ssh -p10001 root@$ip 'rm -vf /etc/init.d/TCgertec'
        sshpass -p root scp -P10001 $FILE1 root@$ip:/confdia/descargas/
        sshpass -p root ssh -p10001 -l root $ip 'tar -xzvf /confdia/descargas/tcgertec3-0.tgz -C /'
        return=$?
        if [ $return -eq 0 ]; then
            dialog --sleep 2 --title "Remote System" --infobox "\nTCGertec instalado com sucesso." 5 70
            sshpass -p root ssh -p10001 -l root $ip 'bash -x /root/TCgertec.sh restart'
            sleep 5
            datae=`date +%Y%m%d -d '+1 days'`
            datai=`date +%Y%m%d -d '+2 days'`
            exist=`echo "SELECT COUNT(id) FROM tb_verificador WHERE tienda = '$loja';" | sqlite3 $BD`
            if [ $exist -eq 0 ]; then
                sqlite3 $BD \
                                "INSERT INTO tb_verificador (tienda, fecha_envio, versao) VALUES ('$loja', '$datae', '3')"
                if [ $? != 0 ]; then
                    dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Falha ao inserir dados no banco de dados." 5 70
                    sleep 1
                    menuTCGertec3
                else
                    dialog --sleep 2 --title "Remote System" --infobox "\nDados salvos com sucesso." 5 70
                    sleep 1
                    menuTCGertec3
                fi
            else
                sqlite3 $BD \
                                "UPDATE tb_verificador SET fecha_envio = '$datae', versao = '3' WHERE ID=(SELECT MAX(ID) FROM tb_verificador WHERE tienda = '$loja')"
                if [ $? != 0 ]; then
                    dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Falha ao inserir dados no banco de dados." 5 70
                    sleep 1
                    menuTCGertec3
                else
                    dialog --sleep 2 --title "Remote System" --infobox "\nDados salvos com sucesso." 5 70
                    sleep 1
                    menuTCGertec3
                fi
            fi
        else
            dialog --sleep 2 --title "Remote System" --infobox "\nFalha ao instalar TCGertec." 5 70
            sleep 5
            menuTCGertec3
        fi
    elif [ $FUNC -eq 4 ]; then
        opcion=$(dialog --stdout --ok-label "Avançar" --no-cancel --title "Remote System" --form "Forced Instalation" 10 75 0 "07.1 Especificar PDV" 1 1 "" 1 16 0 0 "07.2 Todos os PDVs" 2 1 "" 1 1 0 0 "Digite a opção: " 3 1 "" 3 16 5 5)
        case $opcion in
            "7.1"|"07.01"|"07.1")
                pdv=$(dialog --stdout --title "Remote Systema" --form "Forced One Shop" 11 50 0 
                [ "$pdv" ] || { dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Não foi detectado nenhum caracter." 5 70; sleep 1 ; menuDisabledTM88 ; } )
                # Validamos se os campos informados são númericos
                if [[ $pdv = ?(+|-)+([0-9]) ]]; then
                    echo ""
                else
                    dialog --sleep 2 --title "Remote System" --infobox "\nOps, valor informado não é um número inteiro." 5 70
                    sleep 1
                    main
                fi
                ntpv=`echo ${#pdv}`
                echo $ntpv
                sleep 1
                if [ $ntpv -ge 2 ]; then
                    dialog --sleep 2 --title "Remote System" --infobox "\nInforme até 1 caracteres númericos. Ex.: 1, 2, 9." 5 70
                    sleep 1
                    main
                fi
                
                if [ $pdv -ge 1 ]; then
                    sshpass -p root ssh -o ConnectTimeout=1 $ip -p1000$pdv exit
                    return=$?
                    if [ $return -eq 0 ]; then
                        sshpass -p root ssh -o ConnectTimeout=1 $ip -p1000$pdv ' . /confdia/bin/setvari ; if [ ${IMPRCUPO} -eq 1 ] ;then
                            echo "Impressora TM-88 desativada com sucesso:['$loja'].['$ip'].['1000$pdv'].";
                            sed -e 's/CUPOPARA=1/CUPOPARA=0/' -e 's/IMPRCUPO=1/IMPRCUPO=0/' /confdia/bin/setvari > /confdia/bin/setvari.new ; mv /confdia/bin/setvari.new /confdia/bin/setvari;
                        else
                            echo "Ação não necessária para a loja:['$loja'].['$ip'].['1000$pdv'].";
                        fi'
                        datae=`date +%Y%m%d -d '+1 days'`
                        datai=`date +%Y%m%d -d '+2 days'`
                        exist=`echo "SELECT COUNT(id) FROM tb_imp_fidelizacao WHERE loja = '$loja';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            sqlite3 $BD \
                                            "INSERT INTO tb_imp_fidelizacao (loja, data) VALUES ('$loja', '$datae')"
                            if [ $? != 0 ]; then
                                dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Falha ao inserir dados no banco de dados." 5 70
                                sleep 1
                                menuDisabledTM88
                            else
                                dialog --sleep 2 --title "Remote System" --infobox "\nDados salvos com sucesso." 5 70
                                sleep 1
                                menuDisabledTM88
                            fi
                        else
                            sqlite3 $BD \
                                            "UPDATE tb_imp_fidelizacao SET data = '$datae' WHERE ID=(SELECT MAX(ID) FROM tb_imp_fidelizacao WHERE loja LIKE '$loja')"
                            if [ $? != 0 ]; then
                                dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Falha ao inserir dados no banco de dados." 5 70
                                sleep 1
                                menuDisabledTM88
                            else
                                dialog --sleep 2 --title "Remote System" --infobox "\nDados salvos com sucesso." 5 70
                                sleep 1
                                menuDisabledTM88
                            fi
                        fi
                    else
                        dialog --sleep 2 --title "Remote System" --infobox "\nErro ao conectar com a loja." 5 70
                        sleep 5
                        menuDisabledTM88
                    fi
                else
                    dialog --sleep 2 --title "Remote System" --infobox "\nValor informado deve ser maior que 0." 5 70
                    sleep 5
                    menuDisabledTM88
                fi
            ;;
            "7.2"|"07.02"|"07.2")
                sshpass -p root ssh -o ConnectTimeout=1 $ip -p10001 exit
                return=$?
                if [ $return -eq 0 ]; then
                    n_tpvs=$(sshpass -p root ssh -o ConnectTimeout=1 $ip -p10001 '. /confdia/bin/setvari ; echo "${NUMETPVS}"')
                    return=$?
                    if [ $return -eq 0 ]; then
                        for y in $(seq 1 $n_tpvs); do sshpass -p root ssh -o ConnectTimeout=1 $ip -p1000$y ' . /confdia/bin/setvari ; if [ ${IMPRCUPO} -eq 1 ] ; then
                            echo "Impressora TM-88 desativada com sucesso:['$loja'].['$ip'].['1000$y'].";
                            sed -e 's/CUPOPARA=1/CUPOPARA=0/' -e 's/IMPRCUPO=1/IMPRCUPO=0/' /confdia/bin/setvari > /confdia/bin/setvari.new ; mv /confdia/bin/setvari.new /confdia/bin/setvari;
                        else
                            echo "Ação não necessária para a loja:['$loja'].['$ip'].['1000$y'].";
                        fi' ; 
                        done
                        datae=`date +%Y%m%d -d '+1 days'`
                        datai=`date +%Y%m%d -d '+2 days'`
                        exist=`echo "SELECT COUNT(id) FROM tb_imp_fidelizacao WHERE loja = '$loja';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            sqlite3 $BD \
                                            "INSERT INTO tb_imp_fidelizacao (loja, data) VALUES ('$loja', '$datae')"
                            if [ $? != 0 ]; then
                                dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Falha ao inserir dados no banco de dados." 5 70
                                sleep 1
                                menuDisabledTM88
                            else
                                dialog --sleep 2 --title "Remote System" --infobox "\nDados salvos com sucesso." 5 70
                                sleep 1
                                menuDisabledTM88
                            fi
                        else
                            sqlite3 $BD \
                                            "UPDATE tb_imp_fidelizacao SET data = '$datae' WHERE ID=(SELECT MAX(ID) FROM tb_imp_fidelizacao WHERE loja LIKE '$loja')"
                            if [ $? != 0 ]; then
                                dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Falha ao inserir dados no banco de dados." 5 70
                                sleep 1
                                menuDisabledTM88
                            else
                                dialog --sleep 2 --title "Remote System" --infobox "\nDados salvos com sucesso." 5 70
                                sleep 1
                                menuDisabledTM88
                            fi
                        fi
                    else
                        dialog --sleep 2 --title "Remote System" --infobox "\nErro ao obter número de pdvs." 5 70
                        sleep 5
                        menuDisabledTM88
                    fi
                else
                    dialog --sleep 2 --title "Remote System" --infobox "\nErro ao conectar com a loja." 5 70
                    sleep 5
                    menuDisabledTM88
                fi
            ;;
            *)
                dialog --sleep 2 --title "Remote System" --infobox "\nOpção inválida." 5 70
                menuDisabledTM88
        esac
    elif [ $FUNC -eq 5 ]; then
        sshpass -p root ssh -o ConnectTimeout=1 -l root $ip -p10001 exit
        return=$?
        if [ $return -eq 0 ]; then
            actualpaf=$(sshpass -p root ssh -l root $ip -p10001 'ls -ltr /confdia/actualPAF/ | wc -l ')
            return=$?
            if [ $return -eq 0 ]; then
                if [ $actualpaf -gt 1000 ]; then
					sshpass -p root ssh -l root $ip -p10001 'rm /confdia/actualPAF/*txt'
					return=$?
					if [ $return -eq 0 ]; then
                        dialog --sleep 2 --title "Remote System" --infobox "\nArquivos deletados com sucesso." 5 70
                        sleep 1
                        datae=`date +%Y%m%d -d '+1 days'`
                        datai=`date +%Y%m%d -d '+2 days'`
                        exist=`echo "SELECT COUNT(id) FROM tb_actual_paf WHERE tienda = '$loja';" | sqlite3 $BD`
                        if [ $exist -eq 0 ]; then
                            sqlite3 $BD \
                                            "INSERT INTO tb_actual_paf (tienda, active) VALUES ('$loja', '1')"
                            if [ $? != 0 ]; then
                                dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Falha ao inserir dados no banco de dados." 5 70
                                sleep 1
                                menuDeleteActualPAF
                            else
                                dialog --sleep 2 --title "Remote System" --infobox "\nDados salvos com sucesso." 5 70
                                sleep 1
                                menuDeleteActualPAF
                            fi
                        else
                            sqlite3 $BD \
                                            "UPDATE tb_actual_paf SET active = '1' WHERE ID=(SELECT MAX(ID) FROM tb_actual_paf WHERE tienda LIKE '$loja')"
                            if [ $? != 0 ]; then
                                dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Falha ao inserir dados no banco de dados." 5 70
                                sleep 1
                                menuDeleteActualPAF
                            else
                                dialog --sleep 2 --title "Remote System" --infobox "\nDados salvos com sucesso." 5 70
                                sleep 1
                                menuDeleteActualPAF
                            fi
                        fi
                    else
                        dialog --sleep 2 --title "Remote System" --infobox "\nErro ao deletar arquivos." 5 70
                        sleep 5
                        menuDeleteActualPAF
                    fi
                else
                    dialog --sleep 2 --title "Remote System" --infobox "\nQuantidade de arquivos menor que 1000." 5 70
                    sleep 5 
                    menuDeleteActualPAF
                fi
            else
                dialog --sleep 2 --title "Remote System" --infobox "\nErro ao obter quantidade de do diretorio /confdia/actualPAF/" 5 70
                sleep 5
                menuDeleteActualPAF
            fi
        else
            dialog --sleep 2 --title "Remote System" --infobox "\nErro ao conectar com a loja." 5 70
            sleep 5
            menuDeleteActualPAF
        fi
    elif [ $FUNC -eq 6 ]; then
        opcion=$(dialog --stdout --ok-label "Avançar" --no-cancel --title "Remote System" --form "Forced Instalation" 10 75 0 "07.1 Especificar PDV" 1 1 "" 1 16 0 0 "07.2 Todos os PDVs" 2 1 "" 1 1 0 0 "Digite a opção: " 3 1 "" 3 16 5 5)
        case $opcion in
            "7.1"|"07.01"|"07.1")
                pdv=$(dialog --stdout --title "Remote Systema" --form "Forced One Shop" 11 50 0 
                [ "$pdv" ] || { dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Não foi detectado nenhum caracter." 5 70 ; sleep 1 ; menuDisabledTM88 ; } )
                # Validamos se os campos informados são númericos
                if [[ $pdv = ?(+|-)+([0-9]) ]]; then
                    echo ""
                else
                    dialog --sleep 2 --title "Remote System" --infobox "\nOps, valor informado não é um número inteiro." 5 70
                    sleep 1
                    main
                fi
                ntpv=`echo ${#pdv}`
                echo $ntpv
                sleep 1
                if [ $ntpv -ge 2 ]; then
                    dialog --sleep 2 --title "Remote System" --infobox "\nInforme até 1 caracteres númericos. Ex.: 1, 2, 9." 5 70
                    sleep 1
                    main
                fi
                
                if [ $pdv -ge 1 ]; then
                    sshpass -p root ssh -o ConnectTimeout=1 $ip -p1000$pdv exit
                    return=$?
                    if [ $return -eq 0 ]; then
                        FILE1="/root/srv_remoto/tgz/ofertaMenosVenda.tgz"
                        sshpass -p root scp -P1000$pdv $FILE1 root@$ip:/confdia/descargas/
                        sshpass -p root ssh -p1000$pdv -l root $ip 'tar -xzvf /confdia/descargas/ofertaMenosVenda.tgz -C /'
                        return=$?
                        if [ $return -eq 0 ]; then
                            dialog --sleep 2 --title "Remote System" --infobox "\nOferta Menos Venda configurada com sucesso." 5 70
                            datae=`date +%Y%m%d -d '+1 days'`
                            datai=`date +%Y%m%d -d '+2 days'`
                            sleep 5
                            if [ $exist -eq 0 ]; then
                                sqlite3 $BD \
                                                "INSERT INTO tb_install_oferta (tienda, n_tpvs,data_instalacao, data_envio, status) VALUES ('$loja', '0','$datai', '$datae', 'pendente')"
                                if [ $? != 0 ]; then
                                    dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Falha ao inserir dados no banco de dados." 5 70
                                    sleep 1
                                    menuOfertaMenosVenda
                                else
                                    dialog --sleep 2 --title "Remote System" --infobox "\nDados salvos com sucesso." 5 70
                                    sleep 1
                                    menuOfertaMenosVenda
                                fi
                            else
                                sqlite3 $BD \
                                                "UPDATE tb_install_oferta SET data_instalacao = '$datai', data_envio = '$datae' WHERE ID=(SELECT MAX(ID) FROM tb_install_oferta WHERE tienda = '$loja')"
                                if [ $? != 0 ]; then
                                    dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Falha ao inserir dados no banco de dados." 5 70
                                    sleep 1
                                    menuOfertaMenosVenda
                                else
                                    dialog --sleep 2 --title "Remote System" --infobox "\nDados salvos com sucesso." 5 70
                                    sleep 1
                                    menuOfertaMenosVenda
                                fi
                            fi
                        else
                            dialog --sleep 2 --title "Remote System" --infobox "\nFalha ao configurar Oferta Menos Venda." 5 70
                            sleep 5
                            menuOfertaMenosVenda
                        fi
                    else
                        dialog --sleep 2 --title "Remote System" --infobox "\nErro ao conectar com a loja." 5 70
                        sleep 5
                        menuOfertaMenosVenda
                    fi
                else
                    dialog --sleep 2 --title "Remote System" --infobox "\nValor informado deve ser maior que 0." 5 70
                    sleep 5
                    menuOfertaMenosVenda
                fi
            ;;
            "7.2"|"07.02"|"07.2")
                sshpass -p root ssh -o ConnectTimeout=1 $ip -p10001 exit
                return=$?
                if [ $return -eq 0 ]; then
                    n_tpvs=$(sshpass -p root ssh -o ConnectTimeout=1 $ip -p10001 '. /confdia/bin/setvari ; echo "${NUMETPVS}"')
                    return=$?
                    if [ $return -eq 0 ]; then
                        FILE1="/root/srv_remoto/tgz/ofertaMenosVenda.tgz"
                        for z in $(seq 1 $n_tpvs); do sshpass -p root scp -P1000$z $FILE1 root@$ip:/confdia/descargas/; done
                        for k in $(seq 1 $n_tpvs); do sshpass -p root ssh -p1000$z -l root $ip 'tar -xzvf /confdia/descargas/ofertaMenosVenda.tgz -C /'; done
                        return=$?
                        if [ $return -eq 0 ]; then
                            dialog --sleep 2 --title "Remote System" --infobox "\nOferta Menos Venda configurada com sucesso." 5 70
                            sleep 5
                            datae=`date +%Y%m%d -d '+1 days'`
                            datai=`date +%Y%m%d -d '+2 days'`
                            sleep 5
                            if [ $exist -eq 0 ]; then
                                sqlite3 $BD \
                                                "INSERT INTO tb_install_oferta (tienda, n_tpvs,data_instalacao, data_envio, status) VALUES ('$loja', '0','$datai', '$datae', 'pendente')"
                                if [ $? != 0 ]; then
                                    dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Falha ao inserir dados no banco de dados." 5 70
                                    sleep 1
                                    menuOfertaMenosVenda
                                else
                                    dialog --sleep 2 --title "Remote System" --infobox "\nDados salvos com sucesso." 5 70
                                    sleep 1
                                    menuOfertaMenosVenda
                                fi
                            else
                                sqlite3 $BD \
                                                "UPDATE tb_install_oferta SET data_instalacao = '$datai', data_envio = '$datae' WHERE ID=(SELECT MAX(ID) FROM tb_install_oferta WHERE tienda = '$loja')"
                                if [ $? != 0 ]; then
                                    dialog --sleep 2 --title "Remote System" --infobox "\nERROR: Falha ao inserir dados no banco de dados." 5 70
                                    sleep 1
                                    menuOfertaMenosVenda
                                else
                                    dialog --sleep 2 --title "Remote System" --infobox "\nDados salvos com sucesso." 5 70
                                    sleep 1
                                    menuOfertaMenosVenda
                                fi
                            fi
                        else
                            dialog --sleep 2 --title "Remote System" --infobox "\nFalha ao configurar Oferta Menos Venda." 5 70
                            sleep 5
                            menuOfertaMenosVenda
                        fi
                    else
                        dialog --sleep 2 --title "Remote System" --infobox "\nErro ao obter número de pdvs." 5 70
                        sleep 5
                        menuOfertaMenosVenda
                    fi
                else
                    dialog --sleep 2 --title "Remote System" --infobox "\nErro ao conectar com a loja." 5 70
                    sleep 5
                    menuOfertaMenosVenda
                fi
            ;;
            *)
                dialog --sleep 2 --title "Remote System" --infobox "\nOpção inválida." 5 70
                menuOfertaMenosVenda
        esac
    fi
else
    dialog --sleep 2 --title "Remote System" --infobox "\nFalha ao conectar com a loja." 5 70
    exit 1
fi

}

function ViewSystemLog(){

    if [ $FUNC -eq  1 ]; then
        LOG="/root/srv_remoto/log/install_printer_brother.log"
        if [ -e $LOG ]; then
            dialog --title "Relatório banco de dados" --textbox $LOG 0 0
            menuPrinterBrother
        else
            dialog --sleep 2 --title "Remote System" --infobox "\nLog do sistema não econtrado." 5 70
            menuPrinterBrother
        fi
    elif [ $FUNC -eq 2 ]; then
        LOG="/root/srv_remoto/log/install_tcgertec2.0.log"
        if [ -e $LOG ]; then
            dialog --title "Relatório banco de dados" --textbox $LOG 0 0
            menuTCGertec2
        else
            dialog --sleep 2 --title "Remote System" --infobox "\nLog do sistema não econtrado." 5 70
            menuTCGertec2
        fi
    elif [ $FUNC -eq 3 ]; then
        LOG="/root/srv_remoto/log/install_tcgertec3.0.log"
        if [ -e $LOG ]; then
            dialog --title "Relatório banco de dados" --textbox $LOG 0 0
            menuTCGertec3
        else
            dialog --sleep 2 --title "Remote System" --infobox "\nLog do sistema não econtrado." 5 70
            menuTCGertec3
        fi
    elif [ $FUNC -eq 4 ]; then
        LOG="/root/srv_remoto/log/imp_fidelizacao3.log"
        if [ -e $LOG ]; then
            dialog --title "Relatório banco de dados" --textbox $LOG 0 0
            menuDisabledTM88
        else
            dialog --sleep 2 --title "Remote System" --infobox "\nLog do sistema não econtrado." 5 70
            menuDisabledTM88
        fi
    elif [ $FUNC -eq 5 ]; then
        LOG="/root/srv_remoto/log/actual_paf.log"
        if [ -e $LOG ]; then
            dialog --title "Relatório banco de dados" --textbox $LOG 0 0
            menuDeleteActualPAF
        else
            dialog --sleep 2 --title "Remote System" --infobox "\nLog do sistema não econtrado." 5 70
            menuDeleteActualPAF
        fi
    elif [ $FUNC -eq 6 ]; then
        LOG="/root/srv_remoto/log/install_ofer_venda.log"
        if [ -e $LOG ]; then
            dialog --title "Relatório banco de dados" --textbox $LOG 0 0
            menuOfertaMenosVenda
        else
            dialog --sleep 2 --title "Remote System" --infobox "\nLog do sistema não econtrado." 5 70
            menuOfertaMenosVenda
        fi
    fi
}

main
