#!/bin/bash
#
# Autor: Walter Moura
# Data Criacao: 2019.08.18
# Data Modificacao: 2019.09.05
# Versao: 0.02

function main(){
    
    option=$(yad --fixed --button="Avançar":0 --button="Sair":1 --center --list  --width=600 --height=300 --title="TOOLS ANALISTA 2.0" \
     --text="Selecione uma das opções abaixo:" \
     --radiolist \
     --column="" --column="ID" --column="Descrição" \
     OFF 01 Servidores-Remoto \
     OFF 02 Cópia-Base-Dados \
     OFF 03 Teste-Conexão \
     )

    if [ $? -eq 1 ] || [ $? -eq 252 ]; then
        yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Saindo do aplicativo." --width="300" height="200" --button=gtk-ok:0  --timeout=1
    else
        opt=`echo "$option" | cut -d "|" -f 2`
        case $opt in
                "01")
                    srvRemotos
                ;;
                "02")
                    echo ""
                ;;
                "03")
                    echo ""
                ;;
                " ")
                    yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Não foi selecionado nenhuma opção." --width="300" height="100" --button=gtk-ok:0  --timeout=5
                    main
                ;;
                *)
                    yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Não foi selecionado nenhuma opção." --width="300" height="100" --button=gtk-ok:0  --timeout=5
                    main
                ;;
        esac
    fi

}

function srvRemotos(){

    option=$(yad --fixed --center --button="Avançar":0 --list --width=600 --height=400 --title="TOOLS DIA%" --text="Selecione uma das opções abaixo:" --radiolist --column "" --column "ID" --column "Descrição" FALSE 01 Servidores-de-Balanças FALSE 02 Servidor-SCOPECEFOR FALSE 03 Concentrador FALSE 04 Servidor-Remoto-Support FALSE 05 Servidor-Pruebas-CISS OFF 99 Exit )
    opt=`echo "$option" | cut -d "|" -f 2`
    case $opt in
            "1"|"01")
                connServidoresBalancas
            ;;
            "2"|"02")
                connServidoresCefor
            ;;
            "3"|"03")
                connConcentrador
            ;;
            "4"|"04")
				connSrvRemoto
            ;;
            "5"|"05")
                connSrvPruebasCISS
            ;;
            "99"|"099")
                exit 1
            ;;
            *)
                #zenity --error --title--image="/home/diabrasil/Imagens/servidor.png"="TOOLS DIA%" --text="Opção inválida!!!" --width=200 --height=100
                sleep 1
                #main
            ;;
    esac

}
function connServidoresBalancas(){

CKB=".checkbox_dados"
if [ -e $CKB ]; then
    . .checkbox_dados
    if [ ${CHECK} -eq 0 ]; then
        opt=`yad --image="/home/diabrasil/Imagens/servidor2.png" --fixed --center --button="Conectar":0 --button="Voltar":1 --form  --width=400 --height=400 --title="TOOLS DIA%" --text="Dados para acesso" \
        --field="Servidores:":CB 'SRVMGV01!SRVMGV02!SRVMGV03!SRVBALMAK!SRVMGVAPP!SRVMGVBD' \
        --field="Dominio(Ex. diamtz)" "${DOMAN}" \
        --field="Login" "${USER}" \
        --field="Password:":H "${PASS}" \
        --field="Lembrar usuário e senha ?":CHK TRUE \
        --field="Resolução:":CB '1024x768!1280x720!1280x768!1280x1024!1366x768!1366x800!1400x800!1600x900!1920x1080!Full Screen' \
        --separator=" "`
        if [ $? -eq 1 ]; then
            yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Retornando ao menu anterior." --width="300" height="100" --button=gtk-ok:0  --timeout=5
            srvRemotos 
        else 
            echo $opt | while read hostname dominio login password check resolucao nulo;
                        do 
                            [ "$dominio" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a dominio." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$login" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a login." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$password" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a senha." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$resolucao" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a resolução." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            if [ "$check" == "TRUE" ]; then
                                echo "DOMAN=$dominio" > $CKB
                                echo "USER=$login" >> $CKB
                                echo "PASS=$password" >> $CKB
                                echo "CHECK=0" >> $CKB
                            else
                                echo "DOMAN=NULO" > $CKB
                                echo "USER=NULO" >> $CKB
                                echo "PASS=NULO" >> $CKB
                                echo "CHECK=1" >> $CKB
                            fi
                            case $hostname in
                                "1"|"SRVMGV01")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.68 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.68 &
                                        main
                                    fi
                                ;;
                                "2"|"SRVMGV02")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.97 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.97 &
                                        main
                                    fi
                                ;;
                                "3"|"SRVMGV03")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.209 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.209 &
                                        main
                                    fi
                                ;;
                                "SRVBALMAK")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.212 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.212 &
                                        main
                                    fi
                                ;;
                                "5"|"SRVMGVAPP")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                    nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.163 &
                                    main
                                    else
                                    nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.163 &
                                    main
                                    fi
                                ;;
                                "6"|"SRVMGVBD")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.164 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.164 &
                                        main
                                    fi
                                ;;
                                *)
                                    yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Opção inválida" --width="300" height="100" --button=gtk-ok:0 --timeout=3
                                    srvRemotos
                                ;;
                            esac
                        done
        fi
    else
         opt=`yad --image="/home/diabrasil/Imagens/servidor2.png" --fixed --center --button="Conectar":0 --button="Voltar":1 --form  --width=400 --height=400 --title="TOOLS DIA%" --text="Dados para acesso" \
        --field="Servidores:":CB 'SRVMGV01!SRVMGV02!SRVMGV03!SRVBALMAK!SRVMGVAPP!SRVMGVBD' \
        --field="Dominio(Ex. diamtz)" "" \
        --field="Login" "" \
        --field="Password:":H "" \
        --field="Lembrar usuário e senha ?":CHK FALSE \
        --field="Resolução:":CB '1024x768!1280x720!1280x768!1280x1024!1366x768!1366x800!1400x800!1600x900!1920x1080!Full Screen' \
        --separator=" "`
        if [ $? -eq 1 ]; then 
            srvRemotos; 
        else 
            echo $opt | while read hostname dominio login password check resolucao nulo;
                        do 
                            [ "$dominio" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a dominio." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$login" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a login." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$password" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a senha." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$resolucao" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a resolução." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            if [ "$check" == "TRUE" ]; then
                                echo "DOMAN=$dominio" > $CKB
                                echo "USER=$login" >> $CKB
                                echo "PASS=$password" >> $CKB
                                echo "CHECK=0" >> $CKB
                            else
                                echo "DOMAN=NULO" > $CKB
                                echo "USER=NULO" >> $CKB
                                echo "PASS=NULO" >> $CKB
                                echo "CHECK=1" >> $CKB
                            fi
                            case $hostname in
                                "1"|"SRVMGV01")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.68 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.68 &
                                        main
                                    fi
                                ;;
                                "2"|"SRVMGV02")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.97 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.97 &
                                        main
                                    fi
                                ;;
                                "3"|"SRVMGV03")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.209 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.209 &
                                        main
                                    fi
                                ;;
                                "SRVBALMAK")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.212 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.212 &
                                        main
                                    fi
                                ;;
                                "5"|"SRVMGVAPP")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                    nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.163 &
                                    main
                                    else
                                    nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.163 &
                                    main
                                    fi
                                ;;
                                "6"|"SRVMGVBD")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.164 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.164 &
                                        main
                                    fi
                                ;;
                                *)
                                    yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Opção inválida" --width="300" height="100" --button=gtk-ok:0 --timeout=3
                                    srvRemotos
                                ;;
                            esac
                        done
        fi
    fi
else
    opt=`yad --image="/home/diabrasil/Imagens/servidor2.png" --fixed --center --button="Conectar":0 --button="Voltar":1 --form  --width=400 --height=400 --title="TOOLS DIA%" --text="Dados para acesso" \
    --field="Servidores:":CB 'SRVMGV01!SRVMGV02!SRVMGV03!SRVBALMAK!SRVMGVAPP!SRVMGVBD' \
    --field="Dominio(Ex. diamtz)" "" \
    --field="Login" "" \
    --field="Password:":H "" \
    --field="Lembrar usuário e senha ?":CHK FALSE \
    --field="Resolução:":CB '1024x768!1280x720!1280x768!1280x1024!1366x768!1366x800!1400x800!1600x900!1920x1080!Full Screen' \
    --separator=" "`
    if [ $? -eq 1 ]; then 
            srvRemotos; 
        else 
            echo $opt | while read hostname dominio login password check resolucao nulo;
                        do 
                            [ "$dominio" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a dominio." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$login" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a login." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$password" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a senha." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$resolucao" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a resolução." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            if [ "$check" == "TRUE" ]; then
                                echo "DOMAN=$dominio" > $CKB
                                echo "USER=$login" >> $CKB
                                echo "PASS=$password" >> $CKB
                                echo "CHECK=0" >> $CKB
                            else
                                echo "DOMAN=NULO" > $CKB
                                echo "USER=NULO" >> $CKB
                                echo "PASS=NULO" >> $CKB
                                echo "CHECK=1" >> $CKB
                            fi
                            case $hostname in
                                "1"|"SRVMGV01")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.68 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.68 &
                                        main
                                    fi
                                ;;
                                "2"|"SRVMGV02")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.97 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.97 &
                                        main
                                    fi
                                ;;
                                "3"|"SRVMGV03")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.209 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.209 &
                                        main
                                    fi
                                ;;
                                "SRVBALMAK")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.212 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.212 &
                                        main
                                    fi
                                ;;
                                "5"|"SRVMGVAPP")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                    nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.163 &
                                    main
                                    else
                                    nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.163 &
                                    main
                                    fi
                                ;;
                                "6"|"SRVMGVBD")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.164 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.164 &
                                        main
                                    fi
                                ;;
                                *)
                                    yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Opção inválida" --width="300" height="100" --button=gtk-ok:0 --timeout=3
                                    srvRemotos
                                ;;
                            esac
                        done
        fi
fi

}

function connServidoresCefor(){

CKB=".checkbox_dados"
if [ -e $CKB ]; then
    . .checkbox_dados
    if [ ${CHECK} -eq 0 ]; then
        opt=`yad --image="/home/diabrasil/Imagens/servidor2.png" --fixed --center --button="Conectar":0 --button="Voltar":1 --form  --width=400 --height=400 --title="TOOLS DIA%" --text="Dados para acesso" \
        --field="Servidores:":CB 'SCOPECEFOR!SCOPEPRPD' \
        --field="Dominio(Ex. diamtz)" "${DOMAN}" \
        --field="Login" "${USER}" \
        --field="Password:":H "${PASS}" \
        --field="Lembrar usuário e senha ?":CHK TRUE \
        --field="Resolução:":CB '1024x768!1280x720!1280x768!1280x1024!1366x768!1366x800!1400x800!1600x900!1920x1080!Full Screen' \
        --separator=" "`
        if [ $? -eq 1 ]; then
            yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Retornando ao menu anterior." --width="300" height="100" --button=gtk-ok:0  --timeout=5
            srvRemotos 
        else 
            echo $opt | while read hostname dominio login password check resolucao nulo;
                        do 
                            [ "$dominio" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a dominio." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$login" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a login." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$password" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a senha." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$resolucao" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a resolução." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            if [ "$check" == "TRUE" ]; then
                                echo "DOMAN=$dominio" > $CKB
                                echo "USER=$login" >> $CKB
                                echo "PASS=$password" >> $CKB
                                echo "CHECK=0" >> $CKB
                            else
                                echo "DOMAN=NULO" > $CKB
                                echo "USER=NULO" >> $CKB
                                echo "PASS=NULO" >> $CKB
                                echo "CHECK=1" >> $CKB
                            fi
                            case $hostname in
                                "1"|"SCOPECEFOR")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.118 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.118 &
                                        main
                                    fi
                                ;;
                                "2"|"SCOPEPRPD")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.69 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.69 &
                                        main
                                    fi
                                ;;
                                *)
                                    yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Opção inválida" --width="300" height="100" --button=gtk-ok:0 --timeout=3
                                    srvRemotos
                                ;;
                            esac
                        done
        fi
    else
         opt=`yad --image="/home/diabrasil/Imagens/servidor2.png" --fixed --center --button="Conectar":0 --button="Voltar":1 --form  --width=400 --height=400 --title="TOOLS DIA%" --text="Dados para acesso" \
        --field="Servidores:":CB 'SCOPECEFOR!SCOPEPRPD' \
        --field="Dominio(Ex. diamtz)" "" \
        --field="Login" "" \
        --field="Password:":H "" \
        --field="Lembrar usuário e senha ?":CHK FALSE \
        --field="Resolução:":CB '1024x768!1280x720!1280x768!1280x1024!1366x768!1366x800!1400x800!1600x900!1920x1080!Full Screen' \
        --separator=" "`
        if [ $? -eq 1 ]; then
            yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Retornando ao menu anterior." --width="300" height="100" --button=gtk-ok:0  --timeout=5
            srvRemotos 
        else 
            echo $opt | while read hostname dominio login password check resolucao nulo;
                        do 
                            [ "$dominio" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a dominio." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$login" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a login." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$password" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a senha." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$resolucao" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a resolução." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            if [ "$check" == "TRUE" ]; then
                                echo "DOMAN=$dominio" > $CKB
                                echo "USER=$login" >> $CKB
                                echo "PASS=$password" >> $CKB
                                echo "CHECK=0" >> $CKB
                            else
                                echo "DOMAN=NULO" > $CKB
                                echo "USER=NULO" >> $CKB
                                echo "PASS=NULO" >> $CKB
                                echo "CHECK=1" >> $CKB
                            fi
                            case $hostname in
                                "1"|"SCOPECEFOR")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.118 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.118 &
                                        main
                                    fi
                                ;;
                                "2"|"SCOPEPRPD")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.69 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.69 &
                                        main
                                    fi
                                ;;
                                *)
                                    yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Opção inválida" --width="300" height="100" --button=gtk-ok:0 --timeout=3
                                    srvRemotos
                                ;;
                            esac
                        done
        fi
    fi
else
    opt=`yad --image="/home/diabrasil/Imagens/servidor2.png" --fixed --center --button="Conectar":0 --button="Voltar":1 --form  --width=400 --height=400 --title="TOOLS DIA%" --text="Dados para acesso" \
    --field="Servidores:":CB 'SCOPECEFOR!SCOPEPRPD' \
    --field="Dominio(Ex. diamtz)" "" \
    --field="Login" "" \
    --field="Password:":H "" \
    --field="Lembrar usuário e senha ?":CHK FALSE \
    --field="Resolução:":CB '1024x768!1280x720!1280x768!1280x1024!1366x768!1366x800!1400x800!1600x900!1920x1080!Full Screen' \
    --separator=" "`
    if [ $? -eq 1 ]; then
        yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Retornando ao menu anterior." --width="300" height="100" --button=gtk-ok:0  --timeout=5
        srvRemotos 
    else 
        echo $opt | while read hostname dominio login password check resolucao nulo;
                    do 
                        [ "$dominio" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a dominio." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                        [ "$login" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a login." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                        [ "$password" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a senha." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                        [ "$resolucao" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a resolução." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                        if [ "$check" == "TRUE" ]; then
                            echo "DOMAN=$dominio" > $CKB
                            echo "USER=$login" >> $CKB
                            echo "PASS=$password" >> $CKB
                            echo "CHECK=0" >> $CKB
                        else
                            echo "DOMAN=NULO" > $CKB
                            echo "USER=NULO" >> $CKB
                            echo "PASS=NULO" >> $CKB
                            echo "CHECK=1" >> $CKB
                        fi
                        case $hostname in
                            "1"|"SCOPECEFOR")
                                if [ "$resolucao" == "Full Screen" ]; then
                                    nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.118 &
                                    main
                                else
                                    nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.118 &
                                    main
                                fi
                            ;;
                            "2"|"SCOPEPRPD")
                                if [ "$resolucao" == "Full Screen" ]; then
                                    nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.69 &
                                    main
                                else
                                    nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.69 &
                                    main
                                fi
                            ;;
                            *)
                                yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Opção inválida" --width="300" height="100" --button=gtk-ok:0 --timeout=3
                                srvRemotos
                            ;;
                        esac
                    done
    fi
fi

}

function connConcentrador(){

CKB=".checkbox_dados"
if [ -e $CKB ]; then
    . .checkbox_dados
    if [ ${CHECK} -eq 0 ]; then
        opt=`yad --image="/home/diabrasil/Imagens/servidor2.png" --fixed --center --button="Conectar":0 --button="Voltar":1 --form  --width=400 --height=400 --title="TOOLS DIA%" --text="Dados para acesso" \
        --field="Servidores:":CB 'CWS!' \
        --field="Dominio(Ex. diamtz)" "${DOMAN}" \
        --field="Login" "${USER}" \
        --field="Password:":H "${PASS}" \
        --field="Lembrar usuário e senha ?":CHK TRUE \
        --field="Resolução:":CB '1024x768!1280x720!1280x768!1280x1024!1366x768!1366x800!1400x800!1600x900!1920x1080!Full Screen' \
        --separator=" "`
        if [ $? -eq 1 ]; then
            yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Retornando ao menu anterior." --width="300" height="100" --button=gtk-ok:0  --timeout=5
            srvRemotos 
        else 
            echo $opt | while read hostname dominio login password check resolucao nulo;
                        do 
                            [ "$dominio" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a dominio." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$login" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a login." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$password" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a senha." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$resolucao" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a resolução." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            if [ "$check" == "TRUE" ]; then
                                echo "DOMAN=$dominio" > $CKB
                                echo "USER=$login" >> $CKB
                                echo "PASS=$password" >> $CKB
                                echo "CHECK=0" >> $CKB
                            else
                                echo "DOMAN=NULO" > $CKB
                                echo "USER=NULO" >> $CKB
                                echo "PASS=NULO" >> $CKB
                                echo "CHECK=1" >> $CKB
                            fi
                            case $hostname in
                                "1"|"CWS")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.105.186.168 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.105.186.168 &
                                        main
                                    fi
                                ;;
                                *)
                                    yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Opção inválida" --width="300" height="100" --button=gtk-ok:0 --timeout=3
                                    srvRemotos
                                ;;
                            esac
                        done
        fi
    else
         opt=`yad --image="/home/diabrasil/Imagens/servidor2.png" --fixed --center --button="Conectar":0 --button="Voltar":1 --form  --width=400 --height=400 --title="TOOLS DIA%" --text="Dados para acesso" \
        --field="Servidores:":CB 'CWS!' \
        --field="Dominio(Ex. diamtz)" "" \
        --field="Login" "" \
        --field="Password:":H "" \
        --field="Lembrar usuário e senha ?":CHK FALSE \
        --field="Resolução:":CB '1024x768!1280x720!1280x768!1280x1024!1366x768!1366x800!1400x800!1600x900!1920x1080!Full Screen' \
        --separator=" "`
        if [ $? -eq 1 ]; then
            yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Retornando ao menu anterior." --width="300" height="100" --button=gtk-ok:0  --timeout=5
            srvRemotos 
        else 
            echo $opt | while read hostname dominio login password check resolucao nulo;
                        do 
                            [ "$dominio" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a dominio." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$login" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a login." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$password" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a senha." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$resolucao" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a resolução." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            if [ "$check" == "TRUE" ]; then
                                echo "DOMAN=$dominio" > $CKB
                                echo "USER=$login" >> $CKB
                                echo "PASS=$password" >> $CKB
                                echo "CHECK=0" >> $CKB
                            else
                                echo "DOMAN=NULO" > $CKB
                                echo "USER=NULO" >> $CKB
                                echo "PASS=NULO" >> $CKB
                                echo "CHECK=1" >> $CKB
                            fi
                            case $hostname in
                                "1"|"CWS")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.105.186.168 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.105.186.168 &
                                        main
                                    fi
                                ;;
                                *)
                                    yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Opção inválida" --width="300" height="100" --button=gtk-ok:0 --timeout=3
                                    srvRemotos
                                ;;
                            esac
                        done
        fi
    fi
else
    opt=`yad --image="/home/diabrasil/Imagens/servidor2.png" --fixed --center --button="Conectar":0 --button="Voltar":1 --form  --width=400 --height=400 --title="TOOLS DIA%" --text="Dados para acesso" \
    --field="Servidores:":CB 'CWS!' \
    --field="Dominio(Ex. diamtz)" "" \
    --field="Login" "" \
    --field="Password:":H "" \
    --field="Lembrar usuário e senha ?":CHK FALSE \
    --field="Resolução:":CB '1024x768!1280x720!1280x768!1280x1024!1366x768!1366x800!1400x800!1600x900!1920x1080!Full Screen' \
    --separator=" "`
    if [ $? -eq 1 ]; then
        yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Retornando ao menu anterior." --width="300" height="100" --button=gtk-ok:0  --timeout=5
        srvRemotos 
    else 
        echo $opt | while read hostname dominio login password check resolucao nulo;
                    do 
                        [ "$dominio" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a dominio." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                        [ "$login" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a login." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                        [ "$password" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a senha." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                        [ "$resolucao" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a resolução." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                        if [ "$check" == "TRUE" ]; then
                            echo "DOMAN=$dominio" > $CKB
                            echo "USER=$login" >> $CKB
                            echo "PASS=$password" >> $CKB
                            echo "CHECK=0" >> $CKB
                        else
                            echo "DOMAN=NULO" > $CKB
                            echo "USER=NULO" >> $CKB
                            echo "PASS=NULO" >> $CKB
                            echo "CHECK=1" >> $CKB
                        fi
                        case $hostname in
                            "1"|"CWS")
                                if [ "$resolucao" == "Full Screen" ]; then
                                    nohup rdesktop -d $dominio -u $login -p $password -f 10.105.186.168 &
                                    main
                                else
                                    nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.105.186.168 &
                                    main
                                fi
                            ;;
                            *)
                                yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Opção inválida" --width="300" height="100" --button=gtk-ok:0 --timeout=3
                                srvRemotos
                            ;;
                        esac
                    done
    fi
fi

}
function connSrvRemoto(){
CKB=".checkbox_dados"
if [ -e $CKB ]; then
    . .checkbox_dados
    if [ ${CHECK} -eq 0 ]; then
        opt=`yad --image="/home/diabrasil/Imagens/servidor2.png" --fixed --center --button="Conectar":0 --button="Voltar":1 --form  --width=400 --height=400 --title="TOOLS DIA%" --text="Dados para acesso" \
        --field="Servidores:":CB 'SRVREMOTO!' \
        --field="Dominio(Ex. diamtz)" "${DOMAN}" \
        --field="Login" "${USER}" \
        --field="Password:":H "${PASS}" \
        --field="Lembrar usuário e senha ?":CHK TRUE \
        --field="Resolução:":CB '1024x768!1280x720!1280x768!1280x1024!1366x768!1366x800!1400x800!1600x900!1920x1080!Full Screen' \
        --separator=" "`
        if [ $? -eq 1 ]; then
            yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Retornando ao menu anterior." --width="300" height="100" --button=gtk-ok:0  --timeout=5
            srvRemotos 
        else 
            echo $opt | while read hostname dominio login password check resolucao nulo;
                        do 
                            [ "$dominio" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a dominio." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$login" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a login." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$password" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a senha." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$resolucao" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a resolução." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            if [ "$check" == "TRUE" ]; then
                                echo "DOMAN=$dominio" > $CKB
                                echo "USER=$login" >> $CKB
                                echo "PASS=$password" >> $CKB
                                echo "CHECK=0" >> $CKB
                            else
                                echo "DOMAN=NULO" > $CKB
                                echo "USER=NULO" >> $CKB
                                echo "PASS=NULO" >> $CKB
                                echo "CHECK=1" >> $CKB
                            fi
                            case $hostname in
                                "1"|"SRVREMOTO")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.240 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.240 &
                                        main
                                    fi
                                ;;
                                *)
                                    yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Opção inválida" --width="300" height="100" --button=gtk-ok:0 --timeout=3
                                    srvRemotos
                                ;;
                            esac
                        done
        fi
    else
         opt=`yad --image="/home/diabrasil/Imagens/servidor2.png" --fixed --center --button="Conectar":0 --button="Voltar":1 --form  --width=400 --height=400 --title="TOOLS DIA%" --text="Dados para acesso" \
        --field="Servidores:":CB 'SRVREMOTO!' \
        --field="Dominio(Ex. diamtz)" "" \
        --field="Login" "" \
        --field="Password:":H "" \
        --field="Lembrar usuário e senha ?":CHK FALSE \
        --field="Resolução:":CB '1024x768!1280x720!1280x768!1280x1024!1366x768!1366x800!1400x800!1600x900!1920x1080!Full Screen' \
        --separator=" "`
        if [ $? -eq 1 ]; then
            yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Retornando ao menu anterior." --width="300" height="100" --button=gtk-ok:0  --timeout=5
            srvRemotos 
        else 
            echo $opt | while read hostname dominio login password check resolucao nulo;
                        do 
                            [ "$dominio" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a dominio." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$login" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a login." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$password" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a senha." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            [ "$resolucao" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a resolução." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                            if [ "$check" == "TRUE" ]; then
                                echo "DOMAN=$dominio" > $CKB
                                echo "USER=$login" >> $CKB
                                echo "PASS=$password" >> $CKB
                                echo "CHECK=0" >> $CKB
                            else
                                echo "DOMAN=NULO" > $CKB
                                echo "USER=NULO" >> $CKB
                                echo "PASS=NULO" >> $CKB
                                echo "CHECK=1" >> $CKB
                            fi
                            case $hostname in
                                "1"|"SRVREMOTO")
                                    if [ "$resolucao" == "Full Screen" ]; then
                                        nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.240 &
                                        main
                                    else
                                        nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.240 &
                                        main
                                    fi
                                ;;
                                *)
                                    yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Opção inválida" --width="300" height="100" --button=gtk-ok:0 --timeout=3
                                    srvRemotos
                                ;;
                            esac
                        done
        fi
    fi
else
    opt=`yad --image="/home/diabrasil/Imagens/servidor2.png" --fixed --center --button="Conectar":0 --button="Voltar":1 --form  --width=400 --height=400 --title="TOOLS DIA%" --text="Dados para acesso" \
    --field="Servidores:":CB 'SRVREMOTO!' \
    --field="Dominio(Ex. diamtz)" "" \
    --field="Login" "" \
    --field="Password:":H "" \
    --field="Lembrar usuário e senha ?":CHK FALSE \
    --field="Resolução:":CB '1024x768!1280x720!1280x768!1280x1024!1366x768!1366x800!1400x800!1600x900!1920x1080!Full Screen' \
    --separator=" "`
    if [ $? -eq 1 ]; then
        yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Retornando ao menu anterior." --width="300" height="100" --button=gtk-ok:0  --timeout=5
        srvRemotos 
    else 
        echo $opt | while read hostname dominio login password check resolucao nulo;
                    do 
                        [ "$dominio" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a dominio." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                        [ "$login" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a login." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                        [ "$password" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a senha." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                        [ "$resolucao" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a resolução." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                        if [ "$check" == "TRUE" ]; then
                            echo "DOMAN=$dominio" > $CKB
                            echo "USER=$login" >> $CKB
                            echo "PASS=$password" >> $CKB
                            echo "CHECK=0" >> $CKB
                        else
                            echo "DOMAN=NULO" > $CKB
                            echo "USER=NULO" >> $CKB
                            echo "PASS=NULO" >> $CKB
                            echo "CHECK=1" >> $CKB
                        fi
                        case $hostname in
                            "1"|"SRVREMOTO")
                                if [ "$resolucao" == "Full Screen" ]; then
                                    nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.240 &
                                    main
                                else
                                    nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.240 &
                                    main
                                fi
                            ;;
                            *)
                                yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Opção inválida" --width="300" height="100" --button=gtk-ok:0 --timeout=3
                                srvRemotos
                            ;;
                        esac
                    done
    fi
fi

}

function connSrvPruebasCISS(){

opt=`yad --image="/home/diabrasil/Imagens/servidor2.png" --fixed --center --button="Conectar":0 --button="Voltar":1 --form  --width=400 --height=400 --title="TOOLS DIA%" --text="Dados para acesso" \
--field="Servidores:":CB 'SRVCISS01-SAT!SRVCISS02-NFCe' \
--field="Login" "PDV" \
--field="Password:":H "Pdv@123" \
--field="Resolução:":CB '1024x768!1280x720!1280x768!1280x1024!1366x768!1366x800!1400x800!1600x900!1920x1080!Full Screen' \
--separator=" "`
if [ $? -eq 1 ]; then
    yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Retornando ao menu anterior." --width="300" height="100" --button=gtk-ok:0  --timeout=5
    srvRemotos 
else
    echo $opt | while read hostname login password resolucao nulo;
                do 
                    [ "$login" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a login." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                    [ "$password" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a senha." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                    [ "$resolucao" ] || { yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="ERROR: Não foi informado a resolução." --width="300" height="100" --button=gtk-ok:0 --timeout=3; sleep 1 ; srvRemotos ; }
                    case $hostname in
                        "1"|"SRVCISS01-SAT")
                            if [ "$resolucao" == "Full Screen" ]; then
                                nohup rdesktop -u $login -p $password -f 10.106.68.159 &
                                main
                            else
                                nohup rdesktop -u $login -p $password -g $resolucao 10.106.68.159 &
                                main
                            fi
                        ;;
                        "2"|"SRVCISS02-NFCe")
                            if [ "$resolucao" == "Full Screen" ]; then
                                nohup rdesktop -u $login -p $password -f 10.106.68.156 &
                                main
                            else
                                nohup rdesktop -u $login -p $password -g $resolucao 10.106.68.156 &
                                main
                            fi
                        ;;
                        *)
                            yad --fixed --center --title="TOOLS ANALISTA 2.0" --text="Opção inválida" --width="300" height="100" --button=gtk-ok:0 --timeout=3
                            srvRemotos
                        ;;
                    esac
                done
fi

} 
main