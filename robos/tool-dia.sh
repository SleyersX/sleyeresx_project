#!/bin/bash
#
# Autor: Walter Moura
# Data Criacao: 2019.08.18
# Data Modificacao: 2020.01.
# Versao: 0.01

function main(){
    #option=$(zenity --forms --title="TOOLS DIA%" --text="Program para executar algumas tarefas com\ncom interface.\n01. Servidores balança\n02. Servidor SCOPECEFOR\n99 Exit" --add-entry="Escolha opção: " --ok-label="Avançar" width=200 height=300)
    option=$(zenity --list --width=600 --height=500 --ok-label="Avançar" --title="TOOLS DIA%" --text="Selecione uma das opções abaixo:" --radiolist --column "Marcar" --column "ID" --column "Descrição" FALSE 01 Servidores-de-Balanças FALSE 02 Servidor-SCOPECEFOR FALSE 03 Concentrador FALSE 04 Servidor-Remoto-Support OFF 99 Exit )
    case $option in
            "1"|"01")
                connServidoresBalancas
            ;;
            "2"|"02")
                connServidoresCefor
            ;;
            "3"|"03")
                connConcentrador
            ;;
    		"4"|"04")
				connSrvRemoto
            ;;
            "99"|"099")
                exit 1
            ;;
            *)
                #zenity --error --title="TOOLS DIA%" --text="Opção inválida!!!" --width=200 --height=100
                sleep 1
                #main
            ;;
    esac
}

function connServidoresBalancas(){

zenity --forms  --width=400 --height=400 --title="TOOLS DIA%" --text="Dados para acesso" --add-list="Servidores" --list-values="1|SRVMGV01|2|SRVMGV02|3|SRVMGV03|4|SRVBALMAK|5|SRVMGVAPP|6|SRVMGVBD" --column-values="Id|Servidor" --show-header --add-entry="Dominio(Ex. diamtz)" --add-entry="Login" --add-password="Password" --add-combo="Resolução" --combo-values="1024x768|1280x720|1280x768|1280x1024|1366x768|1366x800|1400x800|1600x900|1920x1080|Full Screen" --ok-label="Conectar" --separator="|" | sed 's/,/|/' | sed 's/,//'  | awk -F "|" '{print $1, $2, $3, $4, $5, $6;}' | while read id hostname dominio login password resolucao; 
do 
[ "$dominio" ] || { zenity --info --title="TOOLS DIA%" --text="ERROR: Não foi informado a dominio." --width=200 --height=100 ; sleep 1 ; main ; }
[ "$login" ] || { zenity --info --title="TOOLS DIA%" --text="ERROR: Não foi informado a login." --width=200 --height=100 ; sleep 1 ; main ; }
[ "$password" ] || { zenity --info --title="TOOLS DIA%" --text="ERROR: Não foi informado a senha." --width=200 --height=100 ; sleep 1 ; main ; }
[ "$resolucao" ] || { zenity --info --title="TOOLS DIA%" --text="ERROR: Não foi informado a resolução." --width=200 --height=100 ; sleep 1 ; main ; }
    case $id in
        "1")
            if [ "$resolucao" == "Full Screen" ]; then
                nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.68 &
                main
            else
                nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.68 &
                main
            fi
        ;;
        "2")
            if [ "$resolucao" == "Full Screen" ]; then
                nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.97 &
                main
            else
                nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.97 &
                main
            fi
        ;;
        "3")
            if [ "$resolucao" == "Full Screen" ]; then
                nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.209 &
                main
            else
                nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.209 &
                main
            fi
        ;;
        "4")
            if [ "$resolucao" == "Full Screen" ]; then
                nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.212 &
                main
            else
                nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.212 &
                main
            fi
        ;;
        "5")
            if [ "$resolucao" == "Full Screen" ]; then
               nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.163 &
               main
            else
               nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.163 &
               main
            fi
        ;;
        "6")
            if [ "$resolucao" == "Full Screen" ]; then
                nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.164 &
                main
            else
                nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.164 &
                main
            fi
        ;;
        *)
            zenity --error --title="TOOLS DIA%" --text="Opção inválida!!!" --width=200 --height=100
            sleep 1
            main
        ;;
    esac

done

}

function connServidoresCefor(){

zenity --forms  --width=400 --height=400 --title="TOOLS DIA%" --text="Dados para acesso" --add-list="Servidores" --list-values="1|SCOPECEFOR" --column-values="Id|Servidor" --show-header --add-entry="Dominio(Ex. diamtz)" --add-entry="Login" --add-password="Password" --add-combo="Resolução" --combo-values="1024x768|1280x720|1280x768|1280x1024|1366x768|1366x800|1400x800|1600x900|1920x1080|Full Screen" --ok-label="Conectar" --separator="|" | sed 's/,/|/' | sed 's/,//'  | awk -F "|" '{print $1, $2, $3, $4, $5, $6;}' | while read id hostname dominio login password resolucao; 
do 
[ "$dominio" ] || { zenity --info --title="TOOLS DIA%" --text="ERROR: Não foi informado a dominio." --width=200 --height=100 ; sleep 1 ; main ; }
[ "$login" ] || { zenity --info --title="TOOLS DIA%" --text="ERROR: Não foi informado a login." --width=200 --height=100 ; sleep 1 ; main ; }
[ "$password" ] || { zenity --info --title="TOOLS DIA%" --text="ERROR: Não foi informado a senha." --width=200 --height=100 ; sleep 1 ; main ; }
[ "$resolucao" ] || { zenity --info --title="TOOLS DIA%" --text="ERROR: Não foi informado a resolução." --width=200 --height=100 ; sleep 1 ; main ; }
    case $id in
        "1")
            if [ "$resolucao" == "Full Screen" ]; then
                nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.118 &
                main
            else
                nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.118 &
                main
            fi
        ;;
        "2")
            if [ "$resolucao" == "Full Screen" ]; then
                nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.97 &
                main
            else
                nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.97 &
                main
            fi
        ;;
        "3")
            if [ "$resolucao" == "Full Screen" ]; then
                nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.209 &
                main
            else
                nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.209 &
                main
            fi
        ;;
        "4")
            if [ "$resolucao" == "Full Screen" ]; then
                nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.212 &
                main
            else
                nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.212 &
                main
            fi
        ;;
        *)
            zenity --error --title="TOOLS DIA%" --text="Opção inválida!!!" --width=200 --height=100
            sleep 1
            main
        ;;
    esac

done

}

function connConcentrador(){

zenity --forms --width=400 --height=400 --title="TOOLS DIA%" --text="Dados para acesso" --add-list="Servidores" --list-values="1|CWS" --column-values="Id|Servidor" --show-header --add-entry="Dominio(Ex. diamtz)" --add-entry="Login" --add-password="Password" --add-combo="Resolução" --combo-values="1024x768|1280x720|1280x768|1280x1024|1366x768|1366x800|1400x800|1600x900|1920x1080|Full Screen" --ok-label="Conectar" --separator="|" | sed 's/,/|/' | sed 's/,//'  | awk -F "|" '{print $1, $2, $3, $4, $5, $6;}' | while read id hostname dominio login password resolucao; 
do 
[ "$dominio" ] || { zenity --info --title="TOOLS DIA%" --text="ERROR: Não foi informado a dominio." --width=200 --height=100 ; sleep 1 ; main ; }
[ "$login" ] || { zenity --info --title="TOOLS DIA%" --text="ERROR: Não foi informado a login." --width=200 --height=100 ; sleep 1 ; main ; }
[ "$password" ] || { zenity --info --title="TOOLS DIA%" --text="ERROR: Não foi informado a senha." --width=200 --height=100 ; sleep 1 ; main ; }
[ "$resolucao" ] || { zenity --info --title="TOOLS DIA%" --text="ERROR: Não foi informado a resolução." --width=200 --height=100 ; sleep 1 ; main ; }
    case $id in
        "1")
            if [ "$resolucao" == "Full Screen" ]; then
                nohup rdesktop -d $dominio -u $login -p $password -f 10.105.186.168 &
                main
            else
                nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.105.186.168 &
                main
            fi
        ;;
        "2")
            if [ "$resolucao" == "Full Screen" ]; then
                nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.97 &
                main
            else
                nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.97 &
                main
            fi
        ;;
        "3")
            if [ "$resolucao" == "Full Screen" ]; then
                nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.209 &
                main
            else
                nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.209 &
                main
            fi
        ;;
        "4")
            if [ "$resolucao" == "Full Screen" ]; then
                nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.212 &
                main
            else
                nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.212 &
                main
            fi
        ;;
        *)
            zenity --error --title="TOOLS DIA%" --text="Opção inválida!!!" --width=200 --height=100
            sleep 1
            main
        ;;
    esac

done

}

function connSrvRemoto(){

zenity --forms  --width=400 --height=400 --title="TOOLS DIA%" --text="Dados para acesso" --add-list="Servidores" --list-values="1|SRVREMOTE" --column-values="Id|Servidor" --show-header --add-entry="Dominio(Ex. diamtz)" --add-entry="Login" --add-password="Password" --add-combo="Resolução" --combo-values="1024x768|1280x720|1280x768|1280x1024|1366x768|1366x800|1400x800|1600x900|1920x1080|Full Screen" --ok-label="Conectar" --separator="|" | sed 's/,/|/' | sed 's/,//'  | awk -F "|" '{print $1, $2, $3, $4, $5, $6;}' | while read id hostname dominio login password resolucao; 
do 
[ "$dominio" ] || { zenity --info --title="TOOLS DIA%" --text="ERROR: Não foi informado a dominio." --width=200 --height=100 ; sleep 1 ; main ; }
[ "$login" ] || { zenity --info --title="TOOLS DIA%" --text="ERROR: Não foi informado a login." --width=200 --height=100 ; sleep 1 ; main ; }
[ "$password" ] || { zenity --info --title="TOOLS DIA%" --text="ERROR: Não foi informado a senha." --width=200 --height=100 ; sleep 1 ; main ; }
[ "$resolucao" ] || { zenity --info --title="TOOLS DIA%" --text="ERROR: Não foi informado a resolução." --width=200 --height=100 ; sleep 1 ; main ; }
    case $id in
        "1")
            if [ "$resolucao" == "Full Screen" ]; then
                nohup rdesktop -d $dominio -u $login -p $password -f 10.106.68.240 &
                main
            else
                nohup rdesktop -d $dominio -u $login -p $password -g $resolucao 10.106.68.240 &
                main
            fi
        ;;
        *)
            zenity --error --title="TOOLS DIA%" --text="Opção inválida!!!" --width=200 --height=100
            sleep 1
            main
        ;;
    esac

done

}

main
