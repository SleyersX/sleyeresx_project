#!/bin/bash

PATH_USER="/home/srvremoto"
PATH_DATOS="$PATH_USER/filesread"
ARQNAME="_installbrother.txt"
TEMPARQARRAYFILES="/root/srv_remoto/arquivos_read/array_files.txt"
BD="/root/srv_remoto/srv_remoto.db"
LOG="$PATH_DATOS/log/errro.updatebdbrother.log"

chmod 775 $LOG

function verifica_diretorio(){
    COUNTFILES=`ls -ltr $PATH_DATOS/*$ARQNAME | wc -l`
    if [ $COUNTFILES -ge 1 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICADIR:Iniciando verifica dir." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICADIR:Acessando diretorio ['$PATH_DATOS']." >> $LOG
        cd $PATH_DATOS
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICADIR:Listando arquivos no diretorio['$PATH_DATOS']." >> $LOG
        TEMPNAMES=`ls -tr *$ARQNAME`
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICADIR:Arquivos encontrados ['$TEMPNAMES']." >> $LOG
        RETURN=$?
        if [ $RETURN -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICADIR:['TEMPNAMES'] -> ['TEMPARQARRAYFILES']." >> $LOG
            echo $TEMPNAMES > $TEMPARQARRAYFILES
            RETURN=$?
            if [ $RETURN -eq 0 ]; then
                echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICADIR:Arquivo temp ['$TEMPARQARRAYFILES'] gerado com sucesso." >> $LOG
                echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICADIR:Iniciando função ['array_files']." >> $LOG
                array_files
                RETURN=$?
                if [ $RETURN -eq 0 ]; then
                    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICADIR:Iniciando função ['read_files']." >> $LOG
                    read_files
                fi
            fi
        fi
    fi
}

function array_files(){

    echo "$(date +%Y%m%d-%H%M%S.%s):ARRAYFILES:Iniciando array files." >> $LOG
    while read filename;
        do
            echo "$(date +%Y%m%d-%H%M%S.%s):ARRAYFILES:File name -> ['$filename']." >> $LOG
            progsx=("$filename")
    done < $TEMPARQARRAYFILES
    ARRAYFILESNAME=(${progsx[0]})

}

function read_files(){

    echo "$(date +%Y%m%d-%H%M%S.%s):READFILES:Iniciando read files." >> $LOG
    i=0
    while [ $i != ${#ARRAYFILESNAME[@]} ]
    do
        SHOP=`cat ${ARRAYFILESNAME[i]} | awk -F "|" '{print $1}'`
        DATESEND=`cat ${ARRAYFILESNAME[i]} | awk -F "|" '{print $2}'`
        DATEINST=`cat ${ARRAYFILESNAME[i]} | awk -F "|" '{print $3}'`
        echo "$(date +%Y%m%d-%H%M%S.%s):$SHOP:READFILES:Verificando se loja ['$SHOP'] já existe no banco de dados." >> $LOG
        EXIST=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(id) FROM tb_printer_brother WHERE loja = '$SHOP'"`
        if [ $EXIST -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):$SHOP:READFILES:Loja ['$SHOP'] não existe no banco de dados." >> $LOG
            mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_printer_brother (loja, data, data_instalacao) VALUES ('$SHOP', '$DATESEND', '$DATEINST')"
            RETURN=$?
            if [ $RETURN != 0 ]; then
                echo "$(date +%Y%m%d-%H%M%S.%s):$SHOP:READFILES:Falha ao inserir dados -> ['$SHOP']['$DATESEND']['$DATEINST']." >> $LOG
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):$SHOP:READFILES:Dados inseridos com sucesso -> ['$SHOP']['$DATESEND']['$DATEINST']." >> $LOG
            fi
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):$SHOP:READFILES:Loja ['$SHOP'] não existe banco de dados." >> $LOG
            mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_printer_brother SET data = '$DATESEND', data_instalacao = '$DATEINST' WHERE loja = '$SHOP' ORDER BY ID DESC LIMIT 1"
            RETURN=$?
            if [ $RETURN != 0 ]; then
                echo "$(date +%Y%m%d-%H%M%S.%s):$SHOP:READFILES:Falha ao realizar update dos dados -> ['$SHOP']['$DATESEND']['$DATEINST']." >> $LOG
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):$SHOP:READFILES:Update dos dados realizado com sucesso -> ['$SHOP']['$DATESEND']['$DATEINST']." >> $LOG
            fi
        fi
        if [ $RETURN -eq 0 ]; then
            rm -vf ${ARRAYFILESNAME}
        fi
        SHOP=
        DATESEND=
        DATEINST=
        let "i = i +1" 
    done
}

function main(){

    while true
    do
        verifica_diretorio
    done
}

main