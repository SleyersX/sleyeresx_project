#!/bin/bash
#====================================================#
# Autor             : Walter Moura
# Data Criacao      : 2020-06-13
# Data Modificacao  : 2020-06-13
#
# Script v1
# Realizar o update no banco de dados de acordo com as
# as lojas informadas no arquivo (ARQ_SHOPS), de acordo com a 
# sintaxe SQL passada no SSH_COMANDO
#=====================================================#


PATH_USER="/root/srv_remoto"
BD="$PATH_USER/srv_remoto.db"
LOG="$PATH_USER/log/error.update-bd-nfce.log"
ARQ_SHOPS="$PATH_USER/arquivos_read/temp_shops_sql.txt"
MYSQL_HOST="10.106.77.224"
MYSQL_PARAM="--connect-timeout=5"
MYSQL_USER="dba"
MYSQL_PASS=""
MYSQL_BD="srvremoto"
SSH_CONFIG="-oKexAlgorithms=+diffie-hellman-group1-sha1 -oConnectTimeout=1 -oStrictHostKeyChecking=no"
SSH_USER="root"
SSH_PASS="root"
SSH_PORT="1000"
SSH_COMANDO="echo 'UPDATE ACUARTI1 SET PorcRedEfecICMS_ST = 0, PorcICF_ICMS_ST = 0 WHERE PorcRedEfecICMS_ST = 99999 OR PorcICF_ICMS_ST = 99999;' | sqlite3"

function fnArrayGroupShop(){

    if [ -e $ARQ_SHOPS ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):ARRAYGROUPSHOP:Arquivo OK." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):ARRAYGROUPSHOP:Iniciamos um while read no arquivo ['$ARQ_SHOPS'] e gravamos cada valor em um array." >> $LOG
        while read shop; 
        do
            progress=("$shop")
            done < $ARQ_SHOPS
        ARRAYSHOPS=(${progress[0]})
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):ARRAYGROUPSHOP:Arquivo NOK." >> $LOG
    fi
}

function fnGetIP(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Verificamos se a loja ['$SHOP'] existe no banco ['$BD']." >> $LOG
    EXISTE=`mysql $MYSQL_PARAM -u $MYSQL_USER $MYSQL_BD -h $MYSQL_HOST -N -e "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$SHOP'"`
    RETURN=$?
    if [ $RETURN -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Retorno existe ['$EXISTE']." >> $LOG
        if [ $EXISTE -eq 1 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Loja ['$SHOP'] existe, buscamos o IP." >> $LOG
            IP=`mysql $MYSQL_PARAM -u $MYSQL_USER $MYSQL_BD -h $MYSQL_HOST -N -e "SELECT ip FROM tb_ip WHERE loja LIKE '$SHOP'"`
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Retorno IP -> ['$IP']." >> $LOG
            RET=0
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Loja ['$SHOP'] não econtrada no banco de dados." >> $LOG
            RET=1
        fi
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Erro ao obter IP loja ['$SHOP'] problema ao acessar banco de dados MySQL." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Saindo por contigencia." >> $LOG
        EXISTE=`echo "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Retorno existe ['$EXISTE']." >> $LOG
        if [ $EXISTE -eq 1 ]; then
            IP=`echo "SELECT ip FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Retorno IP -> ['$IP']." >> $LOG
            RET=0
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Loja ['$SHOP'] não econtrada no banco de dados." >> $LOG
            RET=1
        fi
    fi
}

function fnGetNumPOS(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETNUMPOS:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETNUMPOS:Verificamos se a loja ['$SHOP'] existe no banco ['$BD']." >> $LOG
    EXISTE=`mysql $MYSQL_PARAM -u $MYSQL_USER $MYSQL_BD -h $MYSQL_HOST -N -e "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$SHOP'"`
    RETURN=$?
    if [ $RETURN -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETNUMPOS:Retorno existe ['$EXISTE']." >> $LOG
        if [ $EXISTE -eq 1 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETNUMPOS:Loja ['$SHOP'] existe, buscamos o IP." >> $LOG
            NPOS=`mysql $MYSQL_PARAM -u $MYSQL_USER $MYSQL_BD -h $MYSQL_HOST -N -e "SELECT tpvs FROM tb_ip WHERE loja LIKE '$SHOP'"`
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETNUMPOS:Retorno NPOS -> ['$NPOS']." >> $LOG
            RET=0
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETNUMPOS:Loja ['$SHOP'] não econtrada no banco de dados." >> $LOG
            RET=1
        fi
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETNUMPOS:Erro ao obter IP loja ['$SHOP'] problema ao acessar banco de dados MySQL." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETNUMPOS:Saindo por contigencia." >> $LOG
        EXISTE=`echo "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETNUMPOS:Retorno existe ['$EXISTE']." >> $LOG
        if [ $EXISTE -eq 1 ]; then
            NPOS=`echo "SELECT tpvs FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETNUMPOS:Retorno NPOS -> ['$NPOS']." >> $LOG
            RET=0
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETNUMPOS:Loja ['$SHOP'] não econtrada no banco de dados." >> $LOG
            RET=1
        fi
    fi
}

function fnTestConnection(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VALIDACONEXAO:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VALIDACONEXAO:Validando conexão ['$SHOP:$ip']." >> $LOG
    sshpass -p $SSH_PASS ssh $SSH_CONFIG -p $SSH_PORT$z -l $SSH_USER $IP exit
    RET=$?
}

function fnUpdateBDSQLite(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:UPDATEBDSQLITE:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:UPDATEBDSQLITE:Executando comando SQLite3 [$SSH_COMANDO]:['$SHOP']:['$SSH_PORT$z']." >> $LOG
    sshpass -p $SSH_PASS ssh $SSH_CONFIG -p $SSH_PORT$z -l $SSH_USER $IP "$SSH_COMANDO"
    RET=$?
}

function main(){
    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Inicio programa." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Iniciando a função ['fnArrayGroupShop']." >> $LOG
    fnArrayGroupShop
    i=0
    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Iniciamos um while no array ['ARRAY_SHOPS']" >> $LOG
    while [ $i != ${#ARRAYSHOPS[@]} ]
    do
        if [[ ${ARRAYSHOPS[i]} = ?(+|-)+([0-9]) ]]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Valor ['${ARRAYSHOPS[i]}'] é do tipo número, incluímos 0 a esquerda." >> $LOG
            SHOP=`printf "%05d" ${ARRAYSHOPS[i]}`
        else
             echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Valor ['${ARRAYSHOPS[i]}'] não é do tipo númerico, será ignorado." >> $LOG
        fi
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Iniciando a função ['fnGetIP']." >> $LOG
        fnGetIP
        if [ $RET -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Iniciando a função ['fnGetNumPOS']." >> $LOG
            fnGetNumPOS
            if [ $RET -eq 0 ]; then
                for z in $(seq 1 $NPOS)
                do
                    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Iniciando a função ['fnTestConnection']:['$SHOP']:['$SSH_PORT$z']." >> $LOG
                    fnTestConnection
                    if [ $RET -eq 0 ]; then
                        fnUpdateBDSQLite
                        if [ $RET -eq 0 ]; then
                            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Comando função ['fnTestConnection']:['$SHOP']:['$SSH_PORT$z'] executado com sucesso." >> $LOG
                        else
                            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Erro ao executar função  ['fnTestConnection']:['$SHOP']:['$SSH_PORT$z']." >> $LOG
                        fi
                    else
                        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Erro ao conectar com a loja ['$SHOP']." >> $LOG
                    fi
                done 
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Erro ao obter número de pdvs ['$SHOP']." >> $LOG
            fi
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Erro ao obter ip da loja ['$SHOP']." >> $LOG
        fi
        let "i = i +1"
    done
}
main