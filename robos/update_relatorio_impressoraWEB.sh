#!/bin/bash

ARQREAD="/root/srv_remoto/arquivos_read/relatorio_impressora.txt"
ARQTMP="/root/srv_remoto/arquivos_read/.relatorio_impressora.txt.new"
IFS="|"
Status="Ativo"
DATA=$(date "+%Y-%m-%d %H:%M:%S")

if [ -e $ARQTMP ]; then
	rm -vf $ARQTMP
fi

sed -e "s/\r//g" /root/srv_remoto/arquivos_read/relatorio_impressora.txt > /root/srv_remoto/arquivos_read/relatorio_impressora.txt.new 
cp /root/srv_remoto/arquivos_read/relatorio_impressora.txt.new /root/srv_remoto/arquivos_read/relatorio_impressora.txt

while read loja pdv cpu modelo firmware ; 
do

	if [ -e $modelo ]; then
		echo "$loja""|""$pdv""|""$cpu""|""$modelo""|""$firmware" >> $ARQTMP
	else
		test=`echo $modelo | cut -c 1-1`
		if [ "$test" == "D" ]; then
			ncrt1=$(( $(echo "$modelo" | wc -c) - 2 ))
			modelo=$(echo $modelo | cut -c 1-$ncrt1)
		elif [ "$test" == " " ]; then
			modelo=$(echo $modelo | sed "s/.\{1\}//")
			ncrt2=$(( $(echo "$modelo" | wc -c) - 2 ))
			modelo=$(echo $modelo | cut -c 1-$ncrt2)
		fi	
		D=`echo $modelo | cut -c 1-1`
		if [ "$D" == "D" ] || [ "$D" == "T" ]; then
			echo "$loja""|""$pdv""|""$cpu""|""$modelo""|""$firmware" >> $ARQTMP
		elif [ "$D" == " " ]; then
			echo "$loja""|""$pdv""|""$cpu""|""$modelo""|""$firmware" >> $ARQTMP
		fi
	fi 
done < $ARQREAD

mv $ARQTMP $ARQREAD

while read loja pdv cpu modelo firmware ;
do
	if [ "$modelo" != "" ]; then
		var=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT * FROM tb_consolidado_equipamentos WHERE Loja = '$loja' AND PDV = '$pdv';")
		
		if [ "$var" == "" ]; then 
			mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_consolidado_equipamentos (Loja, PDV, Impressora, Firmware, CPU, Data_Criacao, Data_Modificacao, Contador) VALUES('$loja', '$pdv', '$modelo', '$firmware', '$cpu','$(date "+%Y-%m-%d %H:%M:%S")', '','1');"
        else
			mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_consolidado_equipamentos SET Loja = '$loja', PDV = '$pdv', Impressora = '$modelo', Firmware = '$firmware', CPU = '$cpu',Data_Modificacao = '$(date "+%Y-%m-%d %H:%M:%S")' WHERE loja = '$loja' AND PDV = '$pdv';"
		fi
    fi

	[ "$?" = "0" ] && echo -e "\033[01;32mOperacao OK\033[00;37m" || echo -e "\033[01;31mOperacao Error\033[00;37m"

done < $ARQREAD

data_update=`date "+%Y-%m-%d %H:%M:%S"`
st="Atualizado"
data_ult_update=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT data_atualizacao FROM tb_updates_banco WHERE id = ( SELECT MAX(id) FROM tb_updates_banco WHERE banco = 'TB_IMPRESSORA');"`
idbd=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT id FROM tb_updates_banco WHERE id = ( SELECT MAX(id) FROM tb_updates_banco WHERE banco = 'TB_IMPRESSORA');"`
mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_updates_banco set data_ult_atualizacao = '$data_ult_update', data_atualizacao = '$data_update' , status = '$st' WHERE id = '$idbd';"
