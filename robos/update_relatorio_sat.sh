#!/bin/bash


IFS="|"
Status="Ativo"

while read loja numLoja pdv data codResposta descResposta vazio_1 vazio_2 serie rede ip mac mask gw dns_1 dns_2 status bateria disco discoUsado data_1 firmware layout chave_1 chave_2 chave_3 data_2 data_3 dataAtivacao dataFimAtivacao nSei;
do
	if [ "$serie" != "" ]; then
		var=$(sqlite3 ../srv_remoto.db \ "SELECT Sat FROM tb_sat WHERE Sat = '$serie';")

		if [ "$var" == "" ]; then
				pdv_existe=$(sqlite3 ../srv_remoto.db \ "SELECT Sat FROM tb_sat WHERE Loja = '$numLoja' AND Caixa = '$pdv';")
				if [ "$pdv_existe" == "" ]; then 
           			echo "INSERT INTO tb_sat ( Sat, Loja, Caixa, IP, Mask, GW, DNS_1, DNS_2, MAC, Firmware, Layout, Disco, Disco_Usado, Data_Ativacao, Data_Fim_Ativacao, Status) VALUES('$serie','$numLoja','$pdv','$ip','$mask','$gw','$dns_1','$dns_2','$mac','$firmware','$layout','$disco','$discoUsado','$dataAtivacao','$dataFimAtivacao', '$Status');" >> /root/srv_remoto/arquivos_sql/arquivo_sat.SQL
           		else 
           			if [ "$pdv_existe" != "$serie" ]; then
           				echo "UPDATE tb_sat SET Loja = '$numLoja', Caixa = '$pdv', IP = '$ip', Mask = '$mask', GW = '$gw', DNS_1 = '$dns_1', DNS_2 = '$dns_2', MAC = '$mac', Firmware = '$firmware', Layout = '$layout', Disco = '$disco', Disco_Usado = '$discoUsado', Data_Ativacao = '$dataAtivacao', Data_Fim_Ativacao = '$dataFimAtivacao', Status = '$Status' WHERE Sat = '$serie' AND Caixa = '$pdv';" >> /root/srv_remoto/arquivos_sql/arquivo_update_sat.SQL
           			else
           				echo "UPDATE tb_sat SET Loja = '$numLoja', Caixa = '$pdv', IP = '$ip', Mask = '$mask', GW = '$gw', DNS_1 = '$dns_1', DNS_2 = '$dns_2', MAC = '$mac', Firmware = '$firmware', Layout = '$layout', Disco = '$disco', Disco_Usado = '$discoUsado', Data_Ativacao = '$dataAtivacao', Data_Fim_Ativacao = '$dataFimAtivacao', Status = '$Status' WHERE Sat = '$serie';" >> /root/srv_remoto/arquivos_sql/arquivo_update_sat.SQL
           			fi
           		fi
        else
				echo "UPDATE tb_sat SET Loja = '$numLoja', Caixa = '$pdv', IP = '$ip', Mask = '$mask', GW = '$gw', DNS_1 = '$dns_1', DNS_2 = '$dns_2', MAC = '$mac', Firmware = '$firmware', Layout = '$layout', Disco = '$disco', Disco_Usado = '$discoUsado', Data_Ativacao = '$dataAtivacao', Data_Fim_Ativacao = '$dataFimAtivacao', Status = '$Status' WHERE Sat = '$serie';" >> /root/srv_remoto/arquivos_sql/arquivo_update_sat.SQL
		fi
    fi

	[ "$?" = "0" ] && echo -e "\033[01;32mOperacao OK\033[00;37m" || echo -e "\033[01;31mOperacao Error\033[00;37m"

done < $1
