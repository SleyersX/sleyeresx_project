#!/bin/bash

#TrataArquivo

cat $1 | tr -d '[\200-\377]' > /root/srv_remoto/arquivos_read/relatorio_sat.new
cat /root/srv_remoto/arquivos_read/relatorio_sat.new | sed 's/[^A-Za-z0-9:|. ]//g' > /root/srv_remoto/arquivos_read/relatorio_sat.tmp 
mv /root/srv_remoto/arquivos_read/relatorio_sat.tmp /root/srv_remoto/arquivos_read/relatorio_sat.new
cat /root/srv_remoto/arquivos_read/relatorio_sat.new | sed 'y/áÁàÀãÃâÂéÉêÊíÍóÓõÕôÔúÚçÇ/aAaAaAaAeEeEiIoOoOoOuUcC/' > /root/srv_remoto/arquivos_read/relatorio_sat.tmp
mv /root/srv_remoto/arquivos_read/relatorio_sat.tmp /root/srv_remoto/arquivos_read/relatorio_sat.new
cat /root/srv_remoto/arquivos_read/relatorio_sat.new | cut -d "|" -f 2,3,9,11,12,13,14,15,16,17,19,20,22,23,28,29,30,31 > /root/srv_remoto/arquivos_read/relatorio_sat.tmp
mv /root/srv_remoto/arquivos_read/relatorio_sat.tmp /root/srv_remoto/arquivos_read/relatorio_sat.new

IFS="|"
Status="Ativo"

while read numLoja pdv serie ip mac mask gw dns_1 dns_2 statuswan disco discoUsado firmware layout dataComSefaz dataAtivacao dataFimAtivacao otherData;
do
	if [ "$serie" != "" ]; then
		var=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(id) FROM tb_sat WHERE Sat = $serie ")
		ncrcrt=$(echo "$dataAtivacao" | wc -c)
		if [ $ncrcrt -gt 10 ]; then
			ncrt=$(echo "$dataFimAtivacao" | wc -c)
			if [ $ncrt -gt 10 ]; then
				dataAtivacao=$otherData
				dataFimAtivacao=`date --date="+5 year $dataAtivacao" +%Y%m%d`
			else
				dataAtivacao=$dataFimAtivacao
				dataFimAtivacao=$otherData
			fi
		fi
		if [ $var -ge 1 ]; then
			mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_sat SET Status = 'Inativo' WHERE sat = '$serie'"
			pdv_existe=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(Sat) FROM tb_sat WHERE Loja = '$numLoja' AND Caixa = '$pdv'")
			if [ $pdv_existe -eq 0 ]; then 
			   mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_sat ( Sat, Loja, Caixa, IP, Mask, GW, DNS_1, DNS_2, MAC, Firmware, Layout, Disco, Disco_Usado, Data_Ativacao, Data_Fim_Ativacao, Status,  data_atualizacao, status_wan, data_com_sefaz, data_inclusao  ) VALUES('$serie','$numLoja','$pdv','$ip','$mask','$gw','$dns_1','$dns_2','$mac','$firmware','$layout','$disco','$discoUsado','$dataAtivacao','$dataFimAtivacao', '$Status', '$(date +%Y%m%d)', '$statuswan', '$dataComSefaz', '$(date +%Y%m%d)');"
			elif [ $pdv_existe -eq 1 ]; then
				numsat=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT Sat FROM tb_sat WHERE Loja = '$numLoja' AND Caixa = '$pdv'")
				if [ $numsat -eq $serie ]; then
					mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_sat SET Loja = '$numLoja', Caixa = '$pdv', IP = '$ip', Mask = '$mask', GW = '$gw', DNS_1 = '$dns_1', DNS_2 = '$dns_2', MAC = '$mac', Firmware = '$firmware', Layout = '$layout', Disco = '$disco', Disco_Usado = '$discoUsado', Data_Ativacao = '$dataAtivacao', Data_Fim_Ativacao = '$dataFimAtivacao', Status = 'Ativo', status_wan = '$statuswan', data_atualizacao = '$(date +%Y%m%d)', data_com_sefaz = '$dataComSefaz' WHERE Loja = '$numLoja' AND Caixa = '$pdv'"
				else
					mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_sat SET Status = 'Inativo' WHERE sat = '$numsat'"
					mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_sat ( Sat, Loja, Caixa, IP, Mask, GW, DNS_1, DNS_2, MAC, Firmware, Layout, Disco, Disco_Usado, Data_Ativacao, Data_Fim_Ativacao, Status,  data_atualizacao, status_wan, data_com_sefaz, data_inclusao ) VALUES('$serie','$numLoja','$pdv','$ip','$mask','$gw','$dns_1','$dns_2','$mac','$firmware','$layout','$disco','$discoUsado','$dataAtivacao','$dataFimAtivacao', '$Status', '$(date +%Y%m%d)', '$statuswan', '$dataComSefaz', '$(date +%Y%m%d)')"
				fi
			elif [ $pdv_existe -ge 2 ]; then
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_sat SET Status = 'Inativo' WHERE Loja = '$numLoja' AND Caixa = '$pdv'"
				maxid=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT id FROM tb_sat WHERE Loja = '$numLoja' AND Caixa = '$pdv' AND id = (SELECT MAX(id) FROM tb_sat WHERE Loja = '$numLoja' AND Caixa = '$pdv')")
				nsat=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT Sat FROM tb_sat WHERE id = '$maxid'")
				if [ $nsat -eq $serie ]; then
					mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_sat SET Loja = '$numLoja', Caixa = '$pdv', IP = '$ip', Mask = '$mask', GW = '$gw', DNS_1 = '$dns_1', DNS_2 = '$dns_2', MAC = '$mac', Firmware = '$firmware', Layout = '$layout', Disco = '$disco', Disco_Usado = '$discoUsado', Data_Ativacao = '$dataAtivacao', Data_Fim_Ativacao = '$dataFimAtivacao', Status = 'Ativo', status_wan = '$statuswan', data_atualizacao = '$(date +%Y%m%d)', data_com_sefaz = '$dataComSefaz' WHERE id = '$maxid'"
				else
				   mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_sat ( Sat, Loja, Caixa, IP, Mask, GW, DNS_1, DNS_2, MAC, Firmware, Layout, Disco, Disco_Usado, Data_Ativacao, Data_Fim_Ativacao, Status,  data_atualizacao, status_wan, data_com_sefaz, data_inclusao ) VALUES('$serie','$numLoja','$pdv','$ip','$mask','$gw','$dns_1','$dns_2','$mac','$firmware','$layout','$disco','$discoUsado','$dataAtivacao','$dataFimAtivacao', '$Status', '$(date +%Y%m%d)', '$statuswan', '$dataComSefaz', '$(date +%Y%m%d)')"
				fi
			fi
		else
			pdv_existe=$(mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(Sat) FROM tb_sat WHERE Loja = '$numLoja' AND Caixa = '$pdv'")
			if [ $pdv_existe -eq 0 ]; then 
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_sat ( Sat, Loja, Caixa, IP, Mask, GW, DNS_1, DNS_2, MAC, Firmware, Layout, Disco, Disco_Usado, Data_Ativacao, Data_Fim_Ativacao, Status,  data_atualizacao, status_wan, data_com_sefaz ) VALUES('$serie','$numLoja','$pdv','$ip','$mask','$gw','$dns_1','$dns_2','$mac','$firmware','$layout','$disco','$discoUsado','$dataAtivacao','$dataFimAtivacao', '$Status', '$(date +%Y%m%d)', '$statuswan', '$dataComSefaz');"
			elif [ $pdv_existe -eq 1 ]; then
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_sat SET Status = 'Inativo' WHERE Loja = '$numLoja' AND Caixa = '$pdv'"
				mysql -u dba srvremoto -h 10.106.77.224 -N -e "INSERT INTO tb_sat ( Sat, Loja, Caixa, IP, Mask, GW, DNS_1, DNS_2, MAC, Firmware, Layout, Disco, Disco_Usado, Data_Ativacao, Data_Fim_Ativacao, Status,  data_atualizacao, status_wan, data_com_sefaz, data_inclusao ) VALUES('$serie','$numLoja','$pdv','$ip','$mask','$gw','$dns_1','$dns_2','$mac','$firmware','$layout','$disco','$discoUsado','$dataAtivacao','$dataFimAtivacao', '$Status', '$(date +%Y%m%d)', '$statuswan', '$dataComSefaz', '$(date +%Y%m%d)');"
			fi
		fi
    fi

	[ "$?" = "0" ] && echo -e "\033[01;32mOperacao OK\033[00;37m" || echo -e "\033[01;31mOperacao Error\033[00;37m"

done < /root/srv_remoto/arquivos_read/relatorio_sat.new

mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_sat SET status_wan = 'NAO CONECTADO' WHERE status_wan = 'NAOCONECTADO'"
mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_sat SET modelo_sat = 'SAT-1.0' WHERE firmware = '01.03.00' OR firmware = '01.04.00' OR firmware = '01.04.01' OR firmware = '01.05.00' OR firmware = '01.06.00'"
mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_sat SET modelo_sat = 'SAT-2.0' WHERE firmware = '01.00.00' OR firmware = '01.01.00'"

data_update=`date "+%Y-%m-%d %H:%M:%S"`
st="Atualizado"
data_ult_update=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT data_atualizacao FROM tb_updates_banco WHERE id = ( SELECT MAX(id) FROM tb_updates_banco WHERE banco = 'TB_SAT');"`
idbd=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT id FROM tb_updates_banco WHERE id = ( SELECT MAX(id) FROM tb_updates_banco WHERE banco = 'TB_SAT');"`
mysql -u dba srvremoto -h 10.106.77.224 -N -e "UPDATE tb_updates_banco set data_ult_atualizacao = '$data_ult_update', data_atualizacao = '$data_update' , status = '$st' WHERE id = '$idbd';" 
