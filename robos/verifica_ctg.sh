#!/bin/bash

USERACTUAL=$(grep $EUID /etc/group | awk -F ":" '{print $1}')
PATH_USER="/home/$USERACTUAL/programctg"
BD="/root/srv_remoto/srv_remoto.db"
ARQSHOPS="$PATH_USER/list_shop.txt"
ARQSAIDA="$PATH_USER/resultado.txt"
function fnGroupShops(){
    if [ -e $ARQSHOPS ]; then
        while read idshop;
            do
                progress=("$idshop")
        done < $ARQSHOPS
        ARRAYSHOPS=(${progress[0]})
        RETURN=0
    else
        RETURN=1
    fi
    return $RETURN
}

function fnGetIP(){
    EXISTE=`echo "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
    if [ $EXISTE -eq 1 ]; then
        ip=`echo "SELECT ip FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
        tpvs=`echo "SELECT tpvs FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
        RETURN=0
    else
        RETURN=1
    fi
    return $RETURN

}

function fnValidaComunicacao(){
    sshpass -p root ssh -o ConnectTimeout=1 -p10001 -l root $ip exit
    RETURN=$?
    if [ $RETURN -eq 0 ]; then
        RETURN=0
    else
        RETURN=1
    fi
    return $RETURN
}

function main(){

    fnGroupShops
    if [ $RETURN -eq 0 ]; then
        i=0
        while [ $i != ${#ARRAYSHOPS[@]} ]
        do
            if [[ ${ARRAYSHOPS[i]} = ?(+|-)+([0-9]) ]]; then
                echo "$(date +%Y%m%d-%H%M%S.%s):INICIALIZARRELATORIO:Valor ['${ARRAYSHOPS[i]}'] é do tipo número, incluímos 0 a esquerda."
                SHOP=`printf "%05d" ${ARRAYSHOPS[i]}`
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):INICIALIZARRELATORIO:Valor ['${ARRAYSHOPS[i]}'] não é do tipo númerico, será ignorado."
            fi
            fnGetIP
            if [ $RETURN -eq 0 ]; then
                fnValidaComunicacao
                if [ $RETURN -eq 0 ]; then
                    for x in $(seq 1 $tpvs); do sshpass -p root ssh -p1000$x $ip -l root '. /confdia/bin/setvari; COUNT=$(ls -ltr /srv/Debian6.0/srv/servidorDFW/nfce/*.ctg* | wc -l); 
                    if [ $COUNT -ge 1 ] ; then
                        echo "TIENDA:${NUMETIEN} TPV:${NUMECAJA} PEN-CTG:$(ls -ltr /srv/Debian6.0/srv/servidorDFW/nfce/*.ctg* | wc -l  | tr -d " ") REJ-CTG:$(ls -ltr /srv/Debian6.0/srv/servidorDFW/nfce/rejected/*.ctg* | wc -l  | tr -d " ")"
                    fi'; done >> $ARQSAIDA
                    RETURN=$?
                    if [ $RETURN != 0 ]; then
                        echo "$(date +%Y%m%d-%H%M%S.%s):INICIALIZARRELATORIO:Erro ao executar comando para a loja -> ['$SHOP']:['$ip']."
                    fi
                else
                    echo "$(date +%Y%m%d-%H%M%S.%s):INICIALIZARRELATORIO:Erro ao executar comando para a loja -> ['$SHOP']:['$ip']."
                fi
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):INICIALIZARRELATORIO:Erro ao obter ip para a loja -> ['$SHOP']."
            fi
            let "i = i +1"
        done
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):INICIALIZARRELATORIO:Erro ao obter lista de lojas."
    fi
}

main
